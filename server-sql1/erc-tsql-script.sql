USE [master]
GO
/****** Object:  Database [ERC]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE DATABASE [ERC] ON  PRIMARY 
( NAME = N'ERC_Data', FILENAME = N'F:\DATA\MSSQL\Data\ERC_Data.MDF' , SIZE = 8140800KB , MAXSIZE = UNLIMITED, FILEGROWTH = 204800KB )
 LOG ON 
( NAME = N'ERC_Log', FILENAME = N'G:\DATA\MSSQL\Data\ERC_Log.LDF' , SIZE = 2472128KB , MAXSIZE = 2048GB , FILEGROWTH = 102400KB )
GO
ALTER DATABASE [ERC] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ERC].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [ERC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ERC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ERC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ERC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ERC] SET ARITHABORT OFF 
GO
ALTER DATABASE [ERC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ERC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ERC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ERC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ERC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ERC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ERC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ERC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ERC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ERC] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ERC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ERC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ERC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ERC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ERC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ERC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ERC] SET RECOVERY FULL 
GO
ALTER DATABASE [ERC] SET  MULTI_USER 
GO
ALTER DATABASE [ERC] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [ERC] SET DB_CHAINING OFF 
GO
USE [ERC]
GO
/****** Object:  User [reporting]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [reporting] FOR LOGIN [reporting] WITH DEFAULT_SCHEMA=[reporting]
GO
/****** Object:  User [Psantiago]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [Psantiago] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[Psantiago]
GO
/****** Object:  User [omnilinkservice]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [omnilinkservice] FOR LOGIN [omnilinkservice] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [jblock]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [jblock] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[jblock]
GO
/****** Object:  User [erc_domain\staherian]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [erc_domain\staherian] FOR LOGIN [ERC_DOMAIN\staherian] WITH DEFAULT_SCHEMA=[erc_domain\staherian]
GO
/****** Object:  User [ERC_DOMAIN\jtompkins]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [ERC_DOMAIN\jtompkins] FOR LOGIN [ERC_DOMAIN\jtompkins] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\jtompkins]
GO
/****** Object:  User [AMAC]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [AMAC] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[AMAC]
GO
/****** Object:  User [AHazut]    Script Date: 2/11/2016 1:48:47 PM ******/
CREATE USER [AHazut] FOR LOGIN [ERC_DOMAIN\ahazut] WITH DEFAULT_SCHEMA=[AHazut]
GO
ALTER ROLE [db_datareader] ADD MEMBER [reporting]
GO
ALTER ROLE [db_datareader] ADD MEMBER [omnilinkservice]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [omnilinkservice]
GO
ALTER ROLE [db_owner] ADD MEMBER [erc_domain\staherian]
GO
ALTER ROLE [db_datareader] ADD MEMBER [erc_domain\staherian]
GO
ALTER ROLE [db_owner] ADD MEMBER [ERC_DOMAIN\jtompkins]
GO
ALTER ROLE [db_datareader] ADD MEMBER [AMAC]
GO
ALTER ROLE [db_owner] ADD MEMBER [AHazut]
GO
/****** Object:  Schema [AHazut]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [AHazut]
GO
/****** Object:  Schema [AMAC]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [AMAC]
GO
/****** Object:  Schema [ERC_DOMAIN\jtompkins]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [ERC_DOMAIN\jtompkins]
GO
/****** Object:  Schema [erc_domain\staherian]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [erc_domain\staherian]
GO
/****** Object:  Schema [jblock]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [jblock]
GO
/****** Object:  Schema [Psantiago]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [Psantiago]
GO
/****** Object:  Schema [reporting]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE SCHEMA [reporting]
GO
/****** Object:  UserDefinedFunction [dbo].[fnCountPassivesSincePendant]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[fnCountPassivesSincePendant] (@subid char(6),@CutoffDate datetime) 
RETURNS int
AS 
BEGIN 
DECLARE @VAR int
SET @VAR = 0
select @VAR = count(dailycallsid) from dailycalls where subno=@subid
	 and calldatetime>case when @CutoffDate is null then '01/01/1900' else @CutoffDate end
and callmatch='YES' and alarmcode1='00'

 
RETURN @VAR
END 


GO
/****** Object:  Table [dbo].[AlarmCodes]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlarmCodes](
	[AlarmCodeID] [int] IDENTITY(1,1) NOT NULL,
	[AlarmCode] [char](10) NULL,
	[AlarmSource] [varchar](100) NULL,
	[CodeDesc] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AutoCloseoutList]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AutoCloseoutList](
	[ERCUnitInstance] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [char](4) NULL,
	[CallSource] [char](1) NULL,
	[AlarmCode] [char](2) NULL,
	[Active] [char](1) NULL,
	[UserID] [char](20) NULL,
	[EntryDateTime] [datetime] NULL,
	[DeactivateUserID] [char](20) NULL,
	[DeactivateDateTIme] [datetime] NULL,
 CONSTRAINT [PK_AutoCloseoutList] PRIMARY KEY CLUSTERED 
(
	[ERCUnitInstance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CallHistory]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CallHistory](
	[DailyCallsID] [int] NULL,
	[UnitID] [char](4) NULL,
	[DateOfCall] [datetime] NULL,
	[TimeOfCall] [datetime] NULL,
	[CurStatus] [char](4) NULL,
	[DateTimeStamp] [datetime] NULL,
	[Subno] [char](7) NULL,
	[CallDesignation] [char](4) NULL,
	[CallAction] [char](4) NULL,
	[Disposition] [char](4) NULL,
	[NewStatus] [char](4) NULL,
	[Operator] [char](4) NULL,
	[Responder] [char](35) NULL,
	[ResponderPhone] [char](20) NULL,
	[CallType] [char](1) NULL,
	[Comment] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CallMemo]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CallMemo](
	[DailyCallsID] [int] NULL,
	[UnitID] [char](4) NULL,
	[DateTimeOfCall] [datetime] NULL,
	[CallType] [char](1) NULL,
	[Memofield] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cscont02]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cscont02](
	[contno] [char](10) NOT NULL,
	[subno] [char](6) NOT NULL,
	[contdate] [smalldatetime] NOT NULL,
	[conttime] [char](8) NOT NULL,
	[year] [char](2) NOT NULL,
	[reason] [char](10) NOT NULL,
	[ageno] [char](6) NOT NULL,
	[followup] [bit] NOT NULL,
	[dispcode] [char](10) NOT NULL,
	[oper] [char](10) NOT NULL,
	[inout] [char](9) NOT NULL,
	[contact] [char](10) NOT NULL,
	[remarks] [text] NOT NULL,
	[lckstat] [char](1) NOT NULL,
	[lckuser] [char](4) NOT NULL,
	[lckdate] [smalldatetime] NOT NULL,
	[lcktime] [char](8) NOT NULL,
	[adduser] [char](4) NOT NULL,
	[adddate] [smalldatetime] NOT NULL,
	[addtime] [char](8) NOT NULL,
	[contstat] [char](1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailingUnits]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailingUnits](
	[DailingUnitsID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID] [char](10) NULL,
	[UnitModel] [char](10) NULL,
	[UnitDesc] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dailycal]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dailycal](
	[DailycallsID] [int] IDENTITY(1,1) NOT NULL,
	[TrueUnitID] [char](10) NULL,
	[unitid1] [char](10) NULL,
	[alarmcode1] [char](10) NULL,
	[unitid2] [char](10) NULL,
	[alarmcode2] [char](10) NULL,
	[calldatetime] [datetime] NULL,
	[calltime] [char](10) NULL,
	[callmatch] [char](3) NULL,
	[linenumber] [smallint] NULL,
	[status] [char](4) NULL,
	[CallSource] [varchar](50) NULL,
	[CallDesignation] [char](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DailyCalls]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyCalls](
	[DailyCallsID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID1] [char](10) NULL,
	[UnitID2] [char](10) NULL,
	[TRUEUnitID] [char](10) NULL,
	[AlarmCode1] [char](10) NULL,
	[AlarmCode2] [char](10) NULL,
	[CallDateTime] [datetime] NULL,
	[CallMatch] [char](10) NULL,
	[LineNumber] [smallint] NULL,
	[CallSource] [varchar](50) NULL,
	[Status] [char](4) NULL,
	[CallDesignation] [char](4) NULL,
	[Subno] [char](6) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_DailyCalls_DailyCallsID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE CLUSTERED INDEX [IX_DailyCalls_DailyCallsID] ON [dbo].[DailyCalls]
(
	[DailyCallsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dailycalls_archive]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dailycalls_archive](
	[DailyCallsID] [int] IDENTITY(1,1) NOT NULL,
	[UnitID1] [char](10) NULL,
	[UnitID2] [char](10) NULL,
	[TRUEUnitID] [char](10) NULL,
	[AlarmCode1] [char](10) NULL,
	[AlarmCode2] [char](10) NULL,
	[CallDateTime] [datetime] NULL,
	[CallMatch] [char](10) NULL,
	[LineNumber] [smallint] NULL,
	[CallSource] [varchar](50) NULL,
	[Status] [char](4) NULL,
	[CallDesignation] [char](4) NULL,
	[Subno] [char](6) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Description]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Description](
	[Calltype] [char](1) NOT NULL,
	[CodeType] [char](1) NOT NULL,
	[Code] [char](4) NOT NULL,
	[Descr] [char](50) NULL,
	[Hide] [char](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ERCAlarmCodes]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ERCAlarmCodes](
	[CallType] [char](1) NOT NULL,
	[Code] [char](2) NOT NULL,
	[Description] [char](100) NULL,
	[AutoCloseout] [char](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ERCData]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ERCData](
	[UnitID] [varchar](6) NULL,
	[lnam] [varchar](40) NULL,
	[dob] [varchar](6) NULL,
	[sex] [varchar](1) NULL,
	[street] [varchar](30) NULL,
	[city] [varchar](20) NULL,
	[tzone] [varchar](1) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ezip] [varchar](4) NULL,
	[phone] [varchar](20) NULL,
	[ss] [varchar](11) NULL,
	[mc] [varchar](10) NULL,
	[inst] [varchar](25) NULL,
	[hosp] [varchar](30) NULL,
	[zn1] [varchar](10) NULL,
	[zn2] [varchar](10) NULL,
	[zn3] [varchar](10) NULL,
	[zn4] [varchar](10) NULL,
	[zn5] [varchar](10) NULL,
	[zn6] [varchar](10) NULL,
	[zn7] [varchar](10) NULL,
	[zn8] [varchar](10) NULL,
	[zn9] [varchar](10) NULL,
	[zn10] [varchar](10) NULL,
	[zn11] [varchar](10) NULL,
	[zn12] [varchar](10) NULL,
	[zn13] [varchar](10) NULL,
	[zn14] [varchar](10) NULL,
	[zn15] [varchar](10) NULL,
	[dummy] [varchar](1) NULL,
	[landm] [varchar](74) NULL,
	[specialinst1] [varchar](56) NULL,
	[specialinst2] [varchar](78) NULL,
	[specialinst3] [varchar](78) NULL,
	[police] [varchar](12) NULL,
	[idcode] [varchar](5) NULL,
	[apt] [varchar](5) NULL,
	[dummy0] [varchar](1) NULL,
	[pager] [varchar](3) NULL,
	[dummy1] [varchar](3) NULL,
	[fire] [varchar](12) NULL,
	[hosphone] [varchar](12) NULL,
	[amblnc] [varchar](12) NULL,
	[doctor] [varchar](20) NULL,
	[drphone] [varchar](12) NULL,
	[nam1] [varchar](20) NULL,
	[key1] [varchar](1) NULL,
	[rel1] [varchar](6) NULL,
	[ph1] [varchar](12) NULL,
	[ph1a] [varchar](27) NULL,
	[nam2] [varchar](20) NULL,
	[key2] [varchar](1) NULL,
	[rel2] [varchar](6) NULL,
	[ph2] [varchar](12) NULL,
	[ph2a] [varchar](27) NULL,
	[nam3] [varchar](20) NULL,
	[key3] [varchar](1) NULL,
	[rel3] [varchar](6) NULL,
	[ph3] [varchar](12) NULL,
	[ph3a] [varchar](27) NULL,
	[nam4] [varchar](20) NULL,
	[key4] [varchar](1) NULL,
	[rel4] [varchar](6) NULL,
	[ph4] [varchar](12) NULL,
	[ph4a] [varchar](27) NULL,
	[nam5] [varchar](20) NULL,
	[key5] [varchar](1) NULL,
	[rel5] [varchar](6) NULL,
	[ph5] [varchar](12) NULL,
	[ph5a] [varchar](27) NULL,
	[nam6] [varchar](20) NULL,
	[key6] [varchar](1) NULL,
	[rel6] [varchar](6) NULL,
	[ph6] [varchar](12) NULL,
	[ph6a] [varchar](27) NULL,
	[online] [varchar](6) NULL,
	[emgdat] [varchar](6) NULL,
	[modify] [varchar](6) NULL,
	[nature] [varchar](1) NULL,
	[dummy4] [varchar](1) NULL,
	[qtyemg] [varchar](4) NULL,
	[dummy3] [varchar](5) NULL,
	[impair] [varchar](5) NULL,
	[dealer] [varchar](4) NULL,
	[home] [varchar](2) NULL,
	[dummy2] [varchar](6) NULL,
	[fea] [varchar](1) NULL,
	[ma] [varchar](13) NULL,
	[amb] [varchar](1) NULL,
	[med1] [varchar](80) NULL,
	[med2] [varchar](80) NULL,
	[med3] [varchar](80) NULL,
	[hist1] [datetime] NULL,
	[hist2] [varchar](10) NULL,
	[hist3] [varchar](6) NULL,
	[hist4] [varchar](12) NULL,
	[hist5] [varchar](5) NULL,
	[hist6] [varchar](12) NULL,
	[SBTStatus] [varchar](80) NULL,
	[hist8] [varchar](80) NULL,
	[hist9] [varchar](80) NULL,
	[hist10] [varchar](80) NULL,
	[hist11] [varchar](80) NULL,
	[hist12] [varchar](80) NULL,
	[comm] [varchar](50) NULL,
	[stoffset] [varchar](4) NULL,
	[who] [varchar](3) NULL,
	[spare] [varchar](11) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[History]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History](
	[ID] [int] NOT NULL,
	[AccountNumber] [varchar](5) NULL,
	[NumberOfCall] [varchar](3) NULL,
	[DateofCall] [varchar](10) NULL,
	[TimeOfCall] [varchar](5) NULL,
	[CodeNumber] [varchar](2) NULL,
	[CodeDescr] [varchar](22) NULL,
	[OperatorNumber] [varchar](3) NULL,
	[ActionType] [varchar](1) NULL,
	[ActionTaken] [varchar](20) NULL,
	[ElapsedTime] [varchar](8) NULL,
	[StationNumber] [varchar](1) NULL,
	[Length] [varchar](5) NULL,
	[Agent] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[History99]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History99](
	[ID] [int] NOT NULL,
	[AccountNumber] [varchar](5) NULL,
	[NumberOfCall] [varchar](3) NULL,
	[DateofCall] [varchar](10) NULL,
	[TimeOfCall] [varchar](5) NULL,
	[CodeNumber] [varchar](2) NULL,
	[CodeDescr] [varchar](22) NULL,
	[OperatorNumber] [varchar](3) NULL,
	[ActionType] [varchar](1) NULL,
	[ActionTaken] [varchar](20) NULL,
	[ElapsedTime] [varchar](8) NULL,
	[StationNumber] [varchar](1) NULL,
	[Length] [varchar](5) NULL,
	[Agent] [varchar](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HLINKtblStatsOpLogout]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HLINKtblStatsOpLogout](
	[Opname] [nvarchar](12) NULL,
	[Dialouts] [int] NULL,
	[SecAnswered] [int] NULL,
	[CheckAnswered] [int] NULL,
	[NonLiveAnswered] [int] NULL,
	[NoMessage] [int] NULL,
	[Assigned] [int] NULL,
	[Transferred] [int] NULL,
	[Reverted] [int] NULL,
	[Reverts] [int] NULL,
	[LoggedIn] [float] NULL,
	[OnTime] [float] NULL,
	[SuperTime] [float] NULL,
	[HiLighted] [float] NULL,
	[TTAtime] [float] NULL,
	[TTA1] [float] NULL,
	[TTA2] [float] NULL,
	[TTA3] [float] NULL,
	[TTA4] [float] NULL,
	[TTA5] [float] NULL,
	[TTA6] [float] NULL,
	[TTA7] [float] NULL,
	[TTA8] [float] NULL,
	[TTA9] [float] NULL,
	[TTA10] [float] NULL,
	[SecTalkTime] [float] NULL,
	[SecTalk1] [float] NULL,
	[SecTalk2] [float] NULL,
	[SecTalk3] [float] NULL,
	[SecTalk4] [float] NULL,
	[SecTalk5] [float] NULL,
	[SecTalk6] [float] NULL,
	[SecTalk7] [float] NULL,
	[SecTalk8] [float] NULL,
	[SecTalk9] [float] NULL,
	[SecTalk10] [float] NULL,
	[CheckTalkTime] [float] NULL,
	[CheckTalk1] [float] NULL,
	[CheckTalk2] [float] NULL,
	[CheckTalk3] [float] NULL,
	[CheckTalk4] [float] NULL,
	[CheckTalk5] [float] NULL,
	[CheckTalk6] [float] NULL,
	[CheckTalk7] [float] NULL,
	[CheckTalk8] [float] NULL,
	[CheckTalk9] [float] NULL,
	[CheckTalk10] [float] NULL,
	[HoldTime] [float] NULL,
	[Hold1] [float] NULL,
	[Hold2] [float] NULL,
	[Hold3] [float] NULL,
	[Hold4] [float] NULL,
	[Hold5] [float] NULL,
	[Hold6] [float] NULL,
	[Hold7] [float] NULL,
	[Hold8] [float] NULL,
	[Hold9] [float] NULL,
	[Hold10] [float] NULL,
	[DiscTime] [float] NULL,
	[Disc1] [float] NULL,
	[Disc2] [float] NULL,
	[Disc3] [float] NULL,
	[Disc4] [float] NULL,
	[Disc5] [float] NULL,
	[Disc6] [float] NULL,
	[Disc7] [float] NULL,
	[Disc8] [float] NULL,
	[Disc9] [float] NULL,
	[Disc10] [float] NULL,
	[DialoutTime] [float] NULL,
	[Dialout1] [float] NULL,
	[Dialout2] [float] NULL,
	[Dialout3] [float] NULL,
	[Dialout4] [float] NULL,
	[Dialout5] [float] NULL,
	[Dialout6] [float] NULL,
	[Dialout7] [float] NULL,
	[Dialout8] [float] NULL,
	[Dialout9] [float] NULL,
	[Dialout10] [float] NULL,
	[Reassigns] [int] NULL,
	[LogoutReason] [nvarchar](32) NULL,
	[PrcntOn] [real] NULL,
	[PrcntOff] [real] NULL,
	[PrcntCalls] [real] NULL,
	[PrcntSecAnswered] [real] NULL,
	[PrcntCheckAnswered] [real] NULL,
	[PrcntTalkTime] [real] NULL,
	[PrcntDialoutTime] [real] NULL,
	[PrcntHeld] [real] NULL,
	[PrcntParked] [real] NULL,
	[PrcntNoMsg] [real] NULL,
	[PrcntDialout] [real] NULL,
	[PrcntNonLive] [real] NULL,
	[TotalTTA] [int] NULL,
	[TotalSecTalk] [int] NULL,
	[TotalCheckTalk] [int] NULL,
	[TotalHolds] [int] NULL,
	[TotalDisc] [int] NULL,
	[TotalDialout] [int] NULL,
	[AvgTalkTime] [int] NULL,
	[AvgSecTalkTime] [int] NULL,
	[AvgCheckTalkTime] [int] NULL,
	[AvgTTA] [int] NULL,
	[AvgSecTalk] [int] NULL,
	[AvgCheckTalk] [int] NULL,
	[AvgDialouts] [int] NULL,
	[AvgHolds] [int] NULL,
	[AvgDisconnect] [int] NULL,
	[PrcntDiscTime] [float] NULL,
	[WeekDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IVRResults]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IVRResults](
	[CallDate1] [datetime] NULL,
	[CallTime1] [varchar](10) NULL,
	[SubLastName] [varchar](30) NULL,
	[SubFirstName] [varchar](20) NULL,
	[SubPhone] [varchar](16) NULL,
	[unused1] [varchar](5) NULL,
	[ResponseText] [varchar](50) NULL,
	[CallDate2] [varchar](12) NULL,
	[TrueCallTime2] [varchar](20) NULL,
	[FillUnknown01] [varchar](7) NULL,
	[FillUnknown02] [varchar](10) NULL,
	[FillUnknown03] [varchar](10) NULL,
	[RawData] [varchar](50) NULL,
	[ResponseData] [varchar](1) NULL,
	[FillUnknown04] [varchar](6) NULL,
	[UnitID] [varchar](8) NULL,
	[FillUnknown05] [varchar](5) NULL,
	[CallSequence] [varchar](6) NULL,
	[FillUnknown06] [varchar](6) NULL,
	[SubNO] [varchar](6) NULL,
	[FillUnknown07] [varchar](5) NULL,
	[FillUnknown08] [varchar](5) NULL,
	[FillUnknown09] [varchar](5) NULL,
	[FillUnknown10] [varchar](12) NULL,
	[FillUnknown11] [varchar](5) NULL,
	[FillUnknown12] [varchar](5) NULL,
	[Adddatetime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OmniLinkCallData]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OmniLinkCallData](
	[OmniLinkCallData_id] [int] IDENTITY(1,1) NOT NULL,
	[RecID] [int] NULL,
	[DailyCallsID] [int] NULL,
	[ESN] [varchar](20) NULL,
	[PanicButtonDateTime] [datetime] NULL,
	[GPSLastLoctDateTime] [datetime] NULL,
	[MessageType] [varchar](3) NULL,
	[Latitude] [varchar](30) NULL,
	[Longitude] [varchar](30) NULL,
	[EstimatedError] [varchar](30) NULL,
	[AddDateTime] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[OmniLinkCallData_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OmniLinkErrors]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OmniLinkErrors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecordID] [int] NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [nvarchar](4000) NULL,
	[Processed] [bit] NULL DEFAULT ((0)),
	[AddDateTime] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OmniLinkImport]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OmniLinkImport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeviceESN] [varchar](50) NULL,
	[DivisionCode] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[PrimaryModelHomeAddress1] [varchar](50) NULL,
	[PrimaryModelHomeCity] [varchar](50) NULL,
	[ModelHomeState] [varchar](50) NULL,
	[ModelHomeZip] [varchar](50) NULL,
	[CellPhone] [varchar](50) NULL,
	[VehicleMake] [varchar](50) NULL,
	[VehicleModel] [varchar](50) NULL,
	[VehicleColor] [varchar](50) NULL,
	[VehicleTag] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Race] [varchar](50) NULL,
	[DivisionManagerFirstName] [varchar](50) NULL,
	[DivisionManagerLastName] [varchar](50) NULL,
	[DivisionManagerCellPhoneNumber] [varchar](50) NULL,
	[ShippingContactMDMFirstName] [varchar](50) NULL,
	[ShippingContactMDMLastName] [varchar](50) NULL,
	[DivisionOfficeAddress] [varchar](50) NULL,
	[DivisionOfficeCity] [varchar](50) NULL,
	[DivisionOfficeState] [varchar](50) NULL,
	[DivisionOfficeZip] [varchar](50) NULL,
	[MDMOfficePhone] [varchar](50) NULL,
	[MDMemailaddress] [varchar](50) NULL,
	[ImportDateTime] [datetime] NULL,
	[ImportDateTimeCompleted] [datetime] NULL,
	[FedExTrackingNumber] [varchar](50) NULL,
	[TransactionType] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OmniLinkImportFileInf]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OmniLinkImportFileInf](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Recs] [int] NOT NULL,
	[ImportFile_ID] [varchar](32) NULL,
	[FileName] [varchar](70) NULL,
	[FileDateTimeStamp] [datetime] NULL,
	[DateOfImport] [datetime] NULL,
 CONSTRAINT [PK_ImpFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OmniLinkMessageMap]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OmniLinkMessageMap](
	[OmniLinkMessageMap_id] [int] IDENTITY(1,1) NOT NULL,
	[CallSource] [varchar](50) NULL,
	[OLMessageType] [varchar](10) NULL,
	[AlarmCode] [varchar](2) NULL,
	[Deleted] [bit] NULL DEFAULT ((0)),
	[AddDateTime] [datetime] NULL DEFAULT (getdate()),
PRIMARY KEY CLUSTERED 
(
	[OmniLinkMessageMap_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OmniLinkTemp]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OmniLinkTemp](
	[OmniLink_temp_id] [int] IDENTITY(1,1) NOT NULL,
	[DivisionCode] [varchar](10) NULL,
	[ESN] [varchar](10) NULL,
	[PTN] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperatorNotes]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OperatorNotes](
	[OperatorNotesID] [int] IDENTITY(1,1) NOT NULL,
	[NoteData] [text] NULL,
	[NoteDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operators1]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Operators1](
	[operator] [char](4) NULL,
	[Name] [char](30) NULL,
	[status] [char](1) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Operators] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RecentHist]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecentHist](
	[UnitID] [varchar](6) NULL,
	[hist1] [varchar](80) NULL,
	[hist2] [varchar](80) NULL,
	[hist3] [varchar](80) NULL,
	[hist4] [varchar](80) NULL,
	[hist5] [varchar](80) NULL,
	[hist6] [varchar](80) NULL,
	[hist7] [varchar](80) NULL,
	[hist8] [varchar](80) NULL,
	[hist9] [varchar](80) NULL,
	[hist10] [varchar](80) NULL,
	[hist11] [varchar](80) NULL,
	[hist12] [varchar](80) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reports]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Reports](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[ReportFileName] [varchar](100) NULL,
	[ReportDesc] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SignalAlertsSent]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignalAlertsSent](
	[DailyCallsIDSent] [int] NOT NULL,
 CONSTRAINT [PK_SignalAlertsSent] PRIMARY KEY CLUSTERED 
(
	[DailyCallsIDSent] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaskList]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaskList](
	[TaskListID] [int] IDENTITY(1,1) NOT NULL,
	[taskEntrydate] [datetime] NULL,
	[taskDuedate] [datetime] NULL,
	[TaskDesc] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UnitsModels]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UnitsModels](
	[UnitsModelID] [int] IDENTITY(1,1) NOT NULL,
	[ModelID] [char](10) NULL,
	[ModelDesc] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[DescriptionD]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionD]
as 
Select * from Description where CodeType = 'D'

GO
/****** Object:  View [dbo].[v_MedtimeODSignals]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_MedtimeODSignals]

as

SELECT
    Subscriber.Agency_ID, Subscriber.AgencyName, Subscriber.subscriber_ID,  
    Subscriber.Lastname + ', ' + Subscriber.Firstname as SubName, 
    Dailycalls.CallDateTime, Dailycalls.Alarmcode1 as AlarmCode, 
    ERCAlarmCodes.Description as AlarmDescription,
    callhistory.datetimestamp,descriptiond.descr as Disposition 
 
FROM
    AMAC.dbo.subscriber subscriber
       left outer join ERC.dbo.dailycalls dailycalls on subscriber.subscriber_id = dailycalls.subno  
       left outer join ERC.dbo.ercalarmcodes ercalarmcodes on dailycalls.callsource=ercalarmcodes.calltype 
             and dailycalls.alarmcode1=ercalarmcodes.code
       left outer join ERC.dbo.callhistory on dailycalls.dailycallsid=callhistory.dailycallsid
       left outer join ERC.dbo.descriptiond on callhistory.disposition=descriptiond.code 
        and dailycalls.callsource=descriptiond.calltype

where

    DailyCalls.alarmcode1 in ('0D')

and 
	callhistory.newstatus>='0100'
/*AND
   ERC.dbo.descriptiond.Calltype = 'p'*/

GO
/****** Object:  View [dbo].[CallActionView]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CallActionView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'A')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[CallDesignationView]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CallDesignationView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'C')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[DescriptionA]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionA] 
as 
Select * from Description where CodeType = 'A'

GO
/****** Object:  View [dbo].[DescriptionC]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionC] 
as 
Select * from Description where CodeType = 'C'

GO
/****** Object:  View [dbo].[DescriptionS]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionS]
as 
Select * from Description where CodeType = 'S'

GO
/****** Object:  View [dbo].[DispositionView]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DispositionView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'D')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[Operators]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Operators]
AS
SELECT     '0' + LoginName AS Operator, FirstName + ' ' + LastName AS Name, 'A' AS Status, User_ID AS ID
FROM         AMAC.dbo.Users users
WHERE     (Active = 'Y') AND (Deleted = 'N') AND (NOT (FirstName IS NULL)) AND (FirstName NOT IN ('Dummy', 'System', 'Web', 'Approve', 'ERC', 'Guest', 'Temp', 
                      'AMAC')) AND (User_ID IN
                          (SELECT     user_ID
                            FROM          AMAC.dbo.users
                            WHERE      isnumeric(AMAC.dbo.users.LoginName) = 1)) AND (LoginName NOT IN ('000', '001')) OR
                      (Active = 'Y') AND (Deleted = 'N') AND (FirstName NOT IN ('Dummy', 'System', 'Web', 'Approve', 'ERC', 'Guest', 'Temp', 'AMAC')) AND 
                      (User_ID IN
                          (SELECT     user_ID
                            FROM          AMAC.dbo.users
                            WHERE      isnumeric(AMAC.dbo.users.LoginName) = 1)) AND (LoginName NOT IN ('000', '001')) AND (NOT (LastName IS NULL))

GO
/****** Object:  View [dbo].[PassiveStatus]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[PassiveStatus] as
select top 100 percent Date, Status, count(1) as Count from (
select convert(char(10),calldatetime,121) as Date, case when alarmcode1='NO_TONE' and  callmatch='No' then 'No Tones from unit'
when callmatch='No' and alarmcode1<>'NO_TONE' then 'No Match on unitid/alarmcode'
when callmatch='Yes' and alarmcode1<>'NO_TONE' and subno is not null then 'Matched Subscriber'
else 'Unit id no link to sub' end  status 
 from sql1.erc.dbo.dailycalls where calldatetime>=convert(char(10),getdate()-1,121) and calldatetime<convert(char(10),getdate(),121)
) StatusCheck
group by date, status order by date,status

GO
/****** Object:  View [dbo].[SafecomPassiveCalls]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SafecomPassiveCalls]
AS
SELECT     dbo.DailyCalls.DailyCallsID, dbo.DailyCalls.TRUEUnitID, dbo.DailyCalls.AlarmCode1, dbo.DailyCalls.CallDateTime, dbo.DailyCalls.CallMatch, 
                      dbo.DailyCalls.CallSource, dbo.ERCAlarmCodes.Description, dbo.ERCAlarmCodes.CallType
FROM         dbo.DailyCalls INNER JOIN
                      dbo.ERCAlarmCodes ON dbo.DailyCalls.AlarmCode1 = dbo.ERCAlarmCodes.Code
WHERE     (dbo.DailyCalls.CallSource = 'W') AND (dbo.DailyCalls.CallMatch = 'YES') AND (dbo.ERCAlarmCodes.CallType = 'W')

GO
/****** Object:  View [dbo].[SafeComSubscribers]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SafeComSubscribers]
AS
SELECT     *
FROM         AMAC.dbo.Subscriber
WHERE     (Type = 'Safecom') AND (Status <> 'REMOVED')

GO
/****** Object:  View [dbo].[StatusView]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StatusView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'S')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[vw_MobileSOS_Fields]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_MobileSOS_Fields] as
--9/26/2011 KA
--View to collect Mobile SOS data
select s.subscriber_id, isnull(svmk.type,'') VehicleMake, isnull(svmd.type,'') VehicleModel, 
isnull(svt.type,'') VehicleType, isnull(sa.type,'') Age, isnull(sr.type,'') Race from amac.dbo.subscriber s
left outer join amac.dbo.subscribergeneraldata svmk on s.subscriber_id=svmk.subscriber_id and svmk.generaldata_id='VHCLMAKE'
left outer join amac.dbo.subscribergeneraldata svmd on s.subscriber_id=svmd.subscriber_id and svmd.generaldata_id='VHCLMODEL'
left outer join amac.dbo.subscribergeneraldata svt on s.subscriber_id=svt.subscriber_id and svt.generaldata_id='VHCLTAG'
left outer join amac.dbo.subscribergeneraldata sa on s.subscriber_id=sa.subscriber_id and sa.generaldata_id='AGE'
left outer join amac.dbo.subscribergeneraldata sr on s.subscriber_id=sr.subscriber_id and sr.generaldata_id='RACE'

GO
/****** Object:  View [dbo].[vw_OmniLinkOnboarding_SignalList]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  view [dbo].[vw_OmniLinkOnboarding_SignalList] as
-- erc\omnilinkonboarding.asp
-- 10/5/2011 KA
SELECT TOP 100 PERCENT  dailycal.dailycallsid, dailycal.callsource, dailycal.trueunitid, dailycal.alarmcode1 AS AlarmCode,  
    dailycal.calldatetime, dailycal.DailyCallsID as DCID,   
    dailycal.CallSource AS Type, ErcAlarmCodes.Description AS Description,   
    description1.descr AS RealStatus, dailycal.status, dailycal.alarmcode1,   
    description2.descr as Header, dailycal.calldesignation as   
        hiddencalldesignation, s.Subscriber_id + ' - ' + s.Lastname as Sub, s.status substatus,
	OLCD.Latitude+','+Longitude as PreviousLatLong
    From dailycalls as dailycal   
    LEFT OUTER JOIN ErcAlarmCodes ON  ErcAlarmCodes.CallType=dailycal.CallSource   
	LEFT OUTER JOIN OmniLinkCallData OLCD on OLCD.DailyCallsID=dailycal.DailyCallsID
    AND  ErcAlarmCodes.Code= dailycal.alarmcode1   
    left outer join description as description1 on description1.calltype=dailycal.CallSource and description1.codetype='S' and description1.code=dailycal.status   
    left outer join description as description2 on description2.calltype=dailycal.CallSource and description2.codetype='C' and description2.code=dailycal.calldesignation   
    left outer join amac.dbo.subscriber s on s.subscriber_id=dailycal.subno   
    where (isnull(dailycal.status,'0000')<='0001' and dailycal.callmatch='yes' and dailycal.callsource='O' and 
	(s.status  in ('Pending Install') or ( s.status  in ('Active') and  dailycal.calldatetime< cast(convert(char(10),s.installdate,121) + ' 23:59:59' as datetime)))
		)  
		or   
     (dailycal.callsource='O' and dailycal.callmatch='yes' and dailycal.calldatetime>=dateadd(mi,-30,getdate())) 
	 and amac.dbo.fnSignalStatusLastPassiveSignal(subno) is not null
order by calldatetime

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AlarmCodes_AlarmSource_AlarmCode]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_AlarmCodes_AlarmSource_AlarmCode] ON [dbo].[AlarmCodes]
(
	[AlarmSource] ASC,
	[AlarmCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AutoCloseoutList]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_AutoCloseoutList] ON [dbo].[AutoCloseoutList]
(
	[Active] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DailyCallsID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_DailyCallsID] ON [dbo].[CallHistory]
(
	[DailyCallsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UnitID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_UnitID] ON [dbo].[CallHistory]
(
	[UnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_CallMemo_DAILYCALLSID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [PK_CallMemo_DAILYCALLSID] ON [dbo].[CallMemo]
(
	[DailyCallsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_dailycal_status]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_dailycal_status] ON [dbo].[dailycal]
(
	[status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_dailycal_unitid_calltype_calldate]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_dailycal_unitid_calltype_calldate] ON [dbo].[dailycal]
(
	[TrueUnitID] ASC,
	[CallSource] ASC,
	[calldatetime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DailyCalls]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_DailyCalls] ON [dbo].[DailyCalls]
(
	[CallMatch] ASC,
	[CallSource] ASC,
	[CallDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DailyCalls_status]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_DailyCalls_status] ON [dbo].[DailyCalls]
(
	[CallMatch] ASC,
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DailyCalls_Subno]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_DailyCalls_Subno] ON [dbo].[DailyCalls]
(
	[Subno] ASC,
	[CallDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_DailyCalls_unitid_calltype_calldate]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_DailyCalls_unitid_calltype_calldate] ON [dbo].[DailyCalls]
(
	[TRUEUnitID] ASC,
	[CallSource] ASC,
	[CallDateTime] ASC,
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CallAction]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_CallAction] ON [dbo].[Description]
(
	[Calltype] ASC,
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PK_CallType_CodeType_Code]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [PK_CallType_CodeType_Code] ON [dbo].[Description]
(
	[Calltype] ASC,
	[CodeType] ASC,
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PK_Calltype_Code]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [PK_Calltype_Code] ON [dbo].[ERCAlarmCodes]
(
	[CallType] ASC,
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ERCData_hist3]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_ERCData_hist3] ON [dbo].[ERCData]
(
	[hist3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ERCData_lnam]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_ERCData_lnam] ON [dbo].[ERCData]
(
	[lnam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ERCData_unitid]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_ERCData_unitid] ON [dbo].[ERCData]
(
	[UnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_IVRResults_subno_calldate]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_IVRResults_subno_calldate] ON [dbo].[IVRResults]
(
	[SubNO] ASC,
	[CallDate1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OmniLinkCallData_DailyCallsID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_OmniLinkCallData_DailyCallsID] ON [dbo].[OmniLinkCallData]
(
	[DailyCallsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OmniLinkCallData_RecID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_OmniLinkCallData_RecID] ON [dbo].[OmniLinkCallData]
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OmniLinkErrors_RecordID]    Script Date: 2/11/2016 1:48:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_OmniLinkErrors_RecordID] ON [dbo].[OmniLinkErrors]
(
	[RecordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[amac_UnitStatusUpdate]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Name    		: amac_UnitStatusUpdate
Input Parameter  	: 
Output Parameter 	: 
Author  		: Kirk A
Created 		: 13 OCT 03
Purpose 		: Update the unit table for unit_id's in holding
Modification       	:
****************************************************************************************
AUTHOR          DATE           Purpose
****************************************************************************************
*/

CREATE PROCEDURE [dbo].[amac_UnitStatusUpdate] as

Declare @PendingHoldDays int
Declare @TempHoldDays int

--PendingHoldDays for Removals
select @PendingHoldDays=95

--TempHoldDays for UnitID Swap
select @TempHoldDays=15



--Re-enable unit_id with proper date and status 'P', to available 'Y' and null the description for future removals
update unit set available='Y', description=Null  where available='P' and description>'00000000' and description< convert(varchar(10),getdate()-@PendingHoldDays,112) 


GO
/****** Object:  StoredProcedure [dbo].[ap_Safecom_WLGNSTestingVCR_s]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*Author  		:Solyman
Created 		: 04 / 08 / 2005
Purpose 		: This procedure will retreive Testing and VCR-signals for Walgreens Stores
Modification       :
****************************************************************************************
AUTHOR          DATE           Purpose
****************************************************************************************
*/
CREATE  PROCEDURE [dbo].[ap_Safecom_WLGNSTestingVCR_s]
             @vChrSubscriber_ID varChar(30),
             @vChrRegion varChar(6)
             
AS
     SET NOCOUNT ON
	
	 DECLARE @chrSQL varchar(8000)             
	  
	 
	      SET @chrSQL = "SELECT top 10 SafeComSubscribers.Subscriber_ID, SafecomPassiveCalls.CallDateTime,
				 SafeComSubscribers.State, SafeComSubscribers.Lastname,
                                                  SafecomPassiveCalls.Description, CallHistory.DateTimeStamp, DescriptionD.Descr, CallHistory.Comment           
                                                 FROM SafecomPassiveCalls SafecomPassiveCalls 
                                                 LEFT OUTER JOIN SafecomSubscribers SafeComSubscribers
                                                  ON  SafecomPassiveCalls.TRUEUnitID = SafeComSubscribers.Unit_ID 
                                                 LEFT OUTER JOIN  AMAC.dbo.SafecomRegionDistrict SafecomRegionDistrict 
                                                 ON  SafeComSubscribers.Subscriber_ID = SafecomRegionDistrict.Subscriber_ID
                                                 LEFT OUTER JOIN CallHistory CallHistory 
                                                 ON  SafecomPassiveCalls.DailyCallsID = CallHistory.DailyCallsID 
                                                 LEFT OUTER JOIN DescriptionD DescriptionD 
                                                 ON  CallHistory.Disposition = DescriptionD.Code
                                                 WHERE (DescriptionD.Calltype = 'W') AND (SafecomPassiveCalls.CallSource = 'W') 
                                                  AND SafecomPassiveCalls.AlarmCode1 in ('03','09','08','00','0A','0B','0C')   
                                                  AND SafecomRegionDistrict.subscriber_id = '"+@vChrSubscriber_ID+"'
                                                  AND SafecomRegionDistrict. region = '"+@vChrRegion+"'"       	    	                                    	              
       
	      EXECUTE(@chrSQL)
GO
/****** Object:  StoredProcedure [dbo].[ClearOutTests]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClearOutTests] AS
Update erc.dbo.dailycal set status=110 where 
isnull(status,'NULL')='NULL' and calltype='W' and 
alarmcode1='22' and callmatch='Yes'
GO
/****** Object:  StoredProcedure [dbo].[sp_AutoClearPassive]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_AutoClearPassive] AS
--Per David Garza - signals to auto close - New status code 0111 for SQL Server Agent
--KA 7/20/2011
--KA 7/22/2011 added ('00','05','22')
--KA 11/9/2012 added T records separate
select cast(count(*) as varchar(20)) +' T Records to be cleared' from erc.dbo.dailycalls where status is null and callsource='T' and callmatch='Yes'
Update erc.dbo.dailycalls set status='0111' where status is null and callsource='T' and callmatch='Yes'

select cast(count(*) as varchar(20)) +' Records to be cleared' from erc.dbo.dailycalls where status is null and alarmcode1 in ('00','05','22') and callmatch='Yes'
Update erc.dbo.dailycalls set status='0111' where status is null and alarmcode1 in ('00','05','22') and callmatch='Yes'

GO
/****** Object:  StoredProcedure [dbo].[SP_ERCPassiveCursor]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

/*
ERCPassiveCursor - 10/27/2008 
KRA - Cursor to append new 00 records in the PASSIVE database to the AMACSERVICELIST
7/4/2011 added all subscriber types per rich rallo
7/11/2011 add filter for max dailycallsid where subno is not null to prevent premature loading of data
*/



CREATE procedure [dbo].[SP_ERCPassiveCursor]
	@RowsToRead int = NULL
as
SET NOCOUNT ON

DECLARE @chrSQL varchar(8000)

if @RowsToRead is null
	set @chrSQL='SELECT '
else
	set @chrSQL='SELECT TOP '+cast(@RowsToRead as varchar(10))+' ' 
	
set @chrSQL=@chrSQL+' dailycallsid as PassiveDailyCallsId, null as KshemaCallID, subno as subscriber_id, CallDateTime,'
set @chrSQL=@chrSQL+' alarmcode1 as AlarmCode,dbo.fnCountPassivesSincePendant(subno,amac.dbo.fnEarliestPendantInstall(subno))'
set @chrSQL=@chrSQL+' as PassivesSinceLastPendant,'
set @chrSQL=@chrSQL+' subscriber.installdate, amac.dbo.fnFullPendant(subno) As OnlinePendants, '
set @chrSQL=@chrSQL+' amac.dbo.fnEarliestPendantInstall(subno) as EarliestPendantInstalls,'
set @chrSQL=@chrSQL+' amac.dbo.fnPERSwSerial(subno) as PERSSerial, amac.dbo.fnContactLastServiceFax(subno) '
set @chrSQL=@chrSQL+' as LastContactServiceFax, amac.dbo.fnWorkOrderStatus(subno) as LastWorkOrderStatus,'
set @chrSQL=@chrSQL+' subscriber.Agency_id, subscriber.Subcontractor_id into ##tmpservicelist '
set @chrSQL=@chrSQL+' from dailycalls with (nolock)'
set @chrSQL=@chrSQL+' left outer join amac.dbo.subscriber subscriber on dailycalls.subno=subscriber.subscriber_id'
set @chrSQL=@chrSQL+' where callmatch=''YES'' '
set @chrSQL=@chrSQL+' and dailycallsid > (select max( passivedailycallsid) from licsql08.amac00program.dbo.amacservicelist)'
set @chrSQL=@chrSQL+' and dailycallsid <= (select max(dailycallsid) from dailycalls where subno is not null) '
set @chrSQL=@chrSQL+' and alarmcode1=''00'' order by calldatetime'

EXEC (@chrSQL)

declare ServiceListCursor cursor for select PassiveDailyCallsId, subscriber_id, CallDateTime from ##tmpservicelist
declare @intCallId Int
declare @chrSubscriber_id char(6)
declare @dteCallDateTime DateTime
declare @dteCheck DateTime
declare @intCheck TinyInt

Open ServiceListCursor
Fetch Next From ServiceListCUrsor INTO @intCallId , @chrSubscriber_id, @dteCallDateTime 

while @@fetch_status=0
BEGIN
	
	select @intcheck=count(LastTriggerDateTime) from licsql08.amac00program.dbo.AMACServiceListLookup where subscriber_id=@chrSubscriber_id
	select @dteCheck=LastTriggerDateTime from licsql08.amac00program.dbo.AMACServiceListLookup where subscriber_id=@chrSubscriber_id
	if @intcheck=0
		BEGIN
		if @chrSubscriber_id is not null
		Insert into licsql08.amac00program.dbo.AMACServiceListLookup (subscriber_id,LastTriggerDateTime,TriggerCounter) values (@chrSubscriber_id, @dteCallDateTime, 1)
		insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('Unreviewed' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		END
	ELSE
		if @dteCheck+22>@dteCallDateTime
			insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('No Action Required' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		else
		BEGIN
		if @chrSubscriber_id is not null
			Update licsql08.amac00program.dbo.AMACServiceList set ReviewStatus='Closed Incomplete' where subscriber_id=@chrSubscriber_id and ReviewStatus='Unreviewed'
			Update licsql08.amac00program.dbo.AMACServiceListLookup set LastTriggerDateTime=@dteCallDateTime, TriggerCounter=TriggerCounter+1 where subscriber_id=@chrSubscriber_id
			insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('Unreviewed' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		END

	Fetch Next From ServiceListCUrsor INTO @intCallId , @chrSubscriber_id, @dteCallDateTime 

END
close ServiceListCursor
deallocate ServiceListCursor
drop table ##tmpservicelist

SET QUOTED_IDENTIFIER OFF

GO
/****** Object:  StoredProcedure [dbo].[sp_NoMatch_Clearout]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
-Passive NonMatch closeout-
This procedure will check the PASSIVE ERC 'DailyCalls' table to prevent a large number of null status calls.
These cause the db to generate a high-read query.  
Setting these queries to status of '9999' gets them out of any other queries that are run.


11/16/2003-setup and scheduled
*/

CREATE PROCEDURE [dbo].[sp_NoMatch_Clearout] AS
update dailycalls set status='9999'  where callmatch='no' and status is null
GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveFoxProC]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE        PROCEDURE [dbo].[sp_PassiveFoxProC]
   @Subno varchar(8)

AS 
--declare @Subno varchar(8)

--Set @Subno = '205904'

--the Fox Pro app returns name, address and DOB info for a subscriber when 'C' criteria is submitted



Select 
	(Lastname + ', ' + FirstName + MiddleInitial) as names, --0
	Subscriber_ID as subno, --1
	isnull(Address1, ' ') as address, --2
	isnull(Address2, ' ') as apt, --3
	
	isnull(phone, ' ') as phone,  --4
	isnull(city, ' ') as city, --5
	isnull(state, ' ') as state, --6
	isnull(zip, ' ') as zip, --7

	isnull(dob, ' ') as dob  --8

   FROM amac.dbo.subscriber as subscriber
    WHERE (subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
       and Subscriber.Subscriber_ID=@Subno














GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveFoxProR]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE    PROCEDURE [dbo].[sp_PassiveFoxProR]
  @Subno varchar(8)

AS 

SET NOCOUNT ON 


declare @cnt int,
--@Subno varchar(8),
@subscriberresponder_id varchar (20),
@respondername varchar (30),
@haskey     	varchar (3),
@relationcode   varchar (10),
@callorder      varchar (2),
@nviorder     	varchar (2),
@phonetype1	varchar(20),
@phone1		varchar (22),
@phonetype2	varchar(20),
@phone2		varchar (22),
@phonetype3	varchar(20),
@phone3		varchar (22),
@phonetype4	varchar(20),
@phone4		varchar (22),
@empty		int, --indicates a spot where a second work phone could go
@ph1double 	int --indicates if homephone (phone1) has two numbers in it

--Set @Subno = '267840'
set @empty=0 --initialize as no spots empty
Set @cnt=0
--the Fox Pro app returns the responder for a subscriber when 'R' criteria is submitted



CREATE TABLE #temp_responder
	(cnt int,
	subno		varchar (20)
	,subscriberresponder_id varchar (20)
	,ResponderName  varchar (30)
	,haskey     	varchar (3)
	,relationcode   varchar (10)
	,callorder      varchar (2)
	,nviorder     	varchar (2)
	,phonetype1	varchar(20)
	,phone1		varchar (30)
	,phonetype2	varchar(20)
	,phone2		varchar (30)
	,phonetype3	varchar(20)
	,phone3		varchar (30)
	,phonetype4	varchar(20)
	,phone4		varchar (30)
	,PRIMARY KEY (subscriberresponder_id))
	--,UNIQUE (state, city, emp_id) )

DECLARE responder_cursor CURSOR FOR 
	Select 
		isnull(subscriber.subscriber_id, ' ') as subscriber_id,
		isnull(subscriberresponder_id, ' ') as suscriberresponder_id,
		isnull(ResponderName, ' ') as responder, 
		isnull(haskey, ' ') as 'key', 
		isnull(Codes.description, ' ') as relationcode,
		isnull(callorder, ' ') as callorder,
		isnull(nviorder, ' ') as nviorder,
		isnull(homephone, '0') as homephone,
		isnull(workphone, '0') as workphone,
		isnull(cellphone, '0') as cellphone,
		isnull(pager, '0') as pager
	FROM 	amac.dbo.Subscriber as subscriber left JOIN
                amac.dbo.SubscriberResponder as SubscriberResponder ON Subscriber.Subscriber_ID = SubscriberResponder.Subscriber_ID left JOIN
                amac.dbo.Codes as Codes ON SubscriberResponder.RelationType = Codes.Type AND SubscriberResponder.RelationCode = Codes.Code
   	WHERE 	(subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
   	    	and Subscriber.Subscriber_ID=@Subno


OPEN responder_cursor

FETCH NEXT FROM responder_cursor 
INTO @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode, @callorder,
	@nviorder, @phone1, @phone2, @phone3, @phone4


WHILE @@FETCH_STATUS = 0
BEGIN
	
   --sometimes work phone has 2 numbers in it divided by a ';' character
   --if we have empty space we'll split the work phone and put it in two fields
/*
@ph1double
if len(@phone1)>10

	--home
	IF len(@phone1)>1  set @phonetype1='Home'
	else set @empty=1
	If len(@phone2)>1  set @phonetype1='Work'
	else 
		if @empty=0 set @empty=2
				
	set @empty=2
	--cell
	IF len(@phone3)>1  set @phonetype3='Cell'
	else 
		If @empty=0 set @empty=3
	IF len(@phone4)>1 set @phonetype4='Pager'
	else 
		If @empty=0 set @empty=4

	--if there is an empty spot
	If @empty > 0 and len(@phone2) > 11 
	begin
		If @empty = 1 
		begin
			set @phone1 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype1='Work'		
			set @phonetype2='Work'		
			--set @phonetype2='bob'		
		end
		If @empty = 3
		begin
			set @phone3 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype3='Work'
			set @phonetype2='Work'
		end
		If @empty = 4
		begin
			set @phone4 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype4='Work'
			set @phonetype2='Work'
		end	
	end
	else 
	begin
		--all slots are full so work has to stay in phone2, even if really two numbers
		set @phonetype2='Work'
	end
			--set @phone1 = left(@phone2, 10)
			--set @phone2 = right(@phone2, 10)
						
	--set @phonetype1='Work'		
		--	set @phonetype2='Work'		

		
*/	
	set @phonetype1='Home'
	set @phonetype2='Work'
	set @phonetype3='Cell'
	set @phonetype4='Pager'
		
	set @cnt = @cnt + 1

	INSERT INTO #temp_responder 
	(cnt, subno, subscriberresponder_id, ResponderName, haskey, relationcode, callorder,
	nviorder, phone1, phonetype1, phone2, phonetype2, phone3, phonetype3, phone4,
	phonetype4)
	Values (@cnt, @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode,
	 @callorder, @nviorder, @phone1, @phonetype1, @phone2, @phonetype2, @phone3, @phonetype3,
	@phone4, @phonetype4)
	
	
	
	
	
   FETCH NEXT FROM responder_cursor 
   INTO @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode, @callorder,
	@nviorder, @phone1, @phone2, @phone3, @phone4
END

CLOSE responder_cursor
DEALLOCATE responder_cursor




Select @cnt as cnt, 
--subno, 
--subscriberresponder_id, 
ResponderName, haskey, relationcode, callorder,	nviorder, 
isnull(replace(phone1, ';', ' '), ' ') phone1,  
isnull(phonetype1, ' ') phonetype1, 
isnull(replace(phone2, ';', ' '), ' ') phone2,
isnull(phonetype2, ' ') phonetype2,
isnull(replace(phone3, ';', ' '), ' ') phone3,
isnull(phonetype3, ' ') phonetype3,
isnull(replace(phone4, ';', ' '), ' ') phone4,
isnull(phonetype4, ' ') phonetype4 
 From #temp_responder








GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveGetOmnilink]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_PassiveGetOmnilink]
	@DailyCallsID INT,
	@Subscriber_ID CHAR(6)
AS 

SET NOCOUNT ON 

SELECT	OmnilinkCallData.PanicButtonDateTime,
		DATEDIFF(minute, OmnilinkCallData.GPSLastLoctDateTime, GETDATE()) AS TimeSince,
		OmnilinkCallData.Latitude,
		OmnilinkCallData.Longitude
FROM OmnilinkCallData,DailyCalls
WHERE OmnilinkCallData.DailyCallsID = DailyCalls.DailyCallsID
AND DailyCalls.DailyCallsID = @DailyCallsID

SELECT VehicleMake,VehicleModel,VehicleType,Age,Race
FROM SQL1.ERC.dbo.vw_mobilesos_fields 
WHERE Subscriber_ID = @Subscriber_ID --'291075'















GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveUpdateDailyCallswithSubno]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_PassiveUpdateDailyCallswithSubno]
   @TABLE varchar(30) = 'sys%'
AS
/*
Kirk - 16 Jan 2005 - changed where clause selection
Kirk - 12 Feb 2007 - Added new sub types A-Walgreens B-Mckesson
Kirk - 1 11 2010 - Added sub type D=DSPNET
Kirk - 7 20 2011 - changed where close only callmatch=yes, further refined >=
Kirk - 12 2011 - changing clause
Kirk - 4 7 2012 altered low dailycallsid
Kirk - 5/16/2012 alter low dailyclls id
IC   - 10/12/2015 alter low dailyclls id
*/


update dailycals 
set 
subno=t1.subno
-- subno='der'
FROM 
(   
Select dailycal.DailyCallsID,
dailycal.trueunitid AS UnitID, dailycal.alarmcode1 AS AlarmCode, 
   dailycal.calldatetime AS 'Call Date/Time', dailycal.DailyCallsID as DCID, 
   dailycal.CallSource AS Type, ErcAlarmCodes.Description AS Description, 
   description1.descr AS Status, 
   dailycal.status as HiddenStatus, description2.descr as Header, 
   dailycal.calldesignation as hiddencalldesignation, 
   Subscriber_ID as subno, (Lastname + ', ' + FirstName) as FullName

   FROM ERC.dbo.dailycalls as dailycal 
   LEFT OUTER JOIN ERC.dbo.ErcAlarmCodes as ErcAlarmCodes ON  ErcAlarmCodes.CallType=dailycal.CallSource 
    AND ErcAlarmCodes.Code = dailycal.alarmcode1 
   left outer join ERC.dbo.description as description1 on description1.calltype = dailycal.CallSource and 
description1.codetype='S' and description1.code=dailycal.status 
   left outer join ERC.dbo.description as description2 on description2.calltype=dailycal.CallSource and 
description2.codetype='C' and description2.code=dailycal.calldesignation 
   left join amac.dbo.subscriber as subscriber on subscriber.Unit_ID = dailycal.TrueUnitID 
   and (subscriber.type is null or subscriber.type = case when dailycal.callSource = 'W' then 'Safecom'  
				when dailycal.CallSource = 'A' then 'Walgreens' 
				when dailycal.CallSource = 'B' then 'McKesson' 
				when dailycal.CallSource = 'C' then 'Backup' 
				when dailycal.CallSource = 'D' then 'DSPNET'
				else  'Primary' end)
    WHERE 
	(dailycal.callmatch='yes')
/* (ISNULL(dailycal.status, '0') < '0100') AND (dailycal.callmatch = 'Yes') */
	 and (subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
       and dailycal.subno is null 
--addition 1/10 due to slow system
--and dailycal.dailycallsid >=7643015

--addition 12/27/11 due to slow system 
--and dailycal.dailycallsid >=9117388

--IC - addition 10/12/2015 due to slow system 
and dailycal.dailycallsid >=17086184


--and dailycal.DailyCallsID in (789555)  
)
 as t1, ERC.dbo.dailycalls as dailycals
where dailycals.DailyCallsID=t1.DailyCallsID

--check for lowest 5/16/2012
--select top 10 * from dailycalls where dailycallsid >=8877415 
--select * from dailycalls where dailycallsid >=8877415 and (ISNULL(status, '0') < '0100') order by dailycallsid
--select * from dailycalls where dailycallsid >=8177415 and (ISNULL(status, '0') < '0100') order by dailycallsid

--update ERC.dbo.dailycalls set subno = null where DailyCallsID in (789212, 789192)


GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveUpdateDailyCallswithSubno_bu20111227]    Script Date: 2/11/2016 1:48:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE        PROCEDURE [dbo].[sp_PassiveUpdateDailyCallswithSubno_bu20111227]
   @TABLE varchar(30) = 'sys%'
AS
/*
Kirk - 16 Jan 2005 - changed where clause selection
Kirk - 12 Feb 2007 - Added new sub types A-Walgreens B-Mckesson
Kirk - 1 11 2010 - Added sub type D=DSPNET
Kirk - 7 20 2011 - changed where close only callmatch=yes, further refined >=
*/


update dailycals 
set 
subno=t1.subno
-- subno='der'
FROM 
(   
Select dailycal.DailyCallsID,
dailycal.trueunitid AS UnitID, dailycal.alarmcode1 AS AlarmCode, 
   dailycal.calldatetime AS 'Call Date/Time', dailycal.DailyCallsID as DCID, 
   dailycal.CallSource AS Type, ErcAlarmCodes.Description AS Description, 
   description1.descr AS Status, 
   dailycal.status as HiddenStatus, description2.descr as Header, 
   dailycal.calldesignation as hiddencalldesignation, 
   Subscriber_ID as subno, (Lastname + ', ' + FirstName) as FullName

   FROM ERC.dbo.dailycalls as dailycal 
   LEFT OUTER JOIN ERC.dbo.ErcAlarmCodes as ErcAlarmCodes ON  ErcAlarmCodes.CallType=dailycal.CallSource 
    AND ErcAlarmCodes.Code = dailycal.alarmcode1 
   left outer join ERC.dbo.description as description1 on description1.calltype = dailycal.CallSource and 
description1.codetype='S' and description1.code=dailycal.status 
   left outer join ERC.dbo.description as description2 on description2.calltype=dailycal.CallSource and 
description2.codetype='C' and description2.code=dailycal.calldesignation 
   left join amac.dbo.subscriber as subscriber on subscriber.Unit_ID = dailycal.TrueUnitID 
   and (subscriber.type is null or subscriber.type = case when dailycal.callSource = 'W' then 'Safecom'  
				when dailycal.CallSource = 'A' then 'Walgreens' 
				when dailycal.CallSource = 'B' then 'McKesson' 
				when dailycal.CallSource = 'C' then 'Backup' 
				when dailycal.CallSource = 'D' then 'DSPNET'
				else  'Primary' end)
    WHERE 
	(dailycal.callmatch='yes')
/* (ISNULL(dailycal.status, '0') < '0100') AND (dailycal.callmatch = 'Yes') */
	 and (subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
       and dailycal.subno is null 
--addition 1/10 due to slow system
 and dailycal.dailycallsid >=7643015
--and dailycal.DailyCallsID in (789555)  
)
 as t1, ERC.dbo.dailycalls as dailycals
where dailycals.DailyCallsID=t1.DailyCallsID



--update ERC.dbo.dailycalls set subno = null where DailyCallsID in (789212, 789192)


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'a job runs that searches this database and then closes out all ''falsing'' signals on the tbl_DailyCalls' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AutoCloseoutList'
GO
USE [master]
GO
ALTER DATABASE [ERC] SET  READ_WRITE 
GO
