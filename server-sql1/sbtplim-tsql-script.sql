USE [master]
GO
/****** Object:  Database [SBTPlim]    Script Date: 2/11/2016 1:54:49 PM ******/
CREATE DATABASE [SBTPlim] ON  PRIMARY 
( NAME = N'SBTPLIM', FILENAME = N'f:\data\Mssql\Data\SBTPlim_Data.MDF' , SIZE = 107520KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SBTPLIM_log', FILENAME = N'g:\data\Mssql\Data\SBTPlim_log.LDF' , SIZE = 321088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SBTPlim] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SBTPlim].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [SBTPlim] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SBTPlim] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SBTPlim] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SBTPlim] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SBTPlim] SET ARITHABORT OFF 
GO
ALTER DATABASE [SBTPlim] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SBTPlim] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SBTPlim] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SBTPlim] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SBTPlim] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SBTPlim] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SBTPlim] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SBTPlim] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SBTPlim] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SBTPlim] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SBTPlim] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SBTPlim] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SBTPlim] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SBTPlim] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SBTPlim] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SBTPlim] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SBTPlim] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SBTPlim] SET  MULTI_USER 
GO
ALTER DATABASE [SBTPlim] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SBTPlim] SET DB_CHAINING OFF 
GO
USE [SBTPlim]
GO
/****** Object:  User [PLtest]    Script Date: 2/11/2016 1:54:49 PM ******/
CREATE USER [PLtest] FOR LOGIN [PLtest] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\ahazut]    Script Date: 2/11/2016 1:54:50 PM ******/
CREATE USER [ERC_DOMAIN\ahazut] FOR LOGIN [ERC_DOMAIN\ahazut] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [amac]    Script Date: 2/11/2016 1:54:50 PM ******/
CREATE USER [amac] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [PLtest]
GO
ALTER ROLE [db_owner] ADD MEMBER [ERC_DOMAIN\ahazut]
GO
ALTER ROLE [db_datareader] ADD MEMBER [amac]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [amac]
GO
/****** Object:  Table [dbo].[SBT_ICILOC02]    Script Date: 2/11/2016 1:54:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SBT_ICILOC02](
	[loctid] [varchar](6) NULL,
	[item] [varchar](15) NULL,
	[lsoaloc] [numeric](11, 3) NULL,
	[lwoaloc] [numeric](11, 3) NULL,
	[lonhand] [numeric](11, 3) NULL,
	[ltmaloc] [numeric](11, 3) NULL,
	[adduser] [varchar](4) NULL,
	[adddate] [datetime] NULL,
	[addtime] [varchar](8) NULL,
	[lckstat] [varchar](1) NULL,
	[lckuser] [varchar](4) NULL,
	[lckdate] [datetime] NULL,
	[lcktime] [varchar](8) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SBT_ICIQTY02]    Script Date: 2/11/2016 1:54:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SBT_ICIQTY02](
	[qserial] [varchar](20) NULL,
	[loctid] [varchar](6) NULL,
	[item] [varchar](15) NULL,
	[qsoaloc] [numeric](11, 3) NULL,
	[qwoaloc] [numeric](11, 3) NULL,
	[qonhand] [numeric](11, 3) NULL,
	[qtmaloc] [numeric](11, 3) NULL,
	[issued] [datetime] NULL,
	[adduser] [varchar](4) NULL,
	[adddate] [datetime] NULL,
	[addtime] [varchar](8) NULL,
	[lckstat] [varchar](1) NULL,
	[lckuser] [varchar](4) NULL,
	[lckdate] [datetime] NULL,
	[lcktime] [varchar](8) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SBT_ICITEM02]    Script Date: 2/11/2016 1:54:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SBT_ICITEM02](
	[item] [varchar](15) NULL,
	[itmdesc] [varchar](60) NULL,
	[type] [varchar](1) NULL,
	[comcode] [varchar](2) NULL,
	[useserl] [varchar](1) NULL,
	[itmclss] [varchar](6) NULL,
	[code] [varchar](6) NULL,
	[adduser] [varchar](4) NULL,
	[adddate] [datetime] NULL,
	[addtime] [varchar](8) NULL,
	[lckstat] [varchar](1) NULL,
	[lckuser] [varchar](4) NULL,
	[lckdate] [datetime] NULL,
	[lcktime] [varchar](8) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [SBTPlim] SET  READ_WRITE 
GO
