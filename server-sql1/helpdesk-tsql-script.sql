USE [master]
GO
/****** Object:  Database [HelpDesk]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE DATABASE [HelpDesk] ON  PRIMARY 
( NAME = N'HelpDesk_Data', FILENAME = N'F:\DATA\MSSQL\Data\HelpDesk_Data.MDF' , SIZE = 202432KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'HelpDesk_Log', FILENAME = N'G:\DATA\MSSQL\Data\HelpDesk_Log.LDF' , SIZE = 182272KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [HelpDesk] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HelpDesk].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [HelpDesk] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HelpDesk] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HelpDesk] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HelpDesk] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HelpDesk] SET ARITHABORT OFF 
GO
ALTER DATABASE [HelpDesk] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HelpDesk] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HelpDesk] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HelpDesk] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HelpDesk] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HelpDesk] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HelpDesk] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HelpDesk] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HelpDesk] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HelpDesk] SET  ENABLE_BROKER 
GO
ALTER DATABASE [HelpDesk] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HelpDesk] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HelpDesk] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HelpDesk] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HelpDesk] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HelpDesk] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HelpDesk] SET RECOVERY FULL 
GO
ALTER DATABASE [HelpDesk] SET  MULTI_USER 
GO
ALTER DATABASE [HelpDesk] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [HelpDesk] SET DB_CHAINING OFF 
GO
USE [HelpDesk]
GO
/****** Object:  User [staherian]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [staherian] FOR LOGIN [ERC_DOMAIN\staherian] WITH DEFAULT_SCHEMA=[staherian]
GO
/****** Object:  User [QATracking_DevUser]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [QATracking_DevUser] FOR LOGIN [QATracking_DevUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Psantiago]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [Psantiago] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[Psantiago]
GO
/****** Object:  User [ERC_DOMAIN\SSunko]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [ERC_DOMAIN\SSunko] FOR LOGIN [ERC_DOMAIN\ssunko] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [ERC_DOMAIN\REPORTING] FOR LOGIN [ERC_DOMAIN\Reporting] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\REPORTING]
GO
/****** Object:  User [ERC_DOMAIN\GCaporale]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [ERC_DOMAIN\GCaporale] FOR LOGIN [ERC_DOMAIN\GCaporale] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\GCaporale]
GO
/****** Object:  User [AHazut]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE USER [AHazut] FOR LOGIN [ERC_DOMAIN\ahazut] WITH DEFAULT_SCHEMA=[AHazut]
GO
ALTER ROLE [db_owner] ADD MEMBER [staherian]
GO
ALTER ROLE [db_owner] ADD MEMBER [AHazut]
GO
/****** Object:  Schema [AHazut]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE SCHEMA [AHazut]
GO
/****** Object:  Schema [ERC_DOMAIN\GCaporale]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE SCHEMA [ERC_DOMAIN\GCaporale]
GO
/****** Object:  Schema [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE SCHEMA [ERC_DOMAIN\REPORTING]
GO
/****** Object:  Schema [Psantiago]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE SCHEMA [Psantiago]
GO
/****** Object:  Schema [staherian]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE SCHEMA [staherian]
GO
/****** Object:  UserDefinedFunction [dbo].[GetNotesCount]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Return the notes count for the desired ticket
-- Created by Avi Hazut
CREATE Function [dbo].[GetNotesCount](@TicketID integer)
RETURNS Integer
AS
BEGIN

	Declare @NotesCount integer
	
	select @NotesCount = count(*)
	from TicketNote
	where FKTicketID = @TicketID

	return @NotesCount
END

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[EmpID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[MyHelpDeskPassword] [varchar](50) NOT NULL,
	[EmpTitle] [varchar](50) NULL,
	[EmpNTLogin] [varchar](50) NULL,
	[SupervisorID] [int] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Employee_Active]  DEFAULT ((-1)),
	[DefaultCostCenter] [char](10) NOT NULL,
	[IsSupervisor] [bit] NOT NULL CONSTRAINT [DF_Employee_IsSupervisor]  DEFAULT (0),
	[IsGod] [bit] NOT NULL CONSTRAINT [DF_Employee_IsGod]  DEFAULT (0),
	[Dept] [varchar](50) NOT NULL,
	[IsTemp] [bit] NOT NULL CONSTRAINT [DF_Employee_IsTemp]  DEFAULT (0),
	[Email] [varchar](50) NULL,
	[Email Password] [varchar](50) NULL,
	[NT Password] [varchar](50) NULL,
	[phone] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Employee] UNIQUE NONCLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProblemType]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProblemType](
	[ProblemTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProblemType] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ProblemType_Active]  DEFAULT (1),
 CONSTRAINT [PK_ProblemType] PRIMARY KEY CLUSTERED 
(
	[ProblemTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusText] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ticket](
	[TicketID] [int] IDENTITY(1,1) NOT NULL,
	[Summary] [varchar](255) NOT NULL,
	[Description] [varchar](8000) NULL,
	[FKOwnerID] [int] NOT NULL,
	[FKUserID] [int] NOT NULL,
	[TicketDateCreated] [datetime] NOT NULL CONSTRAINT [DF_Ticket_TicketDateCreated]  DEFAULT (getdate()),
	[FKStatusID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKStatus]  DEFAULT (1),
	[FKProblemTypeID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKProblemType]  DEFAULT (1),
	[CloseDateTarget] [datetime] NULL,
	[CloseDateActual] [datetime] NULL,
	[Priority] [char](10) NOT NULL CONSTRAINT [DF_Ticket_Priority]  DEFAULT ('Med'),
	[Type] [int] NULL CONSTRAINT [DF_Ticket_Type]  DEFAULT (0),
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[TicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketNote]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketNote](
	[TicketNoteID] [int] IDENTITY(1,1) NOT NULL,
	[FKTicketID] [int] NOT NULL,
	[NoteDateCreated] [datetime] NOT NULL CONSTRAINT [DF_TicketNote_NoteDateCreated]  DEFAULT (getdate()),
	[FKNoteOwnerID] [int] NULL,
	[Description] [varchar](2000) NULL,
 CONSTRAINT [PK_TicketNote] PRIMARY KEY CLUSTERED 
(
	[TicketNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[kirk]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[kirk] as
select ticketid as ticket, description  from ticket
union all
select fkticketid as ticket, description from ticketnote





GO
/****** Object:  View [dbo].[rpt_IT_user]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[rpt_IT_user]
as
select * from employee where dept = 'IT'

GO
/****** Object:  View [dbo].[vGetTicketDetailSummary]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/****** Object:  View dbo.vGetTicketDetailSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  VIEW [dbo].[vGetTicketDetailSummary]
AS
--KA 5/15/2012 remove old IT tickets
SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, left(dbo.Ticket.Description,7800) AS Description , Employee_3.FirstName + ' ' + Employee_3.LastName AS Owner, 
                      Employee_1.FirstName + ' ' + Employee_1.LastName AS [User], dbo.Ticket.TicketDateCreated, dbo.Status.StatusText, 
                      dbo.ProblemType.ProblemType, dbo.Ticket.CloseDateTarget, dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, dbo.TicketNote.TicketNoteID, 
                      dbo.TicketNote.NoteDateCreated, Employee_2.FirstName + ' ' + Employee_2.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
                      dbo.Ticket.FKOwnerID, dbo.Ticket.FKUserID, employee_1.email, ticket.type
FROM         dbo.Employee Employee_2 INNER JOIN
                      dbo.TicketNote ON Employee_2.EmpID = dbo.TicketNote.FKNoteOwnerID RIGHT OUTER JOIN
                      dbo.Ticket INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      dbo.Employee Employee_3 ON dbo.Ticket.FKOwnerID = Employee_3.EmpID INNER JOIN
                      dbo.Employee Employee_1 ON dbo.Ticket.FKUserID = Employee_1.EmpID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID ON dbo.TicketNote.FKTicketID = dbo.Ticket.TicketID
where 
dbo.Ticket.FKProblemTypeID in (1,36,38,39,21)
and TicketDateCreated>='10/1/2011'




















GO
/****** Object:  View [dbo].[vGetTicketSummary]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View dbo.vGetTicketSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  VIEW [dbo].[vGetTicketSummary]
AS
-- 5/15/2012 remove Old IT tickets
SELECT     Employee_1.EmpID as EmpID, dbo.Ticket.TicketID,dbo.GetNotesCount(dbo.Ticket.TicketID)as NotesCount, dbo.Ticket.Summary, left(dbo.Ticket.Description,7800) AS Description, dbo.Ticket.FKOwnerID, dbo.Ticket.FKUserID, 
                      dbo.Ticket.TicketDateCreated, dbo.Ticket.FKStatusID, dbo.Ticket.FKProblemTypeID, dbo.Ticket.CloseDateTarget, 
                      dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, Employee_1.FirstName AS OwnerFirstName, Employee_1.LastName AS OwnerLastName, 
                      Employee_2.FirstName AS UserFirstName, Employee_2.LastName AS UserLastName, dbo.Status.StatusText, dbo.ProblemType.ProblemType, Ticket.type,Employee_2.Dept as Dept
FROM         dbo.Ticket INNER JOIN
                      dbo.Employee Employee_1 ON dbo.Ticket.FKOwnerID = Employee_1.EmpID INNER JOIN
                      dbo.Employee Employee_2 ON dbo.Ticket.FKUserID = Employee_2.EmpID INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID
where dbo.Ticket.FKProblemTypeID in (1,36,38,39,21)
and TicketDateCreated>='10/1/2011'

GO
/****** Object:  Index [IX_TicketNote_FKTicketID]    Script Date: 2/11/2016 1:51:48 PM ******/
CREATE NONCLUSTERED INDEX [IX_TicketNote_FKTicketID] ON [dbo].[TicketNote]
(
	[FKTicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Employee] FOREIGN KEY([FKOwnerID])
REFERENCES [dbo].[Employee] ([EmpID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Employee]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Employee1] FOREIGN KEY([FKUserID])
REFERENCES [dbo].[Employee] ([EmpID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Employee1]
GO
ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_ProblemType] FOREIGN KEY([FKProblemTypeID])
REFERENCES [dbo].[ProblemType] ([ProblemTypeID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_ProblemType]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Status] FOREIGN KEY([FKStatusID])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Status]
GO
ALTER TABLE [dbo].[TicketNote]  WITH NOCHECK ADD  CONSTRAINT [FK_TicketNote_Employee] FOREIGN KEY([FKNoteOwnerID])
REFERENCES [dbo].[Employee] ([EmpID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_Employee]
GO
ALTER TABLE [dbo].[TicketNote]  WITH NOCHECK ADD  CONSTRAINT [FK_TicketNote_Ticket] FOREIGN KEY([FKTicketID])
REFERENCES [dbo].[Ticket] ([TicketID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_Ticket]
GO
/****** Object:  StoredProcedure [dbo].[HelpDeskAlert_OpenTickets]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** dbo.HelpDeskAlert1***** Solyman Taherian****Datecreated: 05/23/2005 ******/
CREATE PROCEDURE [dbo].[HelpDeskAlert_OpenTickets]
    @vChrUsername  varchar(100)=''

/* Returns 2 recordsets consisting of the ticket number, datecreated, */

AS


SELECT distinct TicketId AS OpenTickets, TicketDateCreated, (case when ticketnote.description is not null and ticketnote.description <> '' then 1 else 0 end) as note,
b.notedate
 FROM ticket  LEFT OUTER JOIN
                      Employee ON Employee.EmpID = Ticket.FKOwnerID
left join 
(
     Select min(NoteDateCreated) as NoteDate,  fkticketID--,ticketnote.description as minNotes
      From ticketnote left outer join employee on employee.empid = ticketnote.fknoteownerid
      where employee.empntlogin=@vChrUsername --or notedatecreated = null
      Group by fkticketid
) b on b.fkticketid=ticket.ticketid
left outer JOIN TicketNote on b.Notedate = ticketnote.notedatecreated      --ticket.ticketID = TicketNote.FKticketID
WHERE ticket.fkstatusid = 1  and  Employee.EmpNTLogin =  @vChrUsername 


ORDER BY ticket.ticketDateCreated ASC
GO
/****** Object:  StoredProcedure [dbo].[HelpDeskAlert_OpenUnassignedTickets]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** dbo.HelpDeskAlert1***** Solyman Taherian****Datecreated: 05/23/2005 ******/
CREATE PROCEDURE [dbo].[HelpDeskAlert_OpenUnassignedTickets]
            @vChrUsername  varchar(100)=''
/* Returns three recordsets consisting of the ticket number, datecreated, status and closedatetarget of all help desk tickets. */

AS

Select TicketId as OpenTickets , TicketdateCreated from 
ticket
where fkstatusid = 1 and FkOwnerID = '200'
Order BY TicketDateCreated ASC
GO
/****** Object:  StoredProcedure [dbo].[HelpDeskAlert2]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** dbo.HelpDeskAlert2***** Solyman Taherian****Datecreated: 05/23/2005 ******/
CREATE PROCEDURE [dbo].[HelpDeskAlert2]
    @vChrUsername  varchar(100)=''
/* Returns three recordsets consisting of the ticket number, datecreated, status and closedatetarget of all help desk tickets. */

AS

Select ticketID as OpenTickets, ticketDateCreated from 
ticket LEFT OUTER JOIN
                      Employee ON Employee.EmpID = Ticket.FKOwnerID
WHERE ticket.fkstatusid = 1  and  Employee.EmpNTLogin =  @vChrUsername

ORDER by ticketDateCreated ASC
GO
/****** Object:  StoredProcedure [dbo].[spAllEmpList]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spAllEmpList]

/* Returns three recordsets consisting of the id and text for names of IT people, all employees,
and status. */

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and (IsTemp = 0)
  ORDER BY LastName
SELECT EmpID, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and (IsTemp = 0)
  ORDER BY FirstName
GO
/****** Object:  StoredProcedure [dbo].[spEmpList]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spEmpList]

/* Returns three recordsets consisting of the id and text for names of IT people, all employees,
and status. */

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY LastName
SELECT EmpID, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY FirstName
GO
/****** Object:  StoredProcedure [dbo].[spGetAllEmps]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spGetAllEmps]

/* This procedure returns a list of all employees. */

AS
SET NOCOUNT ON

SELECT EmpID,   LastName + ', ' + FirstName AS EmpName
  FROM Employee
  WHERE (Active = 1) and (IsTemp = 0)
  ORDER BY LastName

return (0)
GO
/****** Object:  StoredProcedure [dbo].[spGetEmp]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetEmp]
	@EmpID int

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and (IsTemp = 0) and EmpID = @EmpID
  ORDER BY LastName
GO
/****** Object:  StoredProcedure [dbo].[spGetEmpName]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetEmpName]
  @EmpNTLogin char(50) = '', @EmpID int = 0, @EmpName char(100) OUTPUT, 
  @IsSupervisor bit OUTPUT, @IsGod bit OUTPUT, @EmpIDout int OUTPUT, 
  @EmpNTLoginout char(50) OUTPUT

/* This procedure returns an employee's name when given their NT login name
or employee ID number. 
It also returns whether or not he or she is a supervisor and their employee ID number
(which is the primary key and NOT the employee number most employees are familiar with.)
A few other fields are returned as well.*/

AS
SET NOCOUNT ON

IF (@EmpID = 0)
/*If no employee ID number is supplied, then an NT login name MUST be supplied.  Use the NT
login name when no ID is given.*/
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @IsSupervisor = IsSupervisor,
  @IsGod = IsGod, @EmpIDout = EmpID, @EmpNTLoginout=@EmpNTLogin
  FROM Employee WHERE (EmpNTLogin=@EmpNTLogin)
END
ELSE
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @IsSupervisor = IsSupervisor,
    @IsGod = IsGod, @EmpIDout = EmpID, @EmpNTLoginout=EmpNTLogin
  FROM Employee WHERE (EmpID=@EmpID)
END

IF @EmpName IS NULL
     return (1)
ELSE
     return (0)

GO
/****** Object:  StoredProcedure [dbo].[spGetOptions]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  PROCEDURE [dbo].[spGetOptions]

/* Returns three recordsets consisting of the id and text for names of IT people, all employees,
and status. */

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName FROM Employee
  WHERE Dept='IT' and Active=1 and IsTemp = 0
  ORDER BY LastName
SELECT EmpID, FirstName, LastName FROM Employee
  WHERE Active=1 and IsTemp = 0
  ORDER BY FirstName, LastName
SELECT ProblemTypeID, ProblemType FROM ProblemType
  WHERE Active=1
 ORDER BY ProblemType
SELECT StatusID, StatusText FROM Status
GO
/****** Object:  StoredProcedure [dbo].[spGetUsers]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/* Return users list */
CREATE  PROCEDURE [dbo].[spGetUsers]

AS
	SELECT EmpID, FirstName, LastName FROM Employee
	WHERE Active=1 and IsTemp = 0
	ORDER BY FirstName, LastName
GO
/****** Object:  StoredProcedure [dbo].[spValidateEmp]    Script Date: 2/11/2016 1:51:48 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spValidateEmp]
  @EmpID int, @myhelpdeskpassword Varchar(30)

/* This procedure takes as input an employee id and the last 4 digits of their social
security number and checks to see if these match the database.  If they do, the
procedure returns 0.  If not, it returns 1. */

AS
SET NOCOUNT ON

IF (SELECT COUNT(*) FROM Employee WHERE EmpID = @EmpID AND MyHelpDeskPassword = @myhelpdeskpassword) > 0
     return (0)
ELSE
     return (1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'who''s my boss (links back to this table)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Employee', @level2type=N'COLUMN',@level2name=N'SupervisorID'
GO
USE [master]
GO
ALTER DATABASE [HelpDesk] SET  READ_WRITE 
GO
