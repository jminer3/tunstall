USE [master]
GO
/****** Object:  Database [GMEE_HSMS]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE DATABASE [GMEE_HSMS] ON  PRIMARY 
( NAME = N'GMEE_HSMS', FILENAME = N'F:\DATA\MSSQL\Data\GMEE_HSMS.mdf' , SIZE = 10706944KB , MAXSIZE = UNLIMITED, FILEGROWTH = 51200KB )
 LOG ON 
( NAME = N'GMEE_HSMS_log', FILENAME = N'G:\DATA\MSSQL\Data\GMEE_HSMS_log.ldf' , SIZE = 3923840KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GMEE_HSMS] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GMEE_HSMS].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [GMEE_HSMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GMEE_HSMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GMEE_HSMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GMEE_HSMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GMEE_HSMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GMEE_HSMS] SET RECOVERY FULL 
GO
ALTER DATABASE [GMEE_HSMS] SET  MULTI_USER 
GO
ALTER DATABASE [GMEE_HSMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GMEE_HSMS] SET DB_CHAINING OFF 
GO
USE [GMEE_HSMS]
GO
/****** Object:  User [SalesGMEE]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [SalesGMEE] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [PLtest]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [PLtest] FOR LOGIN [PLtest] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Mccoy]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [Mccoy] FOR LOGIN [Mccoy] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Ivan]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [Ivan] FOR LOGIN [Ivan] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [GMEEReporter]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [GMEEReporter] FOR LOGIN [GMEEReporter] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\svr_logger1]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [ERC_DOMAIN\svr_logger1] FOR LOGIN [ERC_DOMAIN\svr_logger1] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\svr_licsql09]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [ERC_DOMAIN\svr_licsql09] FOR LOGIN [ERC_DOMAIN\svr_licsql09] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [amac]    Script Date: 2/11/2016 1:49:46 PM ******/
CREATE USER [amac] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [SalesGMEE]
GO
ALTER ROLE [db_owner] ADD MEMBER [PLtest]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Mccoy]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Ivan]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Ivan]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [Ivan]
GO
ALTER ROLE [db_owner] ADD MEMBER [GMEEReporter]
GO
ALTER ROLE [db_datareader] ADD MEMBER [GMEEReporter]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [GMEEReporter]
GO
ALTER ROLE [db_owner] ADD MEMBER [ERC_DOMAIN\svr_logger1]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ERC_DOMAIN\svr_licsql09]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ERC_DOMAIN\svr_licsql09]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ERC_DOMAIN\svr_licsql09]
GO
ALTER ROLE [db_datareader] ADD MEMBER [amac]
GO
/****** Object:  UserDefinedFunction [dbo].[fnHasOpenWorkOrder]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[fnHasOpenWorkOrder] (@subid char(6)) 
RETURNS varchar(3) 
AS 
BEGIN 
 DECLARE @VAR varchar(3)
 SET @VAR='' 
 if (select count(*) from sql1.amac.dbo.workorder workorder where subscriber_id=@subid
       and WorkOrder.DateCompleted IS NULL AND (WorkOrder.Status <> 'N') )>0
	select @var='Yes'
 
RETURN @VAR
END 

GO
/****** Object:  UserDefinedFunction [dbo].[fnLastContactComment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[fnLastContactComment] (@subid char(6)) 
RETURNS varchar(1500) 
AS 
BEGIN 
 DECLARE @VAR varchar(1500)
declare @contact_id bigint
 SET @VAR='' 
 SELECT @contact_id=max(contact_id) FROM sql1.amac.dbo.contact contact
	 WHERE subscriber_id in (
		select subscriber_id from sql1.amac.dbo.subscriber where masterreference=@subid or subscriber_id=@subid
		) and isnull(status,'')<>'D'
select @var=comments from sql1.amac.dbo.contact where contact_id=@contact_id
 
RETURN @VAR
END 




GO
/****** Object:  UserDefinedFunction [dbo].[fnSubLanguage]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnSubLanguage] (@subid char(6)) 
RETURNS varchar(200) 
AS 
BEGIN 
 DECLARE @VAR VARCHAR(200) 
 SET @VAR='' 
 SELECT @VAR=@VAR+rtrim(codes.description)+',' FROM sql1.amac.dbo.subscriberlanguage subscriberlanguage
	left outer join sql1.amac.dbo.codes codes on subscriberlanguage.languagetype=codes.type and
		subscriberlanguage.language_id=codes.code	
	 WHERE subscriber_id=@subid and codes.deleted='N'

if len(@var)>1 
	SET @VAR=left(@VAR, len(@var)-1)
 
RETURN @VAR
END 

GO
/****** Object:  Table [dbo].[Account]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AccountName] [varchar](40) NULL,
	[AccountType] [varchar](25) NULL,
	[AccountType_Valid] [varchar](32) NULL,
	[ContactName] [varchar](40) NULL,
	[ContactPhone] [varchar](25) NULL,
	[Description] [varchar](1000) NULL,
	[DisplayName] [varchar](255) NULL,
	[Employees] [varchar](25) NULL,
	[Employees_Valid] [varchar](32) NULL,
	[Industry] [varchar](25) NULL,
	[Industry_Valid] [varchar](32) NULL,
	[MarketSegment] [varchar](100) NULL,
	[MarketSegment_Valid] [varchar](32) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[Ownership] [varchar](25) NULL,
	[Ownership_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[ParentAccountName] [varchar](40) NULL,
	[ParentAccountNotEqual] [bit] NULL,
	[PreferredMethodContact] [varchar](25) NULL,
	[PreferredMethodConta_Valid] [varchar](32) NULL,
	[PrimaryPartnerName] [varchar](40) NULL,
	[Status] [varchar](60) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Tax] [decimal](4, 2) NULL,
	[Territory] [varchar](25) NULL,
	[TickerSymbol] [varchar](7) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[ParentAccountLink_RecID] [varchar](32) NULL,
	[ParentAccountLink_Category] [varchar](61) NULL,
	[ParentAccountLink_VRecID] [varchar](32) NULL,
	[PrimaryPartnerLink_RecID] [varchar](32) NULL,
	[PrimaryPartnerLink_Category] [varchar](61) NULL,
	[PrimaryPartnerLink_VRecID] [varchar](32) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](61) NULL,
	[PrimaryAddressLink_VRecID] [varchar](32) NULL,
	[PrimaryWebLink_RecID] [varchar](32) NULL,
	[PrimaryWebLink_Category] [varchar](61) NULL,
	[PrimaryWebLink_VRecID] [varchar](32) NULL,
	[PhoneLink_RecID] [varchar](32) NULL,
	[PhoneLink_Category] [varchar](61) NULL,
	[PhoneLink_VRecID] [varchar](32) NULL,
	[PrimaryContactLink_RecID] [varchar](32) NULL,
	[PrimaryContactLink_Category] [varchar](61) NULL,
	[PrimaryContactLink_VRecID] [varchar](32) NULL,
	[NAICSCode] [varchar](25) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](61) NULL,
	[PrimaryEmailLink_VRecID] [varchar](32) NULL,
	[PrimaryEmail] [varchar](100) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[AnnualRevenuePCV] [decimal](26, 0) NULL,
	[AnnualRevenueTCV] [decimal](26, 0) NULL,
	[LastName] [varchar](40) NULL,
	[NearIntersection] [varchar](40) NULL,
	[DOB] [datetime] NULL,
	[Language1] [varchar](25) NULL,
	[Sex] [varchar](25) NULL,
	[ProgramType] [varchar](25) NULL,
	[LastSignalDate] [datetime] NULL,
	[CaseNum] [varchar](25) NULL,
	[Medicaid] [varchar](25) NULL,
	[AgencyName] [varchar](50) NULL,
	[SubContractorID] [varchar](25) NULL,
	[SubContractorName] [varchar](40) NULL,
	[CASANum] [varchar](25) NULL,
	[AgencyID] [varchar](25) NULL,
	[Casemanager] [varchar](25) NULL,
	[SubscriberID] [varchar](25) NULL,
	[MiddleInitial] [varchar](1) NULL,
	[KnownName] [varchar](20) NULL,
	[UnitID] [varchar](4) NULL,
	[SSN] [varchar](9) NULL,
	[Ktype] [varchar](25) NULL,
	[ForcedEntry] [varchar](1) NULL,
	[Cancel] [varchar](1) NULL,
	[UnitRentedSold] [varchar](1) NULL,
	[TimZoneCode] [smallint] NULL,
	[IsDayLightSaving] [varchar](1) NULL,
	[OrderDate] [datetime] NULL,
	[EntryDate] [datetime] NULL,
	[InstallDate] [datetime] NULL,
	[RemovalDate] [datetime] NULL,
	[REquestedRemovalDate] [datetime] NULL,
	[EquipmentRecovered] [varchar](1) NULL,
	[FileMagic] [datetime] NULL,
	[ConfirmedReceived] [varchar](1) NULL,
	[SpecialInst] [varchar](1000) NULL,
	[Deleted] [varchar](1) NULL,
	[SecondUser] [varchar](1) NULL,
	[OnlineSince] [datetime] NULL,
	[KProgramType] [varchar](25) NULL,
	[MasterReference] [varchar](6) NULL,
	[SharedID] [smallint] NULL,
	[SystemName] [varchar](20) NULL,
	[IPAddress] [varchar](20) NULL,
	[IsAddressVerified] [varchar](1) NULL,
	[MannualOveride] [varchar](1) NULL,
	[AgencyID_Valid] [varchar](32) NULL,
	[HasOpenWorkOrder] [varchar](25) NULL,
	[WorkOrderLink_RecID] [varchar](32) NULL,
	[WorkOrderLink_Category] [varchar](61) NULL,
	[WorkOrderLink_VRecID] [varchar](32) NULL,
	[LanguageLink_RecID] [varchar](32) NULL,
	[LanguageLink_Category] [varchar](61) NULL,
	[LanguageLink_VRecID] [varchar](32) NULL,
	[LinkLastContact_RecID] [varchar](32) NULL,
	[LinkLastContact_Category] [varchar](61) NULL,
	[LinkLastContact_VRecID] [varchar](32) NULL,
	[LinkInstallDays_RecID] [varchar](32) NULL,
	[LinkInstallDays_Category] [varchar](61) NULL,
	[LinkInstallDays_VRecID] [varchar](32) NULL,
	[CSResponsible] [varchar](25) NULL,
 CONSTRAINT [AccountRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountIndustry]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountIndustry](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Industry] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [AcctIndstryRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountOrder]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountOrder](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OrderDate] [datetime] NULL,
	[OpportunityName] [varchar](100) NULL,
	[Description] [varchar](256) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdAccountOrder] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [AccountStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AccountType] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [AccountTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[ActivityType] [varchar](30) NULL,
	[ActivityType_Valid] [varchar](32) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Purpose] [varchar](60) NULL,
	[Purpose_Valid] [varchar](32) NULL,
	[Subject] [varchar](255) NULL,
	[Priority] [varchar](10) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[DueDateTime] [datetime] NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[EndDateTime_Valid] [varchar](32) NULL,
	[EntryID] [varchar](1024) NULL,
	[ParentName] [varchar](255) NULL,
	[ActivityResult] [varchar](100) NULL,
	[ActivityResult_Valid] [varchar](32) NULL,
	[TimeLength] [smallint] NULL,
	[Notes] [text] NULL,
	[ActivityLocation] [varchar](40) NULL,
	[ActivityLocation_Valid] [varchar](32) NULL,
	[PercentComplete] [smallint] NULL,
	[DueDateTime_Valid] [varchar](32) NULL,
	[ConversationIndex] [varchar](100) NULL,
	[AllDayEvent] [bit] NULL,
	[Reminder] [bit] NULL,
	[ReminderTime] [smallint] NULL,
	[DateRejected] [datetime] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[DueTime] [datetime] NULL,
	[WritenToKshema] [varchar](25) NULL,
	[ActivtyReason] [varchar](25) NULL,
	[ActivtyReason_Valid] [varchar](32) NULL,
	[SpokeTo] [varchar](25) NULL,
	[SpokeTo_Valid] [varchar](32) NULL,
	[FollowUp] [bit] NULL,
	[CommunicationMethod] [varchar](25) NULL,
	[CommunicationMethod_Valid] [varchar](32) NULL,
	[TaskFollowUp] [varchar](25) NULL,
	[TaskFollowUp_Valid] [varchar](32) NULL,
	[CallOutcome] [varchar](25) NULL,
	[CallOutcome_Valid] [varchar](32) NULL,
	[LinkAccount_RecID] [varchar](32) NULL,
	[LinkAccount_Category] [varchar](61) NULL,
	[LinkAccount_VRecID] [varchar](32) NULL,
	[Subscriber_ID] [varchar](25) NULL,
	[Owner1] [varchar](200) NULL,
	[Reason_Valid] [varchar](32) NULL,
 CONSTRAINT [ActivityRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityLocation]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityLocation](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ActivityLocation] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
	[SortField] [smallint] NULL,
 CONSTRAINT [ActyLocatnRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityPriority]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityPriority](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Priority] [varchar](5) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [ActyPriortyRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityPurpose](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ActivityType] [varchar](60) NULL,
	[ActivityType_Valid] [varchar](32) NULL,
	[Purpose] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [ActyPurposeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityResult]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityResult](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ActivityResult] [varchar](100) NULL,
 CONSTRAINT [RecIdActivityResul] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](40) NULL,
	[Description] [varchar](255) NULL,
	[SortField] [decimal](15, 0) NULL,
 CONSTRAINT [ActyStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActivityType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ActivityType] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [ActivityTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Address]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Address](
	[RecId] [char](32) NOT NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[AddressType] [varchar](30) NULL,
	[AddressType_Valid] [varchar](32) NULL,
	[Purpose] [varchar](25) NULL,
	[Purpose_Valid] [varchar](32) NULL,
	[DisplayText] [varchar](100) NULL,
	[Description] [varchar](255) NULL,
	[Locale] [varchar](15) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[DoNotContact] [bit] NULL,
	[Email] [varchar](100) NULL,
	[Email_Valid] [varchar](32) NULL,
	[Locale_Valid] [varchar](32) NULL,
	[PreferredFormat] [varchar](25) NULL,
	[PreferredFormat_Valid] [varchar](32) NULL,
	[Address] [text] NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](40) NULL,
	[State_Valid] [varchar](32) NULL,
	[Zip] [varchar](14) NULL,
	[Country] [varchar](40) NULL,
	[Country_Valid] [varchar](32) NULL,
	[Phone] [varchar](25) NULL,
	[Ext] [varchar](5) NULL,
	[WebAddress] [varchar](1000) NULL,
 CONSTRAINT [PKAddress] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressCountry]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressCountry](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CountryShort] [varchar](32) NULL,
	[CountryLong] [varchar](50) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PKAddressCountry] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressLocale]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressLocale](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[Locale] [varchar](50) NULL,
	[Description] [varchar](255) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
 CONSTRAINT [PKAddressLocale] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressPurpose](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[PurposeType] [varchar](30) NULL,
	[PurposeType_Valid] [varchar](32) NULL,
	[Purpose] [varchar](25) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PKAddressPurpose] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressState]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressState](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[StateShort] [varchar](10) NULL,
	[StateLong] [varchar](32) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[Country] [varchar](50) NULL,
	[Country_Valid] [varchar](32) NULL,
	[Description] [varchar](255) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
 CONSTRAINT [PKAddressState] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[AddressType] [varchar](32) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[Derscription] [varchar](255) NULL,
 CONSTRAINT [PKAddressType] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Agency]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Agency](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AgencyID] [varchar](25) NULL,
	[AgencyName] [varchar](25) NULL,
	[NYCVendorID] [varchar](25) NULL,
	[Address1] [varchar](25) NULL,
	[Address2] [varchar](25) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](25) NULL,
	[ZIP] [varchar](25) NULL,
	[PersContact] [varchar](25) NULL,
	[AgencyType] [varchar](25) NULL,
	[AgencyStatus] [varchar](25) NULL,
	[ProgramType] [varchar](25) NULL,
	[ProgramTypeII] [varchar](25) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractExpDate] [datetime] NULL,
	[RenewalIssuedBy] [varchar](25) NULL,
	[EntryDate] [datetime] NULL,
	[AccountManager] [varchar](25) NULL,
	[SalesPerson] [varchar](25) NULL,
	[RegionalOffice] [varchar](25) NULL,
	[COABAA] [varchar](25) NULL,
	[ProviderLink] [varchar](25) NULL,
	[AutoRenew_RecID] [varchar](32) NULL,
	[AutoRenew_Category] [varchar](61) NULL,
	[AutoRenew_VRecID] [varchar](32) NULL,
	[Instructions] [text] NULL,
	[MedicaidEntryForce] [varchar](25) NULL,
	[PhoneNum] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[BusinessOwner] [varchar](30) NULL,
	[RespondAs] [varchar](30) NULL,
	[ForcedMedicaid] [varchar](1) NULL,
	[KComment] [varchar](25) NULL,
	[DefaultUnitID] [varchar](5) NULL,
	[Deleted] [varchar](1) NULL,
	[DefInvLocation] [varchar](6) NULL,
	[Kdescription] [text] NULL,
	[OfficeID] [varchar](6) NULL,
	[KInstructions] [text] NULL,
	[ParentAgencyID] [varchar](6) NULL,
	[CompanyNumber] [varchar](3) NULL,
	[IsAddressVerified] [varchar](1) NULL,
	[test] [varchar](25) NULL,
	[AgencyEquipmentPackage] [varchar](25) NULL,
	[AgencyEquipmentPkg] [varchar](25) NULL,
	[AgencyEquipmentPacka_Valid] [varchar](32) NULL,
	[Owner] [varchar](25) NULL,
	[Owner_Valid] [varchar](32) NULL,
 CONSTRAINT [RecIdAgency] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyAuthorization]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyAuthorization](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[Agency] [varchar](50) NULL,
	[AgencyID] [varchar](25) NULL,
	[AuthNumber] [varchar](25) NULL,
	[CoverageStartDate] [datetime] NULL,
	[CoverageEndDate] [datetime] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[AgencyAuthorizationWeb_ID] [varchar](25) NULL,
	[CaseNumber] [varchar](25) NULL,
	[AuthorizationType] [varchar](25) NULL,
	[AuthorizationType_Valid] [varchar](32) NULL,
	[DiagCode] [varchar](25) NULL,
	[NumberOfUnits] [smallint] NULL,
	[WebLastModDateTime] [varchar](25) NULL,
	[WebUser] [varchar](25) NULL,
 CONSTRAINT [RecIdAgencyAuthori] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyAuthorizationWeb]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyAuthorizationWeb](
	[AgencyAuthorizationWeb_ID] [int] IDENTITY(1,1) NOT NULL,
	[Agency_ID] [varchar](6) NOT NULL,
	[Subscriber_ID] [char](6) NOT NULL,
	[AuthNumber] [varchar](25) NOT NULL,
	[AuthorizationType] [varchar](25) NOT NULL,
	[NumberOfUnits] [smallint] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DiagCode] [varchar](10) NULL,
	[LastModDateTime] [datetime] NOT NULL,
	[LastModBy] [varchar](200) NOT NULL,
	[RecID] [char](32) NULL,
	[CaseNumber] [varchar](20) NULL,
 CONSTRAINT [PK_AgencyAuthorizationWeb] PRIMARY KEY CLUSTERED 
(
	[AgencyAuthorizationWeb_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyEquipmentPackage]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyEquipmentPackage](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[EquipmentPackage] [varchar](25) NULL,
 CONSTRAINT [RecIdAgencyEquipme] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyException]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyException](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[AgencyCode] [varchar](25) NULL,
	[AgencyName] [varchar](50) NULL,
	[Description] [text] NULL,
 CONSTRAINT [RecIdAgencyExcepti] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyOutcomValidation]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyOutcomValidation](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[AgencyOutCome] [varchar](25) NULL,
 CONSTRAINT [RecIdAgencyOutcomV] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyReason]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyReason](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[AgencyReason] [varchar](25) NULL,
 CONSTRAINT [RecIdAgencyReason] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AgencyTask]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyTask](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[Reason] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[CallOutcome] [varchar](25) NULL,
	[CallOutcome_Valid] [varchar](32) NULL,
	[Owner] [varchar](25) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[Contact] [varchar](25) NULL,
	[CreatedOn] [varchar](25) NULL,
	[Reason_Valid] [varchar](32) NULL,
	[Comments] [text] NULL,
	[StartOnDate] [datetime] NULL,
	[StartOnTime] [datetime] NULL,
 CONSTRAINT [RecIdAgencyTask] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AlternatePricing]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlternatePricing](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[mPricingValuePCV] [decimal](10, 2) NULL,
	[mPricingValueTCV] [decimal](10, 2) NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[PricingText] [varchar](25) NULL,
 CONSTRAINT [RecIdAlternatePric] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Application]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Application](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[applicationtype] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdApplication] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Associates]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Associates](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AssociateType] [varchar](25) NULL,
	[AssociateType_Valid] [varchar](32) NULL,
 CONSTRAINT [RecIdAssociates] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssociateTypes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssociateTypes](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Types] [varchar](25) NULL,
 CONSTRAINT [RecIdAssociateType] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Attachment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Attachment](
	[RecID] [char](32) NOT NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](32) NULL,
	[ATTACHNAME] [varchar](255) NOT NULL,
	[ATTACHDESC] [varchar](255) NULL,
	[CreatedBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[Path] [varchar](100) NULL,
	[Host] [varchar](100) NULL,
 CONSTRAINT [PKAttachment] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditHistory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditHistory](
	[RecId] [char](32) NOT NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[BusObID] [varchar](32) NULL,
	[BusObName] [varchar](32) NULL,
	[EventSource] [varchar](32) NULL,
	[FieldName] [varchar](32) NULL,
	[NewFieldValue] [varchar](32) NULL,
	[OldFieldValue] [varchar](32) NULL,
	[TriggerID] [varchar](32) NULL,
	[TriggerName] [varchar](32) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
 CONSTRAINT [AuditHistoryRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Board]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Board](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[TypeofBoard] [varchar](50) NULL,
	[TypeofBoard_Valid] [varchar](32) NULL,
	[BoardTermExpiration] [datetime] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Accountlink] [varchar](25) NULL,
	[Accountname] [varchar](100) NULL,
	[Accountname_Valid] [varchar](32) NULL,
	[BrdAccount] [varchar](50) NULL,
 CONSTRAINT [RecIdBoard] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BundleItems]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BundleItems](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ItemCount] [smallint] NULL,
	[OfferingLink_RecID] [varchar](32) NULL,
	[OfferingLink_Category] [varchar](61) NULL,
	[OfferingLink_VRecID] [varchar](32) NULL,
	[ProductName] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [BundleItemsRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](25) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[EndDate_Valid] [varchar](32) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[CampaignType] [varchar](25) NULL,
	[CampaignType_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](40) NULL,
	[Target] [varchar](40) NULL,
	[Target_Valid] [varchar](32) NULL,
	[EstimatedSizeOfCampaign] [decimal](10, 0) NULL,
	[ExpectedResponseRate] [smallint] NULL,
	[ExpectedConversionRate] [smallint] NULL,
	[ExpectedGrossMargin] [smallint] NULL,
	[IndefiniteDuration] [bit] NULL,
	[Description] [text] NULL,
	[ExpectedROI] [decimal](4, 1) NULL,
	[CampaignID] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[KeyCodeType] [varchar](25) NULL,
	[KeyCodeType_Valid] [varchar](32) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[Tracking] [bit] NULL,
	[LoginID] [varchar](50) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_ExpectedSalesPerConvers__TCV] [decimal](18, 2) NULL,
	[m_ExpectedSalesPerConvers__PCV] [decimal](10, 2) NULL,
	[m_EstimatedCost__TCV] [decimal](10, 2) NULL,
	[m_EstimatedCost__PCV] [decimal](10, 2) NULL,
	[m_ExpectedSales__TCV] [decimal](28, 2) NULL,
	[m_ExpectedSales__PCV] [decimal](10, 2) NULL,
	[m_ExpectedGrossProfit__TCV] [decimal](28, 2) NULL,
	[m_ExpectedGrossProfit__PCV] [decimal](10, 2) NULL,
	[ActiveCampaign] [bit] NULL,
	[PhoneNumber] [varchar](14) NULL,
	[ProgramLink_RecID] [varchar](32) NULL,
	[ProgramLink_Category] [varchar](61) NULL,
	[ProgramLink_VRecID] [varchar](32) NULL,
	[ActivateCampaignLinking] [varchar](1) NULL,
 CONSTRAINT [CampaignRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignCategories]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignCategories](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CampaignCategory] [varchar](40) NULL,
 CONSTRAINT [CpgnCatsRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignCategory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Category] [varchar](50) NULL,
 CONSTRAINT [CpgnCatRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignKeyCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignKeyCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[KeyCode] [varchar](30) NULL,
 CONSTRAINT [CpgnKeyCodeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignOverview](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[CampaignID] [varchar](25) NULL,
	[SegmentID] [varchar](25) NULL,
	[CellID] [varchar](25) NULL,
	[KeyCode] [varchar](100) NULL,
	[Name] [varchar](50) NULL,
	[Category] [varchar](25) NULL,
	[CampaignLink_RecID] [varchar](32) NULL,
	[CampaignLink_Category] [varchar](61) NULL,
	[CampaignLink_VRecID] [varchar](32) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Prefix] [varchar](25) NULL,
	[Owner] [varchar](50) NULL,
	[Phone1] [varchar](25) NULL,
	[Phone2] [varchar](25) NULL,
	[Phone3] [varchar](25) NULL,
	[Phone4] [varchar](25) NULL,
	[Email1] [varchar](100) NULL,
	[Street] [text] NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](25) NULL,
	[Zip] [varchar](14) NULL,
	[Country] [varchar](25) NULL,
	[DoNotContact] [bit] NULL,
	[MiddleName] [varchar](25) NULL,
 CONSTRAINT [CmpgnOverviewRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [CpgnStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignTarget]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignTarget](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Target] [varchar](60) NULL,
 CONSTRAINT [CpgnTargetRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignTask](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](25) NULL,
	[DependsOn] [varchar](100) NULL,
	[DependsOn_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[CampaignLink_RecID] [varchar](32) NULL,
	[CampaignLink_Category] [varchar](61) NULL,
	[CampaignLink_VRecID] [varchar](32) NULL,
	[Subject] [varchar](100) NULL,
	[Notes] [text] NULL,
	[StartDateTime] [datetime] NULL,
	[DueDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[Priority] [varchar](25) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerEmailAddress] [varchar](100) NULL,
 CONSTRAINT [CampaignTaskRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignTaskNotificationConfig]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignTaskNotificationConfig](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[IsActive] [bit] NULL,
	[GracePeriod] [smallint] NULL,
	[NotificationToTaskOwner] [bit] NULL,
	[NotificationToCampaignOwner] [bit] NULL,
 CONSTRAINT [CpgnTaskNotifRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignTemplate](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](100) NULL,
	[CampaignType] [varchar](50) NULL,
	[CampaignType_Valid] [varchar](32) NULL,
	[Target] [varchar](50) NULL,
	[Target_Valid] [varchar](32) NULL,
	[Status] [varchar](50) NULL,
	[Status_Valid] [varchar](32) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IndefiniteDuration] [bit] NULL,
	[Description] [text] NULL,
	[EstimatedSizeOfCampaign] [decimal](10, 0) NULL,
	[ExpectedResponseRate] [smallint] NULL,
	[ExpectedConversionRate] [smallint] NULL,
	[EstimatedGrossMargin] [smallint] NULL,
	[ExpectedROI] [smallint] NULL,
	[KeyCodeType] [varchar](100) NULL,
	[KeyCodeType_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](50) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Owner] [varchar](50) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[Tracking] [bit] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_ExpectedGrossProfit__TCV] [decimal](10, 2) NULL,
	[m_ExpectedGrossProfit__PCV] [decimal](10, 2) NULL,
	[m_ExpectedSales__TCV] [decimal](10, 2) NULL,
	[m_ExpectedSales__PCV] [decimal](10, 2) NULL,
	[m_ExpectedSalesPerConvers__TCV] [decimal](10, 2) NULL,
	[m_ExpectedSalesPerConvers__PCV] [decimal](10, 2) NULL,
	[m_EstimatedCost__TCV] [decimal](10, 2) NULL,
	[m_EstimatedCost__PCV] [decimal](10, 2) NULL,
	[PhoneNumber] [varchar](14) NULL,
	[ProgramLink_RecID] [varchar](32) NULL,
	[ProgramLink_Category] [varchar](61) NULL,
	[ProgramLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [CpgnTemplateRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CampaignType] [varchar](40) NULL,
 CONSTRAINT [CampaignTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseCategory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Category] [varchar](100) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [CaseCategoryRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseCauseCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseCauseCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CauseCode] [varchar](100) NULL,
	[SortOrder] [smallint] NULL,
 CONSTRAINT [CaseCauseCodeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseResolutionCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseResolutionCategory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ResolutionCategory] [varchar](100) NULL,
 CONSTRAINT [CaseRsltnCatRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cases]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cases](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[CaseNumber] [decimal](10, 0) NULL,
	[Status] [varchar](60) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Priority] [varchar](3) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[CaseType] [varchar](60) NULL,
	[CaseType_Valid] [varchar](32) NULL,
	[Category] [varchar](100) NULL,
	[Category_Valid] [varchar](32) NULL,
	[Source] [varchar](100) NULL,
	[Source_Valid] [varchar](32) NULL,
	[Subject] [varchar](200) NULL,
	[Description] [text] NULL,
	[DueOn] [datetime] NULL,
	[DueOn_Valid] [varchar](32) NULL,
	[CauseCode] [varchar](100) NULL,
	[CauseCode_Valid] [varchar](32) NULL,
	[Resolution] [text] NULL,
	[FirstCallResolution] [varchar](25) NULL,
	[FirstCallResolution_Valid] [varchar](32) NULL,
	[ResolutionCategory] [varchar](100) NULL,
	[ResolutionCategory_Valid] [varchar](32) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](61) NULL,
	[ContactLink_VRecID] [varchar](32) NULL,
	[ContactEmail] [varchar](150) NULL,
	[ContactEmail_Valid] [varchar](32) NULL,
	[ContactPhone] [varchar](25) NULL,
	[ContactExt] [varchar](25) NULL,
	[ClosedBy] [varchar](30) NULL,
	[ClosedDateTime] [datetime] NULL,
	[PrimaryProductLink_RecID] [varchar](32) NULL,
	[PrimaryProductLink_Category] [varchar](61) NULL,
	[PrimaryProductLink_VRecID] [varchar](32) NULL,
	[IPCCContactLink] [varchar](32) NULL,
	[DisplayName] [varchar](255) NULL,
	[PrimaryEmail] [varchar](100) NULL,
	[PrimaryEmail_Valid] [varchar](32) NULL,
	[CaseAccount] [varchar](40) NULL,
	[EmpLink_RecID] [varchar](32) NULL,
	[EmpLink_Category] [varchar](61) NULL,
	[EmpLink_VRecID] [varchar](32) NULL,
	[AccumedInvoice] [varchar](25) NULL,
	[AgencyID] [varchar](25) NULL,
	[SubscriberID] [varchar](25) NULL,
	[ProcedureCode] [varchar](25) NULL,
	[AccumedEntry] [datetime] NULL,
	[DateOfService] [datetime] NULL,
	[ReasonCode] [varchar](25) NULL,
	[ResolutionCode] [varchar](25) NULL,
	[AgencyReasonCode] [varchar](25) NULL,
	[AgencyRCDescription] [varchar](75) NULL,
	[AMACRCDescription] [varchar](75) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[anvresolutionprocessed] [datetime] NULL,
	[AccountName] [varchar](50) NULL,
	[AccountPhone] [varchar](25) NULL,
	[AccountEmail] [varchar](75) NULL,
	[CloseDescription] [text] NULL,
 CONSTRAINT [CasesRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseSource]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseSource](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Source] [varchar](100) NULL,
	[Description] [text] NULL,
 CONSTRAINT [CaseSourceRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [CaseStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CaseType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CaseType] [varchar](60) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [CaseTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cell]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cell](
	[CellType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CellPercentage] [smallint] NULL,
	[CellSize] [int] NULL,
	[Name] [varchar](25) NULL,
	[SegmentName] [varchar](25) NULL,
	[Description] [text] NULL,
	[CellID] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [CellRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompetitionRating]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompetitionRating](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Rating] [varchar](25) NULL,
	[Description] [text] NULL,
	[SortField] [smallint] NULL,
 CONSTRAINT [CmptnRatngRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CompetitionStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompetitionStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [CmptnStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Competitor]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Competitor](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CompetitorName] [varchar](60) NULL,
	[DisplayName] [varchar](255) NULL,
	[Notes] [varchar](500) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[ProfileID] [varchar](30) NULL,
	[ProfileType] [varchar](32) NULL,
	[Rating] [varchar](40) NULL,
	[Rating_Valid] [varchar](32) NULL,
	[RatingDescription] [text] NULL,
	[Status] [varchar](60) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Strategy] [text] NULL,
	[Strengths] [text] NULL,
	[Weaknesses] [text] NULL,
 CONSTRAINT [RecIdCompetitor] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contact](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[AccountLink_RecID] [varchar](32) NULL,
	[AccountLink_Category] [varchar](61) NULL,
	[AccountLink_VRecID] [varchar](32) NULL,
	[Territory] [varchar](60) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[SupervisorLink_RecID] [varchar](32) NULL,
	[SupervisorLink_Category] [varchar](61) NULL,
	[SupervisorLink_VRecID] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[AccountName] [varchar](100) NULL,
	[AssistancePhoneExt] [varchar](5) NULL,
	[AssistantPhone] [varchar](14) NULL,
	[Assistant] [varchar](40) NULL,
	[Birthdate] [datetime] NULL,
	[ContactSupervisorName] [varchar](100) NULL,
	[FullName] [varchar](255) NULL,
	[Prefix] [varchar](30) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[FirstName] [varchar](40) NULL,
	[MiddleName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Suffix] [varchar](30) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[Department] [varchar](50) NULL,
	[Department_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](255) NULL,
	[Phone1Link_RecID] [varchar](32) NULL,
	[Phone1Link_Category] [varchar](61) NULL,
	[Phone1Link_VRecID] [varchar](32) NULL,
	[Phone2Link_RecID] [varchar](32) NULL,
	[Phone2Link_Category] [varchar](61) NULL,
	[Phone2Link_VRecID] [varchar](32) NULL,
	[Phone3Link_RecID] [varchar](32) NULL,
	[Phone3Link_Category] [varchar](61) NULL,
	[Phone3Link_VRecID] [varchar](32) NULL,
	[Phone4Link_RecID] [varchar](32) NULL,
	[Phone4Link_Category] [varchar](61) NULL,
	[Phone4Link_VRecID] [varchar](32) NULL,
	[PreferredMethodContact] [varchar](25) NULL,
	[PreferredMethodConta_Valid] [varchar](32) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](61) NULL,
	[PrimaryAddressLink_VRecID] [varchar](32) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](61) NULL,
	[PrimaryEmailLink_VRecID] [varchar](32) NULL,
	[PrimaryWebLink_RecID] [varchar](32) NULL,
	[PrimaryWebLink_Category] [varchar](61) NULL,
	[PrimaryWebLink_VRecID] [varchar](32) NULL,
	[ContactType] [varchar](40) NULL,
	[ContactType_Valid] [varchar](32) NULL,
	[ContactID] [varchar](30) NULL,
	[Source] [varchar](60) NULL,
	[Source_Valid] [varchar](32) NULL,
	[Spouse] [varchar](120) NULL,
	[Title] [varchar](60) NULL,
	[Title_Valid] [varchar](32) NULL,
	[PrimaryEmail] [varchar](100) NULL,
	[BirthdayNextMonth] [bit] NULL,
 CONSTRAINT [RecIdContact] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactSource]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactSource](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Source] [varchar](60) NULL,
 CONSTRAINT [ContactSourceRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ContactType] [varchar](60) NULL,
 CONSTRAINT [ContactTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contract]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contract](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ContractType] [varchar](25) NULL,
	[ContractType_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[CloseDate] [datetime] NULL,
	[CloseDate_Valid] [varchar](32) NULL,
	[Frequency] [varchar](25) NULL,
	[StartDate] [datetime] NULL,
	[NoOfPeriods] [smallint] NULL,
 CONSTRAINT [ContractRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ContractType] [varchar](60) NULL,
	[Description] [text] NULL,
 CONSTRAINT [ContractTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CurrencyCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CurrencyCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Code] [varchar](100) NULL,
	[IssuingCountry] [varchar](100) NULL,
	[IssuingCountry_Valid] [varchar](32) NULL,
	[Name] [varchar](100) NULL,
	[Description] [text] NULL,
	[IsActive] [bit] NULL,
	[SmallestDenomination] [decimal](6, 2) NULL,
	[GroupingSymbol] [varchar](2) NULL,
	[CurrencySymbol] [varchar](3) NULL,
	[CurrencySymbol_Valid] [varchar](32) NULL,
	[DecimalSymbol] [varchar](2) NULL,
	[UseLocaleConvention] [bit] NULL,
	[CurrencySymbolPosition] [varchar](100) NULL,
	[CurrencySymbolPositi_Valid] [varchar](32) NULL,
 CONSTRAINT [CurrencyCodeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CurrencySymbol]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CurrencySymbol](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Symbol] [nvarchar](3) NULL,
	[Name] [varchar](200) NULL,
 CONSTRAINT [CurrSymblRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeniedClaims]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeniedClaims](
	[DeniedClaims_ID] [int] IDENTITY(1,1) NOT NULL,
	[AccumedInvoice] [int] NOT NULL,
	[AccumedEntryDate] [datetime] NOT NULL,
	[Agency_ID] [char](6) NOT NULL,
	[Subscriber_ID] [char](6) NOT NULL,
	[ProcedureCode] [varchar](20) NOT NULL,
	[DateOfService] [datetime] NOT NULL,
	[ReasonCode] [varchar](6) NOT NULL,
	[AgencyReasonCode] [varchar](8) NOT NULL,
	[Processed] [datetime] NULL,
	[Resolved] [datetime] NULL,
	[ResolvedCode] [varchar](6) NULL,
	[CaseRecID] [char](32) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeniedClaimsCodes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeniedClaimsCodes](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[Agency_ID] [varchar](6) NULL,
	[ReasonCode] [varchar](6) NULL,
	[Description] [varchar](200) NULL,
	[Team] [varchar](50) NULL,
 CONSTRAINT [RecIdDeniedClaimsC] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeniedClaimsCodes_org]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeniedClaimsCodes_org](
	[DeniedClaimsCodes_ID] [int] IDENTITY(1,1) NOT NULL,
	[Agency_ID] [char](6) NOT NULL,
	[ReasonCode] [varchar](6) NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [IX_DeniedClaimsCodes_Unique] UNIQUE NONCLUSTERED 
(
	[Agency_ID] ASC,
	[ReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeniedClaimsCodesMap]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeniedClaimsCodesMap](
	[ReasonCode] [varchar](6) NOT NULL,
	[Agency_ID] [char](6) NOT NULL,
	[AgencyReasonCode] [varchar](8) NOT NULL,
 CONSTRAINT [IX_DeniedClaimsCodesMap_Unique] UNIQUE NONCLUSTERED 
(
	[Agency_ID] ASC,
	[AgencyReasonCode] ASC,
	[ReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Discount]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Discount](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[AccountType] [varchar](60) NULL,
	[AccountType_Valid] [varchar](32) NULL,
	[Country] [varchar](50) NULL,
	[Country_Valid] [varchar](32) NULL,
	[DiscountType] [varchar](50) NULL,
	[DiscountType_Valid] [varchar](32) NULL,
	[DiscountValue] [decimal](10, 2) NULL,
	[DiscountValue_Valid] [varchar](32) NULL,
	[Instructions] [text] NULL,
	[OfferingLink_RecID] [varchar](32) NULL,
	[OfferingLink_Category] [varchar](61) NULL,
	[OfferingLink_VRecID] [varchar](32) NULL,
	[PLLink_RecID] [varchar](32) NULL,
	[PLLink_Category] [varchar](61) NULL,
	[PLLink_VRecID] [varchar](32) NULL,
	[Territory] [varchar](25) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[TypeOfCurrency] [varchar](25) NULL,
	[TypeOfCurrency_Valid] [varchar](32) NULL,
	[UseAccountType] [bit] NULL,
	[UseCountry] [bit] NULL,
	[UseTerritory] [bit] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[DiscountValuePct] [decimal](5, 2) NULL,
	[DiscountValuePct_Valid] [varchar](32) NULL,
	[ShowDiscountPct] [bit] NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_DiscountValue__TCV] [decimal](10, 2) NULL,
	[m_DiscountValue__PCV] [decimal](10, 2) NULL,
	[ShowDiscountAmt] [bit] NULL,
 CONSTRAINT [DiscountRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiscountType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiscountType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[DiscountType] [varchar](25) NULL,
	[Instructions] [text] NULL,
 CONSTRAINT [DiscountTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DracoActivities]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DracoActivities](
	[RecId] [char](32) NOT NULL,
	[ActivityName] [nvarchar](50) NULL,
	[ActivityType] [varchar](25) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[Details] [ntext] NULL,
 CONSTRAINT [PK_DRACOACTIVITIES] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DracoActivityFactory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DracoActivityFactory](
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[Details] [ntext] NULL,
	[RecId] [char](32) NOT NULL,
 CONSTRAINT [PK_DracoActFactory] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Email]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Email](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Attachments] [varchar](5000) NULL,
	[BCC] [varchar](1000) NULL,
	[CC] [varchar](1000) NULL,
	[DisplayText] [varchar](255) NULL,
	[EmailBody] [text] NULL,
	[EntryID] [varchar](255) NULL,
	[FlowDirection] [varchar](12) NULL,
	[FlowDirection_Valid] [varchar](32) NULL,
	[FromAddress] [varchar](50) NULL,
	[Layout] [varchar](255) NULL,
	[MessageID] [varchar](255) NULL,
	[OriginalSender] [varchar](50) NULL,
	[PriorityNumber] [tinyint] NULL,
	[ReceivedDateTime] [datetime] NULL,
	[SentDateTime] [datetime] NULL,
	[SourceApplication] [varchar](40) NULL,
	[Subject] [varchar](255) NULL,
	[ToAddress] [varchar](1000) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[ParentLink1_RecID] [varchar](32) NULL,
	[ParentLink1_Category] [varchar](30) NULL,
	[ExternalID] [varchar](25) NULL,
	[Category] [varchar](25) NULL,
	[Category_Valid] [varchar](32) NULL,
	[AttachmentCount] [smallint] NULL,
	[HasAttachment] [bit] NULL,
	[MailListLink_RecID] [varchar](32) NULL,
	[MailListLink_Category] [varchar](61) NULL,
	[MailListLink_VRecID] [varchar](32) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[ContactFullName] [varchar](100) NULL,
	[ContactFullName_Valid] [varchar](32) NULL,
	[ContactRecID] [varchar](32) NULL,
 CONSTRAINT [Email_RecID] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailFormat]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailFormat](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Format] [varchar](25) NULL,
 CONSTRAINT [EmailFormatRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeForecastEmp]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeForecastEmp](
	[ForecastYear] [varchar](7) NULL CONSTRAINT [DF_EmployeeForecastEmp_ForecastYear]  DEFAULT ('<empty>'),
	[ForecastMonth] [varchar](60) NULL CONSTRAINT [DF_EmployeeForecastEmp_ForecastMonth]  DEFAULT ('<empty>'),
	[ForecastQuarter] [varchar](7) NULL CONSTRAINT [DF_EmployeeForecastEmp_ForecastQuarter]  DEFAULT ('<empty>'),
	[EmployeeFullName] [varchar](255) NULL CONSTRAINT [DF_EmployeeForecastEmp_EmployeeFullName]  DEFAULT ('<empty>'),
	[RecId_ProfileEmployee] [varchar](32) NULL CONSTRAINT [DF_EmployeeForecastEmp_RecId_ProfileEmployee]  DEFAULT ('<empty>'),
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy1] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL CONSTRAINT [DF_EmployeeForecastEmp_AggregateWhichRows]  DEFAULT ('All'),
	[m_BestCase__TCV] [decimal](28, 2) NULL,
	[m_Closed__TCV] [decimal](28, 2) NULL,
	[m_Committed__TCV] [decimal](28, 2) NULL,
	[m_Likely__TCV] [decimal](28, 2) NULL,
	[m_Weighted__TCV] [decimal](28, 2) NULL,
	[m_BestCase__PCV] [decimal](10, 2) NULL,
	[m_Closed__PCV] [decimal](10, 2) NULL,
	[m_Committed__PCV] [decimal](10, 2) NULL,
	[m_Weighted__PCV] [decimal](10, 2) NULL,
	[m_Likely__PCV] [decimal](10, 2) NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
	[Territory] [varchar](100) NULL CONSTRAINT [DF_EmployeeForecastEmp_Territory]  DEFAULT ('<empty>'),
	[CURRENCYCODE] [varchar](7) NULL CONSTRAINT [DF_EmployeeForecastEmp_CURRENCYCODE]  DEFAULT ('<empty>'),
 CONSTRAINT [RecIdEmpFcastEmp] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeForecastTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeForecastTerritory](
	[FC_Month] [varchar](25) NULL CONSTRAINT [DF_EmployeeForecastTerritory_FC_Month]  DEFAULT ('<empty>'),
	[FC_Quarter] [varchar](25) NULL CONSTRAINT [DF_EmployeeForecastTerritory_FC_Quarter]  DEFAULT ('<empty>'),
	[FC_Year] [varchar](7) NULL CONSTRAINT [DF_EmployeeForecastTerritory_FC_Year]  DEFAULT ('<empty>'),
	[Territory] [varchar](25) NULL CONSTRAINT [DF_EmployeeForecastTerritory_Territory]  DEFAULT ('<empty>'),
	[RecId_Territory] [varchar](32) NULL CONSTRAINT [DF_EmployeeForecastTerritory_RecId_Territory]  DEFAULT ('<empty>'),
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL CONSTRAINT [DF_EmployeeForecastTerritory_AggregateWhichRows]  DEFAULT ('All'),
	[m_BestCase__TCV] [decimal](28, 2) NULL,
	[m_Weighted__TCV] [decimal](28, 2) NULL,
	[m_Likely__TCV] [decimal](28, 2) NULL,
	[m_BestCase__PCV] [decimal](10, 2) NULL,
	[m_Closed__PCV] [decimal](10, 2) NULL,
	[m_Committed__PCV] [decimal](10, 2) NULL,
	[m_Likely__PCV] [decimal](10, 2) NULL,
	[m_Weighted__PCV] [decimal](10, 2) NULL,
	[m_Closed__TCV] [decimal](28, 2) NULL,
	[m_Committed__TCV] [decimal](28, 2) NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
 CONSTRAINT [EmpFcastTerrRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeOpptyTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeOpptyTerritory](
	[RecurringRevenue] [decimal](11, 2) NULL,
	[Territory] [varchar](25) NULL,
	[RecId_Territory] [varchar](32) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL,
	[m_Revenue__TCV] [decimal](10, 2) NULL,
	[m_ManualForecast__TCV] [decimal](10, 2) NULL,
 CONSTRAINT [EmpOpptyTerrRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeQuotaEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeQuotaEmployee](
	[Q_Year] [varchar](7) NULL,
	[Q_Month] [varchar](60) NULL,
	[Q_Quarter] [varchar](100) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL,
	[EmployeeFullName] [varchar](255) NULL,
	[RecId_ProfileEmployee] [varchar](32) NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
	[m_Amount__PCV] [decimal](10, 2) NULL,
 CONSTRAINT [EmpQuotaEmpRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeQuotaTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeQuotaTerritory](
	[Q_Year] [varchar](7) NULL,
	[Q_Quarter] [varchar](100) NULL,
	[Q_Month] [varchar](60) NULL,
	[Territory] [varchar](25) NULL,
	[RecId_Territory] [varchar](32) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
	[m_Amount__PCV] [decimal](10, 2) NULL,
 CONSTRAINT [RecIdEmpQuotaTerr] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Equipment](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Equipment] [varchar](25) NULL,
	[LocationID] [varchar](25) NULL,
	[Item] [varchar](25) NULL,
	[SerialNumber] [varchar](25) NULL,
	[EquipmentStatus] [varchar](25) NULL,
	[LocationName] [varchar](25) NULL,
	[Discription] [varchar](25) NULL,
	[Frequency] [varchar](25) NULL,
	[Transmitter] [varchar](25) NULL,
	[InstallDate] [datetime] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdEquipment] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExchangeRate]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExchangeRate](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[SourceCurrencyCode] [varchar](100) NULL,
	[SourceCurrencyCode_Valid] [varchar](32) NULL,
	[TargetCurrencyCode] [varchar](100) NULL,
	[TargetCurrencyCode_Valid] [varchar](32) NULL,
	[EffectiveStartDateTime] [datetime] NULL,
	[ExchangeRate] [decimal](28, 24) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[ReciprocalFlag] [varchar](25) NULL,
	[ReciprocalExchangeRa_RecID] [varchar](32) NULL,
	[ReciprocalExchangeRa_Category] [varchar](61) NULL,
	[ReciprocalExchangeRa_VRecID] [varchar](32) NULL,
 CONSTRAINT [ExchangeRateRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Forecast]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Forecast](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[ForecastMonth] [varchar](60) NULL,
	[ForecastMonth_Valid] [varchar](32) NULL,
	[ForecastQuarter] [varchar](25) NULL,
	[ForecastQuarter_Valid] [varchar](32) NULL,
	[ForecastYear] [varchar](4) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](61) NULL,
	[EmployeeLink_VRecID] [varchar](32) NULL,
	[ForecastMonthNo] [tinyint] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_BestCase__TCV] [decimal](28, 2) NULL,
	[m_BestCase__PCV] [decimal](10, 2) NULL,
	[m_Closed__TCV] [decimal](28, 2) NULL,
	[m_Closed__PCV] [decimal](10, 2) NULL,
	[m_Committed__TCV] [decimal](28, 2) NULL,
	[m_Committed__PCV] [decimal](10, 2) NULL,
	[m_Likely__TCV] [decimal](28, 2) NULL,
	[m_Likely__PCV] [decimal](10, 2) NULL,
	[m_Weighted__TCV] [decimal](28, 2) NULL,
	[m_Weighted__PCV] [decimal](10, 2) NULL,
	[EmployeeFullName] [varchar](100) NULL,
	[Territory] [varchar](100) NULL,
 CONSTRAINT [RecIdForecast] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ForecastAdjustment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ForecastAdjustment](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AdjustmentYear] [varchar](4) NULL,
	[AdjustmentQuarter] [varchar](7) NULL,
	[AdjustmentMonth] [varchar](60) NULL,
	[Description] [text] NULL,
	[EffectiveDate] [datetime] NULL,
	[EffectiveDate_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_Adjustment__TCV] [decimal](28, 2) NULL,
	[m_Adjustment__PCV] [decimal](10, 2) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](61) NULL,
	[EmployeeLink_VRecID] [varchar](32) NULL,
	[Territory] [varchar](100) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[FiscalYearLastMonth] [smallint] NULL,
 CONSTRAINT [FcastAdjRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ForecastStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ForecastStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ForecastStatus] [varchar](25) NULL,
 CONSTRAINT [FcastStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionAppDefDeletes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionAppDefDeletes](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionAppDefDeletes_Perspective]  DEFAULT ('(Base)'),
	[TableName] [nvarchar](50) NOT NULL,
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFAppDefDeletes] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionAppDefs]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionAppDefs](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionAppDefs_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFAppDefs] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionApplications]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionApplications](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL,
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFApps] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionAsd]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionAsd](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionAsd_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFAsd] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionAttachments]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionAttachments](
	[RecID] [char](32) NOT NULL,
	[FileBytes] [image] NOT NULL,
 CONSTRAINT [PKAttach] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionBusProcs]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionBusProcs](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionBusProcs_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFBusProcs] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionCounterRanges]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionCounterRanges](
	[RecID] [char](32) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[CounterValue] [nvarchar](20) NOT NULL,
	[CounterValueStart] [nvarchar](20) NOT NULL,
	[CounterValueEnd] [nvarchar](20) NOT NULL,
	[Owner] [nvarchar](200) NOT NULL,
	[FillRatio] [decimal](5, 2) NOT NULL,
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFCounterRanges] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionCounters]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionCounters](
	[RecID] [char](32) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[CounterValue] [nvarchar](20) NOT NULL,
	[LastResetDateTime] [datetime] NULL,
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFCounters] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_FusionCounters_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionDefDeletes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionDefDeletes](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionDefDeletes_Perspective]  DEFAULT ('(Base)'),
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFDefDeletes] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionDefs]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionDefs](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionDefs_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[AssociatedImage] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFDefs] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionDictionaries]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionDictionaries](
	[CultureName] [varchar](15) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL,
	[DictID] [nvarchar](50) NOT NULL,
	[GenericValue] [nvarchar](50) NOT NULL,
	[CultureValue] [nvarchar](50) NOT NULL,
	[RowsOrder] [int] NULL,
 CONSTRAINT [PKFDicts] PRIMARY KEY CLUSTERED 
(
	[CultureName] ASC,
	[Perspective] ASC,
	[DictID] ASC,
	[GenericValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionImages]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionImages](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionImages_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFImages] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionLink]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionLink](
	[RecID] [char](32) NOT NULL,
	[SourceID] [char](32) NOT NULL,
	[SourceLoc] [char](2) NOT NULL,
	[SourceName] [nvarchar](61) NOT NULL,
	[SourceBase] [nvarchar](61) NOT NULL,
	[TargetID] [char](32) NOT NULL,
	[TargetLoc] [char](2) NOT NULL,
	[TargetName] [nvarchar](61) NOT NULL,
	[TargetBase] [nvarchar](61) NOT NULL,
	[RelationshipName] [nvarchar](100) NULL,
 CONSTRAINT [PKFLink] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionQueryGroups]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionQueryGroups](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionQueryGroups_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFQueryGrp] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionQuickActions]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionQuickActions](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionQuickActions_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKQuickActions] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionReports]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionReports](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL CONSTRAINT [DF_FusionReports_Perspective]  DEFAULT ('(Base)'),
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKRepDefs] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fusionreportsBAK]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fusionreportsBAK](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL,
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionSecMap]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionSecMap](
	[RecID] [char](32) NOT NULL,
	[UserType] [varchar](40) NOT NULL,
	[LoginID] [nvarchar](200) NOT NULL,
	[AuthID] [nvarchar](200) NOT NULL,
	[AuthSource] [varchar](20) NOT NULL,
	[BusObID] [char](32) NOT NULL,
	[BusObName] [nvarchar](30) NOT NULL,
	[BusUnitDefID] [char](32) NULL,
	[GroupID] [char](32) NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[TrackPresence] [bit] NULL,
	[Lockout] [bit] NOT NULL,
	[LastLogin] [datetime] NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModUser] [nvarchar](200) NULL,
 CONSTRAINT [PKFSecMap] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionSessionTracker]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionSessionTracker](
	[SessionID] [char](32) NOT NULL,
	[LoginID] [nvarchar](200) NOT NULL,
	[AuthID] [nvarchar](200) NULL,
	[AuthSource] [varchar](20) NULL,
	[UserStatusID] [varchar](32) NOT NULL,
	[LicenseList] [varchar](750) NULL,
	[SecurityGroupID] [char](32) NULL,
	[BusObDefID] [char](32) NULL,
	[BusObDefName] [nvarchar](30) NULL,
	[UserBusObID] [char](32) NULL,
	[BusUnitDefID] [char](32) NULL,
	[UserBusUnit] [nvarchar](100) NULL,
	[LoginDateTime] [datetime] NOT NULL,
	[LastAccessDateTime] [datetime] NOT NULL,
 CONSTRAINT [PKFSessions] PRIMARY KEY CLUSTERED 
(
	[SessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionSettings]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionSettings](
	[RecID] [char](32) NOT NULL,
	[ModuleName] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](32) NOT NULL,
	[Scope] [varchar](32) NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[Details] [ntext] NOT NULL,
 CONSTRAINT [PKFSettings] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionUpdatePackages]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionUpdatePackages](
	[FDRecID] [char](32) NOT NULL,
	[FDObType] [varchar](30) NOT NULL,
	[FDCategory] [nvarchar](30) NULL,
	[FDName] [nvarchar](100) NOT NULL,
	[FDAlias] [nvarchar](150) NULL,
	[FDDesc] [nvarchar](255) NULL,
	[FDFolder] [nvarchar](255) NULL,
	[FDVersion] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[CultureName] [varchar](15) NOT NULL,
	[DefRecID] [char](32) NOT NULL,
	[Scope] [varchar](30) NOT NULL,
	[ScopeOwner] [nvarchar](127) NOT NULL,
	[Perspective] [nvarchar](30) NOT NULL,
	[LinkedTo] [nvarchar](50) NULL,
	[Flags] [nvarchar](255) NULL,
	[FDDetails] [ntext] NOT NULL,
 CONSTRAINT [PKFUpdPkgs] PRIMARY KEY CLUSTERED 
(
	[FDRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionUsers]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionUsers](
	[RecID] [char](32) NOT NULL,
	[UserType] [varchar](40) NOT NULL,
	[LoginID] [nvarchar](200) NOT NULL,
	[Pwd] [nvarchar](50) NOT NULL,
	[Question] [nvarchar](128) NOT NULL,
	[Answer] [nvarchar](128) NOT NULL,
	[PasswordChangedDateTime] [datetime] NULL,
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFUsers] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FusionVersion]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FusionVersion](
	[RecID] [char](32) NOT NULL,
	[ModuleName] [nvarchar](30) NOT NULL,
	[VersionNum] [int] NOT NULL,
	[LastModDateTime] [datetime] NULL,
 CONSTRAINT [PKFVersion] PRIMARY KEY CLUSTERED 
(
	[RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UN_FusionVersion_ModuleName] UNIQUE NONCLUSTERED 
(
	[ModuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Influencer]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Influencer](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](25) NULL,
	[InfluencerType] [varchar](25) NULL,
	[InfluencerType_Valid] [varchar](32) NULL,
	[ResponseMode] [varchar](25) NULL,
	[ResponseMode_Valid] [varchar](32) NULL,
	[Rating] [smallint] NULL,
	[Rating_Valid] [varchar](32) NULL,
	[Description] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](61) NULL,
	[ContactLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [InfluencerRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InfluencerRating]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InfluencerRating](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Rating] [smallint] NULL,
	[Description] [text] NULL,
 CONSTRAINT [InflncrRatngRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InfluencerResponseMode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InfluencerResponseMode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ResponseMode] [varchar](60) NULL,
	[Description] [text] NULL,
 CONSTRAINT [InflncrRspMdRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Influencers]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Influencers](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[InfluencerType] [varchar](60) NULL,
	[InfluencerType_Valid] [varchar](32) NULL,
	[Rating] [varchar](10) NULL,
	[Rating_Valid] [varchar](32) NULL,
	[ResponseMode] [varchar](60) NULL,
	[ResponseMode_Valid] [varchar](32) NULL,
	[Description] [text] NULL,
 CONSTRAINT [InfluencersRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InfluencerType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InfluencerType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[InfluencerType] [varchar](60) NULL,
	[Description] [text] NULL,
 CONSTRAINT [InflncrTypRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Issue]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Issue](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[Issue] [varchar](50) NULL,
	[Notes] [varchar](1000) NULL,
	[Priority] [varchar](15) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[IssueOwner] [varchar](100) NULL,
	[IssueOwner_Valid] [varchar](32) NULL,
	[DueDate] [datetime] NULL,
 CONSTRAINT [IssueRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IssueStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IssueStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](25) NULL,
 CONSTRAINT [IssueStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobClass]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobClass](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Description] [varchar](250) NULL,
	[KeyName] [varchar](150) NULL,
	[KeyValue] [varchar](150) NULL,
 CONSTRAINT [JobClassRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobTitile]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobTitile](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[JobTitle] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [JobTitileRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KSProfile]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KSProfile](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Agency_Id] [varchar](6) NULL,
	[FrequencyofTest] [tinyint] NULL,
 CONSTRAINT [RecIdKSProfile] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lead]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lead](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ContactFullName] [varchar](40) NULL,
	[AccountName] [varchar](40) NULL,
	[ConvertedAccountLink_RecID] [varchar](32) NULL,
	[ConvertedAccountLink_Category] [varchar](30) NULL,
	[ConvertedContactLink_RecID] [varchar](32) NULL,
	[ConvertedContactLink_Category] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[DisplayName] [varchar](40) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](30) NULL,
	[Phone1Link_RecID] [varchar](32) NULL,
	[Phone1Link_Category] [varchar](30) NULL,
	[Phone2Link_RecID] [varchar](32) NULL,
	[Phone2Link_Category] [varchar](30) NULL,
	[Phone3Link_RecID] [varchar](32) NULL,
	[Phone3Link_Category] [varchar](30) NULL,
	[Phone4Link_RecID] [varchar](32) NULL,
	[Phone4Link_Category] [varchar](30) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](30) NULL,
	[PrimaryWebAddressLink_RecID] [varchar](32) NULL,
	[PrimaryWebAddressLink_Category] [varchar](30) NULL,
	[Territory] [varchar](40) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[KeyCode] [varchar](100) NULL,
	[CampaignSource] [varchar](30) NULL,
	[Score] [smallint] NULL,
	[Rating] [varchar](25) NULL,
	[TimeToPurchase] [varchar](40) NULL,
	[TimeToPurchase_Valid] [varchar](32) NULL,
	[ProductInterest] [varchar](40) NULL,
	[ProductInterest_Valid] [varchar](32) NULL,
	[CompanySize] [varchar](40) NULL,
	[CompanySize_Valid] [varchar](32) NULL,
	[LeadCategory] [varchar](40) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[BudgetScore] [tinyint] NULL,
	[TimeToPurchaseScore] [smallint] NULL,
	[ProductInterestScore] [tinyint] NULL,
	[CompanySizeScore] [tinyint] NULL,
	[LeadSource] [varchar](40) NULL,
	[LeadSource_Valid] [varchar](32) NULL,
	[Prefix] [varchar](25) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[Suffix] [varchar](25) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[FirstName] [varchar](40) NULL,
	[MiddleName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Title] [varchar](40) NULL,
	[Title_Valid] [varchar](32) NULL,
	[Category] [varchar](40) NULL,
	[Category_Valid] [varchar](32) NULL,
	[CategoryScore] [tinyint] NULL,
	[TimesRejected] [tinyint] NULL,
	[CompanyName] [varchar](100) NULL,
	[CampaignLink_RecID] [varchar](32) NULL,
	[CampaignLink_Category] [varchar](30) NULL,
	[Owner] [varchar](100) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[ConvertedOpportunity_RecID] [varchar](32) NULL,
	[ConvertedOpportunity_Category] [varchar](30) NULL,
	[OpportunityName] [varchar](40) NULL,
	[OwnerType] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[NAICSCode] [varchar](25) NULL,
	[Industry] [varchar](125) NULL,
	[Industry_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[BudgetPCV] [decimal](10, 2) NULL,
	[BudgetTCV] [decimal](10, 2) NULL,
	[SearchStatus] [varchar](50) NULL,
	[LeadFollowupDateTime] [datetime] NULL,
	[PrimaryEmail] [varchar](100) NULL,
	[TriggerRecalc] [smallint] NULL,
	[PartnerLink_RecID] [varchar](32) NULL,
	[PartnerLink_Category] [varchar](61) NULL,
	[PartnerLink_VRecID] [varchar](32) NULL,
	[Region] [varchar](10) NULL,
	[Region_Valid] [varchar](32) NULL,
	[PartnerEmailAddress] [varchar](255) NULL,
 CONSTRAINT [LeadRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadCategory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Category] [varchar](40) NULL,
	[CategoryScore] [tinyint] NULL,
	[SortField] [tinyint] NULL,
 CONSTRAINT [LeadCategoryRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadCompanySize]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadCompanySize](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[CompanySize] [varchar](41) NULL,
	[CompanySizeScore] [tinyint] NULL,
	[SortField] [tinyint] NULL,
 CONSTRAINT [LeadCoSizeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadRoutingConfiguration]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadRoutingConfiguration](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[RoutingMode] [varchar](25) NULL,
	[RoutingMode_Valid] [varchar](32) NULL,
	[FollowupActivity] [varchar](255) NULL,
	[FollowupActivity_Valid] [varchar](32) NULL,
	[FollowupWaitTime] [int] NULL,
	[IsActive] [bit] NULL,
	[ProcessUnattendedLeads] [bit] NULL,
 CONSTRAINT [LeadRtCfgRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadSource]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadSource](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Source] [varchar](40) NULL,
 CONSTRAINT [LeadSourceRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [LeadStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadTimeToPurchase]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadTimeToPurchase](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[TimeToPurchase] [varchar](40) NULL,
	[Score] [tinyint] NULL,
	[SortField] [smallint] NULL,
 CONSTRAINT [LdTmePurchseRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Licenses]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Licenses](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](30) NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[LicenseNumber] [int] NULL,
	[licenseType] [varchar](25) NULL,
	[LicenseStatus] [varchar](25) NULL,
 CONSTRAINT [RecIdLicenses] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MailingList]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailingList](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](100) NULL,
	[Description] [text] NULL,
 CONSTRAINT [MailingListRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Manufacturer]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Manufacturer](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Manufacturer] [varchar](100) NULL,
 CONSTRAINT [ManufacturerRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketingListMember]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarketingListMember](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Prefix] [varchar](25) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Suffix] [varchar](25) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[AccountName] [varchar](50) NULL,
	[Owner] [varchar](25) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[Phone1] [varchar](25) NULL,
	[Phone2] [varchar](25) NULL,
	[Phone3] [varchar](25) NULL,
	[Phone4] [varchar](25) NULL,
	[Email1] [varchar](25) NULL,
	[Street] [text] NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](25) NULL,
	[Zip] [decimal](10, 0) NULL,
	[Country] [varchar](25) NULL,
	[DoNotContact] [bit] NULL,
	[Industry] [varchar](125) NULL,
	[Industry_Valid] [varchar](32) NULL,
	[NAICSCode] [varchar](25) NULL,
	[NAICSCode_Valid] [varchar](32) NULL,
	[Title] [varchar](40) NULL,
	[Title_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[BudgetPCV] [decimal](26, 0) NULL,
	[BudgetTCV] [decimal](26, 0) NULL,
	[Budget] [decimal](26, 0) NULL,
	[TimeToPurchase] [varchar](40) NULL,
	[TimeToPurchase_Valid] [varchar](32) NULL,
	[NumberOfTimesContacted] [int] NULL,
	[FullName] [varchar](255) NULL,
 CONSTRAINT [MrktLstMemRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketSegment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarketSegment](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[MarketSegment] [varchar](100) NULL,
 CONSTRAINT [MarketSegmentRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[MessageType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Body] [text] NULL,
	[CcAddress] [varchar](100) NULL,
	[FromAddress] [varchar](100) NULL,
	[Subject] [varchar](300) NULL,
	[ToAddress] [varchar](100) NULL,
	[Notes] [varchar](256) NULL,
	[BccAddress] [varchar](100) NULL,
	[OpportunityLink_RecID] [varchar](32) NULL,
	[OpportunityLink_Category] [varchar](61) NULL,
	[OpportunityLink_VRecID] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[NewAnnouncement] [bit] NULL,
 CONSTRAINT [MessageRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MessageType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MessageType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[MessageType] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [MessageTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Methodology]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Methodology](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Methodology] [varchar](40) NULL,
	[DisableChangeProbability] [bit] NULL,
 CONSTRAINT [MethodologyRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Month_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Month_Valid](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[MonthText] [varchar](25) NULL,
	[MonthNo] [tinyint] NULL,
	[MonthNo_Valid] [varchar](32) NULL,
 CONSTRAINT [Month_ValidRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NAICSCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NAICSCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Code] [varchar](25) NULL,
	[IndustryName] [varchar](100) NULL,
	[Description] [varchar](300) NULL,
 CONSTRAINT [NAICSCodeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notes](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[NoteType] [varchar](40) NULL,
	[NoteType_Valid] [varchar](32) NULL,
	[Description] [text] NULL,
 CONSTRAINT [NotesRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoteTypes]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteTypes](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[NoteType] [varchar](60) NULL,
 CONSTRAINT [NoteTypesRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notification](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Body] [text] NULL,
	[EmailAddressList] [varchar](5000) NULL,
	[ErrorDesc] [varchar](255) NULL,
	[FromLink_RecID] [varchar](32) NULL,
	[FromLink_Category] [varchar](30) NULL,
	[Priority] [varchar](10) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[Status] [varchar](10) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Subject] [varchar](100) NULL,
	[Transport] [varchar](15) NULL,
	[EmailAddressCC] [varchar](1024) NULL,
	[EmailAddressBCC] [varchar](1024) NULL,
	[FromEmployeeRecID] [varchar](32) NULL,
	[BodyFormat] [varchar](5) NULL,
	[BodyFormat_Valid] [varchar](32) NULL,
	[FromAddress] [varchar](128) NULL,
 CONSTRAINT [NotificationRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NotificationStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NotificationStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](10) NULL,
 CONSTRAINT [NotfctnStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Offering]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Offering](
	[OfferingType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ProductLine] [varchar](25) NULL,
	[ProductName] [varchar](50) NULL,
	[ProductType] [varchar](25) NULL,
	[ProductType_Valid] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[Cost] [decimal](8, 2) NULL,
	[Orderable] [bit] NULL,
	[Taxable] [bit] NULL,
	[Bundle] [bit] NULL,
	[Manufacturer] [varchar](100) NULL,
	[ManufacturerPartNo] [varchar](25) NULL,
	[ProductVendorName] [varchar](25) NULL,
	[Description] [text] NULL,
	[ProductLineLink_RecID] [varchar](32) NULL,
	[ProductLineLink_Category] [varchar](61) NULL,
	[ProductLineLink_VRecID] [varchar](32) NULL,
	[ManufacturerLink_RecID] [varchar](32) NULL,
	[ManufacturerLink_Category] [varchar](61) NULL,
	[ManufacturerLink_VRecID] [varchar](32) NULL,
	[VendorLink_RecID] [varchar](32) NULL,
	[VendorLink_Category] [varchar](61) NULL,
	[VendorLink_VRecID] [varchar](32) NULL,
	[ProductVersionNo] [varchar](25) NULL,
	[ProductSku] [varchar](50) NULL,
	[TypeOfCurrency] [varchar](25) NULL,
	[TypeOfCurrency_Valid] [varchar](32) NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[ProductCode] [varchar](50) NULL,
	[Category] [varchar](50) NULL,
	[MSRP] [decimal](8, 2) NULL,
	[PricingValue] [decimal](10, 2) NULL,
	[InternalDescription] [varchar](1000) NULL,
	[BriefDescription] [varchar](1000) NULL,
	[GrossMargin] [decimal](10, 2) NULL,
	[CategoryLink_RecID] [varchar](32) NULL,
	[CategoryLink_Category] [varchar](61) NULL,
	[CategoryLink_VRecID] [varchar](32) NULL,
	[ShowPricingValue] [bit] NULL,
	[PricingText] [varchar](25) NULL,
	[ValidateVolPricing] [bit] NULL,
	[UpdateTerritoryPricing] [bit] NULL,
	[PLLink_RecID] [varchar](32) NULL,
	[PLLink_Category] [varchar](61) NULL,
	[PLLink_VRecID] [varchar](32) NULL,
	[ShowListPrice] [bit] NULL,
	[ShowServiceDuration] [bit] NULL,
	[ServiceDuration] [varchar](25) NULL,
	[ServiceDuration_Valid] [varchar](32) NULL,
	[LeadLink_RecID] [varchar](32) NULL,
	[LeadLink_Category] [varchar](61) NULL,
	[LeadLink_VRecID] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[CaseLink_RecID] [varchar](32) NULL,
	[CaseLink_Category] [varchar](61) NULL,
	[CaseLink_VRecID] [varchar](32) NULL,
	[VLink_RecID] [varchar](32) NULL,
	[VLink_Category] [varchar](61) NULL,
	[VLink_VRecID] [varchar](32) NULL,
	[PricingValuePct] [decimal](5, 2) NULL,
	[PricingValuePct_Valid] [varchar](32) NULL,
	[ShowPricingAmt] [bit] NULL,
	[ShowPricingPct] [bit] NULL,
	[Recurring] [bit] NULL,
	[Frequency] [varchar](25) NULL,
	[Frequency_Valid] [varchar](32) NULL,
	[ShowFixedPrice] [bit] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_Cost__TCV] [decimal](10, 2) NULL,
	[m_Cost__PCV] [decimal](10, 2) NULL,
	[m_ListPrice__TCV] [decimal](10, 2) NULL,
	[m_ListPrice__PCV] [decimal](10, 2) NULL,
	[m_MSRP__TCV] [decimal](10, 2) NULL,
	[m_MSRP__PCV] [decimal](10, 2) NULL,
	[m_PricingValue__TCV] [decimal](10, 2) NULL,
	[m_PricingValue__PCV] [decimal](10, 2) NULL,
	[m_FixedPrice__TCV] [decimal](10, 2) NULL,
	[m_FixedPrice__PCV] [decimal](10, 2) NULL,
	[PricingLink_RecID] [varchar](32) NULL,
	[PricingLink_Category] [varchar](61) NULL,
	[PricingLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [OfferingRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OldDeniedClaimBKUP]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OldDeniedClaimBKUP](
	[DeniedClaims_ID] [int] IDENTITY(1,1) NOT NULL,
	[AccumedInvoice] [int] NOT NULL,
	[AccumedEntryDate] [datetime] NOT NULL,
	[Agency_ID] [char](6) NOT NULL,
	[Subscriber_ID] [char](6) NOT NULL,
	[ProcedureCode] [varchar](20) NOT NULL,
	[DateOfService] [datetime] NOT NULL,
	[ReasonCode] [varchar](6) NOT NULL,
	[AgencyReasonCode] [varchar](8) NOT NULL,
	[Processed] [datetime] NULL,
	[Resolved] [datetime] NULL,
	[ResolvedCode] [varchar](6) NULL,
	[CaseRecID] [char](32) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Opportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opportunity](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[ClosedDateTime] [datetime] NULL,
	[ClosedDateTime_Valid] [varchar](32) NULL,
	[Probability] [smallint] NULL,
	[Probability_Valid] [varchar](32) NULL,
	[Stage] [varchar](50) NULL,
	[Stage_Valid] [varchar](32) NULL,
	[Name] [varchar](100) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](30) NULL,
	[AccountLink_RecID] [varchar](32) NULL,
	[AccountLink_Category] [varchar](30) NULL,
	[Partner] [varchar](40) NULL,
	[Description] [varchar](200) NULL,
	[Details] [varchar](2000) NULL,
	[Source] [varchar](25) NULL,
	[Source_Valid] [varchar](32) NULL,
	[SourceType] [varchar](25) NULL,
	[CustomerReference] [varchar](25) NULL,
	[Territory] [varchar](100) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[Priority] [varchar](25) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[TargetCloseDate] [datetime] NULL,
	[TargetCloseDate_Valid] [varchar](32) NULL,
	[AccountName] [varchar](100) NULL,
	[ContactName] [varchar](100) NULL,
	[Methodology] [varchar](40) NULL,
	[Methodology_Valid] [varchar](32) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](100) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[Revenue] [decimal](11, 2) NULL,
	[Difference] [decimal](8, 2) NULL,
	[LeadLink_RecID] [varchar](32) NULL,
	[LeadLink_Category] [varchar](30) NULL,
	[LeadName] [varchar](40) NULL,
	[ContactPhone] [varchar](14) NULL,
	[ContactEmail] [varchar](255) NULL,
	[ContactEmail_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](40) NULL,
	[CampaignLink_RecID] [varchar](32) NULL,
	[CampaignLink_Category] [varchar](30) NULL,
	[EstimatedBudget] [decimal](11, 2) NULL,
	[Campaign] [varchar](100) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](32) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](30) NULL,
	[Code] [varchar](25) NULL,
	[Tax] [decimal](4, 2) NULL,
	[DaysElapsed] [int] NULL,
	[QuoteNo_LnkKey] [varchar](16) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[TerritoryRecId] [varchar](32) NULL,
	[StageRecId] [varchar](32) NULL,
	[ErrorMsg_Status] [varchar](200) NULL,
	[ErrorMsg_Stage] [varchar](200) NULL,
	[OptLineItemMsg] [varchar](255) NULL,
	[OpptyOwnerTeam] [varchar](30) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_EstimatedBudget__TCV] [decimal](10, 2) NULL,
	[m_EstimatedBudget__PCV] [decimal](10, 2) NULL,
	[m_Revenue__TCV] [decimal](10, 2) NULL,
	[m_Revenue__PCV] [decimal](10, 2) NULL,
	[m_ManualForecast__TCV] [decimal](10, 2) NULL,
	[m_ManualForecast__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueMonthly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueMonthly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__PCV] [decimal](10, 2) NULL,
	[StageLink_RecID] [varchar](32) NULL,
	[StageLink_Category] [varchar](61) NULL,
	[StageLink_VRecID] [varchar](32) NULL,
	[StatusLink_RecID] [varchar](32) NULL,
	[StatusLink_Category] [varchar](61) NULL,
	[StatusLink_VRecID] [varchar](32) NULL,
	[CUForecastFlag] [varchar](32) NULL,
	[ForecastMonth] [varchar](100) NULL,
	[ForecastQuarter] [varchar](7) NULL,
	[ForecastYear] [varchar](7) NULL,
	[FiscalYearLastMonth] [smallint] NULL,
	[DisableChangeProbability] [bit] NULL,
	[CompanyName] [varchar](100) NULL,
	[LastTerritory] [varchar](100) NULL,
	[LastOwner] [varchar](100) NULL,
	[OwnerRecId] [varchar](32) NULL,
	[LastYear] [varchar](7) NULL,
	[LastCurrency] [varchar](25) NULL,
	[PrevForecastRecId] [varchar](32) NULL,
	[PrevForecastLink_RecID] [varchar](32) NULL,
	[PrevForecastLink_Category] [varchar](61) NULL,
	[PrevForecastLink_VRecID] [varchar](32) NULL,
	[PrimaryEmail] [varchar](255) NULL,
	[PrevOpptyValuePCV] [decimal](28, 2) NULL,
	[PrevOpptyValueTCV] [decimal](28, 2) NULL,
	[PrevProbability] [smallint] NULL,
	[EmployeeFullName] [varchar](255) NULL,
	[PipelineDate] [varchar](25) NULL,
	[MethodologyLink_RecID] [varchar](32) NULL,
	[MethodologyLink_Category] [varchar](61) NULL,
	[MethodologyLink_VRecID] [varchar](32) NULL,
	[IsCurrencyCodeValid] [bit] NULL,
 CONSTRAINT [OpportunityRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityAlertConfiguration]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityAlertConfiguration](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[IsActive] [bit] NULL,
	[Methodology] [varchar](40) NULL,
	[Methodology_Valid] [varchar](32) NULL,
	[FirstStage] [varchar](50) NULL,
	[FirstStage_Valid] [varchar](32) NULL,
	[RevenueThreshold] [decimal](28, 2) NULL,
	[DaysToCloseThreshold] [tinyint] NULL,
	[LastStage] [varchar](50) NULL,
	[LastStage_Valid] [varchar](32) NULL,
 CONSTRAINT [OpptyAlertCfgRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Code] [varchar](60) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [OpptyCodeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityLeadRating]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityLeadRating](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[LeadRating] [varchar](60) NULL,
 CONSTRAINT [OpptyLdRatngRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityProductType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityProductType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OpportunityProductType] [varchar](50) NULL,
	[SortOrder] [tinyint] NULL,
 CONSTRAINT [OpptyPrdctTypRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunitySource]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunitySource](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Source] [varchar](60) NULL,
 CONSTRAINT [OpptySrcRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
	[SortField] [smallint] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [OpptyStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityTemplate](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AccountName] [varchar](100) NULL,
	[AssignedTeam] [varchar](25) NULL,
	[Campaign] [varchar](100) NULL,
	[AccountLink_RecID] [varchar](32) NULL,
	[AccountLink_Category] [varchar](61) NULL,
	[AccountLink_VRecID] [varchar](32) NULL,
	[CampaignLink_RecID] [varchar](32) NULL,
	[CampaignLink_Category] [varchar](61) NULL,
	[CampaignLink_VRecID] [varchar](32) NULL,
	[Certainty] [smallint] NULL,
	[ClosedDate] [datetime] NULL,
	[Code] [varchar](25) NULL,
	[ContactEmail] [varchar](25) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](61) NULL,
	[ContactLink_VRecID] [varchar](32) NULL,
	[ContactName] [varchar](100) NULL,
	[ContactPhone] [varchar](14) NULL,
	[CustomerReference] [varchar](25) NULL,
	[Description] [varchar](200) NULL,
	[Details] [varchar](2000) NULL,
	[Difference] [decimal](8, 2) NULL,
	[DisplayName] [varchar](40) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](61) NULL,
	[EmployeeLink_VRecID] [varchar](32) NULL,
	[EstimatedBudget] [decimal](11, 2) NULL,
	[LeadLink_RecID] [varchar](32) NULL,
	[LeadLink_Category] [varchar](61) NULL,
	[LeadLink_VRecID] [varchar](32) NULL,
	[LeadName] [varchar](40) NULL,
	[Methodology] [varchar](40) NULL,
	[Methodology_Valid] [varchar](32) NULL,
	[Name] [varchar](100) NULL,
	[OwnerType] [varchar](30) NULL,
	[Pain] [varchar](25) NULL,
	[Partner] [varchar](40) NULL,
	[Priority] [varchar](25) NULL,
	[Priority_Valid] [varchar](32) NULL,
	[Probability] [smallint] NULL,
	[Revenue] [decimal](11, 2) NULL,
	[Source] [varchar](25) NULL,
	[Source_Valid] [varchar](32) NULL,
	[SourceType] [varchar](25) NULL,
	[Stage] [varchar](50) NULL,
	[Stage_Valid] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[TargetCloseDate] [datetime] NULL,
	[TargetCloseDate_Valid] [varchar](32) NULL,
	[Tax] [decimal](4, 2) NULL,
	[Territory] [varchar](25) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[DaysElapsed] [int] NULL,
	[Owner] [varchar](100) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[mEstimatedBudgetPCV] [decimal](10, 2) NULL,
	[mEstimatedBudgetTCV] [decimal](10, 2) NULL,
	[mManualForecastPCV] [decimal](10, 2) NULL,
	[mManualForecastTCV] [decimal](10, 2) NULL,
	[mRecurrRevenueMonthlyPCV] [decimal](10, 2) NULL,
	[mRecurrRevenueMonthlyTCV] [decimal](10, 2) NULL,
	[mRecurrRevenueQuarterlyPCV] [decimal](10, 2) NULL,
	[mRecurrRevenueQuarterlyTCV] [decimal](10, 2) NULL,
	[mRecurrRevenueYearlyPCV] [decimal](10, 2) NULL,
	[mRecurrRevenueYearlyTCV] [decimal](10, 2) NULL,
	[mRevenuePCV] [decimal](10, 2) NULL,
	[mRevenueTCV] [decimal](10, 2) NULL,
	[StageLink_RecID] [varchar](32) NULL,
	[StageLink_Category] [varchar](61) NULL,
	[StageLink_VRecID] [varchar](32) NULL,
	[StatusLink_RecID] [varchar](32) NULL,
	[StatusLink_Category] [varchar](61) NULL,
	[StatusLink_VRecID] [varchar](32) NULL,
	[CompanyName] [varchar](100) NULL,
 CONSTRAINT [OpptyTemplateRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpptyProduct]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpptyProduct](
	[OpptyProductType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OpportunityLink_RecID] [varchar](32) NULL,
	[OpportunityLink_Category] [varchar](61) NULL,
	[OpportunityLink_VRecID] [varchar](32) NULL,
	[BriefDescription] [varchar](1000) NULL,
	[LineItemName] [varchar](50) NULL,
	[QuoteNo_LnkKey] [varchar](16) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[IsOptional] [bit] NULL,
	[IsDiscountPercentage] [bit] NULL,
	[IsServiceType] [bit] NULL,
	[Quantity] [int] NULL,
	[Quantity_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[AccountLink_RecID] [varchar](32) NULL,
	[AccountLink_Category] [varchar](61) NULL,
	[AccountLink_VRecID] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[ExtendedPrice__TCV] [decimal](10, 2) NULL,
	[ExtendedPrice__PCV] [decimal](10, 2) NULL,
	[QuotedTotal__TCV] [decimal](10, 2) NULL,
	[QuotedTotal__PCV] [decimal](10, 2) NULL,
	[DiscountPerUnit__TCV] [decimal](10, 2) NULL,
	[DiscountPerUnit__PCV] [decimal](10, 2) NULL,
	[ListPrice__TCV] [decimal](10, 2) NULL,
	[ListPrice__PCV] [decimal](10, 2) NULL,
	[QuotedUnitPrice__TCV] [decimal](10, 2) NULL,
	[QuotedUnitPrice__PCV] [decimal](10, 2) NULL,
	[TaxAmountActual__TCV] [decimal](10, 2) NULL,
	[TaxAmountActual__PCV] [decimal](10, 2) NULL,
	[AccountOrderLink_RecID] [varchar](32) NULL,
	[AccountOrderLink_Category] [varchar](61) NULL,
	[AccountOrderLink_VRecID] [varchar](32) NULL,
	[DiscountPerUnitPercentage] [decimal](4, 2) NULL,
	[Taxable] [bit] NULL,
	[TaxRate] [decimal](4, 2) NULL,
	[OfferingLink_RecID] [varchar](32) NULL,
	[OfferingLink_Category] [varchar](61) NULL,
	[OfferingLink_VRecID] [varchar](32) NULL,
	[Recurring] [bit] NULL,
	[Manufacturer] [varchar](100) NULL,
	[ProductCode] [varchar](50) NULL,
	[ProductLine] [varchar](25) NULL,
	[ProductSKU] [varchar](50) NULL,
	[Frequency] [varchar](16) NULL,
	[Frequency_Valid] [varchar](32) NULL,
	[Category2] [varchar](50) NULL,
	[Category2_Valid] [varchar](32) NULL,
	[Status2] [varchar](25) NULL,
	[Status2_Valid] [varchar](32) NULL,
	[ProductType2] [varchar](25) NULL,
	[ProductType2_Valid] [varchar](32) NULL,
	[DiscountType] [varchar](50) NULL,
	[TaxAmount__TCV] [decimal](10, 2) NULL,
	[TaxAmount__PCV] [decimal](10, 2) NULL,
	[TaxManual__TCV] [decimal](10, 2) NULL,
	[TaxManual__PCV] [decimal](10, 2) NULL,
	[QuoteDiscountPercent] [decimal](4, 2) NULL,
	[QuoteDiscount__TCV] [decimal](10, 2) NULL,
	[QuoteDiscount__PCV] [decimal](10, 2) NULL,
	[AccountName_Valid] [varchar](32) NULL,
	[BriefDescription_Valid] [varchar](32) NULL,
	[Category] [varchar](50) NULL,
	[Category_Valid] [varchar](32) NULL,
	[DiscountAccountCountry] [varchar](50) NULL,
	[DiscountAccountTerritory] [varchar](25) NULL,
	[DiscountAccountType] [varchar](25) NULL,
	[DiscountAccountType_Valid] [varchar](32) NULL,
	[IsDiscountPercentage_Valid] [varchar](32) NULL,
	[IsIncrementalVBP] [bit] NULL,
	[IsIncrementalVBP_Valid] [varchar](32) NULL,
	[IsOptional_Valid] [varchar](32) NULL,
	[IsServiceType_Valid] [varchar](32) NULL,
	[IsVolumeBasedPricing] [bit] NULL,
	[IsVolumeBasedPricing_Valid] [varchar](32) NULL,
	[OpportunityName_Valid] [varchar](32) NULL,
	[PricingMethod] [varchar](50) NULL,
	[ProductLine_Valid] [varchar](32) NULL,
	[ProductLineRecId] [varchar](32) NULL,
	[ProductLineRecId_Valid] [varchar](32) NULL,
	[ProductType] [varchar](25) NULL,
	[ProductType_Valid] [varchar](32) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Cost__TCV] [decimal](10, 2) NULL,
	[CostTCV_Valid] [varchar](32) NULL,
	[Cost__PCV] [decimal](10, 2) NULL,
	[ExtendedPriceTCV_Valid] [varchar](32) NULL,
	[DiscountPerUnitTCV_Valid] [varchar](32) NULL,
	[ListPriceTCV_Valid] [varchar](32) NULL,
	[MSRP__TCV] [decimal](10, 2) NULL,
	[MSRPTCV_Valid] [varchar](32) NULL,
	[MSRP__PCV] [decimal](10, 2) NULL,
	[QuotedTotalTCV_Valid] [varchar](32) NULL,
	[QuotedUnitPriceTCV_Valid] [varchar](32) NULL,
	[TaxAmountTCV_Valid] [varchar](32) NULL,
	[TaxAmountActualTCV_Valid] [varchar](32) NULL,
	[TaxManualTCV_Valid] [varchar](32) NULL,
	[IsValidCurrencyCode] [bit] NULL,
	[TextMessage_Valid] [varchar](32) NULL,
 CONSTRAINT [RecIdOpptyProduct] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ownership]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ownership](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Ownership] [varchar](25) NULL,
 CONSTRAINT [OwnershipRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PainPoint]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PainPoint](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[PainPoint] [varchar](40) NULL,
	[PainPoint_Valid] [varchar](32) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PainPointRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PainPoint_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PainPoint_Valid](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[PainPoint] [varchar](100) NULL,
 CONSTRAINT [PainPtValidRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Partner]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Partner](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AccountName] [varchar](40) NULL,
	[Birthdate] [datetime] NULL,
	[ContactFullName] [varchar](60) NULL,
	[Department] [varchar](50) NULL,
	[Department_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](255) NULL,
	[FirstName] [varchar](32) NULL,
	[LastName] [varchar](32) NULL,
	[MiddleName] [varchar](32) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[PartnerName] [varchar](60) NULL,
	[PartnerStatus] [varchar](32) NULL,
	[PartnerStatus_Valid] [varchar](32) NULL,
	[Phone1Link] [varchar](32) NULL,
	[Phone1Link_RecID] [varchar](32) NULL,
	[Phone1Link_Category] [varchar](61) NULL,
	[Phone1Link_VRecID] [varchar](32) NULL,
	[Phone2Link] [varchar](32) NULL,
	[Phone2Link_RecID] [varchar](32) NULL,
	[Phone2Link_Category] [varchar](61) NULL,
	[Phone2Link_VRecID] [varchar](32) NULL,
	[Phone3Link] [varchar](32) NULL,
	[Phone3Link_RecID] [varchar](32) NULL,
	[Phone3Link_Category] [varchar](61) NULL,
	[Phone3Link_VRecID] [varchar](32) NULL,
	[Phone4Link] [varchar](32) NULL,
	[Phone4Link_RecID] [varchar](32) NULL,
	[Phone4Link_Category] [varchar](61) NULL,
	[Phone4Link_VRecID] [varchar](32) NULL,
	[Prefix] [varchar](30) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[PrimaryAddressLink] [varchar](32) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](61) NULL,
	[PrimaryAddressLink_VRecID] [varchar](32) NULL,
	[PrimaryEmailLink] [varchar](32) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](61) NULL,
	[PrimaryEmailLink_VRecID] [varchar](32) NULL,
	[PrimaryWebLink] [varchar](32) NULL,
	[PrimaryWebLink_RecID] [varchar](32) NULL,
	[PrimaryWebLink_Category] [varchar](61) NULL,
	[PrimaryWebLink_VRecID] [varchar](32) NULL,
	[ProfileID] [varchar](30) NULL,
	[ProfileType] [varchar](32) NULL,
	[Suffix] [varchar](30) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[Territory] [varchar](32) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[Title] [varchar](30) NULL,
	[Title_Valid] [varchar](32) NULL,
	[PrimaryEmail] [varchar](255) NULL,
	[Region] [varchar](10) NULL,
	[Region_Valid] [varchar](32) NULL,
 CONSTRAINT [RecIdPartner] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartnerStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartnerStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
	[Description] [text] NULL,
 CONSTRAINT [PartnerStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permissions](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[PermissionStatus] [varchar](60) NULL,
	[PermissionStatus_Valid] [varchar](32) NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PermissionsRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PermissionStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PermissionStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[PermissionStatus] [varchar](60) NULL,
 CONSTRAINT [PermsnStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pricing]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pricing](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[PricingType] [varchar](50) NULL,
	[PricingType_Valid] [varchar](32) NULL,
	[PricingValue] [decimal](10, 2) NULL,
	[PricingValuePct] [decimal](5, 2) NULL,
	[PricingValuePct_Valid] [varchar](32) NULL,
	[PricingText] [varchar](25) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[mPricingValuePCV] [decimal](10, 2) NULL,
	[mPricingValueTCV] [decimal](10, 2) NULL,
	[ShowPricingAmt] [bit] NULL,
	[ShowPricingPct] [bit] NULL,
	[ShowPricingValue] [bit] NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
 CONSTRAINT [Pricingndx] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PricingInstruction]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PricingInstruction](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Pricing] [varchar](25) NULL,
	[Instruction] [text] NULL,
 CONSTRAINT [PrcngInstrctRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PricingType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PricingType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[PricingType] [varchar](50) NULL,
 CONSTRAINT [PricingTypendx] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Priorities]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Priorities](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Priority] [varchar](25) NULL,
	[Description] [text] NULL,
 CONSTRAINT [PrioritiesRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Category] [varchar](60) NULL,
	[Description] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[ParentCategory] [varchar](60) NULL,
	[ParentCategoryLink_RecID] [varchar](32) NULL,
	[ParentCategoryLink_Category] [varchar](61) NULL,
	[ParentCategoryLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [PrdtCatRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductLine]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductLine](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[Description] [text] NULL,
	[ParentLineLink_RecID] [varchar](32) NULL,
	[ParentLineLink_Category] [varchar](30) NULL,
	[ProductLine] [varchar](60) NULL,
	[ProductLineManager] [varchar](40) NULL,
	[ProductLineScore] [tinyint] NULL,
	[ManagerLink_RecID] [varchar](32) NULL,
	[ManagerLink_Category] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[ParentLine] [varchar](60) NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[PricingValue] [decimal](10, 2) NULL,
	[PricingText] [varchar](25) NULL,
	[ShowPricingValue] [bit] NULL,
	[QuoteApprovalAuthority] [varchar](100) NULL,
	[QuoteApprovalAuthori_RecID] [varchar](32) NULL,
	[QuoteApprovalAuthori_Category] [varchar](61) NULL,
	[QuoteApprovalAuthori_VRecID] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[mPricingValuePCV] [decimal](10, 2) NULL,
	[mPricingValueTCV] [decimal](10, 2) NULL,
	[PricingValuePct] [decimal](5, 2) NULL,
	[PricingValuePct_Valid] [varchar](32) NULL,
	[ShowPricingAmt] [bit] NULL,
	[ShowPricingPct] [bit] NULL,
	[PricingLink_RecID] [varchar](32) NULL,
	[PricingLink_Category] [varchar](61) NULL,
	[PricingLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [ProductLineRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [ProductStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ProductType] [varchar](60) NULL,
 CONSTRAINT [ProductTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Profile]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Profile](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ProfileType] [varchar](32) NULL,
	[DisplayName] [varchar](255) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[ProfileID] [varchar](30) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](30) NULL,
	[Phone1Link_RecID] [varchar](32) NULL,
	[Phone1Link_Category] [varchar](30) NULL,
	[Phone2Link_RecID] [varchar](32) NULL,
	[Phone2Link_Category] [varchar](30) NULL,
	[Phone3Link_RecID] [varchar](32) NULL,
	[Phone3Link_Category] [varchar](30) NULL,
	[Phone4Link_RecID] [varchar](32) NULL,
	[Phone4Link_Category] [varchar](30) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](30) NULL,
	[PrimaryWebLink_RecID] [varchar](32) NULL,
	[PrimaryWebLink_Category] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[FirstName] [varchar](25) NULL,
	[LastName] [varchar](25) NULL,
	[MiddleName] [varchar](25) NULL,
	[NetworkUserName] [varchar](25) NULL,
	[HiredDate] [datetime] NULL,
	[TerminatedDate] [datetime] NULL,
	[Status] [varchar](60) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Department] [varchar](50) NULL,
	[Department_Valid] [varchar](32) NULL,
	[DepartmentCode] [varchar](5) NULL,
	[Title] [varchar](30) NULL,
	[Title_Valid] [varchar](32) NULL,
	[Birthdate] [datetime] NULL,
	[SocialSecurityNumber] [varchar](11) NULL,
	[YearsInSchool] [tinyint] NULL,
	[Degree] [varchar](25) NULL,
	[Degree_Valid] [varchar](32) NULL,
	[Prefix] [varchar](30) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[Suffix] [varchar](30) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[Supervisor] [varchar](100) NULL,
	[OLALink_RecID] [varchar](32) NULL,
	[OLALink_Category] [varchar](30) NULL,
	[NotificationLink_RecID] [varchar](32) NULL,
	[NotificationLink_Category] [varchar](30) NULL,
	[FRS] [varchar](1) NULL,
	[LoginID] [varchar](25) NULL,
	[SupervisorLink_RecID] [varchar](32) NULL,
	[SupervisorLink_Category] [varchar](30) NULL,
	[DirectReportCount] [smallint] NULL,
	[IVRPinCode] [decimal](10, 0) NULL,
	[DefaultCurrency] [varchar](100) NULL,
	[DefaultCurrency_Valid] [varchar](32) NULL,
	[Hierarchy_Left] [int] NULL,
	[Hierarchy_Right] [int] NULL,
	[Territory] [varchar](100) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[EmpSupervisorLink_RecID] [varchar](32) NULL,
	[EmpSupervisorLink_Category] [varchar](61) NULL,
	[EmpSupervisorLink_VRecID] [varchar](32) NULL,
	[PrimaryEmail] [varchar](100) NULL,
	[PrimaryEmail_Valid] [varchar](32) NULL,
	[EmployeeFullName] [varchar](255) NULL,
	[Team] [varchar](100) NULL,
	[Team_Valid] [varchar](32) NULL,
	[KshemaUserID] [varchar](25) NULL,
 CONSTRAINT [UN_Profile_RecId] UNIQUE NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileDegree]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileDegree](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Degree] [varchar](60) NULL,
 CONSTRAINT [ProfileDegreeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileDepartment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileDepartment](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Department] [varchar](60) NULL,
	[Description] [varchar](255) NULL,
	[DepartmentCode] [varchar](5) NULL,
 CONSTRAINT [ProfileDeptRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfilePrefix]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfilePrefix](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Prefix] [varchar](10) NULL,
 CONSTRAINT [ProfilePrefixRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
 CONSTRAINT [ProfileStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProfileSuffix]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileSuffix](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Suffix] [varchar](10) NULL,
 CONSTRAINT [ProfileSuffixRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Program]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Program](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](50) NULL,
	[Description] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdProgram] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quarter_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarter_Valid](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Quarter] [varchar](25) NULL,
	[SortField] [tinyint] NULL,
 CONSTRAINT [Quarter_ValidRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QueuedUpdate]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QueuedUpdate](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[QueuedUpdateID] [int] NULL,
	[EffectiveDate] [datetime] NULL,
	[IncDec] [varchar](25) NULL,
	[IncDec_Valid] [varchar](32) NULL,
	[UpdateType] [varchar](25) NULL,
	[UpdateType_Valid] [varchar](32) NULL,
	[UpdateValue] [smallint] NULL,
	[Orderable] [bit] NULL,
	[ApplyQueuedUpdate] [bit] NULL,
	[QueuedUpdateStatus] [varchar](25) NULL,
	[QueuedUpdateStatus_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[LinkQueuedUpdate] [bit] NULL,
	[UpdateValuePct] [smallint] NULL,
	[UpdateValuePct_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[mUpdateValuePCV] [decimal](14, 2) NULL,
	[mUpdateValueTCV] [decimal](14, 2) NULL,
	[ShowUpdateValue] [bit] NULL,
	[ShowUpdateValuePct] [bit] NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[PricingValuePct] [smallint] NULL,
	[mPricingValuePCV] [int] NULL,
	[mPricingValueTCV] [int] NULL,
	[ShowPricingPct] [bit] NULL,
	[ShowPricingAmt] [bit] NULL,
	[PricingText] [varchar](25) NULL,
	[ShowPricingValue] [bit] NULL,
 CONSTRAINT [QueuedUpdateRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quota]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quota](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[QuotaMonth] [varchar](25) NULL,
	[QuotaMonth_Valid] [varchar](32) NULL,
	[QuotaQuarter] [varchar](2) NULL,
	[QuotaQuarter_Valid] [varchar](32) NULL,
	[QuotaYear] [varchar](4) NULL,
	[QuotaYear_Valid] [varchar](32) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](61) NULL,
	[EmployeeLink_VRecID] [varchar](32) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_Adjustment__TCV] [decimal](10, 2) NULL,
	[m_Adjustment__PCV] [decimal](10, 2) NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
	[m_Amount__PCV] [decimal](10, 2) NULL,
	[m_InitialAmount__TCV] [decimal](10, 2) NULL,
	[m_InitialAmount__PCV] [decimal](10, 2) NULL,
	[Recurring] [bit] NULL,
	[RecurringYearlyPCV] [decimal](28, 2) NULL,
	[RecurringYearlyTCV] [decimal](28, 2) NULL,
	[RecurringMonthlyPCV] [decimal](28, 2) NULL,
	[RecurringMonthlyTCV] [decimal](28, 2) NULL,
	[RecurringQuarterlyPCV] [decimal](28, 2) NULL,
	[RecurringQuarterlyTCV] [decimal](28, 2) NULL,
	[Territory] [varchar](100) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[EmployeeFullName] [varchar](255) NULL,
 CONSTRAINT [RecIdQuota] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quote]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quote](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Description] [text] NULL,
	[Name] [varchar](35) NULL,
	[Owner] [varchar](35) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[QuoteNo] [varchar](16) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[OpportunityLink_RecID] [varchar](32) NULL,
	[OpportunityLink_Category] [varchar](61) NULL,
	[OpportunityLink_VRecID] [varchar](32) NULL,
	[OpportunityName] [varchar](100) NULL,
	[ProductLine] [varchar](32) NULL,
	[ApprovalAuthorityID] [varchar](30) NULL,
	[FinalState] [varchar](1) NULL,
	[QuoteNo_LnkKey] [varchar](16) NULL,
	[RejectingComments] [varchar](256) NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[CURRENCYCODE_Valid] [varchar](32) NULL,
	[AccountName] [varchar](100) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[mGrossAmountTCV] [decimal](10, 2) NULL,
	[mGrossAmountPCV] [decimal](10, 2) NULL,
	[mQuoteDiscountTempTCV] [decimal](10, 2) NULL,
	[mQuoteDiscountTempPCV] [decimal](10, 2) NULL,
	[mQuoteDiscountTCV] [decimal](10, 2) NULL,
	[mQuoteDiscountPCV] [decimal](10, 2) NULL,
	[mRevenueTCV] [decimal](10, 2) NULL,
	[mRevenuePCV] [decimal](10, 2) NULL,
	[mTaxAmountTCV] [decimal](10, 2) NULL,
	[mTaxAmountPCV] [decimal](10, 2) NULL,
	[mTaxAmountTempTCV] [decimal](10, 2) NULL,
	[mTaxAmountTempPCV] [decimal](10, 2) NULL,
	[mTotalAmountDueTCV] [decimal](10, 2) NULL,
	[mTotalAmountDuePCV] [decimal](10, 2) NULL,
	[mTotalListPriceTCV] [decimal](10, 2) NULL,
	[mTotalListPricePCV] [decimal](10, 2) NULL,
	[QuoteDiscountPercentage] [decimal](4, 2) NULL,
	[IgnoreStatusWhenGeneratingDoc] [bit] NULL,
 CONSTRAINT [QuoteRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuoteApprovalConfiguration]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuoteApprovalConfiguration](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[IsApprovalRequired] [bit] NULL,
	[Level1IsNotificationRequired] [bit] NULL,
	[Level1FollowupWaitTime] [int] NULL,
	[Level1FollowupWaitTime_Valid] [varchar](32) NULL,
	[IsActive] [bit] NULL,
	[Level1IsActive] [bit] NULL,
	[Level1RevenueThreshold] [decimal](28, 2) NULL,
	[Level2FollowupWaitTime] [int] NULL,
	[Level2IsActive] [bit] NULL,
	[Level2IsNotificationRequired] [bit] NULL,
	[Level3IsNotificationRequired] [bit] NULL,
	[Level3IsActive] [bit] NULL,
	[Level3FollowupWaitTime] [int] NULL,
	[Level2RevenueThreshold] [decimal](28, 2) NULL,
	[Level2ApprovalAuthor_RecID] [varchar](32) NULL,
	[Level2ApprovalAuthor_Category] [varchar](61) NULL,
	[Level2ApprovalAuthor_VRecID] [varchar](32) NULL,
	[Level3ApprovalAuthor_RecID] [varchar](32) NULL,
	[Level3ApprovalAuthor_Category] [varchar](61) NULL,
	[Level3ApprovalAuthor_VRecID] [varchar](32) NULL,
	[L1RevThresholdLEL2RevThreshold] [bit] NULL,
	[L1DisThresholdLEL2DisThreshold] [bit] NULL,
	[Level1DiscountPercentThreshold] [tinyint] NULL,
	[Level2DiscountPercentThreshold] [tinyint] NULL,
	[ApprovalMode] [varchar](25) NULL,
	[ApprovalMode_Valid] [varchar](32) NULL,
	[IsLevel1AAQuoteOwnerManager] [bit] NULL,
	[IsLevel2AAQuoteOwner2LManager] [bit] NULL,
	[Level1ApprovalAuthor_RecID] [varchar](32) NULL,
	[Level1ApprovalAuthor_Category] [varchar](61) NULL,
	[Level1ApprovalAuthor_VRecID] [varchar](32) NULL,
	[Level3RevenueThreshold] [decimal](28, 2) NULL,
	[Level3DiscountPercentThreshold] [tinyint] NULL,
	[L2DisThresholdLEL3DisThreshold] [bit] NULL,
	[L2RevThresholdLEL3RevThreshold] [bit] NULL,
	[L1DisThresholdLEL3DisThreshold] [bit] NULL,
	[L1RevThresholdLEL3RevThreshold] [bit] NULL,
 CONSTRAINT [QutAppConfigRecID] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuoteStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuoteStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](35) NULL,
	[SortField] [smallint] NULL,
 CONSTRAINT [QuoteStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RecurringRevenue]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecurringRevenue](
	[DisplayName] [varchar](255) NULL CONSTRAINT [DF_RecurringRevenue_DisplayName]  DEFAULT ('<empty>'),
	[RecId_ProfileEmployee] [varchar](32) NULL CONSTRAINT [DF_RecurringRevenue_RecId_ProfileEmployee]  DEFAULT ('<empty>'),
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL CONSTRAINT [DF_RecurringRevenue_AggregateWhichRows]  DEFAULT ('All'),
	[m_RecurrRevenueMonthly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueMonthly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__PCV] [decimal](10, 2) NULL,
	[ForecastYear] [varchar](7) NULL CONSTRAINT [DF_RecurringRevenue_ForecastYear]  DEFAULT ('<empty>'),
	[ForecastQuarter] [varchar](7) NULL CONSTRAINT [DF_RecurringRevenue_ForecastQuarter]  DEFAULT ('<empty>'),
	[ForecastMonth] [varchar](100) NULL CONSTRAINT [DF_RecurringRevenue_ForecastMonth]  DEFAULT ('<empty>'),
	[Territory] [varchar](100) NULL CONSTRAINT [DF_RecurringRevenue_Territory]  DEFAULT ('<empty>'),
	[CURRENCYCODE] [varchar](7) NULL CONSTRAINT [DF_RecurringRevenue_CURRENCYCODE]  DEFAULT ('<empty>'),
 CONSTRAINT [RecIdRecurrRevenue] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RecurringRevenueByTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecurringRevenueByTerritory](
	[Territory] [varchar](25) NULL,
	[RecId_Territory] [varchar](32) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[AggregateWhichRows] [varchar](30) NOT NULL,
	[m_RecurrRevenueMonthly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueMonthly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueQuarterly__PCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__TCV] [decimal](10, 2) NULL,
	[m_RecurrRevenueYearly__PCV] [decimal](10, 2) NULL,
 CONSTRAINT [RecurRevnTerRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Referral]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Referral](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ReferralType] [varchar](25) NULL,
	[ReferralType_Valid] [varchar](32) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](61) NULL,
	[ContactLink_VRecID] [varchar](32) NULL,
	[ContactFullName] [varchar](120) NULL,
 CONSTRAINT [ReferralRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReferralType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReferralType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ReferralType] [varchar](25) NULL,
 CONSTRAINT [ReferralTypeRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Region]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Region] [varchar](10) NULL,
	[SortOrder] [smallint] NULL,
 CONSTRAINT [RecIdRegion] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Response]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Response](
	[ResponseType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Subject] [varchar](50) NULL,
	[Description] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Notes] [text] NULL,
	[ResponseParentLink_RecID] [varchar](32) NULL,
	[ResponseParentLink_Category] [varchar](61) NULL,
	[ResponseParentLink_VRecID] [varchar](32) NULL,
	[Name] [varchar](50) NULL,
	[CampaignParentLink_RecID] [varchar](32) NULL,
	[CampaignParentLink_Category] [varchar](61) NULL,
	[CampaignParentLink_VRecID] [varchar](32) NULL,
	[CampaignName] [varchar](50) NULL,
 CONSTRAINT [ResponseRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RMA]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RMA](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[RMAtype] [varchar](25) NULL,
	[RMAtype_Valid] [varchar](32) NULL,
	[RMANumber] [varchar](25) NULL,
	[Reason] [text] NULL,
 CONSTRAINT [RecIdRMA] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[saleschannel]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[saleschannel](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[saleschanneltype] [varchar](25) NULL,
	[description] [varchar](100) NULL,
 CONSTRAINT [RecIdsaleschannel] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduledJob]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduledJob](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[JobName] [varchar](100) NULL,
	[JobClass] [varchar](200) NULL,
	[JobClass_Valid] [varchar](32) NULL,
	[Description] [varchar](200) NULL,
	[IsEnabled] [bit] NULL,
	[QueryGroup] [varchar](32) NULL,
	[JobType] [varchar](200) NULL,
	[JobData] [text] NULL,
	[ActiveEndDate] [datetime] NULL,
	[ActiveEndTime] [datetime] NULL,
	[ActiveStartDate] [datetime] NULL,
	[ActiveStartTime] [datetime] NULL,
	[FreqInterval] [decimal](10, 0) NULL,
	[FreqType] [decimal](10, 0) NULL,
	[IsRecurring] [bit] NULL,
	[NextRunTime] [datetime] NULL,
	[Units] [varchar](50) NULL,
	[Units_Valid] [varchar](32) NULL,
	[QueryGroupName] [varchar](200) NULL,
 CONSTRAINT [ScheduledJobRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduledJobHistory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduledJobHistory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[Message] [varchar](1024) NULL,
	[run_status] [varchar](25) NULL,
	[RunDateTime] [datetime] NULL,
	[JobName] [varchar](100) NULL,
 CONSTRAINT [SchedJobHistRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seeds]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seeds](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](50) NULL,
	[Description] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [SeedsRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Segment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Segment](
	[SegmentType] [varchar](30) NULL,
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](25) NULL,
	[SegmentSize] [int] NULL,
	[Description] [text] NULL,
	[SegmentID] [varchar](25) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [SegmentRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SellingCode]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SellingCode](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](25) NULL,
	[Status_Valid] [varchar](32) NULL,
	[Startdate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[Title] [varchar](25) NULL,
	[sellcode] [int] NULL,
	[SellCodetype] [varchar](25) NULL,
	[SellCodetype_Valid] [varchar](32) NULL,
	[sellingcodnumber] [varchar](6) NULL,
	[sellingcodalpha] [varchar](5) NULL,
 CONSTRAINT [RecIdSellingCode] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SellingCodeType]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SellingCodeType](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[SelCodType] [varchar](25) NULL,
 CONSTRAINT [RecIdSellingCodeTy] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Skill]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Skill](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Skill] [varchar](50) NULL,
	[Rating] [varchar](25) NULL,
	[Rating_Valid] [varchar](32) NULL,
 CONSTRAINT [RecIdSkill] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Stage]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stage](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Stage] [varchar](50) NULL,
	[Probability] [smallint] NULL,
	[SortField] [smallint] NULL,
	[StageDuration] [smallint] NULL,
	[StalledDealLimit] [smallint] NULL,
	[Methodology] [varchar](60) NULL,
	[Methodology_Valid] [varchar](32) NULL,
 CONSTRAINT [StageRecID] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StandardUserTeam]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StandardUserTeam](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Team] [varchar](100) NULL,
	[TeamEmail] [varchar](100) NULL,
	[TeamManagerEmail] [varchar](100) NULL,
	[TeamManager] [varchar](100) NULL,
	[ManagerLink_RecID] [varchar](32) NULL,
	[ManagerLink_Category] [varchar](61) NULL,
	[ManagerLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdStandardUserT] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubContractor]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubContractor](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[SubcontractorID] [varchar](25) NULL,
	[Name] [varchar](25) NULL,
	[Contact] [varchar](25) NULL,
	[Phone] [varchar](25) NULL,
	[Address1] [varchar](25) NULL,
	[Address2] [varchar](25) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](25) NULL,
	[ZIP] [varchar](25) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[TransactionCurrencyC_Valid] [varchar](32) NULL,
	[InstallationFeePCV] [decimal](6, 2) NULL,
	[InstallationFeeTCV] [decimal](6, 2) NULL,
	[RomovalFeePCV] [decimal](6, 2) NULL,
	[RemovalFeeTCV] [decimal](6, 2) NULL,
	[MonthlyServiceFeePCV] [decimal](6, 2) NULL,
	[MonthlyServiceFeeTCV] [decimal](6, 2) NULL,
	[EntryDate] [datetime] NULL,
	[ContractDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
	[County] [varchar](25) NULL,
	[ServiceType] [varchar](40) NULL,
	[ServiceType_Valid] [varchar](32) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Deleted] [varchar](1) NULL,
	[IsAddressVerified] [varchar](1) NULL,
 CONSTRAINT [RecIdSubContractor] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subscription]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscription](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Title] [varchar](40) NULL,
	[Details] [text] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[IsActive] [bit] NULL,
	[Destination] [varchar](60) NULL,
	[Destination_Valid] [varchar](32) NULL,
 CONSTRAINT [SubscriptionRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubscriptionDestination]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubscriptionDestination](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](60) NULL,
	[Description] [varchar](300) NULL,
 CONSTRAINT [SubscrptDestRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuppressionList]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuppressionList](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Name] [varchar](25) NULL,
	[Description] [varchar](25) NULL,
 CONSTRAINT [SuppListRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskOutcome]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaskOutcome](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[OutCome] [varchar](25) NULL,
 CONSTRAINT [RecIdTaskOutcome] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskResult]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaskResult](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](200) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](200) NULL,
	[TaskFollowUp] [varchar](25) NULL,
 CONSTRAINT [RecIdTaskResult] PRIMARY KEY NONCLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Team]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Team](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Owner] [varchar](30) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[Team] [varchar](40) NULL,
	[Description] [text] NULL,
	[ExternalPartner] [bit] NULL,
 CONSTRAINT [TeamRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeamMember](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[DisplayName] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[Title] [varchar](25) NULL,
	[Role] [varchar](50) NULL,
	[Role_Valid] [varchar](32) NULL,
	[ExternalPartner] [bit] NULL,
	[TeamMemberType] [varchar](25) NULL,
	[TeamMemberType_Valid] [varchar](32) NULL,
	[AccountName] [varchar](100) NULL,
	[ContactName] [varchar](100) NULL,
	[EmployeeName] [varchar](100) NULL,
	[AccountLink_RecID] [varchar](32) NULL,
	[AccountLink_Category] [varchar](61) NULL,
	[AccountLink_VRecID] [varchar](32) NULL,
	[ContactLink_RecID] [varchar](32) NULL,
	[ContactLink_Category] [varchar](61) NULL,
	[ContactLink_VRecID] [varchar](32) NULL,
	[EmployeeLink_RecID] [varchar](32) NULL,
	[EmployeeLink_Category] [varchar](61) NULL,
	[EmployeeLink_VRecID] [varchar](32) NULL,
	[ShowTeamMemberAccount] [bit] NULL,
	[ShowTeamMemberContact] [bit] NULL,
	[ShowTeamMemberEmployee] [bit] NULL,
	[AccountDepartment] [varchar](50) NULL,
	[ContactDepartment] [varchar](50) NULL,
	[EmployeeDepartment] [varchar](50) NULL,
	[AccountTitle] [varchar](60) NULL,
	[ContactTitle] [varchar](60) NULL,
	[EmployeeTitle] [varchar](60) NULL,
 CONSTRAINT [TeamMemberRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeamMemberRole]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TeamMemberRole](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Role] [varchar](60) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [TmMembrRoleRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Territory]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Territory](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Territory] [varchar](100) NULL,
	[ParentTerritoryLink_RecID] [varchar](32) NULL,
	[ParentTerritoryLink_Category] [varchar](30) NULL,
	[ParentTerritoryName] [varchar](100) NULL,
	[LeadRecipient] [varchar](100) NULL,
	[ParentTerritoryNotEqual] [bit] NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[LeadRecipientLink_RecID] [varchar](32) NULL,
	[LeadRecipientLink_Category] [varchar](61) NULL,
	[LeadRecipientLink_VRecID] [varchar](32) NULL,
	[QuoteApprovalAuthori_RecID] [varchar](32) NULL,
	[QuoteApprovalAuthori_Category] [varchar](61) NULL,
	[QuoteApprovalAuthori_VRecID] [varchar](32) NULL,
	[QuoteApprovalAuthority] [varchar](100) NULL,
	[Hierarchy_Left] [int] NULL,
	[Hierarchy_Right] [int] NULL,
 CONSTRAINT [TerritoryRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TerritoryPricing]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TerritoryPricing](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[Territory] [varchar](25) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[TypeOfCurrency] [varchar](25) NULL,
	[TypeOfCurrency_Valid] [varchar](32) NULL,
	[MSRP] [decimal](8, 2) NULL,
	[Cost] [decimal](8, 2) NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[Pricing] [varchar](50) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[Instruction] [text] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[PricingValuePct] [decimal](5, 2) NULL,
	[PricingText] [varchar](25) NULL,
	[ShowFixedPrice] [bit] NULL,
	[ShowListPrice] [bit] NULL,
	[ShowPricingValue] [bit] NULL,
	[ShowPricingAmt] [bit] NULL,
	[ShowPricingPct] [bit] NULL,
	[PricingType] [varchar](25) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_MSRP__TCV] [decimal](10, 2) NULL,
	[m_MSRP__PCV] [decimal](10, 2) NULL,
	[m_Cost__TCV] [decimal](10, 2) NULL,
	[m_Cost__PCV] [decimal](10, 2) NULL,
	[m_ListPrice__TCV] [decimal](10, 2) NULL,
	[m_ListPrice__PCV] [decimal](10, 2) NULL,
	[m_PricingValue__TCV] [decimal](10, 2) NULL,
	[m_PricingValue__PCV] [decimal](10, 2) NULL,
	[m_FixedPrice__TCV] [decimal](10, 2) NULL,
	[m_FixedPrice__PCV] [decimal](10, 2) NULL,
 CONSTRAINT [TerrPricingRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TimeUnit]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeUnit](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[DisplayText] [varchar](25) NULL,
	[UnitIndex] [smallint] NULL,
 CONSTRAINT [TimeUnitRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPresence]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserPresence](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](30) NULL,
	[Status] [varchar](25) NULL,
	[Owner] [varchar](60) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[Description] [varchar](100) NULL,
	[Private] [bit] NULL,
 CONSTRAINT [UserPresenceRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendor](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Department] [varchar](50) NULL,
	[Department_Valid] [varchar](32) NULL,
	[DisplayName] [varchar](255) NULL,
	[FirstName] [varchar](25) NULL,
	[LastName] [varchar](25) NULL,
	[MiddleName] [varchar](25) NULL,
	[Owner] [varchar](30) NULL,
	[Owner_Valid] [varchar](32) NULL,
	[OwnerTeam] [varchar](30) NULL,
	[OwnerType] [varchar](30) NULL,
	[Prefix] [varchar](30) NULL,
	[Prefix_Valid] [varchar](32) NULL,
	[ProfileID] [varchar](30) NULL,
	[ProfileType] [varchar](32) NULL,
	[Suffix] [varchar](30) NULL,
	[Suffix_Valid] [varchar](32) NULL,
	[Territory] [varchar](25) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[Title] [varchar](30) NULL,
	[Title_Valid] [varchar](32) NULL,
	[VendorName] [varchar](60) NULL,
	[VendorStatus] [varchar](60) NULL,
	[VendorStatus_Valid] [varchar](32) NULL,
	[Phone1Link_RecID] [varchar](32) NULL,
	[Phone1Link_Category] [varchar](61) NULL,
	[Phone1Link_VRecID] [varchar](32) NULL,
	[Phone2Link_RecID] [varchar](32) NULL,
	[Phone2Link_Category] [varchar](61) NULL,
	[Phone2Link_VRecID] [varchar](32) NULL,
	[Phone3Link_RecID] [varchar](32) NULL,
	[Phone3Link_Category] [varchar](61) NULL,
	[Phone3Link_VRecID] [varchar](32) NULL,
	[Phone4Link_RecID] [varchar](32) NULL,
	[Phone4Link_Category] [varchar](61) NULL,
	[Phone4Link_VRecID] [varchar](32) NULL,
	[PrimaryAddressLink_RecID] [varchar](32) NULL,
	[PrimaryAddressLink_Category] [varchar](61) NULL,
	[PrimaryAddressLink_VRecID] [varchar](32) NULL,
	[PrimaryEmailLink_RecID] [varchar](32) NULL,
	[PrimaryEmailLink_Category] [varchar](61) NULL,
	[PrimaryEmailLink_VRecID] [varchar](32) NULL,
	[PrimaryWebLink_RecID] [varchar](32) NULL,
	[PrimaryWebLink_Category] [varchar](61) NULL,
	[PrimaryWebLink_VRecID] [varchar](32) NULL,
 CONSTRAINT [RecIdVendor] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VendorStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VendorStatus](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[Status] [varchar](60) NULL,
	[Description] [text] NULL,
 CONSTRAINT [VendorStatusRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VolumeBasedPricing]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VolumeBasedPricing](
	[RecId] [char](32) NOT NULL,
	[LastModDateTime] [datetime] NULL,
	[LastModBy] [varchar](30) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ParentLink_RecID] [varchar](32) NULL,
	[ParentLink_Category] [varchar](61) NULL,
	[ParentLink_VRecID] [varchar](32) NULL,
	[MaxUnits] [int] NULL,
	[MinUnits] [int] NULL,
	[MinUnits_Valid] [varchar](32) NULL,
	[Territory] [varchar](25) NULL,
	[Territory_Valid] [varchar](32) NULL,
	[TypeOfCurrency] [varchar](25) NULL,
	[TypeOfCurrency_Valid] [varchar](32) NULL,
	[Amount] [decimal](10, 2) NULL,
	[Pricing] [varchar](25) NULL,
	[Pricing_Valid] [varchar](32) NULL,
	[Instruction] [text] NULL,
	[CURRENCYCODE] [varchar](3) NULL,
	[PrimaryCurrencyCode] [varchar](3) NULL,
	[m_Amount__TCV] [decimal](10, 2) NULL,
	[m_Amount__PCV] [decimal](10, 2) NULL,
 CONSTRAINT [VlmBasedPrcngRecId] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[AuthorizationEndDate]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AuthorizationEndDate]
AS
SELECT     dbo.Account.SubscriberID, dbo.Account.Medicaid, dbo.Account.CaseNum, dbo.Account.ContactName, dbo.Account.DisplayName, 
                      dbo.AgencyAuthorization.Agency, dbo.AgencyAuthorization.AgencyID, MAX(dbo.AgencyAuthorization.CoverageEndDate) AS AuthEnd, 
                      dbo.AgencyAuthorization.AuthNumber
FROM         dbo.Account INNER JOIN
                      dbo.AgencyAuthorization ON dbo.Account.RecId = dbo.AgencyAuthorization.ParentLink_RecID
GROUP BY dbo.Account.SubscriberID, dbo.Account.Medicaid, dbo.Account.CaseNum, dbo.Account.ContactName, dbo.Account.DisplayName, 
                      dbo.AgencyAuthorization.Agency, dbo.AgencyAuthorization.AgencyID, dbo.AgencyAuthorization.AuthNumber

GO
/****** Object:  View [dbo].[VW_AuthAgencyChange]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_AuthAgencyChange]
AS
SELECT     dbo.Account.AccountName, dbo.Account.AgencyID, dbo.AuthorizationEndDate.AgencyID AS AuthAgency, dbo.AuthorizationEndDate.AuthEnd, 
                      dbo.AuthorizationEndDate.SubscriberID, dbo.AuthorizationEndDate.AuthNumber
FROM         dbo.Account INNER JOIN
                      dbo.AuthorizationEndDate ON dbo.Account.SubscriberID = dbo.AuthorizationEndDate.SubscriberID AND 
                      dbo.Account.AgencyID <> dbo.AuthorizationEndDate.AgencyID
WHERE     (dbo.AuthorizationEndDate.AuthEnd > GETDATE())

GO
/****** Object:  View [dbo].[VW_GMEEAuthAgencyChange]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_GMEEAuthAgencyChange]
AS
SELECT     dbo.Account.AccountName, dbo.Account.AgencyID, dbo.AuthorizationEndDate.AgencyID AS AuthAgency, dbo.AuthorizationEndDate.AuthEnd, 
                      dbo.AuthorizationEndDate.SubscriberID, dbo.Account.SubscriberID + dbo.AuthorizationEndDate.AgencyID AS recid, 
                      dbo.AuthorizationEndDate.AuthNumber
FROM         dbo.Account INNER JOIN
                      dbo.AuthorizationEndDate ON dbo.Account.SubscriberID = dbo.AuthorizationEndDate.SubscriberID AND 
                      dbo.Account.AgencyID <> dbo.AuthorizationEndDate.AgencyID
WHERE     (dbo.AuthorizationEndDate.AuthEnd > GETDATE())

GO
/****** Object:  View [dbo].[AgencyAuthMaxStart]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AgencyAuthMaxStart]
AS
SELECT     MAX(CoverageStartDate) AS Coveragestartdate, ParentLink_RecID
FROM         dbo.AgencyAuthorization
WHERE     (AuthorizationType = 'Monthly')
GROUP BY ParentLink_RecID

GO
/****** Object:  View [dbo].[GM_reportAgencyAuth]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[GM_reportAgencyAuth]
AS
SELECT     TOP (100) PERCENT dbo.AgencyAuthorization.RecId, dbo.AgencyAuthorization.LastModDateTime, dbo.AgencyAuthorization.LastModBy, 
                      dbo.AgencyAuthorization.CreatedDateTime, dbo.AgencyAuthorization.CreatedBy, dbo.AgencyAuthorization.Agency, dbo.AgencyAuthorization.AgencyID, 
                      dbo.AgencyAuthorization.AuthNumber, dbo.AgencyAuthorization.CoverageStartDate, dbo.AgencyAuthorization.CoverageEndDate, 
                      dbo.AgencyAuthorization.ParentLink_RecID, dbo.AgencyAuthorization.ParentLink_Category, dbo.AgencyAuthorization.ParentLink_VRecID, 
                      dbo.AgencyAuthorization.AgencyAuthorizationWeb_ID, dbo.AgencyAuthorization.CaseNumber, dbo.AgencyAuthorization.AuthorizationType, 
                      dbo.AgencyAuthorization.AuthorizationType_Valid, dbo.AgencyAuthorization.DiagCode, dbo.AgencyAuthorization.NumberOfUnits, 
                      dbo.AgencyAuthorization.WebLastModDateTime, dbo.AgencyAuthorization.WebUser
FROM         dbo.AgencyAuthMaxStart INNER JOIN
                      dbo.AgencyAuthorization ON dbo.AgencyAuthMaxStart.ParentLink_RecID = dbo.AgencyAuthorization.ParentLink_RecID AND 
                      dbo.AgencyAuthMaxStart.Coveragestartdate = dbo.AgencyAuthorization.CoverageStartDate
WHERE     (dbo.AgencyAuthorization.AuthorizationType = 'Monthly')

GO
/****** Object:  View [dbo].[EmployeeForecastTerritory_FLAT0]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EmployeeForecastTerritory_FLAT0] AS SELECT TEMP.RecId, TEMP.Territory, SUM(ALL TEMP.BestCase) AS BestCase, SUM(ALL TEMP.WeightedRevenue) AS WeightedRevenue, SUM(ALL TEMP.Likely) AS Likely, SUM(ALL TEMP.BestCase_PrimaryValue) AS BestCase_PrimaryValue, SUM(ALL TEMP.Closed_PrimaryValue) AS Closed_PrimaryValue, SUM(ALL TEMP.Committed_PrimaryValue) AS Committed_PrimaryValue, SUM(ALL TEMP.Likely_PrimaryValue) AS Likely_PrimaryValue, SUM(ALL TEMP.WeightedRevenue_PrimaryValue) AS WeightedRevenue_PrimaryValue, SUM(ALL TEMP.Closed) AS Closed, SUM(ALL TEMP.Committed) AS Committed, SUM(ALL TEMP.Quota) AS Quota FROM (SELECT Forecast1.RecId, Forecast1.Territory, SUM(ALL Forecast1.m_BestCase__TCV) AS BestCase, SUM(ALL Forecast1.m_Weighted__TCV) AS WeightedRevenue, SUM(ALL Forecast1.m_Likely__TCV) AS Likely, SUM(ALL Forecast1.m_BestCase__PCV) AS BestCase_PrimaryValue, SUM(ALL Forecast1.m_Closed__PCV) AS Closed_PrimaryValue, SUM(ALL Forecast1.m_Committed__PCV) AS Committed_PrimaryValue, SUM(ALL Forecast1.m_Likely__PCV) AS Likely_PrimaryValue, SUM(ALL Forecast1.m_Weighted__PCV) AS WeightedRevenue_PrimaryValue, SUM(ALL Forecast1.m_Closed__TCV) AS Closed, SUM(ALL Forecast1.m_Committed__TCV) AS Committed, NULL AS Quota FROM Profile LEFT OUTER JOIN Forecast Forecast1 ON (Profile.RecId = Forecast1.EmployeeLink_RecID) WHERE (Profile.ProfileType = 'Employee') GROUP BY Forecast1.RecId,Forecast1.Territory UNION SELECT DISTINCT Quota1.Territory_Valid, Quota1.Territory, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SUM(ALL Quota1.m_Amount__TCV) AS Quota FROM Profile LEFT OUTER JOIN Quota Quota1 ON (Profile.RecId = Quota1.EmployeeLink_RecID) WHERE (Profile.ProfileType = 'Employee') GROUP BY Quota1.Territory_Valid,Quota1.Territory) TEMP GROUP BY TEMP.RecId, TEMP.Territory
GO
/****** Object:  View [dbo].[EmployeeForecastTerritory_HIERARCHICAL1]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EmployeeForecastTerritory_HIERARCHICAL1] AS SELECT Forecast.RecId, Forecast.LastModDateTime, Forecast.LastModBy, Forecast.CreatedDateTime, Forecast.CreatedBy, Forecast.OwnerType, Forecast.Owner, Forecast.OwnerTeam, Forecast.ForecastMonth, Forecast.ForecastMonth_Valid, Forecast.ForecastQuarter, Forecast.ForecastQuarter_Valid, Forecast.ForecastYear, Forecast.EmployeeLink_RecID, Forecast.EmployeeLink_Category, Forecast.EmployeeLink_VRecID, Forecast.ForecastMonthNo, Forecast.CURRENCYCODE, Forecast.CURRENCYCODE_Valid, Forecast.PrimaryCurrencyCode, Forecast.m_BestCase__TCV, Forecast.m_BestCase__PCV, Forecast.m_Closed__TCV, Forecast.m_Closed__PCV, Forecast.m_Committed__TCV, Forecast.m_Committed__PCV, Forecast.m_Likely__TCV, Forecast.m_Likely__PCV, Forecast.m_Weighted__TCV, Forecast.m_Weighted__PCV, Forecast.EmployeeFullName, Forecast.Territory FROM Forecast
GO
/****** Object:  View [dbo].[Kshema_Subscriber_Language]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Kshema_Subscriber_Language] AS SELECT * FROM AMAC.dbo.VV_GMEE_Subscriber_Language
GO
/****** Object:  View [dbo].[KshemaCombinedSubDetail]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaCombinedSubDetail] AS SELECT * FROM AMAC.dbo.VV_GMEE_CombinedSubDetail
GO
/****** Object:  View [dbo].[KshemaContactLog]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaContactLog] AS SELECT * FROM AMAC.dbo.VV_GMEE_ContactLog
GO
/****** Object:  View [dbo].[KshemaDaysSinceOrdered]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaDaysSinceOrdered] AS SELECT * FROM AMAC.dbo.VV_GMEE_DaysSinceOrdered
GO
/****** Object:  View [dbo].[KshemaLastContactLog]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaLastContactLog] AS SELECT * FROM AMAC.dbo.VV_GMEE_LastContactDetail
GO
/****** Object:  View [dbo].[KshemaLastSignal]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaLastSignal] AS SELECT * FROM AMAC.dbo.VW_GMEE_Last_Signal_Detail
GO
/****** Object:  View [dbo].[KshemaNonEmergResp]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaNonEmergResp] AS SELECT * FROM AMAC.dbo.VV_GMEE_NonEmergResp
GO
/****** Object:  View [dbo].[KshemaSubscriberEquipment]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaSubscriberEquipment] AS SELECT * FROM AMAC.dbo.VW_GMEE_SubscriberEquipment
GO
/****** Object:  View [dbo].[KshemaSubscriberGeneralData]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaSubscriberGeneralData] AS SELECT * FROM AMAC.dbo.vw_GMEE_SubscriberGeneralData
GO
/****** Object:  View [dbo].[KshemaWorkorder]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[KshemaWorkorder] AS SELECT * FROM AMAC.dbo.VW_GMEE_Workorder
GO
/****** Object:  View [dbo].[VV_GMEE_Contact_Code]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VV_GMEE_Contact_Code] AS SELECT * FROM AMAC.dbo.VV_GMEE_Contact_Code
GO
/****** Object:  View [dbo].[VV_GMEE_Disposition_Code]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VV_GMEE_Disposition_Code] AS SELECT * FROM AMAC.dbo.VV_GMEE_Disposition_Code
GO
/****** Object:  View [dbo].[VV_GMEE_InOut_Code]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VV_GMEE_InOut_Code] AS SELECT * FROM AMAC.dbo.VV_GMEE_InOut_Code
GO
/****** Object:  View [dbo].[VV_GMEE_Reason_Code]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VV_GMEE_Reason_Code] AS SELECT * FROM AMAC.dbo.VV_GMEE_Reason_Code
GO
/****** Object:  View [dbo].[VW_GmNoAuthSubscriber]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_GmNoAuthSubscriber]
AS
SELECT     dbo.Account.RecId, dbo.Account.LastModDateTime, dbo.Account.LastModBy, dbo.Account.CreatedDateTime, dbo.Account.CreatedBy, 
                      dbo.Account.AccountName, dbo.Account.AccountType, dbo.Account.AccountType_Valid, dbo.Account.ContactName, dbo.Account.ContactPhone, 
                      dbo.Account.Description, dbo.Account.DisplayName, dbo.Account.Employees, dbo.Account.Employees_Valid, dbo.Account.Industry, 
                      dbo.Account.Industry_Valid, dbo.Account.MarketSegment, dbo.Account.MarketSegment_Valid, dbo.Account.Owner, dbo.Account.Owner_Valid, 
                      dbo.Account.Ownership, dbo.Account.Ownership_Valid, dbo.Account.OwnerTeam, dbo.Account.OwnerType, dbo.Account.ParentAccountName, 
                      dbo.Account.ParentAccountNotEqual, dbo.Account.PreferredMethodContact, dbo.Account.PreferredMethodConta_Valid, 
                      dbo.Account.PrimaryPartnerName, dbo.Account.Status, dbo.Account.Status_Valid, dbo.Account.Tax, dbo.Account.Territory, dbo.Account.TickerSymbol, 
                      dbo.Account.ParentLink_RecID, dbo.Account.ParentLink_Category, dbo.Account.ParentLink_VRecID, dbo.Account.ParentAccountLink_RecID, 
                      dbo.Account.ParentAccountLink_Category, dbo.Account.ParentAccountLink_VRecID, dbo.Account.PrimaryPartnerLink_RecID, 
                      dbo.Account.PrimaryPartnerLink_Category, dbo.Account.PrimaryPartnerLink_VRecID, dbo.Account.PrimaryAddressLink_RecID, 
                      dbo.Account.PrimaryAddressLink_Category, dbo.Account.PrimaryAddressLink_VRecID, dbo.Account.PrimaryWebLink_RecID, 
                      dbo.Account.PrimaryWebLink_Category, dbo.Account.PrimaryWebLink_VRecID, dbo.Account.PhoneLink_RecID, dbo.Account.PhoneLink_Category, 
                      dbo.Account.PhoneLink_VRecID, dbo.Account.PrimaryContactLink_RecID, dbo.Account.PrimaryContactLink_Category, 
                      dbo.Account.PrimaryContactLink_VRecID, dbo.Account.NAICSCode, dbo.Account.PrimaryEmailLink_RecID, dbo.Account.PrimaryEmailLink_Category, 
                      dbo.Account.PrimaryEmailLink_VRecID, dbo.Account.PrimaryEmail, dbo.Account.PrimaryCurrencyCode, dbo.Account.TransactionCurrencyCode, 
                      dbo.Account.TransactionCurrencyC_Valid, dbo.Account.AnnualRevenuePCV, dbo.Account.AnnualRevenueTCV, dbo.Account.LastName, 
                      dbo.Account.NearIntersection, dbo.Account.DOB, dbo.Account.Language1, dbo.Account.Sex, dbo.Account.ProgramType, dbo.Account.LastSignalDate, 
                      dbo.Account.CaseNum, dbo.Account.Medicaid, dbo.Account.AgencyName, dbo.Account.SubContractorID, dbo.Account.SubContractorName, 
                      dbo.Account.CASANum, dbo.Account.AgencyID, dbo.Account.Casemanager, dbo.Account.SubscriberID, dbo.Account.MiddleInitial, 
                      dbo.Account.KnownName, dbo.Account.UnitID, dbo.Account.SSN, dbo.Account.Ktype, dbo.Account.ForcedEntry, dbo.Account.Cancel, 
                      dbo.Account.UnitRentedSold, dbo.Account.TimZoneCode, dbo.Account.IsDayLightSaving, dbo.Account.OrderDate, dbo.Account.EntryDate, 
                      dbo.Account.InstallDate, dbo.Account.RemovalDate, dbo.Account.REquestedRemovalDate, dbo.Account.EquipmentRecovered, dbo.Account.FileMagic, 
                      dbo.Account.ConfirmedReceived, dbo.Account.SpecialInst, dbo.Account.Deleted, dbo.Account.SecondUser, dbo.Account.OnlineSince, 
                      dbo.Account.KProgramType, dbo.Account.MasterReference, dbo.Account.SharedID, dbo.Account.SystemName, dbo.Account.IPAddress, 
                      dbo.Account.IsAddressVerified, dbo.Account.MannualOveride, dbo.Account.AgencyID_Valid, dbo.Account.HasOpenWorkOrder, 
                      dbo.Account.WorkOrderLink_RecID, dbo.Account.WorkOrderLink_Category, dbo.Account.WorkOrderLink_VRecID, dbo.Account.LanguageLink_RecID, 
                      dbo.Account.LanguageLink_Category, dbo.Account.LanguageLink_VRecID, dbo.Account.LinkLastContact_RecID, dbo.Account.LinkLastContact_Category, 
                      dbo.Account.LinkLastContact_VRecID, dbo.Account.LinkInstallDays_RecID, dbo.Account.LinkInstallDays_Category, 
                      dbo.Account.LinkInstallDays_VRecID, dbo.Account.CSResponsible, dbo.AgencyAuthorization.ParentLink_RecID AS Expr1, 
                      dbo.Account.AgencyID AS Expr2, dbo.Account.Status AS Expr3
FROM         dbo.Account LEFT OUTER JOIN
                      dbo.AgencyAuthorization ON dbo.Account.RecId = dbo.AgencyAuthorization.ParentLink_RecID
WHERE     (dbo.AgencyAuthorization.ParentLink_RecID IS NULL)

GO
/****** Object:  View [dbo].[VW_UpdateAuthCaseNumber]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_UpdateAuthCaseNumber]
AS
SELECT     dbo.Account.CaseNum, dbo.AgencyAuthorization.CaseNumber
FROM         dbo.Account INNER JOIN
                      dbo.AgencyAuthorization ON dbo.Account.RecId = dbo.AgencyAuthorization.ParentLink_RecID
WHERE     (dbo.Account.CaseNum <> ' ' OR
                      dbo.Account.CaseNum IS NOT NULL) AND (dbo.AgencyAuthorization.CaseNumber = ' ' OR
                      dbo.AgencyAuthorization.CaseNumber IS NULL)

GO
/****** Object:  View [dbo].[VW_UpdateCaseNumber]    Script Date: 2/11/2016 1:49:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_UpdateCaseNumber]
AS
SELECT     AMAC.dbo.vw_GMEE_SubscriberCaseNumber.Subsciber_ID, AMAC.dbo.vw_GMEE_SubscriberCaseNumber.Value, 
                      AMAC.dbo.vw_GMEE_SubscriberCaseNumber.Code, AMAC.dbo.vw_GMEE_SubscriberCaseNumber.uniqueId, dbo.Account.CaseNum
FROM         AMAC.dbo.vw_GMEE_SubscriberCaseNumber INNER JOIN
                      dbo.Account ON AMAC.dbo.vw_GMEE_SubscriberCaseNumber.Subsciber_ID = dbo.Account.SubscriberID
WHERE     (dbo.Account.CaseNum = ' ' OR
                      dbo.Account.CaseNum IS NULL)

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountNameAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountNameAccount] ON [dbo].[Account]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountParentLink_RecID] ON [dbo].[Account]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountTypeAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountTypeAccount] ON [dbo].[Account]
(
	[AccountType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameAccount] ON [dbo].[Account]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IndustryAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IndustryAccount] ON [dbo].[Account]
(
	[Industry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LastNameAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LastNameAccount] ON [dbo].[Account]
(
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerAccount] ON [dbo].[Account]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusAccount] ON [dbo].[Account]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubContractorIDAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubContractorIDAccount] ON [dbo].[Account]
(
	[SubContractorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubscriberIDAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubscriberIDAccount] ON [dbo].[Account]
(
	[SubscriberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryAccount] ON [dbo].[Account]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IndustryAccountIndustry]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IndustryAccountIndustry] ON [dbo].[AccountIndustry]
(
	[Industry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountOrderParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountOrderParentLink_RecID] ON [dbo].[AccountOrder]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityNameAccountOrder]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityNameAccountOrder] ON [dbo].[AccountOrder]
(
	[OpportunityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OrderDateAccountOrder]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OrderDateAccountOrder] ON [dbo].[AccountOrder]
(
	[OrderDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusAccountStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusAccountStatus] ON [dbo].[AccountStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountTypeAccountType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountTypeAccountType] ON [dbo].[AccountType]
(
	[AccountType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ActivityLocationActivityAppoin]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ActivityLocationActivityAppoin] ON [dbo].[Activity]
(
	[ActivityLocation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [DueDateTimeActivityTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DueDateTimeActivityTask] ON [dbo].[Activity]
(
	[DueDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EndDateTimeActivityAppointment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EndDateTimeActivityAppointment] ON [dbo].[Activity]
(
	[EndDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerActivityMeeting]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerActivityMeeting] ON [dbo].[Activity]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityActivityAppointment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityActivityAppointment] ON [dbo].[Activity]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PurposeActivityAppointment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PurposeActivityAppointment] ON [dbo].[Activity]
(
	[Purpose] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [StartDateTimeActivityAppointme]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StartDateTimeActivityAppointme] ON [dbo].[Activity]
(
	[StartDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusActivityAppointment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusActivityAppointment] ON [dbo].[Activity]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectActivityAppointment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectActivityAppointment] ON [dbo].[Activity]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ActivityLocationActivityLocati]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ActivityLocationActivityLocati] ON [dbo].[ActivityLocation]
(
	[ActivityLocation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActivityLocationSortField]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ActivityLocationSortField] ON [dbo].[ActivityLocation]
(
	[SortField] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityActivityPriority]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityActivityPriority] ON [dbo].[ActivityPriority]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ActivityTypeActivityPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ActivityTypeActivityPurpose] ON [dbo].[ActivityPurpose]
(
	[ActivityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PurposeActivityPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PurposeActivityPurpose] ON [dbo].[ActivityPurpose]
(
	[Purpose] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusActivityStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusActivityStatus] ON [dbo].[ActivityStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ActivityTypeActivityType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ActivityTypeActivityType] ON [dbo].[ActivityType]
(
	[ActivityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressAddressTypeIndex]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressAddressTypeIndex] ON [dbo].[Address]
(
	[AddressType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressDisplayTextIndex]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressDisplayTextIndex] ON [dbo].[Address]
(
	[DisplayText] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressEmailEmail]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressEmailEmail] ON [dbo].[Address]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressPhonePhone]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressPhonePhone] ON [dbo].[Address]
(
	[Phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressWebAddressWebAddress]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressWebAddressWebAddress] ON [dbo].[Address]
(
	[WebAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [subAddressParentLinkIndex]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [subAddressParentLinkIndex] ON [dbo].[Address]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryLongAddressCountry]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CountryLongAddressCountry] ON [dbo].[AddressCountry]
(
	[CountryLong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryShortAddressCountry]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CountryShortAddressCountry] ON [dbo].[AddressCountry]
(
	[CountryShort] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LocaleAddressLocale]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LocaleAddressLocale] ON [dbo].[AddressLocale]
(
	[Locale] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PurposeAddressPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PurposeAddressPurpose] ON [dbo].[AddressPurpose]
(
	[Purpose] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PurposeTypeAddressPurpose]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PurposeTypeAddressPurpose] ON [dbo].[AddressPurpose]
(
	[PurposeType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressStateParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressStateParentLink_RecID] ON [dbo].[AddressState]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryAddressState]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CountryAddressState] ON [dbo].[AddressState]
(
	[Country] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StateLongAddressState]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StateLongAddressState] ON [dbo].[AddressState]
(
	[StateLong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StateShortAddressState]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StateShortAddressState] ON [dbo].[AddressState]
(
	[StateShort] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AddressTypeAddressType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AddressTypeAddressType] ON [dbo].[AddressType]
(
	[AddressType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AgencyParentLink_R]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AgencyParentLink_R] ON [dbo].[Agency]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AgencyAuthorizationParentLink_]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AgencyAuthorizationParentLink_] ON [dbo].[AgencyAuthorization]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AgencyCodeAgencyException]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AgencyCodeAgencyException] ON [dbo].[AgencyException]
(
	[AgencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AgencyTaskParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AgencyTaskParentLink_RecID] ON [dbo].[AgencyTask]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AlternatePricingPa]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AlternatePricingPa] ON [dbo].[AlternatePricing]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ApplicationParentL]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ApplicationParentL] ON [dbo].[Application]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AttachmentNameIndex]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AttachmentNameIndex] ON [dbo].[Attachment]
(
	[ATTACHNAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AuditHistoryCreatedDateTime]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AuditHistoryCreatedDateTime] ON [dbo].[AuditHistory]
(
	[CreatedDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AuditHistoryParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AuditHistoryParentLink_RecID] ON [dbo].[AuditHistory]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountlinkBoard]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountlinkBoard] ON [dbo].[Board]
(
	[Accountlink] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [BoardParentLink_Re]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [BoardParentLink_Re] ON [dbo].[Board]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [BundleItemsParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [BundleItemsParentLink_RecID] ON [dbo].[BundleItems]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductNameBundleItems]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductNameBundleItems] ON [dbo].[BundleItems]
(
	[ProductName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignDisplayName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignDisplayName] ON [dbo].[Campaign]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignIDCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignIDCampaign] ON [dbo].[Campaign]
(
	[CampaignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [CampaignName] ON [dbo].[Campaign]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignParentLink_RecID] ON [dbo].[Campaign]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignTypeCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignTypeCampaign] ON [dbo].[Campaign]
(
	[CampaignType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EndDateCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EndDateCampaign] ON [dbo].[Campaign]
(
	[EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerCampaign] ON [dbo].[Campaign]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [StartDateCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StartDateCampaign] ON [dbo].[Campaign]
(
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCampaign] ON [dbo].[Campaign]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TargetCampaign]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TargetCampaign] ON [dbo].[Campaign]
(
	[Target] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignCategoryCampaignCatego]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignCategoryCampaignCatego] ON [dbo].[CampaignCategories]
(
	[CampaignCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryCampaignCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryCampaignCategory] ON [dbo].[CampaignCategory]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [KeyCodeCampaignKeyCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [KeyCodeCampaignKeyCode] ON [dbo].[CampaignKeyCode]
(
	[KeyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignIDCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignIDCampaignOverview] ON [dbo].[CampaignOverview]
(
	[CampaignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignOverviewParentLink_Rec]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignOverviewParentLink_Rec] ON [dbo].[CampaignOverview]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryCampaignOverview] ON [dbo].[CampaignOverview]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CellIDCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CellIDCampaignOverview] ON [dbo].[CampaignOverview]
(
	[CellID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [KeyCodeCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [KeyCodeCampaignOverview] ON [dbo].[CampaignOverview]
(
	[KeyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCampaignOverview] ON [dbo].[CampaignOverview]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SegmentIDCampaignOverview]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SegmentIDCampaignOverview] ON [dbo].[CampaignOverview]
(
	[SegmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCampaignStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCampaignStatus] ON [dbo].[CampaignStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TargetCampaignTarget]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TargetCampaignTarget] ON [dbo].[CampaignTarget]
(
	[Target] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignTaskParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignTaskParentLink_RecID] ON [dbo].[CampaignTask]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [DueDateTimeCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DueDateTimeCampaignTask] ON [dbo].[CampaignTask]
(
	[DueDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EndDateTimeCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EndDateTimeCampaignTask] ON [dbo].[CampaignTask]
(
	[EndDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCampaignTask] ON [dbo].[CampaignTask]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerCampaignTask] ON [dbo].[CampaignTask]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityCampaignTask] ON [dbo].[CampaignTask]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [StartDateTimeCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StartDateTimeCampaignTask] ON [dbo].[CampaignTask]
(
	[StartDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCampaignTask] ON [dbo].[CampaignTask]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectCampaignTask]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectCampaignTask] ON [dbo].[CampaignTask]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IsActiveCampaignTa]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IsActiveCampaignTa] ON [dbo].[CampaignTaskNotificationConfig]
(
	[IsActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignTemplateParentLink_Rec]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignTemplateParentLink_Rec] ON [dbo].[CampaignTemplate]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignTypeCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignTypeCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[CampaignType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EndDateCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EndDateCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [StartDateCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StartDateCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TargetCampaignTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TargetCampaignTemplate] ON [dbo].[CampaignTemplate]
(
	[Target] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignTypeCampaignType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignTypeCampaignType] ON [dbo].[CampaignType]
(
	[CampaignType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryCaseCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryCaseCategory] ON [dbo].[CaseCategory]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CauseCodeCaseCauseCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CauseCodeCaseCauseCode] ON [dbo].[CaseCauseCode]
(
	[CauseCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ResolutionCategoryCaseResoluti]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ResolutionCategoryCaseResoluti] ON [dbo].[CaseResolutionCategory]
(
	[ResolutionCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CaseNumberCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CaseNumberCases] ON [dbo].[Cases]
(
	[CaseNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CasesParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CasesParentLink_RecID] ON [dbo].[Cases]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CaseTypeCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CaseTypeCases] ON [dbo].[Cases]
(
	[CaseType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryCases] ON [dbo].[Cases]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [DueOnCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DueOnCases] ON [dbo].[Cases]
(
	[DueOn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerCases] ON [dbo].[Cases]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityCases] ON [dbo].[Cases]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceCases] ON [dbo].[Cases]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCases] ON [dbo].[Cases]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectCases]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectCases] ON [dbo].[Cases]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceCaseSource]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceCaseSource] ON [dbo].[CaseSource]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCaseStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCaseStatus] ON [dbo].[CaseStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CaseTypeCaseType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CaseTypeCaseType] ON [dbo].[CaseType]
(
	[CaseType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CellIDCellAccountCell]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CellIDCellAccountCell] ON [dbo].[Cell]
(
	[CellID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CellParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CellParentLink_RecID] ON [dbo].[Cell]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCellAccountCell]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCellAccountCell] ON [dbo].[Cell]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SegmentNameCellAccountCell]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SegmentNameCellAccountCell] ON [dbo].[Cell]
(
	[SegmentName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RatingCompetitionRating]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingCompetitionRating] ON [dbo].[CompetitionRating]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCompetitionStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCompetitionStatus] ON [dbo].[CompetitionStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CompetitorNameCompetitor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CompetitorNameCompetitor] ON [dbo].[Competitor]
(
	[CompetitorName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameCompetitor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameCompetitor] ON [dbo].[Competitor]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProfileIDCompetitor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProfileIDCompetitor] ON [dbo].[Competitor]
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RatingCompetitor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingCompetitor] ON [dbo].[Competitor]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusCompetitor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusCompetitor] ON [dbo].[Competitor]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountNameContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountNameContact] ON [dbo].[Contact]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContactIDContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContactIDContact] ON [dbo].[Contact]
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContactParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContactParentLink_RecID] ON [dbo].[Contact]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContactSupervisorNameContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContactSupervisorNameContact] ON [dbo].[Contact]
(
	[ContactSupervisorName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameContact] ON [dbo].[Contact]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FullNameContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [FullNameContact] ON [dbo].[Contact]
(
	[FullName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerContact] ON [dbo].[Contact]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceContact] ON [dbo].[Contact]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryContact] ON [dbo].[Contact]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitleContact]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitleContact] ON [dbo].[Contact]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceContactSource]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceContactSource] ON [dbo].[ContactSource]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContactTypeContactType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContactTypeContactType] ON [dbo].[ContactType]
(
	[ContactType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [CloseDateContract]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CloseDateContract] ON [dbo].[Contract]
(
	[CloseDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContractParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContractParentLink_RecID] ON [dbo].[Contract]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContractTypeContract]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContractTypeContract] ON [dbo].[Contract]
(
	[ContractType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [StartDateContract]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StartDateContract] ON [dbo].[Contract]
(
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContractTypeContractType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContractTypeContractType] ON [dbo].[ContractType]
(
	[ContractType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CodeCurrencyCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CodeCurrencyCode] ON [dbo].[CurrencyCode]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IssueCtryCurrCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IssueCtryCurrCode] ON [dbo].[CurrencyCode]
(
	[IssuingCountry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCurrencyCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCurrencyCode] ON [dbo].[CurrencyCode]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameCurrencySymbol]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameCurrencySymbol] ON [dbo].[CurrencySymbol]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SymbolCurrencySymbol]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SymbolCurrencySymbol] ON [dbo].[CurrencySymbol]
(
	[Symbol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountTypeDiscount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountTypeDiscount] ON [dbo].[Discount]
(
	[AccountType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CountryDiscount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CountryDiscount] ON [dbo].[Discount]
(
	[Country] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DiscountParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DiscountParentLink_RecID] ON [dbo].[Discount]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DiscountTypeDiscount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DiscountTypeDiscount] ON [dbo].[Discount]
(
	[DiscountType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryDiscount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryDiscount] ON [dbo].[Discount]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DiscountTypeDiscountType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DiscountTypeDiscountType] ON [dbo].[DiscountType]
(
	[DiscountType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EmailFromAddress]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EmailFromAddress] ON [dbo].[Email]
(
	[FromAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EmailSubject]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EmailSubject] ON [dbo].[Email]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FormatEmailFormat]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [FormatEmailFormat] ON [dbo].[EmailFormat]
(
	[Format] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EquipmentParentLin]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EquipmentParentLin] ON [dbo].[Equipment]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EffectiveStartDate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EffectiveStartDate] ON [dbo].[ExchangeRate]
(
	[EffectiveStartDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ExchgRtPLnk_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ExchgRtPLnk_RecID] ON [dbo].[ExchangeRate]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SrcCurrExchngRt]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SrcCurrExchngRt] ON [dbo].[ExchangeRate]
(
	[SourceCurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TrgtCurrExchngRt]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TrgtCurrExchngRt] ON [dbo].[ExchangeRate]
(
	[TargetCurrencyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [EmployeeFullNameForecast]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EmployeeFullNameForecast] ON [dbo].[Forecast]
(
	[EmployeeFullName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ForecastMonthForecast]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastMonthForecast] ON [dbo].[Forecast]
(
	[ForecastMonth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ForecastMonthNoForecast]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastMonthNoForecast] ON [dbo].[Forecast]
(
	[ForecastMonthNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ForecastQuarterForecast]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastQuarterForecast] ON [dbo].[Forecast]
(
	[ForecastQuarter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ForecastYearForecast]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastYearForecast] ON [dbo].[Forecast]
(
	[ForecastYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AdjustmentMonthForecastAdjustm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AdjustmentMonthForecastAdjustm] ON [dbo].[ForecastAdjustment]
(
	[AdjustmentMonth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AdjustmentQuarterForecastAdjus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AdjustmentQuarterForecastAdjus] ON [dbo].[ForecastAdjustment]
(
	[AdjustmentQuarter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AdjustmentYearForecastAdjustme]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AdjustmentYearForecastAdjustme] ON [dbo].[ForecastAdjustment]
(
	[AdjustmentYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EffectiveDateForecastAdjustmen]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EffectiveDateForecastAdjustmen] ON [dbo].[ForecastAdjustment]
(
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ForecastAdjustmentParentLink_R]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastAdjustmentParentLink_R] ON [dbo].[ForecastAdjustment]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ForecastStatusForecastStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ForecastStatusForecastStatus] ON [dbo].[ForecastStatus]
(
	[ForecastStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAppDefName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAppDefName] ON [dbo].[FusionAppDefDeletes]
(
	[LastModDateTime] ASC,
	[TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IADefType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IADefType] ON [dbo].[FusionAppDefs]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IADefTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IADefTypeID] ON [dbo].[FusionAppDefs]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IADefTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IADefTypeNm] ON [dbo].[FusionAppDefs]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAppsType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAppsType] ON [dbo].[FusionApplications]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAppsTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAppsTypeID] ON [dbo].[FusionApplications]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAppsTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAppsTypeNm] ON [dbo].[FusionApplications]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IAsdLastUpdated]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAsdLastUpdated] ON [dbo].[FusionAsd]
(
	[LastModDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAsdType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAsdType] ON [dbo].[FusionAsd]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAsdTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAsdTypeID] ON [dbo].[FusionAsd]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IAsdTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IAsdTypeNm] ON [dbo].[FusionAsd]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IBProcLastUpdated]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IBProcLastUpdated] ON [dbo].[FusionBusProcs]
(
	[LastModDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IBProcType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IBProcType] ON [dbo].[FusionBusProcs]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IBProcTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IBProcTypeID] ON [dbo].[FusionBusProcs]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IBProcTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IBProcTypeNm] ON [dbo].[FusionBusProcs]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ICntRngName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ICntRngName] ON [dbo].[FusionCounterRanges]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ICntRngOwner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ICntRngOwner] ON [dbo].[FusionCounterRanges]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ICntName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ICntName] ON [dbo].[FusionCounters]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefDelPers]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefDelPers] ON [dbo].[FusionDefDeletes]
(
	[LastModDateTime] ASC,
	[Perspective] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefObType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefObType] ON [dbo].[FusionDefs]
(
	[FDObType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefPers]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefPers] ON [dbo].[FusionDefs]
(
	[LastModDateTime] ASC,
	[Perspective] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefType] ON [dbo].[FusionDefs]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefTypeID] ON [dbo].[FusionDefs]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDefTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IDefTypeNm] ON [dbo].[FusionDefs]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IImgLastUpdated]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IImgLastUpdated] ON [dbo].[FusionImages]
(
	[LastModDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ImgType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ImgType] ON [dbo].[FusionImages]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ImgTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ImgTypeID] ON [dbo].[FusionImages]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ImgTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ImgTypeNm] ON [dbo].[FusionImages]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ILink1]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ILink1] ON [dbo].[FusionLink]
(
	[SourceName] ASC,
	[TargetID] ASC,
	[RelationshipName] ASC,
	[SourceID] ASC,
	[SourceLoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ILink2]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ILink2] ON [dbo].[FusionLink]
(
	[SourceBase] ASC,
	[TargetID] ASC,
	[RelationshipName] ASC,
	[SourceID] ASC,
	[SourceLoc] ASC,
	[SourceName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ILink3]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ILink3] ON [dbo].[FusionLink]
(
	[TargetID] ASC,
	[SourceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ILnkSrcID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ILnkSrcID] ON [dbo].[FusionLink]
(
	[SourceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ILnkUnique]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ILnkUnique] ON [dbo].[FusionLink]
(
	[SourceID] ASC,
	[TargetID] ASC,
	[RelationshipName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IQGLastUpdated]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQGLastUpdated] ON [dbo].[FusionQueryGroups]
(
	[LastModDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQryType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQryType] ON [dbo].[FusionQueryGroups]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQryTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQryTypeID] ON [dbo].[FusionQueryGroups]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQryTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQryTypeNm] ON [dbo].[FusionQueryGroups]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IQALastUpdated]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQALastUpdated] ON [dbo].[FusionQuickActions]
(
	[LastModDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQkAType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQkAType] ON [dbo].[FusionQuickActions]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQkATypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQkATypeID] ON [dbo].[FusionQuickActions]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IQkATypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IQkATypeNm] ON [dbo].[FusionQuickActions]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IRepType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IRepType] ON [dbo].[FusionReports]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IRepTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IRepTypeID] ON [dbo].[FusionReports]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IRepTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IRepTypeNm] ON [dbo].[FusionReports]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ISecMLoginID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ISecMLoginID] ON [dbo].[FusionSecMap]
(
	[LoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ISecMUniqueAuthID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ISecMUniqueAuthID] ON [dbo].[FusionSecMap]
(
	[AuthID] ASC,
	[AuthSource] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ISecMUniqueLoginID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ISecMUniqueLoginID] ON [dbo].[FusionSecMap]
(
	[LoginID] ASC,
	[AuthSource] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IScopeScopeOwner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IScopeScopeOwner] ON [dbo].[FusionSettings]
(
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ISetLookup]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ISetLookup] ON [dbo].[FusionSettings]
(
	[ModuleName] ASC,
	[Category] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IUpdPkgType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IUpdPkgType] ON [dbo].[FusionUpdatePackages]
(
	[FDObType] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IUpdPkgTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IUpdPkgTypeID] ON [dbo].[FusionUpdatePackages]
(
	[FDObType] ASC,
	[DefRecID] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IUpdPkgTypeNm]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IUpdPkgTypeNm] ON [dbo].[FusionUpdatePackages]
(
	[FDObType] ASC,
	[FDName] ASC,
	[Scope] ASC,
	[ScopeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IUsrTypeID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IUsrTypeID] ON [dbo].[FusionUsers]
(
	[UserType] ASC,
	[LoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IVerModName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IVerModName] ON [dbo].[FusionVersion]
(
	[ModuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [InfluencerParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [InfluencerParentLink_RecID] ON [dbo].[Influencer]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [InfluencerTypeInfluencer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [InfluencerTypeInfluencer] ON [dbo].[Influencer]
(
	[InfluencerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameInfluencer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameInfluencer] ON [dbo].[Influencer]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RatingInfluencer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingInfluencer] ON [dbo].[Influencer]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ResponseModeInfluencer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ResponseModeInfluencer] ON [dbo].[Influencer]
(
	[ResponseMode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RatingInfluencerRating]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingInfluencerRating] ON [dbo].[InfluencerRating]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ResponseModeInfluencerResponse]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ResponseModeInfluencerResponse] ON [dbo].[InfluencerResponseMode]
(
	[ResponseMode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [InfluencersParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [InfluencersParentLink_RecID] ON [dbo].[Influencers]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [InfluencerTypeInfluencers]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [InfluencerTypeInfluencers] ON [dbo].[Influencers]
(
	[InfluencerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RatingInfluencers]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingInfluencers] ON [dbo].[Influencers]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ResponseModeInfluencers]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ResponseModeInfluencers] ON [dbo].[Influencers]
(
	[ResponseMode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [InfluencerTypeInfluencerType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [InfluencerTypeInfluencerType] ON [dbo].[InfluencerType]
(
	[InfluencerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [DueDateIssue]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DueDateIssue] ON [dbo].[Issue]
(
	[DueDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IssueIssue]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IssueIssue] ON [dbo].[Issue]
(
	[Issue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IssueOwnerIssue]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IssueOwnerIssue] ON [dbo].[Issue]
(
	[IssueOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IssueParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IssueParentLink_RecID] ON [dbo].[Issue]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityIssue]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityIssue] ON [dbo].[Issue]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusIssue]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusIssue] ON [dbo].[Issue]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusIssueStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusIssueStatus] ON [dbo].[IssueStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [KeyNameJobClass]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [KeyNameJobClass] ON [dbo].[JobClass]
(
	[KeyName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [KeyValueJobClass]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [KeyValueJobClass] ON [dbo].[JobClass]
(
	[KeyValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [JobTitleJobTitile]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [JobTitleJobTitile] ON [dbo].[JobTitile]
(
	[JobTitle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameLead] ON [dbo].[Lead]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LeadParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LeadParentLink_RecID] ON [dbo].[Lead]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LeadRating]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LeadRating] ON [dbo].[Lead]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LeadSourceLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LeadSourceLead] ON [dbo].[Lead]
(
	[LeadSource] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerLead] ON [dbo].[Lead]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusLead] ON [dbo].[Lead]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryLead] ON [dbo].[Lead]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitleLead]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitleLead] ON [dbo].[Lead]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryLeadCateg]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryLeadCateg] ON [dbo].[LeadCategory]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CompanySizeLeadCompanySize]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CompanySizeLeadCompanySize] ON [dbo].[LeadCompanySize]
(
	[CompanySize] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IsActiveLeadRoutin]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IsActiveLeadRoutin] ON [dbo].[LeadRoutingConfiguration]
(
	[IsActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ProcessUnattendedL]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProcessUnattendedL] ON [dbo].[LeadRoutingConfiguration]
(
	[ProcessUnattendedLeads] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoutingModeLeadRou]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RoutingModeLeadRou] ON [dbo].[LeadRoutingConfiguration]
(
	[RoutingMode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceLeadSource]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceLeadSource] ON [dbo].[LeadSource]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusLeadStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusLeadStatus] ON [dbo].[LeadStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TimeToPurchaseLeadTimeToPurcha]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TimeToPurchaseLeadTimeToPurcha] ON [dbo].[LeadTimeToPurchase]
(
	[TimeToPurchase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameMailingList]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameMailingList] ON [dbo].[MailingList]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ManufacturerManufacturer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ManufacturerManufacturer] ON [dbo].[Manufacturer]
(
	[Manufacturer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FirstNameMarketingListMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [FirstNameMarketingListMember] ON [dbo].[MarketingListMember]
(
	[FirstName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LastNameMarketingListMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LastNameMarketingListMember] ON [dbo].[MarketingListMember]
(
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MarketSegmentMarketSegment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MarketSegmentMarketSegment] ON [dbo].[MarketSegment]
(
	[MarketSegment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FromAddressMessageAlert]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [FromAddressMessageAlert] ON [dbo].[Message]
(
	[FromAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MessageAlertParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MessageAlertParentLink_RecID] ON [dbo].[Message]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectMessageAlert]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectMessageAlert] ON [dbo].[Message]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ToAddressMessageAlert]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ToAddressMessageAlert] ON [dbo].[Message]
(
	[ToAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MessageTypeMessageType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MessageTypeMessageType] ON [dbo].[MessageType]
(
	[MessageType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MethodologyMethodology]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MethodologyMethodology] ON [dbo].[Methodology]
(
	[Methodology] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [MonthNoMonth_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MonthNoMonth_Valid] ON [dbo].[Month_Valid]
(
	[MonthNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MonthTextMonth_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MonthTextMonth_Valid] ON [dbo].[Month_Valid]
(
	[MonthText] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CodeNAICSCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CodeNAICSCode] ON [dbo].[NAICSCode]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IndustryNameNAICSCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IndustryNameNAICSCode] ON [dbo].[NAICSCode]
(
	[IndustryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NotesParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NotesParentLink_RecID] ON [dbo].[Notes]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NoteTypeNotes]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NoteTypeNotes] ON [dbo].[Notes]
(
	[NoteType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NoteTypeNoteTypes]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NoteTypeNoteTypes] ON [dbo].[NoteTypes]
(
	[NoteType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityNotification]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityNotification] ON [dbo].[Notification]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusNotification]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusNotification] ON [dbo].[Notification]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectNotification]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectNotification] ON [dbo].[Notification]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TransportNotification]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TransportNotification] ON [dbo].[Notification]
(
	[Transport] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusNotificationStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusNotificationStatus] ON [dbo].[NotificationStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [OfferingProductBundleBundle]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OfferingProductBundleBundle] ON [dbo].[Offering]
(
	[Bundle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OfferingProductBundleProductNa]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OfferingProductBundleProductNa] ON [dbo].[Offering]
(
	[ProductName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OfferingProductBundleProductTy]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OfferingProductBundleProductTy] ON [dbo].[Offering]
(
	[ProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OfferingProductOfferingParentL]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OfferingProductOfferingParentL] ON [dbo].[Offering]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductCodeOfferingProductOffe]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductCodeOfferingProductOffe] ON [dbo].[Offering]
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductLineOfferingProductOffe]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductLineOfferingProductOffe] ON [dbo].[Offering]
(
	[ProductLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductSkuOfferingProductOffer]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductSkuOfferingProductOffer] ON [dbo].[Offering]
(
	[ProductSku] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountNameOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountNameOpportunity] ON [dbo].[Opportunity]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityName] ON [dbo].[Opportunity]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityParentLink_RecID] ON [dbo].[Opportunity]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerOpportunity] ON [dbo].[Opportunity]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StageOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StageOpportunity] ON [dbo].[Opportunity]
(
	[Stage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusOpportunity] ON [dbo].[Opportunity]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [TargetCloseDateOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TargetCloseDateOpportunity] ON [dbo].[Opportunity]
(
	[TargetCloseDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryOpportunity]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryOpportunity] ON [dbo].[Opportunity]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IsActiveOpportunit]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IsActiveOpportunit] ON [dbo].[OpportunityAlertConfiguration]
(
	[IsActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MethodologyOpportu]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MethodologyOpportu] ON [dbo].[OpportunityAlertConfiguration]
(
	[Methodology] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CodeOpportunityCode]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CodeOpportunityCode] ON [dbo].[OpportunityCode]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LeadRatingOpportunityLeadRatin]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LeadRatingOpportunityLeadRatin] ON [dbo].[OpportunityLeadRating]
(
	[LeadRating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityProductTypeOpportun]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityProductTypeOpportun] ON [dbo].[OpportunityProductType]
(
	[OpportunityProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SourceOpportunitySource]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SourceOpportunitySource] ON [dbo].[OpportunitySource]
(
	[Source] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityStatusParentLink_Re]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityStatusParentLink_Re] ON [dbo].[OpportunityStatus]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusOpportunityStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusOpportunityStatus] ON [dbo].[OpportunityStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameOpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameOpportunityTemplate] ON [dbo].[OpportunityTemplate]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityTemplateAccountName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityTemplateAccountName] ON [dbo].[OpportunityTemplate]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityTemplateDisplayName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityTemplateDisplayName] ON [dbo].[OpportunityTemplate]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerOpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerOpportunityTemplate] ON [dbo].[OpportunityTemplate]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StageOpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StageOpportunityTemplate] ON [dbo].[OpportunityTemplate]
(
	[Stage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusOpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusOpportunityTemplate] ON [dbo].[OpportunityTemplate]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [TargetCloseDateOpportunityTemp]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TargetCloseDateOpportunityTemp] ON [dbo].[OpportunityTemplate]
(
	[TargetCloseDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryOpportunityTemplate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryOpportunityTemplate] ON [dbo].[OpportunityTemplate]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LineItemNameOpptyProductCustom]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LineItemNameOpptyProductCustom] ON [dbo].[OpptyProduct]
(
	[LineItemName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductCodeOpptyProductCustomI]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductCodeOpptyProductCustomI] ON [dbo].[OpptyProduct]
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductLineOpptyProductCustomI]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductLineOpptyProductCustomI] ON [dbo].[OpptyProduct]
(
	[ProductLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductSKUOpptyProductCustomIt]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductSKUOpptyProductCustomIt] ON [dbo].[OpptyProduct]
(
	[ProductSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnershipOwnership]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnershipOwnership] ON [dbo].[Ownership]
(
	[Ownership] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PainPointPainPoint]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PainPointPainPoint] ON [dbo].[PainPoint]
(
	[PainPoint] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PainPointParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PainPointParentLink_RecID] ON [dbo].[PainPoint]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PainPointPainPoint_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PainPointPainPoint_Valid] ON [dbo].[PainPoint_Valid]
(
	[PainPoint] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNamePartner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNamePartner] ON [dbo].[Partner]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PartnerNamePartner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PartnerNamePartner] ON [dbo].[Partner]
(
	[PartnerName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PartnerStatusPartner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PartnerStatusPartner] ON [dbo].[Partner]
(
	[PartnerStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProfileIDPartner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProfileIDPartner] ON [dbo].[Partner]
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitlePartner]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitlePartner] ON [dbo].[Partner]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusPartnerStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusPartnerStatus] ON [dbo].[PartnerStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PermissionsParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PermissionsParentLink_RecID] ON [dbo].[Permissions]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PermissionStatusPermissions]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PermissionStatusPermissions] ON [dbo].[Permissions]
(
	[PermissionStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PermissionStatusPermissionStat]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PermissionStatusPermissionStat] ON [dbo].[PermissionStatus]
(
	[PermissionStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PricingTypePricing]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PricingTypePricing] ON [dbo].[Pricing]
(
	[PricingType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PricingPricingInstruction]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PricingPricingInstruction] ON [dbo].[PricingInstruction]
(
	[Pricing] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PricingTypePricingType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PricingTypePricingType] ON [dbo].[PricingType]
(
	[PricingType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PriorityPriorities]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PriorityPriorities] ON [dbo].[Priorities]
(
	[Priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CategoryProductCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CategoryProductCategory] ON [dbo].[ProductCategory]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ParentCategoryProductCategory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ParentCategoryProductCategory] ON [dbo].[ProductCategory]
(
	[ParentCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductCategoryParentLink_RecI]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductCategoryParentLink_RecI] ON [dbo].[ProductCategory]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ParentLineProductLine]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ParentLineProductLine] ON [dbo].[ProductLine]
(
	[ParentLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductLineManagerProductLine]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductLineManagerProductLine] ON [dbo].[ProductLine]
(
	[ProductLineManager] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductLineParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductLineParentLink_RecID] ON [dbo].[ProductLine]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductLineProductLine]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductLineProductLine] ON [dbo].[ProductLine]
(
	[ProductLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuoteApprovalAuthorityProductL]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuoteApprovalAuthorityProductL] ON [dbo].[ProductLine]
(
	[QuoteApprovalAuthority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusProductStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusProductStatus] ON [dbo].[ProductStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProductTypeProductType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProductTypeProductType] ON [dbo].[ProductType]
(
	[ProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DepartmentProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DepartmentProfileEmployee] ON [dbo].[Profile]
(
	[Department] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LoginIDProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LoginIDProfileEmployee] ON [dbo].[Profile]
(
	[LoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [LR_EmployeeAssociatedBy_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LR_EmployeeAssociatedBy_Hierar] ON [dbo].[Profile]
(
	[Hierarchy_Left] ASC,
	[Hierarchy_Right] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [LR_SupervisorAssociates_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LR_SupervisorAssociates_Hierar] ON [dbo].[Profile]
(
	[Hierarchy_Left] ASC,
	[Hierarchy_Right] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProfileAccountDisplayName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProfileAccountDisplayName] ON [dbo].[Profile]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProfileAccountProfileID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProfileAccountProfileID] ON [dbo].[Profile]
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RL_EmployeeAssociatedBy_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RL_EmployeeAssociatedBy_Hierar] ON [dbo].[Profile]
(
	[Hierarchy_Right] ASC,
	[Hierarchy_Left] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RL_SupervisorAssociates_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RL_SupervisorAssociates_Hierar] ON [dbo].[Profile]
(
	[Hierarchy_Right] ASC,
	[Hierarchy_Left] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusProfileEmployee] ON [dbo].[Profile]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SupervisorProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SupervisorProfileEmployee] ON [dbo].[Profile]
(
	[Supervisor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryProfileEmployee] ON [dbo].[Profile]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitleProfileEmployee]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitleProfileEmployee] ON [dbo].[Profile]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DegreeProfileDegree]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DegreeProfileDegree] ON [dbo].[ProfileDegree]
(
	[Degree] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DepartmentCodeProfileDepartmen]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DepartmentCodeProfileDepartmen] ON [dbo].[ProfileDepartment]
(
	[DepartmentCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DepartmentProfileDepartment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DepartmentProfileDepartment] ON [dbo].[ProfileDepartment]
(
	[Department] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PrefixProfilePrefix]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PrefixProfilePrefix] ON [dbo].[ProfilePrefix]
(
	[Prefix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusProfileStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusProfileStatus] ON [dbo].[ProfileStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SuffixProfileSuffix]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SuffixProfileSuffix] ON [dbo].[ProfileSuffix]
(
	[Suffix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameProgram]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameProgram] ON [dbo].[Program]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProgramParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProgramParentLink_RecID] ON [dbo].[Program]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuarterQuarter_Valid]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuarterQuarter_Valid] ON [dbo].[Quarter_Valid]
(
	[Quarter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [EffectiveDateQueuedUpdate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [EffectiveDateQueuedUpdate] ON [dbo].[QueuedUpdate]
(
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [QueuedUpdateIDQueuedUpdate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QueuedUpdateIDQueuedUpdate] ON [dbo].[QueuedUpdate]
(
	[QueuedUpdateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QueuedUpdateParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QueuedUpdateParentLink_RecID] ON [dbo].[QueuedUpdate]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QueuedUpdateStatusQueuedUpdate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QueuedUpdateStatusQueuedUpdate] ON [dbo].[QueuedUpdate]
(
	[QueuedUpdateStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UpdateTypeQueuedUpdate]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [UpdateTypeQueuedUpdate] ON [dbo].[QueuedUpdate]
(
	[UpdateType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuotaMonthQuota]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuotaMonthQuota] ON [dbo].[Quota]
(
	[QuotaMonth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuotaQuarterQuota]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuotaQuarterQuota] ON [dbo].[Quota]
(
	[QuotaQuarter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuotaYearQuota]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuotaYearQuota] ON [dbo].[Quota]
(
	[QuotaYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccountNameQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [AccountNameQuote] ON [dbo].[Quote]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ApprovalAuthorityIDQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ApprovalAuthorityIDQuote] ON [dbo].[Quote]
(
	[ApprovalAuthorityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameQuote] ON [dbo].[Quote]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OpportunityNameQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OpportunityNameQuote] ON [dbo].[Quote]
(
	[OpportunityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerQuote] ON [dbo].[Quote]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuoteNoQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuoteNoQuote] ON [dbo].[Quote]
(
	[QuoteNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuoteParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuoteParentLink_RecID] ON [dbo].[Quote]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusQuote]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusQuote] ON [dbo].[Quote]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ApprovalModeQuoteApprovalConfi]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ApprovalModeQuoteApprovalConfi] ON [dbo].[QuoteApprovalConfiguration]
(
	[ApprovalMode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IsApprovalRequiredQuoteApprova]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [IsApprovalRequiredQuoteApprova] ON [dbo].[QuoteApprovalConfiguration]
(
	[IsApprovalRequired] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusQuoteStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusQuoteStatus] ON [dbo].[QuoteStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ContactFullNameReferral]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ContactFullNameReferral] ON [dbo].[Referral]
(
	[ContactFullName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ReferralTypeReferral]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ReferralTypeReferral] ON [dbo].[Referral]
(
	[ReferralType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ReferralTypeReferralType]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ReferralTypeReferralType] ON [dbo].[ReferralType]
(
	[ReferralType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [CampaignNameResponseResponseAc]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [CampaignNameResponseResponseAc] ON [dbo].[Response]
(
	[CampaignName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubjectResponseResponseAccount]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubjectResponseResponseAccount] ON [dbo].[Response]
(
	[Subject] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RMAParentLink_RecI]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RMAParentLink_RecI] ON [dbo].[RMA]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [JobClassScheduledJob]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [JobClassScheduledJob] ON [dbo].[ScheduledJob]
(
	[JobClass] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ScheduledJobJobName]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ScheduledJobJobName] ON [dbo].[ScheduledJob]
(
	[JobName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [JobNameScheduledJobHistory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [JobNameScheduledJobHistory] ON [dbo].[ScheduledJobHistory]
(
	[JobName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [run_statusScheduledJobHistory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [run_statusScheduledJobHistory] ON [dbo].[ScheduledJobHistory]
(
	[run_status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RunDateTimeScheduledJobHistory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RunDateTimeScheduledJobHistory] ON [dbo].[ScheduledJobHistory]
(
	[RunDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SchJobHistParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SchJobHistParentLink_RecID] ON [dbo].[ScheduledJobHistory]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameSeeds]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameSeeds] ON [dbo].[Seeds]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SeedsParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SeedsParentLink_RecID] ON [dbo].[Seeds]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameSegmentAccountSegment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameSegmentAccountSegment] ON [dbo].[Segment]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SegmentIDSegmentAccountSegment]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SegmentIDSegmentAccountSegment] ON [dbo].[Segment]
(
	[SegmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [sellcodeSellingCod]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [sellcodeSellingCod] ON [dbo].[SellingCode]
(
	[sellcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RatingSkill]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RatingSkill] ON [dbo].[Skill]
(
	[Rating] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SkillSkill]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SkillSkill] ON [dbo].[Skill]
(
	[Skill] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MethodologyStage]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [MethodologyStage] ON [dbo].[Stage]
(
	[Methodology] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StageStage]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StageStage] ON [dbo].[Stage]
(
	[Stage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StdUserTeamTeam]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [StdUserTeamTeam] ON [dbo].[StandardUserTeam]
(
	[Team] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubContractorParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubContractorParentLink_RecID] ON [dbo].[SubContractor]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DestinationSubscription]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DestinationSubscription] ON [dbo].[Subscription]
(
	[Destination] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubscriptionParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [SubscriptionParentLink_RecID] ON [dbo].[Subscription]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SubscriptionTitle]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [SubscriptionTitle] ON [dbo].[Subscription]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameSubscriptionDestination]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameSubscriptionDestination] ON [dbo].[SubscriptionDestination]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [NameSuppressionList]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [NameSuppressionList] ON [dbo].[SuppressionList]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TeamTeam]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [TeamTeam] ON [dbo].[Team]
(
	[Team] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DepartmentTeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DepartmentTeamMember] ON [dbo].[TeamMember]
(
	[Department] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameTeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameTeamMember] ON [dbo].[TeamMember]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleTeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RoleTeamMember] ON [dbo].[TeamMember]
(
	[Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TeamMemberTypeTeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TeamMemberTypeTeamMember] ON [dbo].[TeamMember]
(
	[TeamMemberType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitleTeamMember]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitleTeamMember] ON [dbo].[TeamMember]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleTeamMemberRole]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RoleTeamMemberRole] ON [dbo].[TeamMemberRole]
(
	[Role] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [LeadRecipientTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LeadRecipientTerritory] ON [dbo].[Territory]
(
	[LeadRecipient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [LR_TerritoryAssociatesC_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [LR_TerritoryAssociatesC_Hierar] ON [dbo].[Territory]
(
	[Hierarchy_Left] ASC,
	[Hierarchy_Right] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ParentTerritoryNameTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ParentTerritoryNameTerritory] ON [dbo].[Territory]
(
	[ParentTerritoryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [QuoteApprovalAuthorityTerritor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [QuoteApprovalAuthorityTerritor] ON [dbo].[Territory]
(
	[QuoteApprovalAuthority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [RL_TerritoryAssociatesC_Hierar]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [RL_TerritoryAssociatesC_Hierar] ON [dbo].[Territory]
(
	[Hierarchy_Right] ASC,
	[Hierarchy_Left] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryParentLink_RecID] ON [dbo].[Territory]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryTerritory]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryTerritory] ON [dbo].[Territory]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PricingTerritoryPricing]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [PricingTerritoryPricing] ON [dbo].[TerritoryPricing]
(
	[Pricing] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryPricingParentLink_Rec]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryPricingParentLink_Rec] ON [dbo].[TerritoryPricing]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryTerritoryPricing]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryTerritoryPricing] ON [dbo].[TerritoryPricing]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayTextTimeUnit]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayTextTimeUnit] ON [dbo].[TimeUnit]
(
	[DisplayText] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [OwnerUserPresence]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [OwnerUserPresence] ON [dbo].[UserPresence]
(
	[Owner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusUserPresence]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusUserPresence] ON [dbo].[UserPresence]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserPresenceParentLink_RecID]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [UserPresenceParentLink_RecID] ON [dbo].[UserPresence]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [DisplayNameVendor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [DisplayNameVendor] ON [dbo].[Vendor]
(
	[DisplayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ProfileIDVendor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [ProfileIDVendor] ON [dbo].[Vendor]
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TitleVendor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TitleVendor] ON [dbo].[Vendor]
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [VendorNameVendor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [VendorNameVendor] ON [dbo].[Vendor]
(
	[VendorName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [VendorStatusVendor]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [VendorStatusVendor] ON [dbo].[Vendor]
(
	[VendorStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [StatusVendorStatus]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [StatusVendorStatus] ON [dbo].[VendorStatus]
(
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [TerritoryVolumeBasedPricing]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [TerritoryVolumeBasedPricing] ON [dbo].[VolumeBasedPricing]
(
	[Territory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [VolumeBasedPricingParentLink_R]    Script Date: 2/11/2016 1:49:47 PM ******/
CREATE NONCLUSTERED INDEX [VolumeBasedPricingParentLink_R] ON [dbo].[VolumeBasedPricing]
(
	[ParentLink_RecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DracoActivityFactory] ADD  CONSTRAINT [DF_DracoActivityFactory_RecId]  DEFAULT ('0') FOR [RecId]
GO
ALTER TABLE [dbo].[EmployeeOpptyTerritory] ADD  CONSTRAINT [DF_EmployeeOpptyTerritory_Territory]  DEFAULT ('<empty>') FOR [Territory]
GO
ALTER TABLE [dbo].[EmployeeOpptyTerritory] ADD  CONSTRAINT [DF_EmployeeOpptyTerritory_RecId_Territory]  DEFAULT ('<empty>') FOR [RecId_Territory]
GO
ALTER TABLE [dbo].[EmployeeOpptyTerritory] ADD  CONSTRAINT [DF_EmployeeOpptyTerritory_AggregateWhichRows]  DEFAULT ('All') FOR [AggregateWhichRows]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_Q_Year]  DEFAULT ('<empty>') FOR [Q_Year]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_Q_Month]  DEFAULT ('<empty>') FOR [Q_Month]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_Q_Quarter]  DEFAULT ('<empty>') FOR [Q_Quarter]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_AggregateWhichRows]  DEFAULT ('All') FOR [AggregateWhichRows]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_EmployeeFullName]  DEFAULT ('<empty>') FOR [EmployeeFullName]
GO
ALTER TABLE [dbo].[EmployeeQuotaEmployee] ADD  CONSTRAINT [DF_EmployeeQuotaEmployee_RecId_ProfileEmployee]  DEFAULT ('<empty>') FOR [RecId_ProfileEmployee]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_Q_Year]  DEFAULT ('<empty>') FOR [Q_Year]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_Q_Quarter]  DEFAULT ('<empty>') FOR [Q_Quarter]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_Q_Month]  DEFAULT ('<empty>') FOR [Q_Month]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_Territory]  DEFAULT ('<empty>') FOR [Territory]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_RecId_Territory]  DEFAULT ('<empty>') FOR [RecId_Territory]
GO
ALTER TABLE [dbo].[EmployeeQuotaTerritory] ADD  CONSTRAINT [DF_EmployeeQuotaTerritory_AggregateWhichRows]  DEFAULT ('All') FOR [AggregateWhichRows]
GO
ALTER TABLE [dbo].[FusionApplications] ADD  CONSTRAINT [DF_FusionApplications_Perspective]  DEFAULT ('(Base)') FOR [Perspective]
GO
ALTER TABLE [dbo].[FusionDictionaries] ADD  CONSTRAINT [DF_FusionDictionaries_Perspective]  DEFAULT ('(Base)') FOR [Perspective]
GO
ALTER TABLE [dbo].[FusionUpdatePackages] ADD  CONSTRAINT [DF_FusionUpdatePackages_Perspective]  DEFAULT ('(Base)') FOR [Perspective]
GO
ALTER TABLE [dbo].[RecurringRevenueByTerritory] ADD  CONSTRAINT [DF_RecurringRevenueByTerritory_Territory]  DEFAULT ('<empty>') FOR [Territory]
GO
ALTER TABLE [dbo].[RecurringRevenueByTerritory] ADD  CONSTRAINT [DF_RecurringRevenueByTerritory_RecId_Territory]  DEFAULT ('<empty>') FOR [RecId_Territory]
GO
ALTER TABLE [dbo].[RecurringRevenueByTerritory] ADD  CONSTRAINT [DF_RecurringRevenueByTerritory_AggregateWhichRows]  DEFAULT ('All') FOR [AggregateWhichRows]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AgencyAuthorization"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 353
               Right = 278
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 2280
         Width = 2940
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1920
         Alias = 1830
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AgencyAuthMaxStart'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AgencyAuthMaxStart'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 0
               Left = 429
               Bottom = 302
               Right = 669
            End
            DisplayFlags = 280
            TopColumn = 71
         End
         Begin Table = "AgencyAuthorization"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 342
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1155
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2040
         Width = 3540
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 4095
         Alias = 1935
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AuthorizationEndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AuthorizationEndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[31] 2[2] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AgencyAuthMaxStart"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 179
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AgencyAuthorization"
            Begin Extent = 
               Top = 0
               Left = 484
               Bottom = 392
               Right = 703
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1950
         Width = 1500
         Width = 2055
         Width = 1500
         Width = 2445
         Width = 1500
         Width = 1500
         Width = 1755
         Width = 1500
         Width = 2730
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2055
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GM_reportAgencyAuth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'GM_reportAgencyAuth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[9] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 293
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 71
         End
         Begin Table = "AuthorizationEndDate"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 292
               Right = 752
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 2115
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1410
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_AuthAgencyChange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_AuthAgencyChange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AuthorizationEndDate"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 339
               Right = 598
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3825
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_GMEEAuthAgencyChange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_GMEEAuthAgencyChange'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[18] 4[22] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AgencyAuthorization"
            Begin Extent = 
               Top = 6
               Left = 363
               Bottom = 114
               Right = 582
            End
            DisplayFlags = 280
            TopColumn = 11
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 129
         Width = 284
         Width = 1860
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_GmNoAuthSubscriber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3315
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_GmNoAuthSubscriber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_GmNoAuthSubscriber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 239
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 61
         End
         Begin Table = "AgencyAuthorization"
            Begin Extent = 
               Top = 6
               Left = 300
               Bottom = 267
               Right = 519
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2340
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 2475
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_UpdateAuthCaseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_UpdateAuthCaseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vw_GMEE_SubscriberCaseNumber (AMAC.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 221
               Right = 195
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 233
               Bottom = 235
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 70
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_UpdateCaseNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VW_UpdateCaseNumber'
GO
USE [master]
GO
ALTER DATABASE [GMEE_HSMS] SET  READ_WRITE 
GO
