USE [master]
GO
/****** Object:  Database [PersSupport]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE DATABASE [PersSupport] ON  PRIMARY 
( NAME = N'PersSupport_Data', FILENAME = N'F:\DATA\MSSQL\Data\PersSupport_Data.MDF' , SIZE = 13568KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'PersSupport_Log', FILENAME = N'G:\DATA\MSSQL\Data\PersSupport_Log.LDF' , SIZE = 6272KB , MAXSIZE = 10240KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PersSupport] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PersSupport].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [PersSupport] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PersSupport] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PersSupport] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PersSupport] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PersSupport] SET ARITHABORT OFF 
GO
ALTER DATABASE [PersSupport] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PersSupport] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PersSupport] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PersSupport] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PersSupport] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PersSupport] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PersSupport] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PersSupport] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PersSupport] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PersSupport] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PersSupport] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PersSupport] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PersSupport] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PersSupport] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PersSupport] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PersSupport] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PersSupport] SET RECOVERY FULL 
GO
ALTER DATABASE [PersSupport] SET  MULTI_USER 
GO
ALTER DATABASE [PersSupport] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [PersSupport] SET DB_CHAINING OFF 
GO
USE [PersSupport]
GO
/****** Object:  User [PSantiago]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE USER [PSantiago] FOR LOGIN [ERC_DOMAIN\PSantiago] WITH DEFAULT_SCHEMA=[PSantiago]
GO
/****** Object:  User [jtompkins]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE USER [jtompkins] FOR LOGIN [ERC_DOMAIN\jtompkins] WITH DEFAULT_SCHEMA=[jtompkins]
GO
/****** Object:  User [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE USER [ERC_DOMAIN\REPORTING] FOR LOGIN [ERC_DOMAIN\Reporting] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\REPORTING]
GO
/****** Object:  User [ERC_DOMAIN\GCaporale]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE USER [ERC_DOMAIN\GCaporale] FOR LOGIN [ERC_DOMAIN\GCaporale] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\GCaporale]
GO
/****** Object:  User [AMAC]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE USER [AMAC] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[AMAC]
GO
ALTER ROLE [db_owner] ADD MEMBER [jtompkins]
GO
ALTER ROLE [db_owner] ADD MEMBER [AMAC]
GO
/****** Object:  Schema [AMAC]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE SCHEMA [AMAC]
GO
/****** Object:  Schema [ERC_DOMAIN\GCaporale]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE SCHEMA [ERC_DOMAIN\GCaporale]
GO
/****** Object:  Schema [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE SCHEMA [ERC_DOMAIN\REPORTING]
GO
/****** Object:  Schema [jtompkins]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE SCHEMA [jtompkins]
GO
/****** Object:  Schema [PSantiago]    Script Date: 2/11/2016 1:52:41 PM ******/
CREATE SCHEMA [PSantiago]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accounts](
	[Code] [int] IDENTITY(1,1) NOT NULL,
	[Agency_ID] [char](6) NULL,
	[Account_Name] [varchar](60) NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Accounts_Status]  DEFAULT (1),
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CareUsers]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CareUsers](
	[EmpID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Title] [varchar](50) NULL,
	[Facility] [varchar](50) NULL,
	[Dept] [varchar](50) NULL CONSTRAINT [DF_Employee_IsTemp]  DEFAULT (0),
	[SupervisorID] [int] NULL,
	[Active] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[Email] [varchar](50) NULL,
	[phone] [varchar](50) NULL,
	[SysPassword] [varchar](50) NOT NULL,
	[LoginName] [varchar](50) NULL,
	[FKAccountID] [int] NULL CONSTRAINT [DF_CareUsers_FKAccountID]  DEFAULT (1),
 CONSTRAINT [PK_CareUser] PRIMARY KEY CLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Employee] UNIQUE NONCLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Descr] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Category_Active]  DEFAULT (1),
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoteType]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NoteType] [varchar](50) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_NoteType_Active]  DEFAULT (1),
 CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersSupportStaff]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersSupportStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_PersSupportStaff_Active]  DEFAULT (1),
	[EmpNTLogin] [varchar](50) NULL,
 CONSTRAINT [PK_PersSupportStaff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProblemType]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProblemType](
	[ProblemTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProblemType] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ProblemType_Active]  DEFAULT (1),
	[OrderId] [int] NULL,
 CONSTRAINT [PK_ProblemType] PRIMARY KEY CLUSTERED 
(
	[ProblemTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusText] [varchar](15) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ticket](
	[TicketID] [int] IDENTITY(1,1) NOT NULL,
	[Summary] [varchar](255) NOT NULL,
	[Description] [varchar](7000) NULL,
	[FKOwnerID] [int] NOT NULL,
	[FKUserID] [int] NOT NULL,
	[FKContactID] [int] NULL,
	[TicketDateCreated] [datetime] NOT NULL CONSTRAINT [DF_Ticket_TicketDateCreated]  DEFAULT (getdate()),
	[FKStatusID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKStatus]  DEFAULT (1),
	[FKProblemTypeID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKProblemType]  DEFAULT (1),
	[FKCategoryID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKCategory]  DEFAULT (1),
	[CloseDateTarget] [datetime] NULL,
	[CloseDateActual] [datetime] NULL,
	[Priority] [char](10) NOT NULL CONSTRAINT [DF_Ticket_Priority]  DEFAULT ('Med'),
	[FKAccountID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKAccountID]  DEFAULT (1),
	[FKAgencyID] [char](10) NULL,
	[CareManager] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[subscriber_id] [varchar](20) NULL,
	[unit_id] [varchar](4) NULL,
	[PartNumber] [varchar](50) NULL,
	[SerialNumber] [varchar](255) NULL,
	[SoftwareRevision] [varchar](50) NULL,
	[RMANumField] [varchar](50) NULL,
	[trackinglabel] [varchar](50) NULL,
	[trackingequipment] [varchar](50) NULL,
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[TicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketNote]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketNote](
	[TicketNoteID] [int] IDENTITY(1,1) NOT NULL,
	[FKTicketID] [int] NOT NULL,
	[FKNoteOwnerID] [int] NOT NULL CONSTRAINT [DF_TicketNote_FKNoteOwnerID]  DEFAULT (200),
	[Description] [varchar](2000) NULL,
	[FKNoteTypeID] [int] NOT NULL CONSTRAINT [DF_TicketNote_FKNoteType]  DEFAULT (1),
	[NoteDateCreated] [datetime] NOT NULL CONSTRAINT [DF_TicketNote_NoteDateCreated]  DEFAULT (getdate()),
	[NoteDateFinished] [datetime] NOT NULL CONSTRAINT [DF_TicketNote_NoteDateFinished]  DEFAULT (getdate()),
	[TotalTime] [varchar](50) NULL CONSTRAINT [DF_TicketNote_TotalTime]  DEFAULT (0),
 CONSTRAINT [PK_TicketNote] PRIMARY KEY CLUSTERED 
(
	[TicketNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[rpt_IT_user]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE view [dbo].[rpt_IT_user]
as
select * from PersSupportStaff




GO
/****** Object:  View [dbo].[vGetTicketDetailSummary]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO




/****** Object:  View dbo.vGetTicketDetailSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  VIEW [dbo].[vGetTicketDetailSummary]
AS
SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, employee.FirstName + ' ' + employee.LastName AS Owner, 
                       dbo.Ticket.TicketDateCreated, dbo.Status.StatusText, 
                      dbo.ProblemType.ProblemType, dbo.problemtype.ProblemTypeID, dbo.Category.Descr, dbo.category.CategoryID, dbo.Ticket.CloseDateTarget, dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, 
	       dbo.Ticket.Location, dbo.Ticket.PartNumber, dbo.Ticket.SerialNumber, dbo.Ticket.SoftwareRevision, dbo.Ticket.RMANumField, dbo.Ticket.trackinglabel, dbo.ticket.trackingequipment,
	       dbo.TicketNote.TicketNoteID, 
                      dbo.TicketNote.NoteDateCreated, employee_2.FirstName + ' ' + employee_2.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
                      dbo.Ticket.FKOwnerID, dbo.NoteType.NoteType, agency.name AS Account, ticket.FKAgencyID AS AccountID, dbo.ticket.caremanager, ticket.unit_id, ticket.subscriber_id, 
                      dbo.Ticket.FKContactID, contact.contact, contact.inout, contact.reason, contact.disposition, contact.followup

FROM         helpdesk.dbo.employee employee_2 INNER JOIN
                      dbo.TicketNote ON employee_2.empID = dbo.TicketNote.FKNoteOwnerID INNER JOIN
                     dbo.NoteType ON dbo.TicketNote.FKNoteTypeID = dbo.NoteType.ID RIGHT OUTER JOIN
                      dbo.Ticket INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID INNER JOIN
                       AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id left join
   	        dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID ON dbo.TicketNote.FKTicketID = dbo.Ticket.TicketID LEFT JOIN
		amac.dbo.contact contact on contact.contact_id=ticket.FKContactID




















GO
/****** Object:  View [dbo].[vGetTicketSummary]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/****** Object:  View dbo.vGetTicketSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  VIEW [dbo].[vGetTicketSummary]
AS
SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, dbo.Ticket.FKOwnerID, dbo.Ticket.FKUserID, 
                      dbo.Ticket.TicketDateCreated, dbo.Ticket.FKStatusID, dbo.Ticket.FKProblemTypeID, dbo.Ticket.FKCategoryID, dbo.Ticket.CloseDateTarget, 
                      dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, employee.FirstName AS OwnerFirstName, employee.LastName AS OwnerLastName, 
                      dbo.Status.StatusText, dbo.ProblemType.ProblemType, ticket.FKAgencyID FKAccountID, agency.name account_name, dbo.ticket.caremanager, dbo.Ticket.Location, dbo.Ticket.PartNumber,
	        dbo.Ticket.SerialNumber, dbo.Ticket.SoftwareRevision, dbo.Ticket.RMANumField, dbo.ticket.trackinglabel, dbo.ticket.trackingequipment, ticket.unit_id, ticket.subscriber_id,
	        dbo.Ticket.FKContactID, contact.contact, contact.inout, contact.reason, contact.disposition, contact.followup
FROM         dbo.Ticket INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID left JOIN
	         dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID INNER JOIN
                      AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id LEFT JOIN
		amac.dbo.contact contact on contact.contact_id=ticket.FKContactID





















GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_ProblemType] FOREIGN KEY([FKProblemTypeID])
REFERENCES [dbo].[ProblemType] ([ProblemTypeID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_ProblemType]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Status] FOREIGN KEY([FKStatusID])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Status]
GO
ALTER TABLE [dbo].[TicketNote]  WITH CHECK ADD  CONSTRAINT [FK_TicketNote_NoteType] FOREIGN KEY([FKNoteTypeID])
REFERENCES [dbo].[NoteType] ([ID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_NoteType]
GO
ALTER TABLE [dbo].[TicketNote]  WITH NOCHECK ADD  CONSTRAINT [FK_TicketNote_Ticket] FOREIGN KEY([FKTicketID])
REFERENCES [dbo].[Ticket] ([TicketID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_Ticket]
GO
/****** Object:  StoredProcedure [dbo].[rp_SubscriberOnlineEquipmentList]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE   PROCEDURE [dbo].[rp_SubscriberOnlineEquipmentList]
  --@year varchar(4)

AS 

SET NOCOUNT ON 


declare 
	@subscriber_id  	int
	,@agency_id			varchar (50)
	,@agencyname			varchar (50)
	,@city 				varchar (50)
	,@state 				varchar (50)
	,@phone 				varchar (50)
	,@firstname			varchar (50)
	,@middleinitial		varchar (50)
	,@lastname	 		varchar (50)
	,@unit_id	 		varchar (50)
	,@status		 		varchar (50)
	,@subphone	 		varchar (50)
	,@installdate		smalldatetime
	,@item_id			varchar (15)
	,@serialNo	 		varchar (20)
	,@pendantitem_id1 	varchar (15)
	,@pendantfrequency1 	varchar (6)
	,@pendanttransmitter1	varchar (6)
	,@pendantitem_id2 	varchar (15)
	,@pendantfrequency2 	varchar (6)
	,@pendanttransmitter2	varchar (6)
	,@pendantitem_id3 	varchar (15)
	,@pendantfrequency3 	varchar (6)
	,@pendanttransmitter3	varchar (6)
	,@pendantitem_id4 	varchar (15)
	,@pendantfrequency4 	varchar (6)
	,@pendanttransmitter4	varchar (6)
	,@smokeitem_id1	 	varchar (15)
	,@smokeitem_id2	 	varchar (15)
	
	,@temppendantitem_id 		varchar (15)
	,@temppendantfrequency 		varchar (6)
	,@temppendanttransmitter	varchar (6)
	,@tempsmokeitem_id			varchar (15)

	,@pendantcount				int
	,@smokecount				int



CREATE TABLE #temp_equip
	(subscriber_id  	int
	,agency_id			varchar (50)
	,agencyname			varchar (50)
	,city 				varchar (50)
	,state 				varchar (50)
	,phone 				varchar (50)
	,firstname			varchar (50)
	,middleinitial		varchar (50)
	,lastname	 		varchar (50)
	,unit_id	 		varchar (50)
	,status		 		varchar (50)
	,subphone	 		varchar (50)
	,installdate		smalldatetime
	,item_id			varchar (15)
	,serialNo	 		varchar (20)
	,pendantitem_id1 	varchar (15)
	,pendantfrequency1 	varchar (6)
	,pendanttransmitter1	varchar (6)
	,pendantitem_id2 	varchar (15)
	,pendantfrequency2 	varchar (6)
	,pendanttransmitter2	varchar (6)
	,pendantitem_id3 	varchar (15)
	,pendantfrequency3 	varchar (6)
	,pendanttransmitter3	varchar (6)
	,pendantitem_id4 	varchar (15)
	,pendantfrequency4 	varchar (6)
	,pendanttransmitter4	varchar (6)
	,smokeitem_id1	 	varchar (15)
	,smokeitem_id2	 	varchar (15)
	,PRIMARY KEY (agency_id))
	--,UNIQUE (state, city, emp_id) )


DECLARE subscriber_cursor CURSOR FOR 
	select top 10 subscriber_id, agency_id, agencyname, city, state, phone, firstname, middleinitial, lastname,
		unit_id, status, subphone, installdate
	FROM 	subscriber
	where 	    
		Subscriber."Deleted" <> 'Y'
		and Subscriber."Unit_ID" <> 'CANC'
		and subscriber.status='active'
	order by subscriber_id

OPEN subscriber_cursor

FETCH NEXT FROM subscriber_cursor 
INTO @subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, @lastname,
		@unit_id, @status, @subphone, @installdate


WHILE @@FETCH_STATUS = 0
BEGIN

		set @pendantcount=0
		set @smokecount=0
		

		--get our PERS (console) Equipment
		select @item_id=subscriberequipment.item_id, @serialNo=serialNo
		FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id 
		where 	itemtype='pers'    




		--get our Pendants
		DECLARE pendant_cursor CURSOR FOR 
			select subscriberequipment.item_id, frequency, transmitter
			FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id
			where 	itemtype='pendant'    

			order by subscriberequipment.item_id

		OPEN pendant_cursor
		FETCH NEXT FROM pendant_cursor 
		INTO @temppendantitem_id, @temppendantfrequency, @temppendanttransmitter
		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @pendantcount = @pendantcount +1
			if @pendantcount =1 
			begin
				set @pendantitem_id1=@temppendantitem_id 
				set @pendantfrequency1=@temppendantfrequency
				set @pendanttransmitter1=@temppendanttransmitter
			end
			else if @pendantcount=2	
			begin
				set @pendantitem_id2=@temppendantitem_id 
				set @pendantfrequency2=@temppendantfrequency
				set @pendanttransmitter2=@temppendanttransmitter
			end
			else if @pendantcount=3	
			begin
				set @pendantitem_id3=@temppendantitem_id 
				set @pendantfrequency3=@temppendantfrequency
				set @pendanttransmitter3=@temppendanttransmitter
			end
			else if @pendantcount=4	
			begin
				set @pendantitem_id4=@temppendantitem_id 
				set @pendantfrequency4=@temppendantfrequency
				set @pendanttransmitter4=@temppendanttransmitter
			end

			FETCH NEXT FROM pendant_cursor 
			INTO @temppendantitem_id, @temppendantfrequency, @temppendanttransmitter
		END
		CLOSE pendant_cursor
		DEALLOCATE pendant_cursor






		--Get our smoke detector equipment
		DECLARE smoke_cursor CURSOR FOR 
			select item_id
			FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id
			where 	itemtype='smoke'    
			order by subscriberequipment.item_id
		OPEN smoke_cursor
		FETCH NEXT FROM pendant_cursor 
		INTO @tempsmokeitem_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @smokecount = @smokecount +1
			if @smokecount=1 
			begin
				set @smokeitem_id1=@tempsmokeitem_id
			end
			else if @smokecount=2	
			begin
				set @smokeitem_id2=@tempsmokeitem_id
			end
			
			FETCH NEXT FROM smoke_cursor 
			INTO @tempsmokeitem_id
		END
		CLOSE smoke_cursor
		DEALLOCATE smoke_cursor




		INSERT INTO #temp_equip 
		(subscriber_id, agency_id, agencyname, city, state, phone, firstname, middleinitial, lastname,
			unit_id, status, subphone, installdate, item_id, serialNo, pendantitem_id1, pendantfrequency1, 
			pendanttransmitter1, pendantitem_id2, pendantfrequency2, pendanttransmitter2, pendantitem_id3,
			pendantfrequency3, pendanttransmitter3, pendantitem_id4, pendantfrequency4, pendanttransmitter4, smokeitem_id1, smokeitem_id2
		)
		Values (@subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, 
				@lastname, @unit_id, @status, @subphone, @installdate, isnull(@item_id, ''),isnull(@serialNo, ''),
				isnull(@pendantitem_id1, ''), isnull(@pendantfrequency1, ''), isnull(@pendanttransmitter1, ''), 
				isnull(@pendantitem_id2, ''), isnull(@pendantfrequency2, ''), isnull(@pendanttransmitter2, ''), 
				isnull(@pendantitem_id3, ''), isnull(@pendantfrequency3, ''), isnull(@pendanttransmitter3, ''),
				isnull(@pendantitem_id4, ''), isnull(@pendantfrequency4, ''), isnull(@pendanttransmitter4, ''),
				isnull(@smokeitem_id1, ''), isnull(@smokeitem_id2 , '') 
		)
		
		
		
		
		FETCH NEXT FROM subscriber_cursor 
		INTO @subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, @lastname,
			@unit_id, @status, @subphone, @installdate
END

CLOSE subscriber_cursor
DEALLOCATE subscriber_cursor



--get our output recordset
	Select * 
	From #temp_equip order by Agency, agency_id, subscriber_id

drop table #temp_equip



GO
/****** Object:  StoredProcedure [dbo].[spEmpList]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spEmpList]

/* Returns three recordsets consisting of the id and text for names of IT people, all employees,
and status. */

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY LastName
SELECT EmpID, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY FirstName


GO
/****** Object:  StoredProcedure [dbo].[spGetAllEmps]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetAllEmps]

/* This procedure returns a list of all employees. */

AS
SET NOCOUNT ON

SELECT EmpID,  FirstName + ' ' + LastName AS EmpName
  FROM CareUsers	
  WHERE (Active = 1)
  ORDER BY FirstName

return (0)

GO
/****** Object:  StoredProcedure [dbo].[spGetEmpName]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetEmpName]
  @EmpNTLogin char(50) = '', @EmpID int = 0, @EmpName char(100) OUTPUT, 
  @IsSupervisor bit OUTPUT, @IsGod bit OUTPUT, @EmpIDout int OUTPUT, 
  @EmpNTLoginout char(50) OUTPUT

/* This procedure returns an employee's name when given their NT login name
or employee ID number. 
It also returns whether or not he or she is a supervisor and their employee ID number
(which is the primary key and NOT the employee number most employees are familiar with.)
A few other fields are returned as well.*/

AS
SET NOCOUNT ON

IF (@EmpID = 0)
/*If no employee ID number is supplied, then an NT login name MUST be supplied.  Use the NT
login name when no ID is given.*/
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @EmpIDout = EmpID, @EmpNTLoginout=@EmpNTLogin
  FROM Employee WHERE (EmpNTLogin=@EmpNTLogin)
END
ELSE
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @IsSupervisor = IsSupervisor,
    @IsGod = IsGod, @EmpIDout = EmpID, @EmpNTLoginout=EmpNTLogin
  FROM Employee WHERE (EmpID=@EmpID)
END

IF @EmpName IS NULL
     return (1)
ELSE
     return (0)

GO
/****** Object:  StoredProcedure [dbo].[spGetOptions]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spGetOptions]

/* Returns seven recordsets consisting of the id and text for names of IT people, all users, all account names, note types,
and status. */
/* The selects MUST be ordered in the order they will be used on the ASP page.  If not then the page code will not worlk  */

AS


select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
where Active=1 and (dept like 'engineer%' or dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or empNTLogin='JohnC')
 ORDER BY LastName

SELECT EmpID, FirstName, LastName FROM CareUsers
  WHERE Active=1
  ORDER BY FirstName

SELECT ProblemTypeID, ProblemType FROM ProblemType
  WHERE Active=1
 ORDER BY ProblemType

SELECT CategoryID, Descr FROM Category
  WHERE Active=1
 ORDER BY Descr

SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spGetOptions2]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



/****** Object:  Stored Procedure dbo.spGetOptions2    Script Date: 1/16/2003 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spGetOptions2]

/* Returns seven recordsets consisting of the id and text for names of IT people, all users, all account names, note types,
and status. */
/* The selects MUST be ordered in the order they will be used on the ASP page.  If not then the page code will not worlk  */

AS

select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
where Active=1 and (dept like 'engineer%' or dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'Frank' or empNTLogin='JohnC')
 ORDER BY LastName

SELECT agency_id Code, name Account_Name FROM AMAC.dbo.agency
  WHERE Deleted='N'
 ORDER BY Name



SELECT description as contact, code from amac.dbo.codes
where type='con'

select description as contactmethod, code from amac.dbo.codes
where type='fax'

SELECT description as reason, code from amac.dbo.codes
where type='ctr'

select description as disposition, code from amac.dbo.codes
where type='ctd'



SELECT ProblemTypeID, ProblemType FROM ProblemType
  WHERE Active=1
 ORDER BY OrderID

SELECT CategoryID, Descr FROM Category
  WHERE Active=1
 ORDER BY Descr

SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spValidateEmp]    Script Date: 2/11/2016 1:52:41 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spValidateEmp]
  @EmpID int, @SysPassword Varchar(30)

/* This procedure takes as input an employee id and the last 4 digits of their social
security number and checks to see if these match the database.  If they do, the
procedure returns 0.  If not, it returns 1. */

AS
SET NOCOUNT ON

IF (SELECT COUNT(*) FROM CareUsers WHERE EmpID = @EmpID AND SysPassword = @SysPassword) > 0
     return (0)
ELSE
     return (1)

GO
USE [master]
GO
ALTER DATABASE [PersSupport] SET  READ_WRITE 
GO
