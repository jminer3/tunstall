USE [master]
GO
/****** Object:  Database [GMWGSales]    Script Date: 2/11/2016 1:50:52 PM ******/
CREATE DATABASE [GMWGSales] ON  PRIMARY 
( NAME = N'GMWGSales', FILENAME = N'F:\DATA\MSSQL\Data\GMWGSales.mdf' , SIZE = 1239040KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'GMWGSales_log', FILENAME = N'G:\DATA\MSSQL\Data\GMWGSales_log.ldf' , SIZE = 2003200KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GMWGSales] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GMWGSales].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [GMWGSales] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GMWGSales] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GMWGSales] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GMWGSales] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GMWGSales] SET ARITHABORT OFF 
GO
ALTER DATABASE [GMWGSales] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GMWGSales] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GMWGSales] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GMWGSales] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GMWGSales] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GMWGSales] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GMWGSales] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GMWGSales] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GMWGSales] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GMWGSales] SET  ENABLE_BROKER 
GO
ALTER DATABASE [GMWGSales] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GMWGSales] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GMWGSales] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GMWGSales] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GMWGSales] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GMWGSales] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GMWGSales] SET RECOVERY FULL 
GO
ALTER DATABASE [GMWGSales] SET  MULTI_USER 
GO
ALTER DATABASE [GMWGSales] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GMWGSales] SET DB_CHAINING OFF 
GO
USE [GMWGSales]
GO
/****** Object:  User [ReportingSF]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [ReportingSF] FOR LOGIN [ReportingSF] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Mccoy]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [Mccoy] FOR LOGIN [Mccoy] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [JSimons]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [JSimons] FOR LOGIN [JSimons] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [isuser]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [isuser] FOR LOGIN [isuser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [gm]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [gm] FOR LOGIN [gm] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ETLADMINSVC]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [ETLADMINSVC] FOR LOGIN [ETLADMINSVC] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\rmccoy]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [ERC_DOMAIN\rmccoy] FOR LOGIN [ERC_DOMAIN\rmccoy] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [amacbusiness]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [amacbusiness] FOR LOGIN [amacbusiness] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [amac]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE USER [amac] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ReportingSF]
GO
ALTER ROLE [db_owner] ADD MEMBER [Mccoy]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Mccoy]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Mccoy]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [Mccoy]
GO
ALTER ROLE [db_datareader] ADD MEMBER [JSimons]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ETLADMINSVC]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ETLADMINSVC]
GO
ALTER ROLE [db_owner] ADD MEMBER [ERC_DOMAIN\rmccoy]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ERC_DOMAIN\rmccoy]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ERC_DOMAIN\rmccoy]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ERC_DOMAIN\rmccoy]
GO
ALTER ROLE [db_datareader] ADD MEMBER [amacbusiness]
GO
ALTER ROLE [db_datareader] ADD MEMBER [amac]
GO
/****** Object:  Synonym [dbo].[Kshema_Agency]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_Agency] FOR [amac].[dbo].[Agency]
GO
/****** Object:  Synonym [dbo].[Kshema_AgencyService]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_AgencyService] FOR [amac].[dbo].[agencyservice]
GO
/****** Object:  Synonym [dbo].[Kshema_AMACccinfo]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_AMACccinfo] FOR [amac].[dbo].[AMACccinfo]
GO
/****** Object:  Synonym [dbo].[Kshema_ap_Subscriber_i]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_ap_Subscriber_i] FOR [amac].[dbo].[ap_Subscriber_i]
GO
/****** Object:  Synonym [dbo].[Kshema_ap_SubscriberLanguage_i ]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_ap_SubscriberLanguage_i ] FOR [amac].[dbo].[ap_SubscriberLanguage_i ]
GO
/****** Object:  Synonym [dbo].[Kshema_Codes]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_Codes] FOR [amac].[dbo].[codes]
GO
/****** Object:  Synonym [dbo].[Kshema_pl2_ap_LinkSubscribers_u]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_pl2_ap_LinkSubscribers_u] FOR [amac].[dbo].[pl2_ap_LinkSubscribers_u]
GO
/****** Object:  Synonym [dbo].[Kshema_Subscriber]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_Subscriber] FOR [amac].[dbo].[subscriber]
GO
/****** Object:  Synonym [dbo].[Kshema_SubscriberResponder]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE SYNONYM [dbo].[Kshema_SubscriberResponder] FOR [amac].[dbo].[SubscriberResponder]
GO
/****** Object:  UserDefinedFunction [dbo].[fnRemoveNonNumericCharacters]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[fnRemoveNonNumericCharacters](@strText VARCHAR(1000))
RETURNS VARCHAR(1000)
AS
BEGIN
    WHILE PATINDEX('%[^0-9]%', @strText) > 0
    BEGIN
        SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '')
    END
    RETURN @strText
END

GO
/****** Object:  Table [dbo].[AgencyPromoCodes]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AgencyPromoCodes](
	[Agency_ID] [char](6) NOT NULL,
	[PromoCode_ID] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contact]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact](
	[Contact_ID] [int] IDENTITY(1,1) NOT NULL,
	[Company] [char](10) NULL,
	[impACTIVFEE] [char](3) NULL,
	[impADDQUEST] [char](3) NULL,
	[impANI] [char](20) NULL,
	[impCALLADD1] [char](30) NULL,
	[impCALLADD2] [char](30) NULL,
	[impCALLBACK] [char](3) NULL,
	[impCALLBDATE] [char](8) NULL,
	[impCALLBTIME] [char](7) NULL,
	[impCALLBZONE] [char](30) NULL,
	[impCALLEMAIL] [char](30) NULL,
	[impCALLFNAME] [varchar](15) NULL,
	[impCALLLNAME] [varchar](15) NULL,
	[Phone1] [char](25) NULL,
	[UCARDNUM1] [char](16) NULL,
	[UCARDNUM2] [char](16) NULL,
	[impCAUSE] [char](60) NULL,
	[UCCAdd1_1] [char](30) NULL,
	[UCCAdd1_2] [char](30) NULL,
	[UCCAdd2_1] [char](30) NULL,
	[UCCAdd2_2] [char](30) NULL,
	[impCCADDR] [char](25) NULL,
	[impCCADDR2] [char](25) NULL,
	[UCCAgree] [char](3) NULL,
	[UCCNAME1] [char](30) NULL,
	[UCCNAME2] [char](30) NULL,
	[City] [char](25) NULL,
	[userdef56] [char](25) NULL,
	[UCCCity4] [char](25) NULL,
	[UCCCity5] [char](25) NULL,
	[userdef84] [char](25) NULL,
	[impCITY7] [char](25) NULL,
	[UserDef89] [char](60) NULL,
	[UserDef90] [char](60) NULL,
	[UserDef91] [char](60) NULL,
	[USERDEF92] [char](60) NULL,
	[impCOMMENT5] [char](60) NULL,
	[impCOMMENT6] [char](60) NULL,
	[Uconfigure] [char](15) NULL,
	[UCreditCr1] [char](30) NULL,
	[UCreditCr2] [char](30) NULL,
	[UCVVCD1] [char](4) NULL,
	[UCVVCD2] [char](4) NULL,
	[UPinDate] [datetime] NULL,
	[impDDACCT] [char](25) NULL,
	[impDDBANK] [char](30) NULL,
	[impDDNAME] [char](30) NULL,
	[impDDROUTING] [char](25) NULL,
	[impDDVOID] [char](3) NULL,
	[UDID] [varchar](10) NULL,
	[impDIFPAYMENT] [char](3) NULL,
	[impEMPLOYNAM] [char](30) NULL,
	[impEMPLOYNUM] [char](10) NULL,
	[Uexp_DT1] [char](5) NULL,
	[Uexp_DT2] [char](5) NULL,
	[UserDef77] [char](25) NULL,
	[UINSTFEE] [char](3) NULL,
	[impINTPAYMENT] [char](15) NULL,
	[impLEADQUAL] [char](60) NULL,
	[UserDef61] [char](15) NULL,
	[impLITEMAIL] [char](3) NULL,
	[impMAILADD1] [char](30) NULL,
	[impMAILADD2] [char](30) NULL,
	[impMAILADDR] [char](25) NULL,
	[impMAILEMAIL] [char](30) NULL,
	[impMAILFNAME] [char](15) NULL,
	[impMAILINFO] [char](3) NULL,
	[impMAILLNAME] [char](15) NULL,
	[impOPER_ID] [char](4) NULL,
	[impORDEMAIL] [char](3) NULL,
	[impORDER_NUM] [char](12) NULL,
	[impORDERNOW] [char](3) NULL,
	[impPAYMENT] [char](25) NULL,
	[impPRODUCTFOR] [char](25) NULL,
	[UPROFINST] [char](3) NULL,
	[Userdef01] [char](45) NULL,
	[Title] [char](20) NULL,
	[UserDef63] [char](25) NULL,
	[impRESPEMAIL] [char](30) NULL,
	[impRESPFNAME] [char](15) NULL,
	[UWGRPHN1] [char](25) NULL,
	[UWGRKEY] [char](3) NULL,
	[impRESPLNAME] [char](15) NULL,
	[impRESPONDER] [char](10) NULL,
	[UWGRRELTN] [char](20) NULL,
	[UserDef87] [char](3) NULL,
	[userdef82] [char](30) NULL,
	[userdef83] [char](30) NULL,
	[userdef79] [char](25) NULL,
	[UserDef88] [char](3) NULL,
	[userdef80] [char](15) NULL,
	[userdef81] [char](15) NULL,
	[State] [char](2) NULL,
	[userdef57] [char](2) NULL,
	[UCCState4] [char](2) NULL,
	[UCCState5] [char](2) NULL,
	[userdef85] [char](2) NULL,
	[impSTATE7] [char](2) NULL,
	[impSTATION] [char](3) NULL,
	[UserDef11] [char](10) NULL,
	[userdef53] [char](30) NULL,
	[userdef55] [char](30) NULL,
	[userdef60] [char](30) NULL,
	[userdef50] [char](15) NULL,
	[userdef59] [char](30) NULL,
	[userdef51] [char](15) NULL,
	[userdef52] [char](25) NULL,
	[impTAS_CSN] [char](10) NULL,
	[UPinTIme] [char](8) NULL,
	[Zip] [char](10) NULL,
	[userdef58] [char](10) NULL,
	[UCCZip4] [char](10) NULL,
	[UCCZip5] [char](10) NULL,
	[UserDef86] [char](10) NULL,
	[impZIP7] [char](10) NULL,
	[impSold] [char](1) NULL,
	[impStatus] [char](10) NULL,
	[impCloseDateT] [datetime] NULL,
	[impEnteredKsh] [datetime] NULL,
	[impAMACNotes] [varchar](200) NULL,
	[impPrinted] [char](1) NULL,
	[impImportdate] [datetime] NULL,
	[impDIDMap] [char](20) NULL,
	[impRADPROG] [char](30) NULL,
	[impREFBY] [char](30) NULL,
	[impCCVERIFY1] [char](3) NULL,
	[impCCVERIFY2] [char](3) NULL,
	[USUBFNAME2] [char](15) NULL,
	[USUBLNAME2] [char](15) NULL,
	[Uconfig2] [char](15) NULL,
	[Utype] [char](15) NULL,
	[USUBSEC] [char](3) NULL,
	[impGENDER] [char](10) NULL,
	[UShipType] [char](20) NULL,
	[UEMPDISC] [char](3) NULL,
	[impEPN] [char](9) NULL,
	[UTVSTA] [char](20) NULL,
	[impOperName] [char](50) NULL,
	[userdef95] [char](6) NULL,
	[UValidSub] [char](3) NULL,
	[URefSub] [char](9) NULL,
	[UCoupon] [char](6) NULL,
	[impNotes] [text] NULL,
	[Key3] [varchar](50) NULL,
	[accountno] [varchar](50) NULL,
	[Usubs] [varchar](6) NULL,
	[CreateOn] [datetime] NULL,
	[CreateAt] [varchar](10) NULL,
	[Contact] [varchar](75) NULL,
	[UWGRNAME] [varchar](75) NULL,
	[usubs2] [varchar](6) NULL,
	[Key4] [varchar](50) NULL,
	[Key1] [varchar](50) NULL,
	[UCCContactPhone1] [char](25) NULL,
	[UCCContactPhone2] [char](25) NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contact_Test]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact_Test](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[SubscriberID__c] [nvarchar](6) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DataDictionary]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataDictionary](
	[ID] [int] NOT NULL,
	[Value] [varchar](100) NULL,
	[Text] [varchar](100) NULL,
	[Agency] [varchar](100) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Leads]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Leads](
	[Lead_ID] [int] IDENTITY(1,1) NOT NULL,
	[LeadCreateDate] [datetime] NULL,
	[LeadCreateBy] [varchar](50) NULL,
	[LeadLastModDate] [datetime] NULL,
	[LeadLastModBy] [varchar](50) NULL,
	[SubscriberAddedToKshema] [datetime] NULL,
	[Status] [varchar](25) NULL,
	[ActivationKit] [int] NULL,
	[Agency_ID] [char](6) NULL,
	[Subscriber_ID] [char](6) NULL,
	[CallerFirstName] [varchar](15) NULL,
	[CallerLastName] [varchar](15) NULL,
	[CallerPhone] [varchar](25) NULL,
	[CallerRelation] [varchar](20) NULL,
	[CallerAddress1] [varchar](30) NULL,
	[CallerAddress2] [varchar](30) NULL,
	[CallerCity] [varchar](25) NULL,
	[CallerState] [char](2) NULL,
	[CallerZip] [varchar](10) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubFirstName] [varchar](20) NULL,
	[SubLastName] [varchar](30) NULL,
	[SubPhone] [varchar](10) NULL,
	[SubAddress1] [varchar](30) NULL,
	[SubAddress2] [varchar](30) NULL,
	[SubCity] [varchar](20) NULL,
	[SubState] [char](2) NULL,
	[SubZip] [char](6) NULL,
	[SubZipPlus4] [char](4) NULL,
	[SubLanguage] [char](6) NULL,
	[SubLineType] [varchar](15) NULL,
	[RespName] [varchar](50) NULL,
	[RespRelation] [char](6) NULL,
	[RespHomePhone] [varchar](60) NULL,
	[RespCellPhone] [varchar](25) NULL,
	[RespHasKey] [int] NULL,
	[ProdModel] [varchar](15) NULL,
	[ProdMonthlyRate] [smallmoney] NULL,
	[ProdActivationFee] [smallmoney] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [int] NULL,
	[POptLockbox] [bit] NOT NULL CONSTRAINT [DF_Leads_POptLockbox]  DEFAULT ((0)),
	[POptLockboxPurchase] [bit] NOT NULL CONSTRAINT [DF_Leads_POptLockboxPurchase]  DEFAULT ((0)),
	[ProfInstall] [int] NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [smallmoney] NULL,
	[ShipAddrSameAs] [int] NULL,
	[ShipFirstName] [varchar](15) NULL,
	[ShipLastName] [varchar](15) NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [char](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCCardType] [varchar](30) NULL,
	[CCName] [varchar](30) NULL,
	[CCNumber] [varchar](16) NULL,
	[CCExpDate] [char](5) NULL,
	[CCCVV] [varchar](4) NULL,
	[CCAddrSameAs] [varchar](25) NULL,
	[CCContactPhone] [varchar](25) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCAddress2] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [char](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[InstallContactPhone] [varchar](25) NULL,
	[InstallContactComment] [varchar](100) NULL,
	[Comment] [varchar](500) NULL,
	[SBTSONO] [char](6) NULL,
	[SBTSODATE] [datetime] NULL,
	[iVi1] [int] NULL,
	[iVi1Height] [decimal](6, 2) NULL,
	[iVi2] [int] NULL,
	[iVi2Height] [decimal](6, 2) NULL,
	[CallerEmail] [varchar](50) NULL,
	[SubEmail] [varchar](50) NULL,
	[Sub2Subscriber_ID] [char](6) NULL,
	[Sub2SubscriberAddedToKshema] [datetime] NULL,
	[Sub2FirstName] [varchar](20) NULL,
	[Sub2LastName] [varchar](20) NULL,
	[Sub2Language] [char](6) NULL,
	[PromoCode] [nvarchar](50) NULL,
	[PromoCodeExpiration] [datetime] NULL,
	[PromoCodeID] [int] NULL,
	[PromoCodeDiscountDollars] [decimal](5, 2) NULL,
	[PromoCodeDiscountPercent] [int] NULL,
 CONSTRAINT [PK_SalesLeads] PRIMARY KEY CLUSTERED 
(
	[Lead_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Leads_20151217]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Leads_20151217](
	[Lead_ID] [int] IDENTITY(1,1) NOT NULL,
	[LeadCreateDate] [datetime] NULL,
	[LeadCreateBy] [varchar](50) NULL,
	[LeadLastModDate] [datetime] NULL,
	[LeadLastModBy] [varchar](50) NULL,
	[SubscriberAddedToKshema] [datetime] NULL,
	[Status] [varchar](25) NULL,
	[ActivationKit] [int] NULL,
	[Agency_ID] [char](6) NULL,
	[Subscriber_ID] [char](6) NULL,
	[CallerFirstName] [varchar](15) NULL,
	[CallerLastName] [varchar](15) NULL,
	[CallerPhone] [varchar](25) NULL,
	[CallerRelation] [varchar](20) NULL,
	[CallerAddress1] [varchar](30) NULL,
	[CallerAddress2] [varchar](30) NULL,
	[CallerCity] [varchar](25) NULL,
	[CallerState] [char](2) NULL,
	[CallerZip] [varchar](10) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubFirstName] [varchar](20) NULL,
	[SubLastName] [varchar](30) NULL,
	[SubPhone] [varchar](10) NULL,
	[SubAddress1] [varchar](30) NULL,
	[SubAddress2] [varchar](30) NULL,
	[SubCity] [varchar](20) NULL,
	[SubState] [char](2) NULL,
	[SubZip] [char](6) NULL,
	[SubZipPlus4] [char](4) NULL,
	[SubLanguage] [char](6) NULL,
	[SubLineType] [varchar](15) NULL,
	[RespName] [varchar](50) NULL,
	[RespRelation] [char](6) NULL,
	[RespHomePhone] [varchar](60) NULL,
	[RespCellPhone] [varchar](25) NULL,
	[RespHasKey] [int] NULL,
	[ProdModel] [varchar](15) NULL,
	[ProdMonthlyRate] [smallmoney] NULL,
	[ProdActivationFee] [smallmoney] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [int] NULL,
	[POptLockbox] [int] NULL,
	[ProfInstall] [int] NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [smallmoney] NULL,
	[ShipAddrSameAs] [int] NULL,
	[ShipFirstName] [varchar](15) NULL,
	[ShipLastName] [varchar](15) NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [char](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCCardType] [varchar](30) NULL,
	[CCName] [varchar](30) NULL,
	[CCNumber] [varchar](16) NULL,
	[CCExpDate] [char](5) NULL,
	[CCCVV] [varchar](4) NULL,
	[CCAddrSameAs] [varchar](25) NULL,
	[CCContactPhone] [varchar](25) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCAddress2] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [char](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[InstallContactPhone] [varchar](25) NULL,
	[InstallContactComment] [varchar](100) NULL,
	[Comment] [varchar](500) NULL,
	[SBTSONO] [char](6) NULL,
	[SBTSODATE] [datetime] NULL,
	[iVi1] [int] NULL,
	[iVi1Height] [decimal](6, 2) NULL,
	[iVi2] [int] NULL,
	[iVi2Height] [decimal](6, 2) NULL,
	[CallerEmail] [varchar](50) NULL,
	[SubEmail] [varchar](50) NULL,
	[Sub2Subscriber_ID] [char](6) NULL,
	[Sub2SubscriberAddedToKshema] [datetime] NULL,
	[Sub2FirstName] [varchar](20) NULL,
	[Sub2LastName] [varchar](20) NULL,
	[Sub2Language] [char](6) NULL,
	[PromoCode] [nvarchar](50) NULL,
	[PromoCodeExpiration] [datetime] NULL,
	[PromoCodeID] [int] NULL,
	[PromoCodeDiscountDollars] [decimal](5, 2) NULL,
	[PromoCodeDiscountPercent] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OLE DB Destination]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OLE DB Destination](
	[ContactRecID] [nvarchar](18) NULL,
	[ContactCreateDate] [datetime] NULL,
	[ContractCreateBy] [varchar](50) NULL,
	[ContactLastModDate] [datetime] NULL,
	[ContractLastModBy] [varchar](50) NULL,
	[ActivationKit] [varchar](255) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[FirstName] [nvarchar](20) NULL,
	[LastName] [nvarchar](30) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[Address1] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[StateProv] [varchar](2) NULL,
	[Zip] [varchar](10) NULL,
	[Phone] [varchar](1000) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [varchar](1000) NULL,
	[HomePhone] [varchar](1000) NULL,
	[OtherPhone] [varchar](1000) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubLanguage] [varchar](25) NULL,
	[SubLineType] [varchar](15) NULL,
	[Caller] [bit] NULL,
	[SecondaryContact] [bit] NULL,
	[Responder] [bit] NULL,
	[PrimaryContact] [varchar](18) NULL,
	[RespRelation] [varchar](20) NULL,
	[RespHasKey] [int] NULL,
	[ProdModel] [varchar](25) NULL,
	[ProdMonthlyRate] [float] NULL,
	[ProdActivationFee] [float] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [varchar](15) NULL,
	[POptLockbox] [varchar](15) NULL,
	[ProfInstall] [varchar](15) NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [money] NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [varchar](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCName] [varchar](30) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [varchar](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[CCCardType] [binary](128) NULL,
	[CCNumber] [binary](128) NULL,
	[CCCVV] [binary](128) NULL,
	[CCExpMonth] [varchar](2) NULL,
	[CCExpYear] [varchar](4) NULL,
	[ShipToName] [nvarchar](255) NULL,
	[CouponCode] [nvarchar](255) NULL,
	[ReferrerName] [nvarchar](255) NULL,
	[ReferralCode] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration] [nvarchar](255) NULL,
	[Billing_Start_Date__c] [datetime] NULL,
	[Additional_Activator_Fee] [float] NULL,
	[Additional_User_Fee] [float] NULL,
	[iVi_Height] [varchar](20) NULL,
	[Key_Storage_Lock_Fee] [float] NULL,
	[Primary_User_Fee] [float] NULL,
	[Prof_Install_Fee] [float] NULL,
	[Safety_Pendant_iVi] [varchar](15) NULL,
	[iVi_Pendant_Additional_User_Fee] [float] NULL,
	[iVi_Pendant_Primary_Fee] [float] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PromoCodes]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromoCodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[DurationDate] [datetime] NULL,
	[DurationDays] [int] NULL,
	[DurationMonths] [int] NULL,
	[DiscountDollars] [decimal](5, 2) NULL,
	[DiscountPercent] [int] NULL,
	[DiscountItem] [nvarchar](50) NULL,
	[AllAgencies] [bit] NOT NULL CONSTRAINT [DF_PromoCodes_AllAgencies]  DEFAULT ((0)),
 CONSTRAINT [PK_PromoCodes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SBTSOITEMS]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SBTSOITEMS](
	[Agency_ID] [char](6) NOT NULL,
	[ProdModel] [varchar](25) NOT NULL,
	[ItemNo] [varchar](15) NOT NULL,
	[ItemGroup] [varchar](4) NOT NULL,
	[Sequence] [tinyint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SF_ACCOUNT_AGENCY_MAP]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SF_ACCOUNT_AGENCY_MAP](
	[SF_ACCOUNTID] [nvarchar](18) NOT NULL,
	[AGENCY_ID] [char](6) NOT NULL,
	[Name] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SF_CC]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SF_CC](
	[Id] [nvarchar](18) NULL,
	[CCType] [varbinary](128) NULL,
	[CCNumber] [varbinary](128) NULL,
	[CVV] [varbinary](128) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Exp_Month__c] [char](2) NULL,
	[Exp_Year__c] [char](4) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SF_CONTACT]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_CONTACT](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL,
	[Additional_Extra_Activator_Fee__c] [float] NULL,
	[Additional_User_Fee__c] [float] NULL,
	[Height_inches__c] [float] NULL,
	[Key_Storage_Lockbox_Price__c] [float] NULL,
	[Primary_User_Fee__c] [float] NULL,
	[Professional_Installation_Fee__c] [float] NULL,
	[Safety_Pendant_iVi__c] [nvarchar](255) NULL,
	[iVi_Pendant_Additional_User__c] [float] NULL,
	[iVi_Pendant_Primary_User__c] [float] NULL,
	[Promo_Code__c] [nvarchar](255) NULL,
	[Promo_Code_Item_ID__c] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SF_Contact_all]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SF_Contact_all](
	[ContactRecID] [varchar](18) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SF_CONTACT_bk]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_CONTACT_bk](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL,
	[Additional_Extra_Activator_Fee__c] [float] NULL,
	[Additional_User_Fee__c] [float] NULL,
	[Height_inches__c] [float] NULL,
	[Key_Storage_Lockbox_Price__c] [float] NULL,
	[Primary_User_Fee__c] [float] NULL,
	[Professional_Installation_Fee__c] [float] NULL,
	[Safety_Pendant_iVi__c] [nvarchar](255) NULL,
	[iVi_Pendant_Additional_User__c] [float] NULL,
	[iVi_Pendant_Primary_User__c] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SF_CONTACT_COMPARE]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_CONTACT_COMPARE](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SF_CONTACT_OLD]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_CONTACT_OLD](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SF_CONTACT20150120]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_CONTACT20150120](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SF_ContactSubscriber]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SF_ContactSubscriber](
	[ContactRecID] [varchar](18) NOT NULL,
	[Subscriber_ID] [char](6) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SF_USERS]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SF_USERS](
	[Id] [nvarchar](18) NULL,
	[Username] [nvarchar](80) NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[CompanyName] [nvarchar](80) NULL,
	[Division] [nvarchar](80) NULL,
	[Department] [nvarchar](80) NULL,
	[Title] [nvarchar](80) NULL,
	[Street] [nvarchar](255) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](80) NULL,
	[PostalCode] [nvarchar](20) NULL,
	[Country] [nvarchar](80) NULL,
	[Email] [nvarchar](128) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[Alias] [nvarchar](8) NULL,
	[CommunityNickname] [nvarchar](40) NULL,
	[IsActive] [bit] NULL,
	[TimeZoneSidKey] [nvarchar](40) NULL,
	[UserRoleId] [nvarchar](18) NULL,
	[LocaleSidKey] [nvarchar](40) NULL,
	[ReceivesInfoEmails] [bit] NULL,
	[ReceivesAdminInfoEmails] [bit] NULL,
	[EmailEncodingKey] [nvarchar](40) NULL,
	[ProfileId] [nvarchar](18) NULL,
	[UserType] [nvarchar](40) NULL,
	[LanguageLocaleKey] [nvarchar](40) NULL,
	[EmployeeNumber] [nvarchar](20) NULL,
	[DelegatedApproverId] [nvarchar](18) NULL,
	[ManagerId] [nvarchar](18) NULL,
	[LastLoginDate] [datetime] NULL,
	[LastPasswordChangeDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[OfflineTrialExpirationDate] [datetime] NULL,
	[OfflinePdaTrialExpirationDate] [datetime] NULL,
	[UserPermissionsMarketingUser] [bit] NULL,
	[UserPermissionsOfflineUser] [bit] NULL,
	[UserPermissionsAvantgoUser] [bit] NULL,
	[UserPermissionsCallCenterAutoLogin] [bit] NULL,
	[UserPermissionsMobileUser] [bit] NULL,
	[UserPermissionsSFContentUser] [bit] NULL,
	[UserPermissionsInteractionUser] [bit] NULL,
	[UserPermissionsSupportUser] [bit] NULL,
	[UserPermissionsSiteforceContributorUser] [bit] NULL,
	[UserPermissionsSiteforcePublisherUser] [bit] NULL,
	[UserPermissionsChatterAnswersUser] [bit] NULL,
	[ForecastEnabled] [bit] NULL,
	[UserPreferencesActivityRemindersPopup] [bit] NULL,
	[UserPreferencesEventRemindersCheckboxDefault] [bit] NULL,
	[UserPreferencesTaskRemindersCheckboxDefault] [bit] NULL,
	[UserPreferencesReminderSoundOff] [bit] NULL,
	[UserPreferencesDisableAllFeedsEmail] [bit] NULL,
	[UserPreferencesDisableFollowersEmail] [bit] NULL,
	[UserPreferencesDisableProfilePostEmail] [bit] NULL,
	[UserPreferencesDisableChangeCommentEmail] [bit] NULL,
	[UserPreferencesDisableLaterCommentEmail] [bit] NULL,
	[UserPreferencesDisProfPostCommentEmail] [bit] NULL,
	[UserPreferencesDisableAutoSubForFeeds] [bit] NULL,
	[UserPreferencesApexPagesDeveloperMode] [bit] NULL,
	[UserPreferencesHideCSNGetChatterMobileTask] [bit] NULL,
	[UserPreferencesDisableMentionsPostEmail] [bit] NULL,
	[UserPreferencesDisMentionsCommentEmail] [bit] NULL,
	[UserPreferencesHideCSNDesktopTask] [bit] NULL,
	[UserPreferencesDisCommentAfterLikeEmail] [bit] NULL,
	[UserPreferencesDisableLikeEmail] [bit] NULL,
	[UserPreferencesDisableMessageEmail] [bit] NULL,
	[UserPreferencesOptOutOfTouch] [bit] NULL,
	[UserPreferencesDisableBookmarkEmail] [bit] NULL,
	[UserPreferencesDisableSharePostEmail] [bit] NULL,
	[ContactId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[CallCenterId] [nvarchar](18) NULL,
	[Extension] [nvarchar](40) NULL,
	[FederationIdentifier] [nvarchar](512) NULL,
	[AboutMe] [nvarchar](1000) NULL,
	[CurrentStatus] [nvarchar](1000) NULL,
	[FullPhotoUrl] [nvarchar](1024) NULL,
	[SmallPhotoUrl] [nvarchar](1024) NULL,
	[DigestFrequency] [nvarchar](40) NULL,
	[DefaultGroupNotificationFrequency] [nvarchar](40) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SFContactStage]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SFContactStage](
	[ContactRecID] [varchar](18) NOT NULL,
	[ContactCreateDate] [datetime] NULL,
	[ContactCreateBy] [varchar](50) NULL,
	[ContactLastModDate] [datetime] NULL,
	[ContactLastModBy] [varchar](50) NULL,
	[SubscriberAddedToKshema] [datetime] NULL,
	[ActivationKit] [varchar](255) NULL,
	[Agency_ID] [char](6) NULL,
	[Subscriber_ID] [char](6) NULL,
	[FirstName] [varchar](20) NULL,
	[LastName] [varchar](30) NULL,
	[Caller] [bit] NULL,
	[Responder] [bit] NULL,
	[RespRelation] [varchar](20) NULL,
	[RespHasKey] [int] NULL,
	[SecondaryContact] [bit] NULL,
	[PrimaryContact] [varchar](18) NULL,
	[HomePhone] [varchar](25) NULL,
	[MobilePhone] [varchar](25) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[StateProv] [char](2) NULL,
	[Zip] [varchar](10) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubLanguage] [varchar](25) NULL,
	[SubLineType] [varchar](15) NULL,
	[ProdModel] [varchar](25) NULL,
	[ProdMonthlyRate] [float] NULL,
	[ProdActivationFee] [float] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [varchar](15) NULL,
	[POptLockbox] [varchar](15) NULL,
	[ProfInstall] [varchar](15) NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [smallmoney] NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [char](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCCardType] [varbinary](128) NULL,
	[CCName] [varchar](30) NULL,
	[CCNumber] [varbinary](128) NULL,
	[CCExpMonth] [char](2) NULL,
	[CCExpYear] [char](4) NULL,
	[CCCVV] [varbinary](128) NULL,
	[CCContactPhone] [varchar](25) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCAddress2] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [char](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[Comment] [varchar](500) NULL,
	[SBTSONO] [char](6) NULL,
	[SBTSODATE] [datetime] NULL,
	[Phone] [varchar](25) NULL,
	[OtherPhone] [varchar](25) NULL,
	[SubscriberResponderID] [bigint] NULL,
	[Status] [varchar](20) NULL,
	[ShipToName] [varchar](255) NULL,
	[CouponCode] [varchar](255) NULL,
	[ReferrerName] [varchar](255) NULL,
	[ReferralCode] [varchar](50) NULL,
	[ShippingPhone] [varchar](25) NULL,
	[Additional_Extra_Activator_Configuration] [varchar](255) NULL,
	[BillingStartDate] [datetime] NULL,
	[Primary_User_Fee] [float] NULL,
	[Additional_User_Fee] [float] NULL,
	[Additional_Activator_Fee] [float] NULL,
	[iVi_Pendant_Primary_Fee] [float] NULL,
	[iVi_Pendant_Additional_User_Fee] [float] NULL,
	[Key_Storage_Lock_Fee] [float] NULL,
	[Prof_Install_Fee] [float] NULL,
	[iVi_Height] [varchar](20) NULL,
	[Safety_Pendant_iVi] [varchar](15) NULL,
	[Promo_Code] [varchar](20) NULL,
	[Promo_Code_Item_Id] [varchar](20) NULL,
 CONSTRAINT [PK_SFContactStage] PRIMARY KEY CLUSTERED 
(
	[ContactRecID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SFContactStage_bk]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SFContactStage_bk](
	[ContactRecID] [varchar](18) NOT NULL,
	[ContactCreateDate] [datetime] NULL,
	[ContactCreateBy] [varchar](50) NULL,
	[ContactLastModDate] [datetime] NULL,
	[ContactLastModBy] [varchar](50) NULL,
	[SubscriberAddedToKshema] [datetime] NULL,
	[ActivationKit] [varchar](255) NULL,
	[Agency_ID] [char](6) NULL,
	[Subscriber_ID] [char](6) NULL,
	[FirstName] [varchar](20) NULL,
	[LastName] [varchar](30) NULL,
	[Caller] [bit] NULL,
	[Responder] [bit] NULL,
	[RespRelation] [varchar](20) NULL,
	[RespHasKey] [int] NULL,
	[SecondaryContact] [bit] NULL,
	[PrimaryContact] [varchar](18) NULL,
	[HomePhone] [varchar](25) NULL,
	[MobilePhone] [varchar](25) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[StateProv] [char](2) NULL,
	[Zip] [varchar](10) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubLanguage] [varchar](25) NULL,
	[SubLineType] [varchar](15) NULL,
	[ProdModel] [varchar](25) NULL,
	[ProdMonthlyRate] [float] NULL,
	[ProdActivationFee] [float] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [varchar](15) NULL,
	[POptLockbox] [varchar](15) NULL,
	[ProfInstall] [varchar](15) NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [smallmoney] NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [char](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCCardType] [varbinary](128) NULL,
	[CCName] [varchar](30) NULL,
	[CCNumber] [varbinary](128) NULL,
	[CCExpMonth] [char](2) NULL,
	[CCExpYear] [char](4) NULL,
	[CCCVV] [varbinary](128) NULL,
	[CCContactPhone] [varchar](25) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCAddress2] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [char](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[Comment] [varchar](500) NULL,
	[SBTSONO] [char](6) NULL,
	[SBTSODATE] [datetime] NULL,
	[Phone] [varchar](25) NULL,
	[OtherPhone] [varchar](25) NULL,
	[SubscriberResponderID] [bigint] NULL,
	[Status] [varchar](20) NULL,
	[ShipToName] [varchar](255) NULL,
	[CouponCode] [varchar](255) NULL,
	[ReferrerName] [varchar](255) NULL,
	[ReferralCode] [varchar](50) NULL,
	[ShippingPhone] [varchar](25) NULL,
	[Additional_Extra_Activator_Configuration] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SFContactStage_New]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SFContactStage_New](
	[ContactRecID] [varchar](18) NOT NULL,
	[ContactCreateDate] [datetime] NULL,
	[ContactCreateBy] [varchar](50) NULL,
	[ContactLastModDate] [datetime] NULL,
	[ContactLastModBy] [varchar](50) NULL,
	[SubscriberAddedToKshema] [datetime] NULL,
	[ActivationKit] [varchar](255) NULL,
	[Agency_ID] [char](6) NULL,
	[Subscriber_ID] [char](6) NULL,
	[FirstName] [varchar](20) NULL,
	[LastName] [varchar](30) NULL,
	[Caller] [bit] NULL,
	[Responder] [bit] NULL,
	[RespRelation] [varchar](20) NULL,
	[RespHasKey] [int] NULL,
	[SecondaryContact] [bit] NULL,
	[PrimaryContact] [varchar](18) NULL,
	[HomePhone] [varchar](25) NULL,
	[MobilePhone] [varchar](25) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[City] [varchar](25) NULL,
	[StateProv] [char](2) NULL,
	[Zip] [varchar](10) NULL,
	[HowHear] [varchar](60) NULL,
	[ProductFor] [varchar](25) NULL,
	[ReasonForCall] [varchar](45) NULL,
	[StoreCode] [varchar](10) NULL,
	[ReceiptNum] [varchar](25) NULL,
	[SubLanguage] [varchar](25) NULL,
	[SubLineType] [varchar](15) NULL,
	[ProdModel] [varchar](25) NULL,
	[ProdMonthlyRate] [float] NULL,
	[ProdActivationFee] [float] NULL,
	[ProdConfigure] [varchar](15) NULL,
	[POpt2User] [int] NULL,
	[POpt2UserConfigure] [varchar](15) NULL,
	[POpt2Activator] [varchar](15) NULL,
	[POptLockbox] [varchar](15) NULL,
	[ProfInstall] [varchar](15) NULL,
	[ShipMethod] [varchar](25) NULL,
	[ShipPrice] [smallmoney] NULL,
	[ShipAddress1] [varchar](30) NULL,
	[ShipAddress2] [varchar](30) NULL,
	[ShipCity] [varchar](25) NULL,
	[ShipState] [char](2) NULL,
	[ShipZip] [varchar](10) NULL,
	[CCFrequency] [varchar](25) NULL,
	[CCCardType] [varbinary](128) NULL,
	[CCName] [varchar](30) NULL,
	[CCNumber] [varbinary](128) NULL,
	[CCExpMonth] [char](2) NULL,
	[CCExpYear] [char](4) NULL,
	[CCCVV] [varbinary](128) NULL,
	[CCContactPhone] [varchar](25) NULL,
	[CCAddress1] [varchar](30) NULL,
	[CCAddress2] [varchar](30) NULL,
	[CCCity] [varchar](25) NULL,
	[CCState] [char](2) NULL,
	[CCZip] [varchar](10) NULL,
	[CCAgree] [int] NULL,
	[InstallContactName] [varchar](50) NULL,
	[Comment] [varchar](500) NULL,
	[SBTSONO] [char](6) NULL,
	[SBTSODATE] [datetime] NULL,
	[Phone] [varchar](25) NULL,
	[OtherPhone] [varchar](25) NULL,
	[SubscriberResponderID] [bigint] NULL,
	[Status] [varchar](20) NULL,
	[ShipToName] [varchar](255) NULL,
	[CouponCode] [varchar](255) NULL,
	[ReferrerName] [varchar](255) NULL,
	[ReferralCode] [varchar](50) NULL,
	[ShippingPhone] [varchar](25) NULL,
	[Additional_Extra_Activator_Configuration] [varchar](255) NULL,
	[BillingStartDate] [datetime] NULL,
	[Primary_User_Fee] [float] NULL,
	[Additional_User_Fee] [float] NULL,
	[Additional_Activator_Fee] [float] NULL,
	[iVi_Pendant_Primary_Fee] [float] NULL,
	[iVi_Pendant_Additional_User_Fee] [float] NULL,
	[Key_Storage_Lock_Fee] [float] NULL,
	[Prof_Install_Fee] [float] NULL,
	[iVi_Height] [varchar](20) NULL,
	[Safety_Pendant_iVi] [varchar](15) NULL,
	[Promo_Code] [varchar](20) NULL,
	[Promo_Code_Item_Id] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp](
	[ContactRecID] [varchar](18) NULL,
	[SubscriberID__c] [varchar](6) NULL,
	[SubscriberResponderID] [bigint] NULL,
	[SBT_Order_Number__c] [varchar](6) NULL,
	[status] [varchar](20) NULL,
	[installdate] [datetime] NULL,
	[RemovalDate] [datetime] NULL,
	[OnlineSince] [datetime] NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[Id] [nvarchar](18) NULL,
	[ErrorDescription] [nvarchar](1024) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[test_contact]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[test_contact](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL,
	[Billing_Start_Date__c] [datetime] NULL,
	[Additional_Extra_Activator_Fee__c] [float] NULL,
	[Additional_User_Fee__c] [float] NULL,
	[Height_inches__c] [float] NULL,
	[Key_Storage_Lockbox_Price__c] [float] NULL,
	[Primary_User_Fee__c] [float] NULL,
	[Professional_Installation_Fee__c] [float] NULL,
	[Safety_Pendant_iVi__c] [nvarchar](255) NULL,
	[iVi_Pendant_Additional_User__c] [float] NULL,
	[iVi_Pendant_Primary_User__c] [float] NULL,
	[Promo_Code__c] [nvarchar](255) NULL,
	[Promo_Code_Item_ID__c] [nvarchar](255) NULL,
	[SubscriberID__c] [nvarchar](6) NULL,
	[Subscriber_Status__c] [nvarchar](255) NULL,
	[Install_DateTime__c] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[testContact]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testContact](
	[Id] [nvarchar](18) NULL,
	[MasterRecordId] [nvarchar](18) NULL,
	[AccountId] [nvarchar](18) NULL,
	[IsPersonAccount] [bit] NULL,
	[LastName] [nvarchar](80) NULL,
	[FirstName] [nvarchar](40) NULL,
	[Name] [nvarchar](121) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[MailingStreet] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](40) NULL,
	[MailingState] [nvarchar](80) NULL,
	[MailingPostalCode] [nvarchar](20) NULL,
	[Phone] [nvarchar](40) NULL,
	[Fax] [nvarchar](40) NULL,
	[MobilePhone] [nvarchar](40) NULL,
	[HomePhone] [nvarchar](40) NULL,
	[OtherPhone] [nvarchar](40) NULL,
	[AssistantPhone] [nvarchar](40) NULL,
	[ReportsToId] [nvarchar](18) NULL,
	[Email] [nvarchar](80) NULL,
	[Title] [nvarchar](128) NULL,
	[Department] [nvarchar](80) NULL,
	[AssistantName] [nvarchar](40) NULL,
	[LeadSource] [nvarchar](40) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[Subscriber_Contact__c] [nvarchar](18) NULL,
	[Caller__c] [bit] NULL,
	[Responder__c] [bit] NULL,
	[Relation_to_Caller__c] [nvarchar](255) NULL,
	[What_prompted_you_to_call_us_today__c] [nvarchar](255) NULL,
	[Primary_Subscriber_Configuration__c] [nvarchar](255) NULL,
	[Additional_User_Configuration__c] [nvarchar](255) NULL,
	[Product_Line__c] [nvarchar](255) NULL,
	[Frequency__c] [nvarchar](255) NULL,
	[Payee_Name__c] [nvarchar](100) NULL,
	[Agree_to_Credit_Card_Charges__c] [nvarchar](255) NULL,
	[Shipping_Type__c] [nvarchar](255) NULL,
	[Reccuring_Price__c] [float] NULL,
	[Professional_Installation__c] [nvarchar](255) NULL,
	[Responder_has_access_to_Key__c] [nvarchar](255) NULL,
	[Key_Storage_LockBox__c] [nvarchar](255) NULL,
	[Phone_Line_Type__c] [nvarchar](255) NULL,
	[Language__c] [nvarchar](255) NULL,
	[Additional_Extra_Activator__c] [nvarchar](255) NULL,
	[Primary_User_Activation_Fee__c] [float] NULL,
	[Shipping_Price__c] [float] NULL,
	[Store_Code__c] [nvarchar](50) NULL,
	[Activation_Kit__c] [nvarchar](255) NULL,
	[Sales_Receipt_Number__c] [nvarchar](35) NULL,
	[Installer_Contact__c] [nvarchar](255) NULL,
	[Billing_Street__c] [nvarchar](255) NULL,
	[Shipping_Street__c] [nvarchar](255) NULL,
	[Billing_City__c] [nvarchar](100) NULL,
	[Shipping_City__c] [nvarchar](100) NULL,
	[Billing_State__c] [nvarchar](100) NULL,
	[Shipping_State__c] [nvarchar](100) NULL,
	[Billing_Zip_Code__c] [nvarchar](11) NULL,
	[Shipping_Zip_Code__c] [nvarchar](11) NULL,
	[Second_Subscriber__c] [bit] NULL,
	[Responders_Relation__c] [nvarchar](255) NULL,
	[Has_Second_User__c] [bit] NULL,
	[Ship_To_Name__c] [nvarchar](255) NULL,
	[Coupon_Code__c] [nvarchar](255) NULL,
	[Referrer_Name_c__c] [nvarchar](255) NULL,
	[Refferal_Code__c] [nvarchar](50) NULL,
	[Shipping_Phone__c] [nvarchar](40) NULL,
	[Additional_Extra_Activator_Configuration__c] [nvarchar](255) NULL,
	[Shipping_Apartment__c] [nvarchar](40) NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vw_SFContactsToKshema]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_SFContactsToKshema] as

/*
vw_SFContactsToKshema
20140305 IC
Returns contacts from Salesforce staging table in a format compatible to add to Kshema

20140428 AL
Added filter for not null agency_id

20140610 IC
Added CCNameFirst, CCNameLast

20140918 IC
Added BillingStartDate

20150429 IC
Added new columns for component pricing

20150512 IC
Added Promo_Code_Item_ID

20160121 IC
Added ability to handle 2 digit ccexpyear values coming from Walgreens Ready Response website

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType
--select * from vw_SFContactsToKshema_new --where subscriber_id in ('553970','555289','555609','555612','547841')
--where subscriber_id in('547356','547358')
--select * from vw_SFContactsToKshema_new where subscriber_id is null
select top 100 ccexpdate,* from vw_SFContactsToKshema order by contactrecid desc
close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03

select * from sys.symmetric_keys
select distinct couponcode from vw_SFContactsToKshema
select top 100 * from vw_SFContactsToKshema where subscriber_id is not null  order by subscriber_id desc 
--and (popt2user =1 or popt2activator ='yes' or poptlockbox='yes')
select top 100 * from gmwgsales.dbo.sfcontactstage where prodmodel is not null order by contactcreatedate desc
select * from gmwgsales.dbo.sfcontactstage where subscriber_id in('567522')
select * from gmwgsales.dbo.sfcontactstage where subscriber_id in('567522')
select * from vw_SFContactsToKshema where contactrecid = '003d000002YeC9rAAF'

select top 100 
ccexpmonth, ccexpyear,
RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + right(cs.ccexpyear, 2) as CCExpDate,
RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + RIGHT('20' + ltrim(rtrim(cs.ccexpyear)), 4) as CCExpDate2,
 * 
from sfcontactstage cs
where ccexpmonth is not null
order by contactrecid desc

*/

SELECT cs.ContactRecID
      ,cs.Agency_ID
      ,cs.Subscriber_ID
      ,cs.SubscriberAddedToKshema
      ,cs.SBTSONO
      ,cs.SBTSODate
      ,cs.firstname as SubFirstName
      ,cs.lastname as SubLastName
      ,left(cs.phone, 10) as SubPhone  --SF varchar(25), KS varchar(10)
      ,cs.address1 as SubAddress1
      ,cs.address2 as SubAddress2
      ,left(cs.city, 20) as SubCity --SF varchar(25), KS varchar(20)
      ,cs.stateprov as SubState
      ,left(cs.zip, 5) as SubZip
      ,case when datalength(cs.zip) > 8 then right(cs.zip, 4) else '' end as SubZipPlus4
      ,cs.SubLanguage
      ,cs.SubLineType
      ,cr.firstname + ' ' + cr.lastname as RespName
      --,left(cr.RespRelation, 6) as RespRelation --varchar(20) SF
      ,cr.RespRelation
      ,case when cr.caller = 1 then cr.phone else cr.homephone end as RespHomePhone
      ,cr.mobilephone as RespCellPhone
      ,cr.otherphone as RespWorkPhone
      ,cr.RespHasKey
      ,cs.CouponCode
      ,cs.ProdModel
      ,cs.ProdConfigure
      ,cs.POpt2User
      ,isnull(c2.SecondaryContact, 0) as SecondaryContact
	  ,isnull(cs.POpt2UserConfigure, '') as POpt2UserConfigure
      ,c2.FirstName as Sub2FirstName
      ,c2.LastName as Sub2LastName
      ,c2.SubLanguage as Sub2Language
      ,c2.subscriber_id as Sub2Subscriber_ID
	  ,cs.ShipMethod
	  ,case when charindex('$',isnull(cs.ShipMethod, '')) = 0 then 0 else convert(float, substring(cs.ShipMethod, charindex('$',cs.ShipMethod) + 1, 500)) end as ShipFee
	  ,cs.ShipToName
	  ,cs.ShipAddress1
	  ,isnull(cs.ShipAddress2, '') as ShipAddress2
	  ,cs.ShipCity
	  ,cs.ShipState
	  ,cs.ShipZip
	  ,isnull(cs.ShippingPhone, '') as ShippingPhone
      ,cs.POpt2Activator
      ,isnull(cs.Additional_Extra_Activator_Configuration, '') as Additional_Extra_Activator_Configuration
      ,case when isnull(cs.POpt2Activator, 'no') = 'yes' then 4.99 else 0 end as POpt2ActivatorFee
      ,cs.POptLockbox
      ,case when isnull(cs.POptLockbox, 'no') = 'yes' then 2.99 else 0 end as POptLockboxFee
      ,cs.ProdActivationFee
      ,cs.ProdMonthlyRate
      ,case isnull(cs.ProdModel, '') when 'Mobile Safety Device' then 39.99 when 'Cell-based PERS' then 34.99 else 29.99 end as ProdMonthlyFee  --Had to create because ProdMonthlyRate is supplied as extended total
      ,case when isnull(cs.POpt2User, 0) = 1 then 4.99 else 0 end as POpt2UserFee
      ,cs.ProfInstall
      ,case when isnull(cs.ProfInstall, 'no') = 'yes' then 75 else 0 end as ProfInstallFee
      ,cs.CCFrequency
      ,convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) as CCCardType
      ,cs.CCName
      ,convert(varchar(30), left(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then datalength(cs.CCName) else datalength(cs.CCName) - charindex(' ', reverse(cs.CCName)) end)) as CCNameFirst
      ,convert(varchar(30), right(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then 0 else charindex(' ', reverse(cs.CCName)) - 1 end)) as CCNameLast
      ,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCNumber))) as CCNumber
      --,RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + right(cs.ccexpyear, 2) as CCExpDate
	  ,RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + RIGHT('20' + ltrim(rtrim(cs.ccexpyear)), 4) as CCExpDate
      --,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCCVV))) as CCCVV
      ,convert(varchar(4), RIGHT('0' + convert(nvarchar(128), decryptbykey(cs.CCCVV)), case when convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) = 'American Express' then 4 else 3 end))  as CCCVV
      ,'' as CCAddrSameAs
      ,cs.CCContactPhone
      ,cs.CCAddress1
      ,cs.CCAddress2
      ,cs.CCCity
      ,cs.CCState
      ,cs.CCZip
      ,cs.BillingStartDate
      ,isnull(cs.Primary_User_Fee, 0) as Primary_User_Fee
      ,isnull(cs.Additional_User_Fee, 0) as Additional_User_Fee
      ,isnull(cs.Additional_Activator_Fee, 0) as Additional_Activator_Fee
      ,isnull(cs.iVi_Pendant_Primary_Fee, 0) as iVi_Pendant_Primary_Fee
      ,isnull(cs.iVi_Pendant_Additional_User_Fee, 0) as iVi_Pendant_Additional_User_Fee
      ,isnull(cs.Key_Storage_Lock_Fee, 0) as Key_Storage_Lock_Fee
      ,isnull(cs.Prof_Install_Fee, 0) as Prof_Install_Fee
      ,isnull(cs.Safety_Pendant_iVi, 'No') as Safety_Pendant_iVi
      ,convert(int, isnull(cs.iVi_Height, 0)) as iVi_Height
      ,isnull(c2.Safety_Pendant_iVi, 'No') as Safety_Pendant_iVi2
      ,convert(int, isnull(c2.iVi_Height, 0)) as iVi_Height2
      ,isnull(cs.Promo_Code_Item_ID, '') as Promo_Code_Item_ID


from sfcontactstage cs
left outer join sfcontactstage c2 
	on c2.primarycontact = cs.contactrecid 
	and c2.secondarycontact = 1
	and c2.FirstName is not null and c2.LastName <> '2ndSubscriber'
left outer join sfcontactstage cr on cr.primarycontact = cs.contactrecid and cr.responder = 1
where cs.primarycontact is null and cs.agency_id is not null
--and cs.subscriberaddedtokshema is null  --handle in cursor
and
not 
(
(cs.firstname = 'Marie' and cs.lastname = 'Wilson')
or
(cs.firstname = 'Sunny' and cs.lastname = 'Cohn')
or
(cs.firstname = 'Laura' and cs.lastname = 'Adams')
or
(cs.firstname = 'Joanne' and cs.lastname = 'Andrews')
)







GO
/****** Object:  View [dbo].[vw_SFContactsToKshema.20140611]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_SFContactsToKshema.20140611] as

/*
vw_SFContactsToKshema
20140305 IC
Returns contacts from Salesforce staging table in a format compatible to add to Kshema

20140428 AL
Added filter for not null agency_id

20140610 IC
Added CCNameFirst, CCNameLast

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType
select * from vw_SFContactsToKshema where subscriber_id in ('553970','555289','555609','555612','547841')
--where subscriber_id in('547356','547358')
--select * from vw_SFContactsToKshema where subscriber_id is null
close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03

select * from sys.symmetric_keys
select distinct couponcode from vw_SFContactsToKshema
select top 100 * from vw_SFContactsToKshema where subscriber_id is not null and (popt2user =1 or popt2activator ='yes' or poptlockbox='yes') order by subscriber_id desc
select top 100 * from gmwgsales.dbo.sfcontactstage order by contactcreatedate desc
select * from gmwgsales.dbo.sfcontactstage where subscriber_id in('567522')

*/

SELECT cs.ContactRecID
      ,cs.SubscriberAddedToKshema
      ,cs.Agency_ID
      ,cs.Subscriber_ID
      ,cs.firstname as SubFirstName
      ,cs.lastname as SubLastName
      ,left(cs.phone, 10) as SubPhone  --SF varchar(25), KS varchar(10)
      ,cs.address1 as SubAddress1
      ,cs.address2 as SubAddress2
      ,left(cs.city, 20) as SubCity --SF varchar(25), KS varchar(20)
      ,cs.stateprov as SubState
      ,left(cs.zip, 5) as SubZip
      ,case when datalength(cs.zip) > 8 then right(cs.zip, 4) else '' end as SubZipPlus4
      ,cs.SubLanguage
      ,cs.SubLineType
      ,cr.firstname + ' ' + cr.lastname as RespName
      --,left(cr.RespRelation, 6) as RespRelation --varchar(20) SF
      ,cr.RespRelation
      ,case when cr.caller = 1 then cr.phone else cr.homephone end as RespHomePhone
      ,cr.mobilephone as RespCellPhone
      ,cr.otherphone as RespWorkPhone
      ,cr.RespHasKey
      ,cs.CouponCode
      ,cs.ProdModel
      ,cs.ProdConfigure
      ,cs.POpt2User
      ,cs.POpt2Activator
      ,cs.POptLockbox
      ,cs.CCFrequency
      ,convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) as CCCardType
      ,cs.CCName
      ,convert(varchar(30), left(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then datalength(cs.CCName) else datalength(cs.CCName) - charindex(' ', reverse(cs.CCName)) end)) as CCNameFirst
      ,convert(varchar(30), right(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then 0 else charindex(' ', reverse(cs.CCName)) - 1 end)) as CCNameLast
      ,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCNumber))) as CCNumber
      ,RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + right(cs.ccexpyear, 2) as CCExpDate
      --,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCCVV))) as CCCVV
      ,convert(varchar(4), RIGHT('0' + convert(nvarchar(128), decryptbykey(cs.CCCVV)), case when convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) = 'American Express' then 4 else 3 end))  as CCCVV
      ,'' as CCAddrSameAs
      ,cs.CCContactPhone
      ,cs.CCAddress1
      ,cs.CCAddress2
      ,cs.CCCity
      ,cs.CCState
      ,cs.CCZip
      ,isnull(c2.SecondaryContact, 0) as SecondaryContact
      ,c2.FirstName as Sub2FirstName
      ,c2.LastName as Sub2LastName
      ,c2.SubLanguage as Sub2Language
      ,c2.subscriber_id as Sub2Subscriber_ID
      ,cs.SBTSONO
      ,cs.SBTSODate
  

from sfcontactstage cs
left outer join sfcontactstage c2 on c2.primarycontact = cs.contactrecid and c2.secondarycontact = 1
left outer join sfcontactstage cr on cr.primarycontact = cs.contactrecid and cr.responder = 1
where cs.primarycontact is null and cs.agency_id is not null
--and cs.subscriberaddedtokshema is null  --handle in cursor



GO
/****** Object:  View [dbo].[vw_SFContactsToKshema.20150430]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_SFContactsToKshema.20150430] as

/*
vw_SFContactsToKshema
20140305 IC
Returns contacts from Salesforce staging table in a format compatible to add to Kshema

20140428 AL
Added filter for not null agency_id

20140610 IC
Added CCNameFirst, CCNameLast

20140918 IC
Added BillingStartDate

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType
select * from vw_SFContactsToKshema --where subscriber_id in ('553970','555289','555609','555612','547841')
--where subscriber_id in('547356','547358')
--select * from vw_SFContactsToKshema where subscriber_id is null
close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03

select * from sys.symmetric_keys
select distinct couponcode from vw_SFContactsToKshema
select top 100 * from vw_SFContactsToKshema where subscriber_id is not null --and (popt2user =1 or popt2activator ='yes' or poptlockbox='yes') order by subscriber_id desc
select top 100 * from gmwgsales.dbo.sfcontactstage where prodmodel is not null order by contactcreatedate desc
select * from gmwgsales.dbo.sfcontactstage where subscriber_id in('567522')
select * from gmwgsales.dbo.sfcontactstage where subscriber_id in('567522')
select * from vw_SFContactsToKshema where contactrecid = '003d000001zmqA2AAI'
*/

SELECT cs.ContactRecID
      ,cs.Agency_ID
      ,cs.Subscriber_ID
      ,cs.SubscriberAddedToKshema
      ,cs.SBTSONO
      ,cs.SBTSODate
      ,cs.firstname as SubFirstName
      ,cs.lastname as SubLastName
      ,left(cs.phone, 10) as SubPhone  --SF varchar(25), KS varchar(10)
      ,cs.address1 as SubAddress1
      ,cs.address2 as SubAddress2
      ,left(cs.city, 20) as SubCity --SF varchar(25), KS varchar(20)
      ,cs.stateprov as SubState
      ,left(cs.zip, 5) as SubZip
      ,case when datalength(cs.zip) > 8 then right(cs.zip, 4) else '' end as SubZipPlus4
      ,cs.SubLanguage
      ,cs.SubLineType
      ,cr.firstname + ' ' + cr.lastname as RespName
      --,left(cr.RespRelation, 6) as RespRelation --varchar(20) SF
      ,cr.RespRelation
      ,case when cr.caller = 1 then cr.phone else cr.homephone end as RespHomePhone
      ,cr.mobilephone as RespCellPhone
      ,cr.otherphone as RespWorkPhone
      ,cr.RespHasKey
      ,cs.CouponCode
      ,cs.ProdModel
      ,cs.ProdConfigure
      ,cs.POpt2User
      ,isnull(c2.SecondaryContact, 0) as SecondaryContact
	  ,isnull(cs.POpt2UserConfigure, '') as POpt2UserConfigure
      ,c2.FirstName as Sub2FirstName
      ,c2.LastName as Sub2LastName
      ,c2.SubLanguage as Sub2Language
      ,c2.subscriber_id as Sub2Subscriber_ID

	  ,cs.ShipMethod
	  ,case when charindex('$',isnull(cs.ShipMethod, '')) = 0 then 0 else convert(float, substring(cs.ShipMethod, charindex('$',cs.ShipMethod) + 1, 500)) end as ShipFee
	  ,cs.ShipToName
	  ,cs.ShipAddress1
	  ,isnull(cs.ShipAddress2, '') as ShipAddress2
	  ,cs.ShipCity
	  ,cs.ShipState
	  ,cs.ShipZip
	  ,isnull(cs.ShippingPhone, '') as ShippingPhone
      ,cs.POpt2Activator
      ,isnull(cs.Additional_Extra_Activator_Configuration, '') as Additional_Extra_Activator_Configuration
      ,case when isnull(cs.POpt2Activator, 'no') = 'yes' then 4.99 else 0 end as POpt2ActivatorFee
      ,cs.POptLockbox
      ,case when isnull(cs.POptLockbox, 'no') = 'yes' then 2.99 else 0 end as POptLockboxFee
      ,cs.ProdActivationFee
      ,cs.ProdMonthlyRate
      ,case isnull(cs.ProdModel, '') when 'Mobile Safety Device' then 39.99 when 'Cell-based PERS' then 34.99 else 29.99 end as ProdMonthlyFee  --Had to create because ProdMonthlyRate is supplied as extended total
      ,case when isnull(cs.POpt2User, 0) = 1 then 4.99 else 0 end as POpt2UserFee
      ,cs.ProfInstall
      ,case when isnull(cs.ProfInstall, 'no') = 'yes' then 75 else 0 end as ProfInstallFee
      ,cs.CCFrequency
      ,convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) as CCCardType
      ,cs.CCName
      ,convert(varchar(30), left(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then datalength(cs.CCName) else datalength(cs.CCName) - charindex(' ', reverse(cs.CCName)) end)) as CCNameFirst
      ,convert(varchar(30), right(cs.CCName, case when charindex(' ', reverse(cs.CCName)) = 0 then 0 else charindex(' ', reverse(cs.CCName)) - 1 end)) as CCNameLast
      ,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCNumber))) as CCNumber
      ,RIGHT('000' + ltrim(rtrim(cs.ccexpmonth)), 2) + '/' + right(cs.ccexpyear, 2) as CCExpDate
      --,convert(varchar(16), convert(nvarchar(128), decryptbykey(cs.CCCVV))) as CCCVV
      ,convert(varchar(4), RIGHT('0' + convert(nvarchar(128), decryptbykey(cs.CCCVV)), case when convert(varchar(30), convert(nvarchar(128), decryptbykey(cs.CCCardType))) = 'American Express' then 4 else 3 end))  as CCCVV
      ,'' as CCAddrSameAs
      ,cs.CCContactPhone
      ,cs.CCAddress1
      ,cs.CCAddress2
      ,cs.CCCity
      ,cs.CCState
      ,cs.CCZip
      ,cs.BillingStartDate
      

from sfcontactstage cs
left outer join sfcontactstage c2 
	on c2.primarycontact = cs.contactrecid 
	and c2.secondarycontact = 1
	and c2.FirstName is not null and c2.LastName <> '2ndSubscriber'
left outer join sfcontactstage cr on cr.primarycontact = cs.contactrecid and cr.responder = 1
where cs.primarycontact is null and cs.agency_id is not null
--and cs.subscriberaddedtokshema is null  --handle in cursor


GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_contact_accountno]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_contact_accountno] ON [dbo].[contact]
(
	[accountno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_contact_company]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_contact_company] ON [dbo].[contact]
(
	[Company] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_contact_key3LeadStatus]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_contact_key3LeadStatus] ON [dbo].[contact]
(
	[Key3] ASC,
	[UCCAgree] ASC,
	[Contact_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_contact_Usubs]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE NONCLUSTERED INDEX [IX_contact_Usubs] ON [dbo].[contact]
(
	[Usubs] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Ix_PrimaryContact]    Script Date: 2/11/2016 1:50:53 PM ******/
CREATE NONCLUSTERED INDEX [Ix_PrimaryContact] ON [dbo].[SFContactStage]
(
	[PrimaryContact] ASC,
	[Responder] ASC,
	[ContactRecID] ASC
)
INCLUDE ( 	[FirstName],
	[LastName],
	[Caller],
	[RespRelation],
	[RespHasKey],
	[HomePhone],
	[MobilePhone],
	[Phone],
	[OtherPhone]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AgencyPromoCodes]  WITH CHECK ADD  CONSTRAINT [FK_AgencyPromoCodes_PromoCodes] FOREIGN KEY([PromoCode_ID])
REFERENCES [dbo].[PromoCodes] ([ID])
GO
ALTER TABLE [dbo].[AgencyPromoCodes] CHECK CONSTRAINT [FK_AgencyPromoCodes_PromoCodes]
GO
/****** Object:  StoredProcedure [dbo].[AddLeadsToKshema]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AddLeadsToKshema]

as

/*
AddLeadsToKshema
20140123 IC		Added Sold leads to Kshema and amacccinfo tables
20150625 IC		Made responder optional
20150706 IC		Added 2nd user support

Called by scheduled job
exec AddLeadsToKshema
*/

begin

--procedure variables
declare @AgencyName varchar(50)
declare @SubType varchar(15)
declare @ProgramType varchar(25)
declare @Today datetime
declare @NoLandlineAvailable varchar(10)
declare @RequestMSD varchar(10)
declare @NextSubscriberResponder_ID bigint
declare @HasKey char(1)
declare @KshemaCode char(6)

--leads variables
declare @Lead_ID int
declare @SubscriberAddedToKshema datetime
declare @Agency_ID char(6)
declare @Subscriber_ID char(6)
declare @SubFirstName varchar(20)
declare @SubLastName varchar(30)
declare @SubPhone varchar(10)
declare @SubAddress1 varchar(30)
declare @SubAddress2 varchar(30)
declare @SubCity varchar(20)
declare @SubState char(2)
declare @SubZip char(6)
declare @SubZipPlus4 char(4)
declare @SubLanguage char(6)
declare @SubLineType varchar(15)
declare @RespName varchar(50)
declare @RespRelation char(6)
declare @RespHomePhone varchar(60)
declare @RespCellPhone varchar(25)
declare @RespHasKey int
declare @ProdModel varchar(15)
declare @CCFrequency varchar(25)
declare @CCCardType varchar(30)
declare @CCName varchar(30)
declare @CCNumber varchar(16)
declare @CCExpDate char(5)
declare @CCCVV varchar(4)
declare @CCAddrSameAs varchar(25)
declare @CCContactPhone varchar(25)
declare @CCAddress1 varchar(30)
declare @CCAddress2 varchar(30)
declare @CCCity varchar(25)
declare @CCState char(2)
declare @CCZip varchar(10)
declare @Sub2Subscriber_ID char(6)
declare @Sub2SubscriberAddedToKshema datetime
declare @Sub2FirstName varchar(20)
declare @Sub2LastName varchar(20)
declare @Sub2Language char(6)


SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
-- Start the transaction
BEGIN TRANSACTION

declare leadscursor cursor for

SELECT Lead_ID
      ,SubscriberAddedToKshema
      ,Agency_ID
      ,Subscriber_ID
      ,SubFirstName
      ,SubLastName
      ,SubPhone
      ,SubAddress1
      ,SubAddress2
      ,SubCity
      ,SubState
      ,SubZip
      ,SubZipPlus4
      ,SubLanguage
      ,SubLineType
      ,RespName
      ,RespRelation
      ,RespHomePhone
      ,RespCellPhone
      ,RespHasKey
      ,ProdModel
      ,CCFrequency
      ,CCCardType
      ,CCName
      ,CCNumber
      ,CCExpDate
      ,CCCVV
      ,CCAddrSameAs
      ,CCContactPhone
      ,CCAddress1
      ,CCAddress2
      ,CCCity
      ,CCState
      ,CCZip
      ,Sub2Subscriber_ID
	  ,Sub2SubscriberAddedToKshema
	  ,Sub2FirstName
	  ,Sub2LastName
	  ,Sub2Language

FROM Leads
where status = 'sold' and SubscriberAddedToKshema is null
order by lead_id

for update of SubscriberAddedToKshema, Subscriber_ID, Sub2SubscriberAddedToKshema, Sub2Subscriber_ID

open leadscursor

fetch next from leadscursor
into
@Lead_ID,
@SubscriberAddedToKshema,
@Agency_ID,
@Subscriber_ID,
@SubFirstName,
@SubLastName,
@SubPhone,
@SubAddress1,
@SubAddress2,
@SubCity,
@SubState,
@SubZip,
@SubZipPlus4,
@SubLanguage,
@SubLineType,
@RespName,
@RespRelation,
@RespHomePhone,
@RespCellPhone,
@RespHasKey,
@ProdModel,
@CCFrequency,
@CCCardType,
@CCName,
@CCNumber,
@CCExpDate,
@CCCVV,
@CCAddrSameAs,
@CCContactPhone,
@CCAddress1,
@CCAddress2,
@CCCity,
@CCState,
@CCZip,
@Sub2Subscriber_ID,
@Sub2SubscriberAddedToKshema,
@Sub2FirstName,
@Sub2LastName,
@Sub2Language

while @@fetch_status = 0
begin

	select @Today = DATEADD(day, DATEDIFF(day, 0, getdate()), 0)
	select @AgencyName = name from kshema_agency where agency_id = @Agency_ID
	select top 1 @ProgramType = ProgramType from Kshema_AgencyService where agency_id = @Agency_ID and programtype is not null
	if @ProdModel = 'cel450'  set @NoLandlineAvailable = 'YES' else set @NoLandlineAvailable = 'NO'
	if @ProdModel = 'msd' set @RequestMSD = 'YES' else set @RequestMSD = 'NO'
	if @ProdModel = 'msd' set @SubType = 'MSD' else set @SubType = '(PNC)'
	if isnull(@RespHasKey, 0) = 1 set @HasKey ='Y' else set @HasKey = 'N'
	
	--add subscriber, default responders, subscriberpl2info, subscriberoptionalservice
	EXEC Kshema_ap_Subscriber_i
	   @SubFirstName	--@vchrFirstName
	  ,''				--@vchrMiddleInitial
	  ,@SubLastName		--@vchrLastName
	  ,''				--@vchrKnownName
	  ,@SubAddress1		--@vchrAddress1
	  ,@SubAddress2		--@vchrAddress2
	  ,''				--@vchrNearIntersection
	  ,@SubState		--@chrState
	  ,@SubCity			--@vchrCity
	  ,''				--@vchrCounty
	  ,@SubZip			--@vchrZip
	  ,@SubZipPlus4		--@chrPlus4
	  ,@SubPhone		--@vchrPhone
	  ,''				--@vchrEMail
	  ,null				--@dteDOB
	  ,null				--@chrSex
	  ,null				--@vchrSSN
	  ,null				--@vchrMedicaid
	  ,null				--@vchrFax
	  ,'N'				--@chrSecondUser
	  ,''				--@vchrSpecialInst
	  ,''				--@vchrComments
	  ,@Agency_ID		--@chrAgency_ID
	  ,@AgencyName		--@vchrAgencyName
	  ,''				--@vchrUnit_ID
	  ,null				--@chrSubContractor_ID
	  ,'PENDING INSTALL'--@vchrStatus
	  ,@SubType			--@vchrType
	  ,'N'				--@chrForcedEntry
	  ,'R'				--@chrUnitRented_Sold
	  ,'N'				--@chrCancel
	  ,@Today			--@dteOrderDate
	  ,@Today			--@dteEntryDate
	  ,null				--@dteInstallDate
	  ,null				--@dteOnlineSince
	  ,null				--@dteRemovalDate
	  ,null				--@dteRequestedRemovalDate
	  ,'N'				--@chrEquipmentRecovered
	  ,'N'				--@chrConfirmReceived
	  ,null				--@dteFileMagic
	  ,@ProgramType		--@vchrProgramTypes
	  ,null				--@chrIsDayLightSaving
	  ,0				--@intTimeZonecode
	  ,''				--@chrLocked
	  ,'N'				--@chrDeleted
	  ,'N'				--@chrIsAddressVerified
	  ,@NoLandlineAvailable		--@NoLandlineAvailable
	  ,@RequestMSD				--@RequestMSD
	  ,null				--@MSDPhone


	--get subscriber_id
	select @Subscriber_ID = max(Subscriber_ID) from Kshema_Subscriber where lastname = @SubLastName and firstname = @SubFirstName

	--Add subscriber language
	exec Kshema_ap_SubscriberLanguage_i @Subscriber_ID, @SubLanguage, 'LAN'
	
	--add responder
	--select top 1 * from Kshema_SubscriberResponder	
	if isnull(@RespRelation, '') <> ''
	begin
		select @NextSubscriberResponder_ID = max(SubscriberResponder_ID) + 1 from Kshema_SubscriberResponder
		
		INSERT INTO Kshema_SubscriberResponder
			   (SubscriberResponder_ID
			   ,RelationType
			   ,RelationCode
			   ,Subscriber_ID
			   ,ResponderName
			   ,HasKey
			   ,NVI
			   ,HomePhone
			   ,WorkPhone
			   ,CellPhone
			   ,Pager
			   ,CallOrder
			   ,NVIOrder
			   ,ResponderVoid
			   ,Deleted
			   ,Comments)
		 VALUES
			   (@NextSubscriberResponder_ID
			   ,'REL'
			   ,@RespRelation
			   ,@Subscriber_ID
			   ,@RespName
			   ,@HasKey
			   ,'N'
			   ,@RespHomePhone
			   ,''
			   ,@RespCellPhone
			   ,''
			   ,4
			   ,0
			   ,'Y'
			   ,'N'
			   ,''
			   )
	end

	--Add credit card info
	INSERT INTO Kshema_AMACccinfo
           (subscriber_id
           ,cardmembername
           ,ccnumber
           ,expdate
           ,ccvcode
           ,address1
           ,address2
           ,city
           ,state
           ,zipcode
           ,encccnumber
           ,encexpdate
           ,encccvcode
           ,deleted)
     VALUES
           (@Subscriber_ID
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,''
           ,''
           ,''
           ,'N'
			)

	--2nd user
	if @Sub2Subscriber_ID is null and isnull(@Sub2LastName, '') <> ''
	begin
		--add second subscriber, default responders, subscriberpl2info, subscriberoptionalservice
		EXEC Kshema_ap_Subscriber_i
		   @Sub2FirstName	--@vchrFirstName
		  ,''				--@vchrMiddleInitial
		  ,@Sub2LastName	--@vchrLastName
		  ,''				--@vchrKnownName
		  ,@SubAddress1		--@vchrAddress1
		  ,@SubAddress2		--@vchrAddress2
		  ,''				--@vchrNearIntersection
		  ,@SubState		--@chrState
		  ,@SubCity			--@vchrCity
		  ,''				--@vchrCounty
		  ,@SubZip			--@vchrZip
		  ,@SubZipPlus4		--@chrPlus4
		  ,@SubPhone		--@vchrPhone
		  ,''				--@vchrEMail
		  ,null				--@dteDOB
		  ,null				--@chrSex
		  ,null				--@vchrSSN
		  ,null				--@vchrMedicaid
		  ,null				--@vchrFax
		  ,'N'				--@chrSecondUser
		  ,''				--@vchrSpecialInst
		  ,''				--@vchrComments
		  ,@Agency_ID		--@chrAgency_ID
		  ,@AgencyName		--@vchrAgencyName
		  ,''				--@vchrUnit_ID
		  ,null				--@chrSubContractor_ID
		  ,'PENDING INSTALL'--@vchrStatus
		  ,@SubType			--@vchrType
		  ,'N'				--@chrForcedEntry
		  ,'R'				--@chrUnitRented_Sold
		  ,'N'				--@chrCancel
		  ,@Today			--@dteOrderDate
		  ,@Today			--@dteEntryDate
		  ,null				--@dteInstallDate
		  ,null				--@dteOnlineSince
		  ,null				--@dteRemovalDate
		  ,null				--@dteRequestedRemovalDate
		  ,'N'				--@chrEquipmentRecovered
		  ,'N'				--@chrConfirmReceived
		  ,null				--@dteFileMagic
		  ,@ProgramType		--@vchrProgramTypes
		  ,null				--@chrIsDayLightSaving
		  ,0				--@intTimeZonecode
		  ,''				--@chrLocked
		  ,'N'				--@chrDeleted
		  ,'N'				--@chrIsAddressVerified
		  ,@NoLandlineAvailable		--@NoLandlineAvailable
		  ,@RequestMSD				--@RequestMSD
		  ,null				--@MSDPhone


		--get subscriber_id
		select @Sub2Subscriber_ID = max(Subscriber_ID) from Kshema_Subscriber where lastname = @Sub2LastName and firstname = @Sub2FirstName

		--Add subscriber language
		set @KshemaCode = 'NOCODE'
		select @KshemaCode = code from Kshema_Codes where type = 'lan' and deleted <> 'y' and code = @Sub2Language
		if @KshemaCode <> 'NOCODE'
		--2nd user languages seem to be all null (salesforce?)
			exec Kshema_ap_SubscriberLanguage_i @Sub2Subscriber_ID, @KshemaCode, 'LAN'
		
		--join subscribers
		exec Kshema_pl2_ap_LinkSubscribers_u @Agency_ID, @subscriber_id, @Sub2Subscriber_ID

	end

	--update leads table
	update leads 
	set subscriber_id = @subscriber_id, SubscriberAddedToKshema = getdate(),
		Sub2Subscriber_ID = @Sub2Subscriber_ID, Sub2SubscriberAddedToKshema = getdate()
	where current of leadscursor

	--get next lead
	fetch next from leadscursor
	into
	@Lead_ID,
	@SubscriberAddedToKshema,
	@Agency_ID,
	@Subscriber_ID,
	@SubFirstName,
	@SubLastName,
	@SubPhone,
	@SubAddress1,
	@SubAddress2,
	@SubCity,
	@SubState,
	@SubZip,
	@SubZipPlus4,
	@SubLanguage,
	@SubLineType,
	@RespName,
	@RespRelation,
	@RespHomePhone,
	@RespCellPhone,
	@RespHasKey,
	@ProdModel,
	@CCFrequency,
	@CCCardType,
	@CCName,
	@CCNumber,
	@CCExpDate,
	@CCCVV,
	@CCAddrSameAs,
	@CCContactPhone,
	@CCAddress1,
	@CCAddress2,
	@CCCity,
	@CCState,
	@CCZip,
	@Sub2Subscriber_ID,
	@Sub2SubscriberAddedToKshema,
	@Sub2FirstName,
	@Sub2LastName,
	@Sub2Language

end

close leadscursor
deallocate leadscursor

if (@@ERROR > 0) 
    ROLLBACK TRANSACTION
ELSE
    COMMIT TRANSACTION

-- Set back the locking to default
SET TRANSACTION ISOLATION LEVEL READ COMMITTED


end

GO
/****** Object:  StoredProcedure [dbo].[AddLeadsToKshema_20150708]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AddLeadsToKshema_20150708]

as

/*
AddLeadsToKshema
20140123 IC		Added Sold leads to Kshema and amacccinfo tables
20150625 IC		Made responder optional

Called by scheduled job
exec AddLeadsToKshema
*/

begin

--procedure variables
declare @AgencyName varchar(50)
declare @SubType varchar(15)
declare @ProgramType varchar(25)
declare @Today datetime
declare @NoLandlineAvailable varchar(10)
declare @RequestMSD varchar(10)
declare @NextSubscriberResponder_ID bigint
declare @HasKey char(1)

--leads variables
declare @Lead_ID int
declare @SubscriberAddedToKshema datetime
declare @Agency_ID char(6)
declare @Subscriber_ID char(6)
declare @SubFirstName varchar(20)
declare @SubLastName varchar(30)
declare @SubPhone varchar(10)
declare @SubAddress1 varchar(30)
declare @SubAddress2 varchar(30)
declare @SubCity varchar(20)
declare @SubState char(2)
declare @SubZip char(6)
declare @SubZipPlus4 char(4)
declare @SubLanguage char(6)
declare @SubLineType varchar(15)
declare @RespName varchar(50)
declare @RespRelation char(6)
declare @RespHomePhone varchar(60)
declare @RespCellPhone varchar(25)
declare @RespHasKey int
declare @ProdModel varchar(15)
declare @CCFrequency varchar(25)
declare @CCCardType varchar(30)
declare @CCName varchar(30)
declare @CCNumber varchar(16)
declare @CCExpDate char(5)
declare @CCCVV varchar(4)
declare @CCAddrSameAs varchar(25)
declare @CCContactPhone varchar(25)
declare @CCAddress1 varchar(30)
declare @CCAddress2 varchar(30)
declare @CCCity varchar(25)
declare @CCState char(2)
declare @CCZip varchar(10)

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
-- Start the transaction
BEGIN TRANSACTION

declare leadscursor cursor for

SELECT Lead_ID
      ,SubscriberAddedToKshema
      ,Agency_ID
      ,Subscriber_ID
      ,SubFirstName
      ,SubLastName
      ,SubPhone
      ,SubAddress1
      ,SubAddress2
      ,SubCity
      ,SubState
      ,SubZip
      ,SubZipPlus4
      ,SubLanguage
      ,SubLineType
      ,RespName
      ,RespRelation
      ,RespHomePhone
      ,RespCellPhone
      ,RespHasKey
      ,ProdModel
      ,CCFrequency
      ,CCCardType
      ,CCName
      ,CCNumber
      ,CCExpDate
      ,CCCVV
      ,CCAddrSameAs
      ,CCContactPhone
      ,CCAddress1
      ,CCAddress2
      ,CCCity
      ,CCState
      ,CCZip
FROM Leads
where status = 'sold' and SubscriberAddedToKshema is null
order by lead_id

for update of SubscriberAddedToKshema, Subscriber_ID

open leadscursor

fetch next from leadscursor
into
@Lead_ID,
@SubscriberAddedToKshema,
@Agency_ID,
@Subscriber_ID,
@SubFirstName,
@SubLastName,
@SubPhone,
@SubAddress1,
@SubAddress2,
@SubCity,
@SubState,
@SubZip,
@SubZipPlus4,
@SubLanguage,
@SubLineType,
@RespName,
@RespRelation,
@RespHomePhone,
@RespCellPhone,
@RespHasKey,
@ProdModel,
@CCFrequency,
@CCCardType,
@CCName,
@CCNumber,
@CCExpDate,
@CCCVV,
@CCAddrSameAs,
@CCContactPhone,
@CCAddress1,
@CCAddress2,
@CCCity,
@CCState,
@CCZip

while @@fetch_status = 0
begin

	select @Today = DATEADD(day, DATEDIFF(day, 0, getdate()), 0)
	select @AgencyName = name from kshema_agency where agency_id = @Agency_ID
	select top 1 @ProgramType = ProgramType from Kshema_AgencyService where agency_id = @Agency_ID and programtype is not null
	if @ProdModel = 'cel450'  set @NoLandlineAvailable = 'YES' else set @NoLandlineAvailable = 'NO'
	if @ProdModel = 'msd' set @RequestMSD = 'YES' else set @RequestMSD = 'NO'
	if @ProdModel = 'msd' set @SubType = 'MSD' else set @SubType = '(PNC)'
	if isnull(@RespHasKey, 0) = 1 set @HasKey ='Y' else set @HasKey = 'N'
	
	--add subscriber, default responders, subscriberpl2info, subscriberoptionalservice
	EXEC Kshema_ap_Subscriber_i
	   @SubFirstName	--@vchrFirstName
	  ,''				--@vchrMiddleInitial
	  ,@SubLastName		--@vchrLastName
	  ,''				--@vchrKnownName
	  ,@SubAddress1		--@vchrAddress1
	  ,@SubAddress2		--@vchrAddress2
	  ,''				--@vchrNearIntersection
	  ,@SubState		--@chrState
	  ,@SubCity			--@vchrCity
	  ,''				--@vchrCounty
	  ,@SubZip			--@vchrZip
	  ,@SubZipPlus4		--@chrPlus4
	  ,@SubPhone		--@vchrPhone
	  ,''				--@vchrEMail
	  ,null				--@dteDOB
	  ,null				--@chrSex
	  ,null				--@vchrSSN
	  ,null				--@vchrMedicaid
	  ,null				--@vchrFax
	  ,'N'				--@chrSecondUser
	  ,''				--@vchrSpecialInst
	  ,''				--@vchrComments
	  ,@Agency_ID		--@chrAgency_ID
	  ,@AgencyName		--@vchrAgencyName
	  ,''				--@vchrUnit_ID
	  ,null				--@chrSubContractor_ID
	  ,'PENDING INSTALL'--@vchrStatus
	  ,@SubType			--@vchrType
	  ,'N'				--@chrForcedEntry
	  ,'R'				--@chrUnitRented_Sold
	  ,'N'				--@chrCancel
	  ,@Today			--@dteOrderDate
	  ,@Today			--@dteEntryDate
	  ,null				--@dteInstallDate
	  ,null				--@dteOnlineSince
	  ,null				--@dteRemovalDate
	  ,null				--@dteRequestedRemovalDate
	  ,'N'				--@chrEquipmentRecovered
	  ,'N'				--@chrConfirmReceived
	  ,null				--@dteFileMagic
	  ,@ProgramType		--@vchrProgramTypes
	  ,null				--@chrIsDayLightSaving
	  ,0				--@intTimeZonecode
	  ,''				--@chrLocked
	  ,'N'				--@chrDeleted
	  ,'N'				--@chrIsAddressVerified
	  ,@NoLandlineAvailable		--@NoLandlineAvailable
	  ,@RequestMSD				--@RequestMSD
	  ,null				--@MSDPhone


	--get subscriber_id
	select @Subscriber_ID = max(Subscriber_ID) from Kshema_Subscriber where lastname = @SubLastName and firstname = @SubFirstName

	--Add subscriber language
	exec Kshema_ap_SubscriberLanguage_i @Subscriber_ID, @SubLanguage, 'LAN'
	
	--add responder
	--select top 1 * from Kshema_SubscriberResponder	
	
	if isnull(@RespRelation, '') <> ''
	begin
		select @NextSubscriberResponder_ID = max(SubscriberResponder_ID) + 1 from Kshema_SubscriberResponder
		
		INSERT INTO Kshema_SubscriberResponder
			   (SubscriberResponder_ID
			   ,RelationType
			   ,RelationCode
			   ,Subscriber_ID
			   ,ResponderName
			   ,HasKey
			   ,NVI
			   ,HomePhone
			   ,WorkPhone
			   ,CellPhone
			   ,Pager
			   ,CallOrder
			   ,NVIOrder
			   ,ResponderVoid
			   ,Deleted
			   ,Comments)
		 VALUES
			   (@NextSubscriberResponder_ID
			   ,'REL'
			   ,@RespRelation
			   ,@Subscriber_ID
			   ,@RespName
			   ,@HasKey
			   ,'N'
			   ,@RespHomePhone
			   ,''
			   ,@RespCellPhone
			   ,''
			   ,4
			   ,0
			   ,'Y'
			   ,'N'
			   ,''
			   )
	end

	--Add credit card info
	INSERT INTO Kshema_AMACccinfo
           (subscriber_id
           ,cardmembername
           ,ccnumber
           ,expdate
           ,ccvcode
           ,address1
           ,address2
           ,city
           ,state
           ,zipcode
           ,encccnumber
           ,encexpdate
           ,encccvcode
           ,deleted)
     VALUES
           (@Subscriber_ID
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,''
           ,''
           ,''
           ,'N'
			)

	--update leads table
	update leads set subscriber_id = @subscriber_id, SubscriberAddedToKshema = getdate() where current of leadscursor

	--get next lead
	fetch next from leadscursor
	into
	@Lead_ID,
	@SubscriberAddedToKshema,
	@Agency_ID,
	@Subscriber_ID,
	@SubFirstName,
	@SubLastName,
	@SubPhone,
	@SubAddress1,
	@SubAddress2,
	@SubCity,
	@SubState,
	@SubZip,
	@SubZipPlus4,
	@SubLanguage,
	@SubLineType,
	@RespName,
	@RespRelation,
	@RespHomePhone,
	@RespCellPhone,
	@RespHasKey,
	@ProdModel,
	@CCFrequency,
	@CCCardType,
	@CCName,
	@CCNumber,
	@CCExpDate,
	@CCCVV,
	@CCAddrSameAs,
	@CCContactPhone,
	@CCAddress1,
	@CCAddress2,
	@CCCity,
	@CCState,
	@CCZip

end

close leadscursor
deallocate leadscursor

if (@@ERROR > 0) 
    ROLLBACK TRANSACTION
ELSE
    COMMIT TRANSACTION

-- Set back the locking to default
SET TRANSACTION ISOLATION LEVEL READ COMMITTED


end

GO
/****** Object:  StoredProcedure [dbo].[AddLeadsToKshema_BU_20140128_AL]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AddLeadsToKshema_BU_20140128_AL]
as
--AL backed up to prepare for modification to include an "execute as amacbusiness"

/*
AddLeadsToKshema
20140123 IC
Added Sold leads to Kshema and amacccinfo tables
Called by scheduled job
exec AddLeadsToKshema
*/

begin

--procedure variables
declare @AgencyName varchar(50)
declare @SubType varchar(15)
declare @ProgramType varchar(25)
declare @Today datetime
declare @NoLandlineAvailable varchar(10)
declare @RequestMSD varchar(10)
declare @NextSubscriberResponder_ID bigint
declare @HasKey char(1)

--leads variables
declare @Lead_ID int
declare @SubscriberAddedToKshema datetime
declare @Agency_ID char(6)
declare @Subscriber_ID char(6)
declare @SubFirstName varchar(20)
declare @SubLastName varchar(30)
declare @SubPhone varchar(10)
declare @SubAddress1 varchar(30)
declare @SubAddress2 varchar(30)
declare @SubCity varchar(20)
declare @SubState char(2)
declare @SubZip char(6)
declare @SubZipPlus4 char(4)
declare @SubLanguage char(6)
declare @SubLineType varchar(15)
declare @RespName varchar(50)
declare @RespRelation char(6)
declare @RespHomePhone varchar(60)
declare @RespCellPhone varchar(25)
declare @RespHasKey int
declare @ProdModel varchar(15)
declare @CCFrequency varchar(25)
declare @CCCardType varchar(30)
declare @CCName varchar(30)
declare @CCNumber varchar(16)
declare @CCExpDate char(5)
declare @CCCVV varchar(4)
declare @CCAddrSameAs varchar(25)
declare @CCContactPhone varchar(25)
declare @CCAddress1 varchar(30)
declare @CCAddress2 varchar(30)
declare @CCCity varchar(25)
declare @CCState char(2)
declare @CCZip varchar(10)

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
-- Start the transaction
BEGIN TRANSACTION

declare leadscursor cursor for

SELECT Lead_ID
      ,SubscriberAddedToKshema
      ,Agency_ID
      ,Subscriber_ID
      ,SubFirstName
      ,SubLastName
      ,SubPhone
      ,SubAddress1
      ,SubAddress2
      ,SubCity
      ,SubState
      ,SubZip
      ,SubZipPlus4
      ,SubLanguage
      ,SubLineType
      ,RespName
      ,RespRelation
      ,RespHomePhone
      ,RespCellPhone
      ,RespHasKey
      ,ProdModel
      ,CCFrequency
      ,CCCardType
      ,CCName
      ,CCNumber
      ,CCExpDate
      ,CCCVV
      ,CCAddrSameAs
      ,CCContactPhone
      ,CCAddress1
      ,CCAddress2
      ,CCCity
      ,CCState
      ,CCZip
FROM Leads
where status = 'sold' and SubscriberAddedToKshema is null
order by lead_id

for update of SubscriberAddedToKshema, Subscriber_ID

open leadscursor

fetch next from leadscursor
into
@Lead_ID,
@SubscriberAddedToKshema,
@Agency_ID,
@Subscriber_ID,
@SubFirstName,
@SubLastName,
@SubPhone,
@SubAddress1,
@SubAddress2,
@SubCity,
@SubState,
@SubZip,
@SubZipPlus4,
@SubLanguage,
@SubLineType,
@RespName,
@RespRelation,
@RespHomePhone,
@RespCellPhone,
@RespHasKey,
@ProdModel,
@CCFrequency,
@CCCardType,
@CCName,
@CCNumber,
@CCExpDate,
@CCCVV,
@CCAddrSameAs,
@CCContactPhone,
@CCAddress1,
@CCAddress2,
@CCCity,
@CCState,
@CCZip

while @@fetch_status = 0
begin

	select @Today = DATEADD(day, DATEDIFF(day, 0, getdate()), 0)
	select @AgencyName = name from kshema_agency where agency_id = @Agency_ID
	select top 1 @ProgramType = ProgramType from Kshema_AgencyService where agency_id = @Agency_ID and programtype is not null
	if @ProdModel = 'cel450'  set @NoLandlineAvailable = 'YES' else set @NoLandlineAvailable = 'NO'
	if @ProdModel = 'msd' set @RequestMSD = 'YES' else set @RequestMSD = 'NO'
	if @ProdModel = 'msd' set @SubType = 'MSD' else set @SubType = '(PNC)'
	if isnull(@RespHasKey, 0) = 1 set @HasKey ='Y' else set @HasKey = 'N'
	
	--add subscriber, default responders, subscriberpl2info, subscriberoptionalservice
	EXEC Kshema_ap_Subscriber_i
	   @SubFirstName	--@vchrFirstName
	  ,''				--@vchrMiddleInitial
	  ,@SubLastName		--@vchrLastName
	  ,''				--@vchrKnownName
	  ,@SubAddress1		--@vchrAddress1
	  ,@SubAddress2		--@vchrAddress2
	  ,''				--@vchrNearIntersection
	  ,@SubState		--@chrState
	  ,@SubCity			--@vchrCity
	  ,''				--@vchrCounty
	  ,@SubZip			--@vchrZip
	  ,@SubZipPlus4		--@chrPlus4
	  ,@SubPhone		--@vchrPhone
	  ,''				--@vchrEMail
	  ,null				--@dteDOB
	  ,null				--@chrSex
	  ,null				--@vchrSSN
	  ,null				--@vchrMedicaid
	  ,null				--@vchrFax
	  ,'N'				--@chrSecondUser
	  ,''				--@vchrSpecialInst
	  ,''				--@vchrComments
	  ,@Agency_ID		--@chrAgency_ID
	  ,@AgencyName		--@vchrAgencyName
	  ,''				--@vchrUnit_ID
	  ,null				--@chrSubContractor_ID
	  ,'PENDING INSTALL'--@vchrStatus
	  ,@SubType			--@vchrType
	  ,'N'				--@chrForcedEntry
	  ,'R'				--@chrUnitRented_Sold
	  ,'N'				--@chrCancel
	  ,@Today			--@dteOrderDate
	  ,@Today			--@dteEntryDate
	  ,null				--@dteInstallDate
	  ,null				--@dteOnlineSince
	  ,null				--@dteRemovalDate
	  ,null				--@dteRequestedRemovalDate
	  ,'N'				--@chrEquipmentRecovered
	  ,'N'				--@chrConfirmReceived
	  ,null				--@dteFileMagic
	  ,@ProgramType		--@vchrProgramTypes
	  ,null				--@chrIsDayLightSaving
	  ,0				--@intTimeZonecode
	  ,''				--@chrLocked
	  ,'N'				--@chrDeleted
	  ,'N'				--@chrIsAddressVerified
	  ,@NoLandlineAvailable		--@NoLandlineAvailable
	  ,@RequestMSD				--@RequestMSD
	  ,null				--@MSDPhone


	--get subscriber_id
	select @Subscriber_ID = max(Subscriber_ID) from Kshema_Subscriber where lastname = @SubLastName and firstname = @SubFirstName

	--Add subscriber language
	exec Kshema_ap_SubscriberLanguage_i @Subscriber_ID, @SubLanguage, 'LAN'
	
	--add responder
	--select top 1 * from Kshema_SubscriberResponder	
	
	select @NextSubscriberResponder_ID = max(SubscriberResponder_ID) + 1 from Kshema_SubscriberResponder
	
	INSERT INTO Kshema_SubscriberResponder
           (SubscriberResponder_ID
           ,RelationType
           ,RelationCode
           ,Subscriber_ID
           ,ResponderName
           ,HasKey
           ,NVI
           ,HomePhone
           ,WorkPhone
           ,CellPhone
           ,Pager
           ,CallOrder
           ,NVIOrder
           ,ResponderVoid
           ,Deleted
           ,Comments)
     VALUES
           (@NextSubscriberResponder_ID
           ,'REL'
           ,@RespRelation
           ,@Subscriber_ID
           ,@RespName
           ,@HasKey
           ,'N'
           ,@RespHomePhone
           ,''
           ,@RespCellPhone
           ,''
           ,4
           ,0
           ,'Y'
           ,'N'
           ,''
           )

	--Add credit card info
	INSERT INTO Kshema_AMACccinfo
           (subscriber_id
           ,cardmembername
           ,ccnumber
           ,expdate
           ,ccvcode
           ,address1
           ,address2
           ,city
           ,state
           ,zipcode
           ,encccnumber
           ,encexpdate
           ,encccvcode
           ,deleted)
     VALUES
           (@Subscriber_ID
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,''
           ,''
           ,''
           ,'N'
			)

	--update leads table
	update leads set subscriber_id = @subscriber_id, SubscriberAddedToKshema = getdate() where current of leadscursor

	--get next lead
	fetch next from leadscursor
	into
	@Lead_ID,
	@SubscriberAddedToKshema,
	@Agency_ID,
	@Subscriber_ID,
	@SubFirstName,
	@SubLastName,
	@SubPhone,
	@SubAddress1,
	@SubAddress2,
	@SubCity,
	@SubState,
	@SubZip,
	@SubZipPlus4,
	@SubLanguage,
	@SubLineType,
	@RespName,
	@RespRelation,
	@RespHomePhone,
	@RespCellPhone,
	@RespHasKey,
	@ProdModel,
	@CCFrequency,
	@CCCardType,
	@CCName,
	@CCNumber,
	@CCExpDate,
	@CCCVV,
	@CCAddrSameAs,
	@CCContactPhone,
	@CCAddress1,
	@CCAddress2,
	@CCCity,
	@CCState,
	@CCZip

end

close leadscursor
deallocate leadscursor

if (@@ERROR > 0) 
    ROLLBACK TRANSACTION
ELSE
    COMMIT TRANSACTION

-- Set back the locking to default
SET TRANSACTION ISOLATION LEVEL READ COMMITTED


end

GO
/****** Object:  StoredProcedure [dbo].[AddSFContactsToKshema]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddSFContactsToKshema]
as
/*
AddSFLeadsToKshema
20140306 IC
Added Sold Salesforce contacts to Kshema and amacccinfo tables
Called by scheduled job
exec AddSFContactsToKshema_test
exec AddSFContactsToKshema

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType
select top 10 * from vw_SFContactsToKshema 
order by subscriberaddedtokshema desc
close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03


--update sfcontactstage set subscriber

select distinct prodmodel from vw_SFContactsToKshema 
select * from Kshema_Codes where type = 'lan' and deleted <> 'y'
*/

begin

--procedure variables
declare @AgencyName varchar(50)
declare @SubType varchar(15)
declare @ProgramType varchar(25)
declare @Today datetime
declare @AddToKshemaDT datetime
declare @NoLandlineAvailable varchar(10)
declare @RequestMSD varchar(10)
declare @NextSubscriberResponder_ID bigint
declare @HasKey char(1)
declare @Subscriber_ID1st char(6)
declare @Subscriber_ID2nd char(6)
declare @KshemaCode char(6)

--sf contacts variables
declare @ContactRecID varchar(18)
declare @SubscriberAddedToKshema datetime
declare @Agency_ID char(6)
declare @Subscriber_ID char(6)
declare @SubFirstName varchar(20)
declare @SubLastName varchar(30)
declare @SubPhone varchar(10)
declare @SubAddress1 varchar(30)
declare @SubAddress2 varchar(30)
declare @SubCity varchar(20)
declare @SubState char(2)
declare @SubZip char(6)
declare @SubZipPlus4 char(4)
declare @SubLanguage varchar(25)
declare @SubLineType varchar(15)
declare @RespName varchar(50)
declare @RespRelation varchar(20)
declare @RespHomePhone varchar(60)
declare @RespWorkPhone varchar(60)
declare @RespCellPhone varchar(25)
declare @RespHasKey int
declare @ProdModel varchar(25)
declare @ProdConfigure varchar(15)
declare @POpt2User int
declare @POpt2Activator varchar(15)
declare @POptLockbox varchar(15)
declare @CCFrequency varchar(25)
declare @CCCardType varchar(30)
declare @CCName varchar(30)
declare @CCNameFirst varchar(30)
declare @CCNameLast varchar(30)
declare @CCNumber varchar(16)
declare @CCExpDate char(5)
declare @CCCVV varchar(4)
declare @CCAddrSameAs varchar(25)
declare @CCContactPhone varchar(25)
declare @CCAddress1 varchar(30)
declare @CCAddress2 varchar(30)
declare @CCCity varchar(25)
declare @CCState char(2)
declare @CCZip varchar(10)
declare @SecondaryContact int
declare @Sub2FirstName varchar(20)
declare @Sub2LastName varchar(30)
declare @Sub2Language varchar(25)

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
-- Start the transaction
BEGIN TRANSACTION

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType

declare contactcursor cursor for

SELECT 

	   ContactRecID
      ,SubscriberAddedToKshema
      ,Agency_ID
      ,Subscriber_ID
      ,SubFirstName
      ,SubLastName
      ,SubPhone
      ,SubAddress1
      ,SubAddress2
      ,SubCity
      ,SubState
      ,SubZip
      ,SubZipPlus4
      ,SubLanguage
      ,SubLineType
      ,RespName
      ,RespRelation
      ,RespHomePhone
      ,RespWorkPhone
      ,RespCellPhone
      ,RespHasKey
      ,ProdModel
      ,ProdConfigure
      ,POpt2User
      ,POpt2Activator
      ,POptLockbox
      ,CCFrequency
      ,CCCardType
      ,CCName
      ,CCNameFirst
      ,CCNameLast
      ,CCNumber
      ,CCExpDate
      ,CCCVV
      ,CCAddrSameAs
      ,CCContactPhone
      ,CCAddress1
      ,CCAddress2
      ,CCCity
      ,CCState
      ,CCZip
      ,SecondaryContact
      ,Sub2FirstName
      ,Sub2LastName
      ,Sub2Language
FROM vw_SFContactsToKshema
where SubscriberAddedToKshema is null
order by ContactRecID

--for update of SubscriberAddedToKshema, Subscriber_ID

open contactcursor

fetch next from contactcursor
into
@ContactRecID,
@SubscriberAddedToKshema,
@Agency_ID,
@Subscriber_ID,
@SubFirstName,
@SubLastName,
@SubPhone,
@SubAddress1,
@SubAddress2,
@SubCity,
@SubState,
@SubZip,
@SubZipPlus4,
@SubLanguage,
@SubLineType,
@RespName,
@RespRelation,
@RespHomePhone,
@RespWorkPhone,
@RespCellPhone,
@RespHasKey,
@ProdModel,
@ProdConfigure,
@POpt2User,
@POpt2Activator,
@POptLockbox,
@CCFrequency,
@CCCardType,
@CCName,
@CCNameFirst,
@CCNameLast,
@CCNumber,
@CCExpDate,
@CCCVV,
@CCAddrSameAs,
@CCContactPhone,
@CCAddress1,
@CCAddress2,
@CCCity,
@CCState,
@CCZip,
@SecondaryContact,
@Sub2FirstName,
@Sub2LastName,
@Sub2Language

while @@fetch_status = 0
begin

	select @Today = DATEADD(day, DATEDIFF(day, 0, getdate()), 0)
	set @AddToKshemaDT = getdate()
	select @AgencyName = name from kshema_agency where agency_id = @Agency_ID
	select top 1 @ProgramType = ProgramType from Kshema_AgencyService where agency_id = @Agency_ID and programtype is not null

	--default - Standard PERS
	set @NoLandlineAvailable = 'NO'
	set @RequestMSD = 'NO'
	set @SubType = '(PNC)'
	-- Discounted Legacy
	--if left(@ProdModel, 15) = 'Discounted LEGA'
	if @ProdModel like 'Discounted LEGACY%'
	begin
		set @SubType = 'DSPNET' --???
	end
	--cellular
	if @ProdModel like 'Cell-based PERS%'  --in('Cell Based PERS','Cell-based PERS') 
	begin
		set @NoLandlineAvailable = 'YES'
		set @SubType = 'Cellular'
	end
	--msd
	--if left(@ProdModel, 15) = 'Mobile Safety D'
	if @ProdModel like 'Mobile Safety Device%'
	begin
		set @RequestMSD = 'YES'
		set @SubType = 'MSD'
	end

	if isnull(@RespHasKey, 0) = 1 set @HasKey ='Y' else set @HasKey = 'N'
	
	--add primary subscriber, default responders, subscriberpl2info, subscriberoptionalservice
	EXEC Kshema_ap_Subscriber_i
	   @SubFirstName	--@vchrFirstName
	  ,''				--@vchrMiddleInitial
	  ,@SubLastName		--@vchrLastName
	  ,''				--@vchrKnownName
	  ,@SubAddress1		--@vchrAddress1
	  ,@SubAddress2		--@vchrAddress2
	  ,''				--@vchrNearIntersection
	  ,@SubState		--@chrState
	  ,@SubCity			--@vchrCity
	  ,''				--@vchrCounty
	  ,@SubZip			--@vchrZip
	  ,@SubZipPlus4		--@chrPlus4
	  ,@SubPhone		--@vchrPhone
	  ,''				--@vchrEMail
	  ,null				--@dteDOB
	  ,null				--@chrSex
	  ,null				--@vchrSSN
	  ,null				--@vchrMedicaid
	  ,null				--@vchrFax
	  ,'N'				--@chrSecondUser
	  ,''				--@vchrSpecialInst
	  ,''				--@vchrComments
	  ,@Agency_ID		--@chrAgency_ID
	  ,@AgencyName		--@vchrAgencyName
	  ,''				--@vchrUnit_ID
	  ,'AMACNY'			--@chrSubContractor_ID
	  ,'PENDING INSTALL'--@vchrStatus
	  ,@SubType			--@vchrType
	  ,'N'				--@chrForcedEntry
	  ,'R'				--@chrUnitRented_Sold
	  ,'N'				--@chrCancel
	  ,@Today			--@dteOrderDate
	  ,@Today			--@dteEntryDate
	  ,null				--@dteInstallDate
	  ,null				--@dteOnlineSince
	  ,null				--@dteRemovalDate
	  ,null				--@dteRequestedRemovalDate
	  ,'N'				--@chrEquipmentRecovered
	  ,'N'				--@chrConfirmReceived
	  ,null				--@dteFileMagic
	  ,@ProgramType		--@vchrProgramTypes
	  ,null				--@chrIsDayLightSaving
	  ,0				--@intTimeZonecode
	  ,''				--@chrLocked
	  ,'N'				--@chrDeleted
	  ,'N'				--@chrIsAddressVerified
	  ,@NoLandlineAvailable		--@NoLandlineAvailable
	  ,@RequestMSD				--@RequestMSD
	  ,null				--@MSDPhone


	--get subscriber_id
	select @Subscriber_ID1st = max(Subscriber_ID) from Kshema_Subscriber where lastname = @SubLastName and firstname = @SubFirstName

	--Add subscriber language
	set @KshemaCode = 'NOCODE'
	select @KshemaCode = code from Kshema_Codes where type = 'lan' and deleted <> 'y' and description = @SubLanguage
	if @KshemaCode <> 'NOCODE'
		exec Kshema_ap_SubscriberLanguage_i @Subscriber_ID1st, @KshemaCode, 'LAN'

	if @SecondaryContact = 1 
	begin
		--add second subscriber, default responders, subscriberpl2info, subscriberoptionalservice
		EXEC Kshema_ap_Subscriber_i
		   @Sub2FirstName	--@vchrFirstName
		  ,''				--@vchrMiddleInitial
		  ,@Sub2LastName	--@vchrLastName
		  ,''				--@vchrKnownName
		  ,@SubAddress1		--@vchrAddress1
		  ,@SubAddress2		--@vchrAddress2
		  ,''				--@vchrNearIntersection
		  ,@SubState		--@chrState
		  ,@SubCity			--@vchrCity
		  ,''				--@vchrCounty
		  ,@SubZip			--@vchrZip
		  ,@SubZipPlus4		--@chrPlus4
		  ,@SubPhone		--@vchrPhone
		  ,''				--@vchrEMail
		  ,null				--@dteDOB
		  ,null				--@chrSex
		  ,null				--@vchrSSN
		  ,null				--@vchrMedicaid
		  ,null				--@vchrFax
		  ,'N'				--@chrSecondUser
		  ,''				--@vchrSpecialInst
		  ,''				--@vchrComments
		  ,@Agency_ID		--@chrAgency_ID
		  ,@AgencyName		--@vchrAgencyName
		  ,''				--@vchrUnit_ID
		  ,null				--@chrSubContractor_ID
		  ,'PENDING INSTALL'--@vchrStatus
		  ,@SubType			--@vchrType
		  ,'N'				--@chrForcedEntry
		  ,'R'				--@chrUnitRented_Sold
		  ,'N'				--@chrCancel
		  ,@Today			--@dteOrderDate
		  ,@Today			--@dteEntryDate
		  ,null				--@dteInstallDate
		  ,null				--@dteOnlineSince
		  ,null				--@dteRemovalDate
		  ,null				--@dteRequestedRemovalDate
		  ,'N'				--@chrEquipmentRecovered
		  ,'N'				--@chrConfirmReceived
		  ,null				--@dteFileMagic
		  ,@ProgramType		--@vchrProgramTypes
		  ,null				--@chrIsDayLightSaving
		  ,0				--@intTimeZonecode
		  ,''				--@chrLocked
		  ,'N'				--@chrDeleted
		  ,'N'				--@chrIsAddressVerified
		  ,@NoLandlineAvailable		--@NoLandlineAvailable
		  ,@RequestMSD				--@RequestMSD
		  ,null				--@MSDPhone


		--get subscriber_id
		select @Subscriber_ID2nd = max(Subscriber_ID) from Kshema_Subscriber where lastname = @Sub2LastName and firstname = @Sub2FirstName

		--Add subscriber language
		set @KshemaCode = 'NOCODE'
		select @KshemaCode = code from Kshema_Codes where type = 'lan' and deleted <> 'y' and description = @Sub2Language
		if @KshemaCode <> 'NOCODE'
		--2nd user languages seem to be all null
			exec Kshema_ap_SubscriberLanguage_i @Subscriber_ID2nd, @KshemaCode, 'LAN'
		
		--join subscribers
		exec Kshema_pl2_ap_LinkSubscribers_u @Agency_ID, @Subscriber_ID1st, @Subscriber_ID2nd

	end
	
	if @RespName is not null
	begin

		--add responder
		
		select @NextSubscriberResponder_ID = max(SubscriberResponder_ID) + 1 from Kshema_SubscriberResponder

		--Get responder relation
		set @KshemaCode = 'UNKNOW'
		select @KshemaCode = code from Kshema_Codes where type = 'rel' and deleted <> 'y' and description = @RespRelation
		
		INSERT INTO Kshema_SubscriberResponder
			   (SubscriberResponder_ID
			   ,RelationType
			   ,RelationCode
			   ,Subscriber_ID
			   ,ResponderName
			   ,HasKey
			   ,NVI
			   ,HomePhone
			   ,WorkPhone
			   ,CellPhone
			   ,Pager
			   ,CallOrder
			   ,NVIOrder
			   ,ResponderVoid
			   ,Deleted
			   ,Comments)
		 VALUES
			   (@NextSubscriberResponder_ID
			   ,'REL'
			   ,@KshemaCode
			   ,@Subscriber_ID1st
			   ,@RespName
			   ,@HasKey
			   ,'N'
			   ,isnull(@RespHomePhone, '')
			   ,isnull(@RespWorkPhone, '')
			   ,isnull(@RespCellPhone, '')
			   ,''
			   ,4
			   ,0
			   ,'Y'
			   ,'N'
			   ,''
			   )
	end

	--Add credit card info
	if db_name() = 'GMWGSales'  --production
	INSERT INTO Kshema_AMACccinfo
           (subscriber_id
           ,cardmembername
           ,ccnumber
           ,expdate
           ,ccvcode
           ,address1
           ,address2
           ,city
           ,state
           ,zipcode
           ,encccnumber
           ,encexpdate
           ,encccvcode
           ,deleted)
     VALUES
           (@Subscriber_ID1st
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,''
           ,''
           ,''
           ,'N'
			)
/*
	exec amac.dbo.AMACccinfo_Insert            
			@Subscriber_ID1st
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,'N'
*/
	else  --everything else = test
	exec amac_pnc_test.dbo.AMACccinfo_Insert            
			@Subscriber_ID1st
           ,@CCName
           ,@CCNumber
           ,@CCExpDate
           ,@CCCVV
           ,@CCAddress1
           ,@CCAddress2
           ,@CCCity
           ,@CCState
           ,@CCZip
           ,'N'
	
	--update leads table
	--update leads set subscriber_id = @subscriber_id, SubscriberAddedToKshema = getdate() where current of leadscursor
	
	--update sfcontactstage table
	update sfcontactstage set subscriber_id = @subscriber_id1st, SubscriberAddedToKshema = @AddToKshemaDT where ContactRecID = @ContactRecID
	update sfcontactstage set subscriber_id = @subscriber_id2nd, SubscriberAddedToKshema = @AddToKshemaDT where PrimaryContact = @ContactRecID and SecondaryContact = 1
	update sfcontactstage set subscriber_id = @subscriber_id1st, SubscriberAddedToKshema = @AddToKshemaDT, SubscriberResponderID = @NextSubscriberResponder_ID where PrimaryContact = @ContactRecID and Responder = 1

	--get next contact
	fetch next from contactcursor
	into
	@ContactRecID,
	@SubscriberAddedToKshema,
	@Agency_ID,
	@Subscriber_ID,
	@SubFirstName,
	@SubLastName,
	@SubPhone,
	@SubAddress1,
	@SubAddress2,
	@SubCity,
	@SubState,
	@SubZip,
	@SubZipPlus4,
	@SubLanguage,
	@SubLineType,
	@RespName,
	@RespRelation,
	@RespHomePhone,
	@RespWorkPhone,
	@RespCellPhone,
	@RespHasKey,
	@ProdModel,
	@ProdConfigure,
	@POpt2User,
	@POpt2Activator,
	@POptLockbox,
	@CCFrequency,
	@CCCardType,
	@CCName,
	@CCNameFirst,
	@CCNameLast,
	@CCNumber,
	@CCExpDate,
	@CCCVV,
	@CCAddrSameAs,
	@CCContactPhone,
	@CCAddress1,
	@CCAddress2,
	@CCCity,
	@CCState,
	@CCZip,
	@SecondaryContact,
	@Sub2FirstName,
	@Sub2LastName,
	@Sub2Language

end

close contactcursor
deallocate contactcursor

close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03

if (@@ERROR > 0) 
    begin
		print @@error
		ROLLBACK TRANSACTION
	end
ELSE
    COMMIT TRANSACTION

-- Set back the locking to default
SET TRANSACTION ISOLATION LEVEL READ COMMITTED


end
GO
/****** Object:  StoredProcedure [dbo].[ENCRYPT_CC]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Alireza, Jamzad>
-- Create date: <2/24/2014>
-- Description:	<Encryt Credit Car info>
-- =============================================
CREATE PROCEDURE [dbo].[ENCRYPT_CC] 
@Id nvarchar(18),
@CardType nvarchar(255),
@CardNumber nvarchar(255),
@Cvv_c float,
@CreatedDate datetime,
@ModifiedDate datetime,
@ExpMonth nvarchar(255),
@ExpYear nvarchar(255)

AS
BEGIN
DECLARE @CCNumber varbinary(128)
DECLARE @CVV varbinary(128)
DECLARE @CCType varbinary(128)
DECLARE @Exp_Month char(2)
DECLARE @Exp_Year char(4)

	SET NOCOUNT ON;

	OPEN SYMMETRIC KEY CCN_Key_01
    DECRYPTION BY CERTIFICATE CreditCardNumber;

	SET @CCNumber = EncryptByKey(Key_GUID('CCN_Key_01'), @CardNumber);

	OPEN SYMMETRIC KEY CCN_Key_02
    DECRYPTION BY CERTIFICATE CVV;

	SET @CVV = EncryptByKey(Key_GUID('CCN_Key_02'), CONVERT(NVARCHAR(255),@Cvv_c));

	OPEN SYMMETRIC KEY CCN_Key_03
    DECRYPTION BY CERTIFICATE CreditCardType

	SET @CCType = EncryptByKey(Key_GUID('CCN_Key_03'), @CardType);

	SET @Exp_Month = CONVERT(CHAR(2),SUBSTRING(LTRIM(RTRIM(@ExpMonth)),1,2))
	SET @Exp_Year = CONVERT(CHAR(4),SUBSTRING(LTRIM(RTRIM(@ExpYear)),1,4))
	
	Insert into [dbo].[SF_CC]([Id],[CCType],[CCNumber], [CVV],[CreatedDate], [ModifiedDate], [Exp_Month__c], [Exp_Year__c])
	values(@Id, @CCType, @CCNumber, @Cvv, @CreatedDate, @ModifiedDate, @ExpMonth, @ExpYear)
END



select case when max(ModifiedDate) is not null then max(ModifiedDate)
  else convert(datetime,'01/01/2013')  
  end as LastModifiedDate from dbo.SF_CC

GO
/****** Object:  StoredProcedure [dbo].[ProductsPrices]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ProductsPrices] 
    @Agency_ID varchar(6)
as

declare @ProductsPrices table(Model varchar(15), MonthlyRate smallmoney, ActivationFee smallmoney)

-- 'Vi'
insert into @ProductsPrices 
values
(
    'Vi',    
    case 
	   when @Agency_ID = 'WG0005' then 29.99 
	   when @Agency_ID = 'APR401' then 29.95
	   else 29.95 
    end,    
    0
)

-- CEL450
insert into @ProductsPrices 
values
(
    'CEL450',    
    case 
	   when @Agency_ID = 'WG0005' then 36.99 
	   when @Agency_ID = 'APR401' then 39.95
	   else 34.95 
    end, 
    0
)

-- MSD
if @Agency_ID = 'WG0005'
begin
    insert into @ProductsPrices values('MSD',39.99,14.99)
end

if @Agency_ID = 'APR401'
begin
    insert into @ProductsPrices values('MSD',49.95,20.00)
end

select * from @ProductsPrices
GO
/****** Object:  StoredProcedure [dbo].[rpt_SMSA_SF]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_SMSA_SF] (@Subscriber_ID char(6)) as

/*
rpt_SMSA_SF
20140612 IC
Returns one SF primary contact with related contacts and decrypted CC info
Used by SMSA report

exec rpt_SMSA_SF '560393'
*/

begin

open symmetric key CCN_Key_01 decryption by certificate CreditCardNumber
open symmetric key CCN_Key_02 decryption by certificate CVV
open symmetric key CCN_Key_03 decryption by certificate CreditCardType
--select distinct cccardtype from vw_SFContactsToKshema 
select * from vw_SFContactsToKshema 
where subscriber_id = @Subscriber_ID
--where subscriber_id in ('564752','565625','564478','566578')
--where subscriber_id in ('553970','555289','555609','555612','547841')
--where subscriber_id in('547356','547358')
--select * from vw_SFContactsToKshema where subscriber_id is null
close symmetric key CCN_Key_01
close symmetric key CCN_Key_02
close symmetric key CCN_Key_03

end
GO
/****** Object:  StoredProcedure [dbo].[SF2SBTSOHelper]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SF2SBTSOHelper]
(
@Subscriber_ID char(6)
)

/*
SF2SBTSOHelper
20140717 IC
Returns line items, comments, email comment for a subscriber in sfcontactstage

test subs
570339 - AK VI
570215 - VK VI 
570325 - NK VI 2A LB 
570108 - VK VI 2U 
570300 - AK CEL 
570343 - AK CEL LB 
568164 - VK CEL 2U 2A LB
570327 - NK MSD LB 

exec SF2SBTSOHelper '570327'
select * from vw_sfcontactstokshema where subscriber_id = '570325'
*/


as

begin

--db vars
declare @Additional_Extra_Activator_Configuration varchar(255)
declare @Agency_ID char(6)
declare @CCFrequency varchar(25)
declare @CouponCode varchar(255)
declare @POpt2Activator varchar(15)
declare @POpt2User int
declare @POpt2UserConfigure varchar(15)
declare @POptLockbox varchar(15)
declare @ProdActivationFee float
declare @ProdConfigure varchar(15)
declare @ProdModel varchar(25)
declare @ProdMonthlyRate float
declare @ProfInstall varchar(15)
declare @ShipMethod varchar(25)
declare @SBTSODATE datetime
declare @SBTSONO char(6)
declare @Sub2FirstName varchar(20)
declare @Sub2LastName varchar(30)
declare @Sub2Subscriber_ID char(6)
declare @SubLastName varchar(30)

--proc vars
declare @ActQty int
declare @OptString varchar(15)
declare @Comment1 varchar(255) --OptStringExp
declare @Comment2 varchar(255) --EmailComment
declare @Comment3 varchar(255) --Comment3
declare @EmailBody varchar(255)
declare @EmailSubject varchar(255)

create table #SOItems (linenum int IDENTITY(1,1) NOT NULL, itemno varchar(15) not null, qty int not null) 

select 
@Additional_Extra_Activator_Configuration = isnull(Additional_Extra_Activator_Configuration, ''),
@Agency_ID = isnull(Agency_ID, ''),
@CCFrequency = isnull(CCFrequency, ''),
@CouponCode = isnull(CouponCode, ''),
@ProdConfigure = isnull(ProdConfigure, ''),
@ProdModel = isnull(ProdModel, ''),
@ProdMonthlyRate = isnull(ProdMonthlyRate, 0),
@POpt2Activator = isnull(POpt2Activator, ''),
@POpt2User = isnull(POpt2User, 0),
@POpt2UserConfigure = isnull(POpt2UserConfigure, ''),
@POptLockbox = isnull(POptLockbox, ''),
@ProdActivationFee = isnull(ProdActivationFee, 0),
@ProfInstall = isnull(ProfInstall, ''),
@SBTSODATE = isnull(SBTSODATE, '1/1/1980'),
@SBTSONO = isnull(SBTSONO, ''),
@ShipMethod = isnull(ShipMethod, ''),
@Sub2FirstName = isnull(Sub2FirstName, ''),
@Sub2LastName = isnull(Sub2LastName, ''),
@Sub2Subscriber_ID = isnull(Sub2Subscriber_ID, ''),
@SubLastName = isnull(SubLastName, '')
from vw_sfcontactstokshema where subscriber_id = @subscriber_id


--base item(s)
insert into #SOItems (itemno, qty)
select itemno, 1
from SBTSOITEMS
where agency_id = @Agency_ID and prodmodel = @ProdModel
and itemgroup = 'base'
order by sequence

--2nd user/activators options  (exta activators) (need to rework if more than just activator are offered)
set @ActQty = 0
if @POpt2User <>0 set @ActQty = @ActQty + 1
if @POpt2Activator = 'yes' set @ActQty = @ActQty + 1
if @ActQty > 0
	insert into #SOItems (itemno, qty)
	select itemno, @ActQty
	from SBTSOITEMS
	where agency_id = @Agency_ID and prodmodel = @ProdModel
	and itemgroup = '2A2U'
	order by sequence

--Lockbox rental
if @POptLockbox = 'yes'
	insert into #SOItems (itemno, qty)
	select itemno, 1
	from SBTSOITEMS
	where agency_id = @Agency_ID and prodmodel = @ProdModel
	and itemgroup = 'LB'
	order by sequence


-------------------------------------------------------------------------------------------------------------
--Comments/Email/PONum
-------------------------------------------------------------------------------------------------------------

--activation kit
set @OptString = 'NK'
select @OptString = case when @CouponCode = 'ACK KIT' then 'AK' else @OptString end
select @OptString = case when @CouponCode in('VIRT KIT','VIRTUAL KIT') then 'VK' else @OptString end

set @Comment1 = 'No Act Kit'
select @Comment1 = case when @CouponCode = 'ACK KIT' then 'Act Kit' else @Comment1 end
select @Comment1 = case when @CouponCode in('VIRT KIT','VIRTUAL KIT') then 'Virtual Kit' else @Comment1 end

select @OptString = @OptString + 
	case @ProdModel
		when 'Standard PERS' then ' VI'
		when 'Cell-based PERS' then ' CEL'
		when 'Mobile Safety Device' then ' MSD'
		else ''
	end

select @Comment1 = @Comment1 + 
	case @ProdModel
		when 'Standard PERS' then ' / STD PERS'
		when 'Cell-based PERS' then ' / Cellular'
		when 'Mobile Safety Device' then ' / MSD'
		else ''
	end

select @OptString = @OptString + 
	case @POpt2User when 1 then ' 2U' else '' end

select @Comment1 = @Comment1 + 
	case @POpt2User when 1 then ' /2nd User' else '' end

select @OptString = @OptString + 
	case @POpt2Activator when 'yes' then ' 2A' else '' end

select @Comment1 = @Comment1 + 
	case @POpt2Activator when 'yes' then ' /2nd Activator' else '' end

select @OptString = @OptString + 
	case @POptLockBox when 'yes' then ' LB' else '' end

select @Comment1 = @Comment1 + 
	case @POptLockBox when 'yes' then ' /Lockbox' else '' end

select @Comment1 = @Comment1 + ' Ship Date :'

select @Comment2 = 
	case @ProdConfigure
		when 'wristband' then 'Wrist'
		when 'pendant' then 'Neck'
		when 'belt clip' then 'Belt'
		else @ProdConfigure
	end

select @Comment2 = @Comment2 + 
	case @POpt2UserConfigure
		when 'wristband' then ' - Wrist'
		when 'pendant' then ' - Neck'
		when 'belt clip' then ' - Belt'
		else ''
	end

select @Comment2 = @Comment2 + 
	case @Additional_Extra_Activator_Configuration
		when 'wristband' then ' - Wrist'
		when 'pendant' then ' - Neck'
		when 'belt clip' then ' - Belt'
		when 'mounted/velcro' then ' - Bathroom'
		else ''
	end

select @Comment2 = @Comment2 + 
	case @POpt2User
		when 1 then '/ 2nd User ' + isnull(@Sub2LastName, '') + ', ' + isnull(@Sub2FirstName, '')
		else '' 
	end

select @Comment3 = 
	case when @ProfInstall = 'yes' then '*PROINSTALL* $75* ' else '' end

select @Comment3 = @Comment3 +
	case when @ProdActivationFee <> 0 then '1x $' + convert(varchar,round(convert(smallmoney,@ProdActivationFee ), 2)) + ' Act Fee *' else '' end

select @Comment3 = @Comment3 + '$' + convert(varchar,round(convert(smallmoney,@ProdMonthlyRate ), 2))
	
select @Comment3 = @Comment3 + ' ' + @CCFrequency

if @ProdModel in ('Standard PERS','Cell-based PERS')
	select @Comment3 = @Comment3 + '* PROG TO WG#S*'

if @ShipMethod <> 'Regular FREE'
	select @Comment3 = @Comment3 + @ShipMethod

-------------------------------------------------------------------------------------------------------------
--  Email Subject
-------------------------------------------------------------------------------------------------------------

select @EmailSubject = 'New SF SO Created - STO# ' + @SBTSONO + ' - Sub# ' + @Subscriber_ID
					 + ' - PO Num ' + upper(left(@SubLastName, 10)) + ' ' + @OptString 

-------------------------------------------------------------------------------------------------------------
--  Email Body
-------------------------------------------------------------------------------------------------------------

select @EmailBody = 'STO#: ' + @SBTSONO + ' / ' + upper(left(@SubLastName, 10)) + ' ' + @OptString + ' / SUB#:' + @Subscriber_ID
					+ ' / SUB NAME: ' + upper(@SubLastName) + ' / DATE:' + convert(varchar, @SBTSODATE, 101)
					+ ' / SHIPPING:' + case when charindex(' ', @ShipMethod) > 0 then left(@ShipMethod, charindex(' ', @ShipMethod) - 1) else @ShipMethod end
					+ ' / ' + @Comment2

-------------------------------------------------------------------------------------------------------------
--  Output Resultsets
-------------------------------------------------------------------------------------------------------------

select convert(char(78), @Comment1) as Comments
union all
select convert(char(78), @Comment2)
union all
select convert(char(78), @Comment3)

select * from #SOItems

select @EmailSubject as EmailSubject

select @EmailBody as EmailBody

drop table #SOItems

end

GO
/****** Object:  StoredProcedure [dbo].[sp_GMWGSalesInsert]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--sql1
Create Procedure [dbo].[sp_GMWGSalesInsert] as
--11/27/2012
--Used to insert new gmwgsales contacts
--use GMWGSales
insert into contact (
Company, 
impACTIVFEE, 
impADDQUEST, 
impANI, 
impCALLADD1, 
impCALLADD2, 
impCALLBACK, 
impCALLBDATE, 
impCALLBTIME, 
impCALLBZONE, 
impCALLEMAIL, 
impCALLFNAME, 
impCALLLNAME, 
Phone1, 
UCARDNUM1, 
UCARDNUM2, 
impCAUSE, 
UCCAdd1_1, 
UCCAdd1_2, 
UCCAdd2_1, 
UCCAdd2_2, 
impCCADDR, 
impCCADDR2, 
UCCAgree, 
UCCNAME1, 
UCCNAME2, 
City, 
userdef56, 
UCCCity4, 
UCCCity5, 
userdef84, 
impCITY7, 
UserDef89, 
UserDef90, 
UserDef91, 
USERDEF92, 
impCOMMENT5, 
impCOMMENT6, 
Uconfigure, 
UCreditCr1, 
UCreditCr2, 
UCVVCD1, 
UCVVCD2, 
UPinDate, 
impDDACCT, 
impDDBANK, 
impDDNAME, 
impDDROUTING, 
impDDVOID, 
UDID, 
impDIFPAYMENT, 
impEMPLOYNAM, 
impEMPLOYNUM, 
Uexp_DT1, 
Uexp_DT2, 
UserDef77, 
UINSTFEE, 
impINTPAYMENT, 
impLEADQUAL, 
UserDef61, 
impLITEMAIL, 
impMAILADD1, 
impMAILADD2, 
impMAILADDR, 
impMAILEMAIL, 
impMAILFNAME, 
impMAILINFO, 
impMAILLNAME, 
impOPER_ID, 
impORDEMAIL, 
impORDER_NUM, 
impORDERNOW, 
impPAYMENT, 
impPRODUCTFOR, 
UPROFINST, 
Userdef01, 
Title, 
UserDef63, 
impRESPEMAIL, 
impRESPFNAME, 
UWGRPHN1, 
UWGRKEY, 
impRESPLNAME, 
impRESPONDER, 
UWGRRELTN, 
UserDef87, 
userdef82, 
userdef83, 
userdef79,--impSHIPADDR, 
UserDef88, 
userdef80, 
userdef81, 
State, 
userdef57, 
UCCState4, 
UCCState5, 
userdef85, 
impSTATE7, 
impSTATION, 
UserDef11, 
userdef53, 
userdef55, 
userdef60, 
userdef50, 
userdef59, 
userdef51, 
userdef52, 
impTAS_CSN, 
UPinTIme, 
Zip, 
userdef58, 
UCCZip4, 
UCCZip5, 
UserDef86, 
impZIP7, 
impSold, 
impStatus, 
impCloseDateT, 
impEnteredKsh, 
impAMACNotes, 
impPrinted, 
impImportdate, 
impDIDMap, 
impRADPROG, 
impREFBY, 
impCCVERIFY1, 
impCCVERIFY2, 
USUBFNAME2, 
USUBLNAME2, 
Uconfig2, 
Utype, 
USUBSEC, 
impGENDER, 
UShipType, 
UEMPDISC, 
impEPN, 
UTVSTA, 
impOperName, 
userdef95, 
UValidSub, 
URefSub, 
UCoupon, 
impNotes,
Key3, 
accountno, 
Usubs, 
CreateOn, 
CreateAt, 
Contact, 
UWGRNAME, 
usubs2, 
Key4, 
Key1

) 

select v.*, '' as leadstatus,pinnacleor as accountno, '' as subid, getdate(), convert (char(8),getdate(),108) as createat,isnull(CALLFNAME,'') + ' ' + isnull(CALLLNAME,'') as Contact,
isnull(RESPFNAME,'') + ' ' + isnull(RESPLNAME,'') as UWGRName, '' as usubs2, isnull(o.OperatorName,'') as key4,'' as key1
 from amacas3.walgreens.dbo.vwpinnacleorders v
left outer join amacas3.walgreens.dbo.PinnacleOperatorMap o on o.OperatorID=v.oper_id
where --ccagree is not null  and 
pinnacleor not in (select cast(accountno as bigint) from contact where accountno is not null)
order by pinnacleor --desc

select * from amac.dbo.[Report_GMWGSalesRecon]  where key3='' and uccagree='YES'
update amac.dbo.[Report_GMWGSalesRecon] set key3='SOLD' where key3='' and uccagree='YES'

---select convert (char(8),getdate(),108)


--select top 10 * from contact order by contact_id desc

--select * from contact where impdidmap='VNSNY1' and usubs<>'' order by cast (accountno as bigint) desc

GO
/****** Object:  StoredProcedure [dbo].[spPromoCodes]    Script Date: 2/11/2016 1:50:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Roger McCoy
-- Create date: 2015-11-17
-- Description:      Retrieve applicable promo codes
-- =============================================
CREATE PROCEDURE [dbo].[spPromoCodes] 
       @Agency_ID char(6)
AS
BEGIN
       SET NOCOUNT ON;

       SELECT TOP 1000 [ID]
                ,[Code]
                ,[Description]
                ,[DurationDate]
                ,[DurationDays]
                ,[DurationMonths]
                ,[DiscountDollars]
                ,[DiscountPercent]
                ,[DiscountItem]
         FROM [PromoCodes]
              WHERE AllAgencies = 1 OR ID IN (SELECT PromoCode_ID FROM AgencyPromoCodes WHERE Agency_ID = @Agency_ID)
                     AND (StartDate IS NULL OR StartDate > getdate())
                     AND (EndDate IS NULL OR EndDate <= CONVERT(datetime, Floor(CONVERT(float, getdate()))))

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code that must be entered by the user to obtain the discount.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of the discount.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The first day that the discount will be accepted on the web site' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'StartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The last day that the discount will be accepted on the web site. If the discount is available in March 2016, this should be "3/31/16", not "4/1/16".' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'EndDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date that the discount will stop being applied. Only used for recurring charges. Not always applicable.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DurationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of days that the discount will last once used. Only used for recurring charges. Not always applicable.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DurationDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of months that the discount will last once used. Only used for recurring charges. Not always applicable.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DurationMonths'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The discount in dollars.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DiscountDollars'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The discount as a percent.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DiscountPercent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A description of the discounted item. Note that the text description *must* be understandable to the program to be applied correctly. Long term this might be changed to use a foreign key either to a list of acceptable descriptions or to item tables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'DiscountItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indicates if the discount applies to all agencies. If =0 then agencies must be explicitly listed via AgencyPromoCodes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PromoCodes', @level2type=N'COLUMN',@level2name=N'AllAgencies'
GO
USE [master]
GO
ALTER DATABASE [GMWGSales] SET  READ_WRITE 
GO
