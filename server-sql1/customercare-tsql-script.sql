USE [master]
GO
/****** Object:  Database [CustomerCare]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE DATABASE [CustomerCare] ON  PRIMARY 
( NAME = N'CustomerCare_Data', FILENAME = N'F:\DATA\MSSQL\Data\CustomerCare_Data.MDF' , SIZE = 4416KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'CustomerCare_Log', FILENAME = N'G:\DATA\MSSQL\Data\CustomerCare_Log.LDF' , SIZE = 1280KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [CustomerCare] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CustomerCare].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [CustomerCare] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CustomerCare] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CustomerCare] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CustomerCare] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CustomerCare] SET ARITHABORT OFF 
GO
ALTER DATABASE [CustomerCare] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CustomerCare] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CustomerCare] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CustomerCare] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CustomerCare] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CustomerCare] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CustomerCare] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CustomerCare] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CustomerCare] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CustomerCare] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CustomerCare] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CustomerCare] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CustomerCare] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CustomerCare] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CustomerCare] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CustomerCare] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CustomerCare] SET RECOVERY FULL 
GO
ALTER DATABASE [CustomerCare] SET  MULTI_USER 
GO
ALTER DATABASE [CustomerCare] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [CustomerCare] SET DB_CHAINING OFF 
GO
USE [CustomerCare]
GO
/****** Object:  User [staherian]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE USER [staherian] FOR LOGIN [ERC_DOMAIN\staherian] WITH DEFAULT_SCHEMA=[staherian]
GO
/****** Object:  User [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE USER [ERC_DOMAIN\REPORTING] FOR LOGIN [ERC_DOMAIN\Reporting] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\REPORTING]
GO
/****** Object:  User [AMAC]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE USER [AMAC] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[AMAC]
GO
ALTER ROLE [db_owner] ADD MEMBER [staherian]
GO
ALTER ROLE [db_owner] ADD MEMBER [AMAC]
GO
/****** Object:  Schema [AMAC]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE SCHEMA [AMAC]
GO
/****** Object:  Schema [ERC_DOMAIN\REPORTING]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE SCHEMA [ERC_DOMAIN\REPORTING]
GO
/****** Object:  Schema [staherian]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE SCHEMA [staherian]
GO
/****** Object:  Table [dbo].[AccountsTAS]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountsTAS](
	[AccountsTASID] [int] IDENTITY(1,1) NOT NULL,
	[Infinity_ID] [int] NULL,
	[Account_Name] [varchar](60) NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Accounts_Status]  DEFAULT (1),
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[AccountsTASID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Descr] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Category_Active]  DEFAULT (1),
	[complaintsystem] [char](3) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NoteType]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NoteType] [varchar](50) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_NoteType_Active]  DEFAULT (1),
 CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperatorsTAS]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OperatorsTAS](
	[OperatorsTASID] [int] IDENTITY(1,1) NOT NULL,
	[EmpIDFK] [int] NOT NULL,
	[OperatorID] [char](10) NOT NULL,
	[Active] [bit] NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProblemType]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProblemType](
	[ProblemTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProblemType] [varchar](50) NOT NULL,
	[Description] [varchar](150) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_ProblemType_Active]  DEFAULT (1),
	[complaintsystem] [char](3) NOT NULL,
	[OrderId] [int] NULL,
 CONSTRAINT [PK_ProblemType] PRIMARY KEY CLUSTERED 
(
	[ProblemTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusText] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ticket](
	[TicketID] [int] IDENTITY(1,1) NOT NULL,
	[Summary] [varchar](500) NULL,
	[Description] [varchar](7000) NULL,
	[FKOwnerID] [int] NOT NULL,
	[ReportedTo] [char](55) NULL,
	[FKUserID] [char](45) NULL,
	[FKUserID2] [char](45) NULL,
	[FKUserID3] [char](45) NULL,
	[FKUserID4] [char](45) NULL,
	[TicketDateCreated] [datetime] NOT NULL CONSTRAINT [DF_Ticket_TicketDateCreated]  DEFAULT (getdate()),
	[FKStatusID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKStatus]  DEFAULT (1),
	[FKProblemTypeID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKProblemType]  DEFAULT (1),
	[FKCategoryID] [int] NOT NULL CONSTRAINT [DF_Ticket_FKCategory]  DEFAULT (1),
	[CloseDateTarget] [datetime] NULL,
	[CloseDateActual] [datetime] NULL,
	[Priority] [char](15) NOT NULL CONSTRAINT [DF_Ticket_Priority]  DEFAULT ('Med'),
	[FKAccountID] [int] NULL,
	[FKAgencyID] [char](10) NULL,
	[CareManager] [varchar](50) NULL,
	[ContactNumber] [varchar](50) NULL,
	[subscriber_id] [varchar](20) NULL,
	[ContactNumber2] [varchar](10) NULL,
	[complaintsystem] [char](4) NULL,
	[IncidentDate] [datetime] NULL,
	[escalatedto1] [varchar](50) NULL,
	[escalatedby1] [int] NULL,
	[escalatedto2] [varchar](50) NULL,
	[escalatedby2] [int] NULL,
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[TicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TicketNote]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TicketNote](
	[TicketNoteID] [int] IDENTITY(1,1) NOT NULL,
	[FKTicketID] [int] NOT NULL,
	[FKNoteOwnerID] [int] NOT NULL CONSTRAINT [DF_TicketNote_FKNoteOwnerID]  DEFAULT (200),
	[Description] [varchar](2000) NULL,
	[FKNoteTypeID] [int] NOT NULL CONSTRAINT [DF_TicketNote_FKNoteType]  DEFAULT (1),
	[NoteDateCreated] [datetime] NOT NULL CONSTRAINT [DF_TicketNote_NoteDateCreated]  DEFAULT (getdate()),
	[NoteDateFinished] [datetime] NOT NULL CONSTRAINT [DF_TicketNote_NoteDateFinished]  DEFAULT (getdate()),
	[TotalTime] [varchar](50) NULL CONSTRAINT [DF_TicketNote_TotalTime]  DEFAULT (0),
	[contact] [char](50) NULL,
	[contactnumber] [char](20) NULL,
 CONSTRAINT [PK_TicketNote] PRIMARY KEY CLUSTERED 
(
	[TicketNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[rpt_IT_user]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  View dbo.rpt_IT_user    Script Date: 11/9/2005 5:49:24 PM ******/


CREATE view [dbo].[rpt_IT_user]
as
select * from CustomerCareStaff





GO
/****** Object:  View [dbo].[vGetTicketDetailSummary]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/****** Object:  View dbo.vGetTicketDetailSummary    Script Date: 11/9/2005 5:49:25 PM ******/



/****** Object:  View dbo.vGetTicketDetailSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE  VIEW [dbo].[vGetTicketDetailSummary]
AS

SELECT     dbo.Ticket.TicketID, dbo.Ticket.CareManager, dbo.Ticket.ContactNumber, dbo.Ticket.ReportedTo,  
	dbo.Ticket.FKAccountID, dbo.AccountsTAS.account_name, dbo.Ticket.TicketDateCreated, dbo.Ticket.CloseDateActual, dbo.Ticket.CloseDateTarget, 
	dbo.Ticket.FKProblemTypeID, dbo.ProblemType.ProblemType,
	dbo.Ticket.FKCategoryID, Category.descr,
	dbo.Ticket.Priority, dbo.Ticket.FKStatusID, dbo.Status.StatusText, 
	dbo.Ticket.FKUserID,
	case when dbo.Ticket.FKUserID2='0' then ' ' else ticket.FKUserID2 end as FKUserID2, case when dbo.Ticket.FKUserID3='0' then ' ' else ticket.FKUserID3 end as FKUserID3, 
	case when dbo.Ticket.FKUserID4='0' then ' ' else ticket.FKUserID4 end as FKUserID4, dbo.Ticket.Description,
	dbo.TicketNote.TicketNoteID, dbo.TicketNote.NoteDateCreated, employee.FirstName + ' ' + employee.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
	TicketNote.contact as NoteContact, TicketNote.contactnumber as NotePhone, NoteType.notetype

FROM         dbo.Ticket 
		INNER JOIN dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID 
		INNER JOIN dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID 
		INNER JOIN dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID 
		inner join dbo.AccountsTAS on dbo.ticket.FKAccountID=dbo.AccountsTAS.Infinity_id
		left join dbo.TicketNote on TicketNote.FKTicketID=Ticket.TicketID
		LEFT join dbo.NoteType ON TicketNote.FKNoteTypeID = dbo.NoteType.ID
		LEFT join helpdesk.dbo.employee employee ON employee.empID = dbo.TicketNote.FKNoteOwnerID

/*SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, employee.FirstName + ' ' + employee.LastName AS Owner, 
                       dbo.Ticket.TicketDateCreated, dbo.Status.StatusText, 
                      dbo.ProblemType.ProblemType, dbo.problemtype.ProblemTypeID, dbo.Category.Descr, dbo.category.CategoryID, dbo.Ticket.CloseDateTarget, dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, 
	        dbo.TicketNote.TicketNoteID, 
                      dbo.TicketNote.NoteDateCreated, employee_2.FirstName + ' ' + employee_2.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
                      dbo.Ticket.FKOwnerID, dbo.Ticket.FKUserID, dbo.NoteType.NoteType, agency.name AS Account, ticket.FKAgencyID AS AccountID, dbo.ticket.caremanager, ticket.unit_id, ticket.subscriber_id

FROM         helpdesk.dbo.employee employee_2 INNER JOIN
                      dbo.TicketNote ON employee_2.empID = dbo.TicketNote.FKNoteOwnerID INNER JOIN
                     dbo.NoteType ON dbo.TicketNote.FKNoteTypeID = dbo.NoteType.ID RIGHT OUTER JOIN
                      dbo.Ticket INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID INNER JOIN
                       AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id inner join
   	        dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID ON dbo.TicketNote.FKTicketID = dbo.Ticket.TicketID
*/







































GO
/****** Object:  View [dbo].[vGetTicketDetailSummaryERC]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/****** Object:  View dbo.vGetTicketDetailSummaryERC    Script Date: 12/7/2005 JOEL TOMPKINS ******/
CREATE   VIEW [dbo].[vGetTicketDetailSummaryERC]
AS
SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, employee.FirstName + ' ' + employee.LastName AS Owner, 
                       dbo.Ticket.TicketDateCreated, dbo.Status.StatusText, 
                      dbo.ProblemType.ProblemType, dbo.ProblemType.description as problem, dbo.problemtype.ProblemTypeID, 
		dbo.ticket.FKCategoryID, category.descr, dbo.Ticket.CloseDateTarget, dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, 
	       	       dbo.TicketNote.TicketNoteID, 
                      dbo.TicketNote.NoteDateCreated, employee_2.FirstName + ' ' + employee_2.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
                      dbo.Ticket.FKOwnerID, case when dbo.Ticket.FKUserID2='0' then ' ' else ticket.FKUserID end as FKUserID,
	case when dbo.Ticket.FKUserID2='0' then ' ' else ticket.FKUserID2 end as FKUserID2,
	case when dbo.Ticket.FKUserID3='0' then ' ' else ticket.FKUserID2 end as FKUserID3,
	dbo.NoteType.NoteType, agency.agency_id, agency.name AS Account, ticket.FKAgencyID AS AccountID, dbo.ticket.caremanager, 
	ticket.ContactNumber,ticket.ContactNumber2, ticket.subscriber_id, subscriber.firstname + ' ' + subscriber.LastName as Subscriber,
	employee_3.FirstName + ' ' + employee_3.LastName AS op1, employee_4.FirstName + ' ' + employee_4.LastName AS op2, employee_5.FirstName + ' ' + employee_5.LastName AS op3,
	ticket.incidentdate, ticket.escalatedto1, employee_6.FirstName + ' ' + employee_6.LastName AS escalatedby1, ticket.escalatedto2, employee_7.FirstName + ' ' + employee_7.LastName AS escalatedby2,
	ticket.reportedto, ticket.FKUserID4

FROM         helpdesk.dbo.employee employee_2 INNER JOIN
                      dbo.TicketNote ON employee_2.empID = dbo.TicketNote.FKNoteOwnerID INNER JOIN
                     dbo.NoteType ON dbo.TicketNote.FKNoteTypeID = dbo.NoteType.ID RIGHT OUTER JOIN
                      dbo.Ticket INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID LEFT JOIN
		helpdesk.dbo.employee employee_3 ON dbo.Ticket.FKUserID = employee_3.empID LEFT JOIN
  		helpdesk.dbo.employee employee_4 ON dbo.Ticket.FKUserID2 = employee_4.empID LEFT JOIN
   		helpdesk.dbo.employee employee_5 ON dbo.Ticket.FKUserID3 = employee_5.empID LEFT JOIN
 		helpdesk.dbo.employee employee_6 ON dbo.Ticket.escalatedby1 = employee_6.empID LEFT JOIN
  		helpdesk.dbo.employee employee_7 ON dbo.Ticket.escalatedby2 = employee_7.empID LEFT JOIN
                      AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id inner join
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID ON dbo.TicketNote.FKTicketID = dbo.Ticket.TicketID
		left join dbo.category on dbo.ticket.FKCategoryID = category.categoryID inner join amac.dbo.subscriber subscriber on
		ticket.subscriber_id = subscriber.subscriber_id



















GO
/****** Object:  View [dbo].[vGetTicketSummary]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  View dbo.vGetTicketSummary    Script Date: 11/9/2005 5:49:25 PM ******/



/****** Object:  View dbo.vGetTicketSummary    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE VIEW [dbo].[vGetTicketSummary]
AS

SELECT     dbo.Ticket.TicketID, dbo.Ticket.CareManager, dbo.Ticket.ContactNumber, dbo.Ticket.ReportedTo,  
	dboTicket.FKAccountID, dbo.AccountsTAS.account_name, dbo.Ticket.TicketDateCreated, dbo.Ticket.CloseDateActual, dbo.Ticket.CloseDateTarget, dbo.Ticket.FKProblemTypeID, 
	dbo.ProblemType.ProblemType, dbo.Ticket.FKCategoryID, dbo.Category.descr, dbo.Ticket.Priority, dbo.Ticket.FKStatusID, dbo.status.statustext, dbo.Ticket.FKUserID, 
	case when dbo.Ticket.FKUserID2='0' then ' ' else ticket.FKUserID2 end as FKUserID2,
	case when dbo.Ticket.FKUserID3='0' then ' ' else ticket.FKUserID3 end as FKUserID3, case when dbo.Ticket.FKUserID4='0' then ' ' else ticket.FKUserID4 end as FKUserID4, dbo.Ticket.Description,
	dbo.TicketNote.TicketNoteID, dbo.TicketNote.NoteDateCreated, employee.FirstName + ' ' + employee.LastName AS NoteAuthor, dbo.TicketNote.Description AS NoteText, 
	TicketNote.contact as NoteContact, TicketNote.contactnumber as NotePhone, NoteType.NoteType

FROM         dbo.Ticket 
		INNER JOIN dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID 
		INNER JOIN dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID 
		INNER JOIN dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID 
		left join dbo.TicketNote on TicketNote.FKTicketID=Ticket.TicketID
		left join dbo.NoteType ON TicketNote.FKNoteTypeID = dbo.NoteType.ID
		left join helpdesk.dbo.employee employee ON employee.empID = dbo.TicketNote.FKNoteOwnerID
		inner join dbo.AccountsTAS on dbo.ticket.FKAccountID=dbo.AccountsTAS.Infinity_id
where	  ticket.complaintsystem='tas'

/*SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, dbo.Ticket.FKOwnerID, dbo.Ticket.FKUserID, 
                      dbo.Ticket.TicketDateCreated, dbo.Ticket.FKStatusID, dbo.Ticket.FKProblemTypeID, dbo.Ticket.FKCategoryID, dbo.Ticket.CloseDateTarget, 
                      dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, employee.FirstName AS OwnerFirstName, employee.LastName AS OwnerLastName, 
                      dbo.Status.StatusText, dbo.ProblemType.ProblemType, ticket.FKAgencyID FKAccountID, agency.name account_name, dbo.ticket.caremanager, dbo.Ticket.Location, dbo.Ticket.PartNumber,
	        dbo.Ticket.SerialNumber, dbo.Ticket.SoftwareRevision, dbo.Ticket.RMANumField, ticket.unit_id, ticket.subscriber_id
FROM         dbo.Ticket INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
	         dbo.Category ON dbo.Ticket.FKCategoryID = dbo.Category.CategoryID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID INNER JOIN
                      AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id
*/































GO
/****** Object:  View [dbo].[vGetTicketSummaryERC]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/****** Object:  View dbo.vGetTicketSummaryERC    Script Date: 12/7/2005 JOEL TOMPKINS ******/
CREATE VIEW [dbo].[vGetTicketSummaryERC]
AS
SELECT     dbo.Ticket.TicketID, dbo.Ticket.Summary, dbo.Ticket.Description, dbo.Ticket.FKOwnerID, 
                      dbo.Ticket.TicketDateCreated, dbo.Ticket.FKStatusID, dbo.Ticket.FKProblemTypeID, dbo.ProblemType.description as problem, dbo.ProblemType.ProblemType, 
		dbo.Ticket.FKCategoryID, dbo.Ticket.CloseDateTarget, dbo.Ticket.CloseDateActual, dbo.Ticket.Priority, employee.FirstName AS OwnerFirstName, 
		employee.LastName AS OwnerLastName, dbo.Status.StatusText, ticket.FKAgencyID FKAccountID, agency.name account_name, dbo.ticket.caremanager, 
	       ticket.ContactNumber, ticket.ContactNumber2, ticket.subscriber_id,
	ticket.FKUserID, ticket.FKUserID2, ticket.FKUserID3,
	ticket.incidentdate, ticket.escalatedto1, ticket.escalatedby1, ticket.escalatedto2, ticket.escalatedby2, ticket.reportedto, ticket.FKUserID4,
	employee_1.lastname as escalatedlastname, employee_2.lastname as escalatedlastname2
FROM         dbo.Ticket INNER JOIN
                      helpdesk.dbo.employee employee ON dbo.Ticket.FKOwnerID = employee.empID INNER JOIN
                      dbo.Status ON dbo.Ticket.FKStatusID = dbo.Status.StatusID INNER JOIN
                      dbo.ProblemType ON dbo.Ticket.FKProblemTypeID = dbo.ProblemType.ProblemTypeID LEFT JOIN
                      AMAC.dbo.Agency agency ON dbo.Ticket.FKAgencyID = agency.agency_id LEFT JOIN
 		helpdesk.dbo.employee employee_1 ON dbo.Ticket.escalatedby1 = employee_1.empID LEFT JOIN
  		helpdesk.dbo.employee employee_2 ON dbo.Ticket.escalatedby2 = employee_2.empID
where		ticket.complaintsystem = 'erc'










GO
/****** Object:  Index [IX_Employee]    Script Date: 2/11/2016 1:46:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_Employee] ON [dbo].[OperatorsTAS]
(
	[EmpIDFK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_ProblemType] FOREIGN KEY([FKProblemTypeID])
REFERENCES [dbo].[ProblemType] ([ProblemTypeID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_ProblemType]
GO
ALTER TABLE [dbo].[Ticket]  WITH NOCHECK ADD  CONSTRAINT [FK_Ticket_Status] FOREIGN KEY([FKStatusID])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Status]
GO
ALTER TABLE [dbo].[TicketNote]  WITH CHECK ADD  CONSTRAINT [FK_TicketNote_NoteType] FOREIGN KEY([FKNoteTypeID])
REFERENCES [dbo].[NoteType] ([ID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_NoteType]
GO
ALTER TABLE [dbo].[TicketNote]  WITH NOCHECK ADD  CONSTRAINT [FK_TicketNote_Ticket] FOREIGN KEY([FKTicketID])
REFERENCES [dbo].[Ticket] ([TicketID])
GO
ALTER TABLE [dbo].[TicketNote] CHECK CONSTRAINT [FK_TicketNote_Ticket]
GO
/****** Object:  StoredProcedure [dbo].[rp_SubscriberOnlineEquipmentList]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.rp_SubscriberOnlineEquipmentList    Script Date: 11/9/2005 5:49:25 PM ******/





CREATE   PROCEDURE [dbo].[rp_SubscriberOnlineEquipmentList]
  --@year varchar(4)

AS 

SET NOCOUNT ON 


declare 
	@subscriber_id  	int
	,@agency_id			varchar (50)
	,@agencyname			varchar (50)
	,@city 				varchar (50)
	,@state 				varchar (50)
	,@phone 				varchar (50)
	,@firstname			varchar (50)
	,@middleinitial		varchar (50)
	,@lastname	 		varchar (50)
	,@unit_id	 		varchar (50)
	,@status		 		varchar (50)
	,@subphone	 		varchar (50)
	,@installdate		smalldatetime
	,@item_id			varchar (15)
	,@serialNo	 		varchar (20)
	,@pendantitem_id1 	varchar (15)
	,@pendantfrequency1 	varchar (6)
	,@pendanttransmitter1	varchar (6)
	,@pendantitem_id2 	varchar (15)
	,@pendantfrequency2 	varchar (6)
	,@pendanttransmitter2	varchar (6)
	,@pendantitem_id3 	varchar (15)
	,@pendantfrequency3 	varchar (6)
	,@pendanttransmitter3	varchar (6)
	,@pendantitem_id4 	varchar (15)
	,@pendantfrequency4 	varchar (6)
	,@pendanttransmitter4	varchar (6)
	,@smokeitem_id1	 	varchar (15)
	,@smokeitem_id2	 	varchar (15)
	
	,@temppendantitem_id 		varchar (15)
	,@temppendantfrequency 		varchar (6)
	,@temppendanttransmitter	varchar (6)
	,@tempsmokeitem_id			varchar (15)

	,@pendantcount				int
	,@smokecount				int



CREATE TABLE #temp_equip
	(subscriber_id  	int
	,agency_id			varchar (50)
	,agencyname			varchar (50)
	,city 				varchar (50)
	,state 				varchar (50)
	,phone 				varchar (50)
	,firstname			varchar (50)
	,middleinitial		varchar (50)
	,lastname	 		varchar (50)
	,unit_id	 		varchar (50)
	,status		 		varchar (50)
	,subphone	 		varchar (50)
	,installdate		smalldatetime
	,item_id			varchar (15)
	,serialNo	 		varchar (20)
	,pendantitem_id1 	varchar (15)
	,pendantfrequency1 	varchar (6)
	,pendanttransmitter1	varchar (6)
	,pendantitem_id2 	varchar (15)
	,pendantfrequency2 	varchar (6)
	,pendanttransmitter2	varchar (6)
	,pendantitem_id3 	varchar (15)
	,pendantfrequency3 	varchar (6)
	,pendanttransmitter3	varchar (6)
	,pendantitem_id4 	varchar (15)
	,pendantfrequency4 	varchar (6)
	,pendanttransmitter4	varchar (6)
	,smokeitem_id1	 	varchar (15)
	,smokeitem_id2	 	varchar (15)
	,PRIMARY KEY (agency_id))
	--,UNIQUE (state, city, emp_id) )


DECLARE subscriber_cursor CURSOR FOR 
	select top 10 subscriber_id, agency_id, agencyname, city, state, phone, firstname, middleinitial, lastname,
		unit_id, status, subphone, installdate
	FROM 	subscriber
	where 	    
		Subscriber."Deleted" <> 'Y'
		and Subscriber."Unit_ID" <> 'CANC'
		and subscriber.status='active'
	order by subscriber_id

OPEN subscriber_cursor

FETCH NEXT FROM subscriber_cursor 
INTO @subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, @lastname,
		@unit_id, @status, @subphone, @installdate


WHILE @@FETCH_STATUS = 0
BEGIN

		set @pendantcount=0
		set @smokecount=0
		

		--get our PERS (console) Equipment
		select @item_id=subscriberequipment.item_id, @serialNo=serialNo
		FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id 
		where 	itemtype='pers'    




		--get our Pendants
		DECLARE pendant_cursor CURSOR FOR 
			select subscriberequipment.item_id, frequency, transmitter
			FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id
			where 	itemtype='pendant'    

			order by subscriberequipment.item_id

		OPEN pendant_cursor
		FETCH NEXT FROM pendant_cursor 
		INTO @temppendantitem_id, @temppendantfrequency, @temppendanttransmitter
		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @pendantcount = @pendantcount +1
			if @pendantcount =1 
			begin
				set @pendantitem_id1=@temppendantitem_id 
				set @pendantfrequency1=@temppendantfrequency
				set @pendanttransmitter1=@temppendanttransmitter
			end
			else if @pendantcount=2	
			begin
				set @pendantitem_id2=@temppendantitem_id 
				set @pendantfrequency2=@temppendantfrequency
				set @pendanttransmitter2=@temppendanttransmitter
			end
			else if @pendantcount=3	
			begin
				set @pendantitem_id3=@temppendantitem_id 
				set @pendantfrequency3=@temppendantfrequency
				set @pendanttransmitter3=@temppendanttransmitter
			end
			else if @pendantcount=4	
			begin
				set @pendantitem_id4=@temppendantitem_id 
				set @pendantfrequency4=@temppendantfrequency
				set @pendanttransmitter4=@temppendanttransmitter
			end

			FETCH NEXT FROM pendant_cursor 
			INTO @temppendantitem_id, @temppendantfrequency, @temppendanttransmitter
		END
		CLOSE pendant_cursor
		DEALLOCATE pendant_cursor






		--Get our smoke detector equipment
		DECLARE smoke_cursor CURSOR FOR 
			select item_id
			FROM 	subscriberequipment inner join amacequipment on amacequipment.item_id=subscriberequipment.item_id
			where 	itemtype='smoke'    
			order by subscriberequipment.item_id
		OPEN smoke_cursor
		FETCH NEXT FROM pendant_cursor 
		INTO @tempsmokeitem_id
		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @smokecount = @smokecount +1
			if @smokecount=1 
			begin
				set @smokeitem_id1=@tempsmokeitem_id
			end
			else if @smokecount=2	
			begin
				set @smokeitem_id2=@tempsmokeitem_id
			end
			
			FETCH NEXT FROM smoke_cursor 
			INTO @tempsmokeitem_id
		END
		CLOSE smoke_cursor
		DEALLOCATE smoke_cursor




		INSERT INTO #temp_equip 
		(subscriber_id, agency_id, agencyname, city, state, phone, firstname, middleinitial, lastname,
			unit_id, status, subphone, installdate, item_id, serialNo, pendantitem_id1, pendantfrequency1, 
			pendanttransmitter1, pendantitem_id2, pendantfrequency2, pendanttransmitter2, pendantitem_id3,
			pendantfrequency3, pendanttransmitter3, pendantitem_id4, pendantfrequency4, pendanttransmitter4, smokeitem_id1, smokeitem_id2
		)
		Values (@subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, 
				@lastname, @unit_id, @status, @subphone, @installdate, isnull(@item_id, ''),isnull(@serialNo, ''),
				isnull(@pendantitem_id1, ''), isnull(@pendantfrequency1, ''), isnull(@pendanttransmitter1, ''), 
				isnull(@pendantitem_id2, ''), isnull(@pendantfrequency2, ''), isnull(@pendanttransmitter2, ''), 
				isnull(@pendantitem_id3, ''), isnull(@pendantfrequency3, ''), isnull(@pendanttransmitter3, ''),
				isnull(@pendantitem_id4, ''), isnull(@pendantfrequency4, ''), isnull(@pendanttransmitter4, ''),
				isnull(@smokeitem_id1, ''), isnull(@smokeitem_id2 , '') 
		)
		
		
		
		
		FETCH NEXT FROM subscriber_cursor 
		INTO @subscriber_id, @agency_id, @agencyname, @city, @state, @phone, @firstname, @middleinitial, @lastname,
			@unit_id, @status, @subphone, @installdate
END

CLOSE subscriber_cursor
DEALLOCATE subscriber_cursor



--get our output recordset
	Select * 
	From #temp_equip order by Agency, agency_id, subscriber_id

drop table #temp_equip




GO
/****** Object:  StoredProcedure [dbo].[spEmpList]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  Stored Procedure dbo.spEmpList    Script Date: 11/9/2005 5:49:25 PM ******/



/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spEmpList]

/* Returns three recordsets consisting of the id and text for names of IT people, all employees,
and status. */

AS

SELECT EmpID, EmpNTLogin, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY LastName
SELECT EmpID, FirstName, LastName, Email FROM Employee
  WHERE Active=1 and IsSupervisor =1
  ORDER BY FirstName



GO
/****** Object:  StoredProcedure [dbo].[spGetAllEmps]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.spGetAllEmps    Script Date: 11/9/2005 5:49:25 PM ******/



CREATE PROCEDURE [dbo].[spGetAllEmps]

/* This procedure returns a list of all employees. */

AS
SET NOCOUNT ON

SELECT EmpID,  FirstName + ' ' + LastName AS EmpName
  FROM CareUsers	
  WHERE (Active = 1)
  ORDER BY FirstName

return (0)


GO
/****** Object:  StoredProcedure [dbo].[spGetEmpName]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.spGetEmpName    Script Date: 11/9/2005 5:49:25 PM ******/



CREATE PROCEDURE [dbo].[spGetEmpName]
  @EmpNTLogin char(50) = '', @EmpID int = 0, @EmpName char(100) OUTPUT, 
  @IsSupervisor bit OUTPUT, @IsGod bit OUTPUT, @EmpIDout int OUTPUT, 
  @EmpNTLoginout char(50) OUTPUT

/* This procedure returns an employee's name when given their NT login name
or employee ID number. 
It also returns whether or not he or she is a supervisor and their employee ID number
(which is the primary key and NOT the employee number most employees are familiar with.)
A few other fields are returned as well.*/

AS
SET NOCOUNT ON

IF (@EmpID = 0)
/*If no employee ID number is supplied, then an NT login name MUST be supplied.  Use the NT
login name when no ID is given.*/
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @EmpIDout = EmpID, @EmpNTLoginout=@EmpNTLogin
  FROM Employee WHERE (EmpNTLogin=@EmpNTLogin)
END
ELSE
BEGIN
  SELECT @EmpName = FirstName + ' ' + LastName, @IsSupervisor = IsSupervisor,
    @IsGod = IsGod, @EmpIDout = EmpID, @EmpNTLoginout=EmpNTLogin
  FROM Employee WHERE (EmpID=@EmpID)
END

IF @EmpName IS NULL
     return (1)
ELSE
     return (0)


GO
/****** Object:  StoredProcedure [dbo].[spGetOptions]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 11/9/2005 5:49:25 PM ******/



/****** Object:  Stored Procedure dbo.spGetOptions    Script Date: 3/21/2002 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spGetOptions]

/* Returns seven recordsets consisting of the id and text for names of IT people, all users, all account names, note types,
and status. */
/* The selects MUST be ordered in the order they will be used on the ASP page.  If not then the page code will not worlk  */

AS


select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank')
 ORDER BY LastName

SELECT EmpID, FirstName, LastName FROM CareUsers
  WHERE Active=1
  ORDER BY FirstName

SELECT ProblemTypeID, ProblemType, description FROM ProblemType
  WHERE Active=1
 ORDER BY ProblemType

SELECT CategoryID, Descr FROM Category
  WHERE Active=1
 ORDER BY Descr

SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spGetOptions2]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  Stored Procedure dbo.spGetOptions2    Script Date: 11/9/2005 5:49:25 PM ******/



/****** Object:  Stored Procedure dbo.spGetOptions2    Script Date: 1/16/2003 8:43:04 AM ******/
CREATE PROCEDURE [dbo].[spGetOptions2]

/* Returns seven recordsets consisting of the id and text for names of IT people, all users, all account names, note types,
and status. */
/* The selects MUST be ordered in the order they will be used on the ASP page.  If not then the page code will not worlk  */

AS




--account
/*SELECT Infinity_id, accountsTASID, Account_Name FROM accountsTAS
  WHERE status=1
 ORDER BY Infinity_id
*/

--Issue Type
SELECT ProblemTypeID, ProblemType, description FROM ProblemType
  WHERE Active=1 and complaintsystem='tas'
 ORDER BY OrderID

--Source
--SELECT CategoryID, Descr FROM Category
 -- WHERE Active=1
-- ORDER BY Descr


--Op1
/*select empID as ID, empNTLogin, FirstName, LastName, OperatorID from helpdesk.dbo.employee left join OperatorsTAS on OperatorsTAS.empIDFK=helpdesk.dbo.employee.EmpID
where helpdesk.dbo.employee.Active=1 and operatorsTAS.Active=1 and (dept like 'TAS%')
 ORDER BY LastName

Op2
select empID as ID, empNTLogin, FirstName, LastName, OperatorID from helpdesk.dbo.employee left join OperatorsTAS on OperatorsTAS.empIDFK=helpdesk.dbo.employee.EmpID
where helpdesk.dbo.employee.Active=1 and operatorsTAS.Active=1 and (dept like 'TAS%')
 ORDER BY LastName

Op3
select empID as ID, empNTLogin, FirstName, LastName, OperatorID from helpdesk.dbo.employee left join OperatorsTAS on OperatorsTAS.empIDFK=helpdesk.dbo.employee.EmpID
where helpdesk.dbo.employee.Active=1 and operatorsTAS.Active=1 and (dept like 'TAS%')
 ORDER BY LastName


Op4
select empID as ID, empNTLogin, FirstName, LastName, OperatorID from helpdesk.dbo.employee left join OperatorsTAS on OperatorsTAS.empIDFK=helpdesk.dbo.employee.EmpID
where helpdesk.dbo.employee.Active=1 and operatorsTAS.Active=1 and (dept like 'TAS%')
 ORDER BY LastName
*/

--reported to
select empID as ID, empNTLogin, FirstName, LastName, OperatorID from helpdesk.dbo.employee left join OperatorsTAS on OperatorsTAS.empIDFK=helpdesk.dbo.employee.EmpID
where helpdesk.dbo.employee.Active=1 and operatorsTAS.Active=1 and (dept like 'TAS%')
 ORDER BY LastName


--Status
SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spGetOptions2ERC]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*  12/7/2005 JOEL TOMPKINS */
/* The original SP located at Avi Hazut desktop*/
/*Changes by Avi Hazut*/

CREATE  PROCEDURE  [dbo].[spGetOptions2ERC] AS

--** Entered By
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

--** Agency #
SELECT agency_id Code, name Account_Name,agency_ID FROM AMAC.dbo.agency
  WHERE Deleted='N'
 ORDER BY agency_id

--** Issue type
SELECT ProblemTypeID, description FROM ProblemType
  WHERE Active=1 and complaintsystem='erc'
 ORDER BY description

--** Handled by (1)
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

--** Handled by (2)
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

--** Employee(s)
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

--** #2
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName


--** #3
--Escalation handled by 2
select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

--** Resolution
--Resolution (Category)
SELECT CategoryID, Descr FROM Category
  WHERE Active=1 and complaintsystem='erc'
 ORDER BY Descr


--** Status
SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spGetOptionsERC]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.spGetOptionsERC    Script Date: 12/7/2005 JOEL TOMPKINS******/
CREATE  PROCEDURE [dbo].[spGetOptionsERC]

/* Returns seven recordsets consisting of the id and text for names of ERC people, all users, all account names, note types,
and status. */
/* The selects MUST be ordered in the order they will be used on the ASP page.  If not then the page code will not worlk  */

AS


select empID as ID, empNTLogin, FirstName, LastName from helpdesk.dbo.employee
 where Active=1 and (dept like 'Cust%' or dept like 'ERC%' or empNTLogin = 'frank' or dept in ('sales','Accounting'))
 ORDER BY LastName

SELECT EmpID, FirstName, LastName FROM CareUsers
  WHERE Active=1
  ORDER BY FirstName

SELECT ProblemTypeID, description FROM ProblemType
  WHERE Active=1 and complaintsystem='erc'
 ORDER BY ProblemType

SELECT CategoryID, Descr FROM Category
  WHERE Active=1 and complaintsystem='erc' 
 ORDER BY Descr

SELECT StatusID, StatusText FROM Status

SELECT ID, NoteType FROM NoteType
  WHERE Active=1
 ORDER BY NoteType
GO
/****** Object:  StoredProcedure [dbo].[spValidateEmp]    Script Date: 2/11/2016 1:46:25 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.spValidateEmp    Script Date: 11/9/2005 5:49:25 PM ******/



CREATE PROCEDURE [dbo].[spValidateEmp]
  @EmpID int, @SysPassword Varchar(30)

/* This procedure takes as input an employee id and the last 4 digits of their social
security number and checks to see if these match the database.  If they do, the
procedure returns 0.  If not, it returns 1. */

AS
SET NOCOUNT ON

IF (SELECT COUNT(*) FROM CareUsers WHERE EmpID = @EmpID AND SysPassword = @SysPassword) > 0
     return (0)
ELSE
     return (1)


GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'refers to Helpdesk.Employee table empID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperatorsTAS', @level2type=N'COLUMN',@level2name=N'EmpIDFK'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TAS Operator id (MDR db)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OperatorsTAS', @level2type=N'COLUMN',@level2name=N'OperatorID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'not used - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'Summary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ReportTo - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKOwnerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKUserID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKUserID2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator3' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKUserID3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Operator4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKUserID4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKCategoryID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shift - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'not used - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'FKAgencyID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ReportedBy - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'CareManager'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact # - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'ContactNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'not used - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'subscriber_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'not used - TAS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Ticket', @level2type=N'COLUMN',@level2name=N'ContactNumber2'
GO
USE [master]
GO
ALTER DATABASE [CustomerCare] SET  READ_WRITE 
GO
