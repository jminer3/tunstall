USE [master]
GO
/****** Object:  Database [VNSNY]    Script Date: 2/11/2016 2:00:03 PM ******/
CREATE DATABASE [VNSNY] ON  PRIMARY 
( NAME = N'ERC_Data', FILENAME = N'c:\sqldata\vnsny.mdf' , SIZE = 4454400KB , MAXSIZE = UNLIMITED, FILEGROWTH = 204800KB )
 LOG ON 
( NAME = N'ERC_Log', FILENAME = N'c:\sqldata\vnsny.ldf' , SIZE = 99904KB , MAXSIZE = 153600KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VNSNY] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VNSNY].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [VNSNY] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VNSNY] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VNSNY] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VNSNY] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VNSNY] SET ARITHABORT OFF 
GO
ALTER DATABASE [VNSNY] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VNSNY] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VNSNY] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VNSNY] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VNSNY] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VNSNY] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VNSNY] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VNSNY] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VNSNY] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VNSNY] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VNSNY] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VNSNY] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VNSNY] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VNSNY] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VNSNY] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VNSNY] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VNSNY] SET RECOVERY FULL 
GO
ALTER DATABASE [VNSNY] SET  MULTI_USER 
GO
ALTER DATABASE [VNSNY] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [VNSNY] SET DB_CHAINING OFF 
GO
USE [VNSNY]
GO
/****** Object:  User [TDayle]    Script Date: 2/11/2016 2:00:03 PM ******/
CREATE USER [TDayle] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SSunko]    Script Date: 2/11/2016 2:00:03 PM ******/
CREATE USER [SSunko] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SHaugh]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [SHaugh] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [RMcCoy]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [RMcCoy] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [reporting]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [reporting] FOR LOGIN [reporting] WITH DEFAULT_SCHEMA=[reporting]
GO
/****** Object:  User [Psantiago]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [Psantiago] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[Psantiago]
GO
/****** Object:  User [PFong]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [PFong] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [OmniuserCert]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [OmniuserCert] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Omniuser]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [Omniuser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [omnilinkservice]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [omnilinkservice] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [OFigueroa]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [OFigueroa] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MGalicki]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [MGalicki] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MBlstt]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [MBlstt] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [MBlatt]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [MBlatt] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [Maynor]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [Maynor] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [JTerragni]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [JTerragni] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [jblock]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [jblock] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[jblock]
GO
/****** Object:  User [JBennion]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [JBennion] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [erc_domain\staherian]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [erc_domain\staherian] FOR LOGIN [ERC_DOMAIN\staherian] WITH DEFAULT_SCHEMA=[erc_domain\staherian]
GO
/****** Object:  User [ERC_DOMAIN\mblatt]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [ERC_DOMAIN\mblatt] FOR LOGIN [ERC_DOMAIN\mblatt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [ERC_DOMAIN\jtompkins]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [ERC_DOMAIN\jtompkins] FOR LOGIN [ERC_DOMAIN\jtompkins] WITH DEFAULT_SCHEMA=[ERC_DOMAIN\jtompkins]
GO
/****** Object:  User [AMAC]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [AMAC] FOR LOGIN [amac] WITH DEFAULT_SCHEMA=[AMAC]
GO
/****** Object:  User [AHazut]    Script Date: 2/11/2016 2:00:04 PM ******/
CREATE USER [AHazut] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [TDayle]
GO
ALTER ROLE [db_datareader] ADD MEMBER [TDayle]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [TDayle]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [SSunko]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SSunko]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SSunko]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [SHaugh]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SHaugh]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SHaugh]
GO
ALTER ROLE [db_datareader] ADD MEMBER [reporting]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [PFong]
GO
ALTER ROLE [db_datareader] ADD MEMBER [PFong]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [PFong]
GO
ALTER ROLE [db_datareader] ADD MEMBER [OmniuserCert]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [OmniuserCert]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Omniuser]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [Omniuser]
GO
ALTER ROLE [db_datareader] ADD MEMBER [omnilinkservice]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [omnilinkservice]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [OFigueroa]
GO
ALTER ROLE [db_datareader] ADD MEMBER [OFigueroa]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [OFigueroa]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [MGalicki]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MGalicki]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [MGalicki]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MBlstt]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [MBlatt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MBlatt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [MBlatt]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Maynor]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Maynor]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [Maynor]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [JTerragni]
GO
ALTER ROLE [db_datareader] ADD MEMBER [JTerragni]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [JTerragni]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [JBennion]
GO
ALTER ROLE [db_datareader] ADD MEMBER [JBennion]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [JBennion]
GO
ALTER ROLE [db_owner] ADD MEMBER [erc_domain\staherian]
GO
ALTER ROLE [db_datareader] ADD MEMBER [erc_domain\staherian]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ERC_DOMAIN\mblatt]
GO
ALTER ROLE [db_owner] ADD MEMBER [ERC_DOMAIN\jtompkins]
GO
ALTER ROLE [db_datareader] ADD MEMBER [AMAC]
GO
ALTER ROLE [db_owner] ADD MEMBER [AHazut]
GO
/****** Object:  Schema [AMAC]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [AMAC]
GO
/****** Object:  Schema [ERC_DOMAIN\jtompkins]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [ERC_DOMAIN\jtompkins]
GO
/****** Object:  Schema [erc_domain\staherian]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [erc_domain\staherian]
GO
/****** Object:  Schema [jblock]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [jblock]
GO
/****** Object:  Schema [Psantiago]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [Psantiago]
GO
/****** Object:  Schema [reporting]    Script Date: 2/11/2016 2:00:05 PM ******/
CREATE SCHEMA [reporting]
GO
/****** Object:  UserDefinedFunction [dbo].[fnCountPassivesSincePendant]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[fnCountPassivesSincePendant] (@subid char(6),@CutoffDate datetime) 
RETURNS int
AS 
BEGIN 
DECLARE @VAR int
SET @VAR = 0
select @VAR = count(dailycallsid) from dailycalls where subno=@subid
	 and calldatetime>case when @CutoffDate is null then '01/01/1900' else @CutoffDate end
and callmatch='YES' and alarmcode1='00'

 
RETURN @VAR
END 


GO
/****** Object:  Table [dbo].[VNSNYLanguageIndicatorCode]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VNSNYLanguageIndicatorCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LanguageCode] [char](10) NULL,
	[LanguageDesc] [varchar](100) NULL,
 CONSTRAINT [PK_VNSNYLanguageIndicatorCode] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VNSNYRelationshipIndicatorCode]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VNSNYRelationshipIndicatorCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RelationshipCode] [char](10) NULL,
	[RelationshipDesc] [varchar](100) NULL,
 CONSTRAINT [PK_VNSNYRelationshipIndicatorCode] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VNSNYSubscriberDIA]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VNSNYSubscriberDIA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](70) NULL,
	[DescOfUse] [varchar](60) NULL,
	[StartPosition] [varchar](7) NULL,
	[EndingPosition] [varchar](7) NULL,
	[Length] [varchar](7) NULL,
	[Notes] [varchar](70) NULL,
 CONSTRAINT [PK_VNSNYSubscriberDIA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VNSNYSubscriberImportFileInf]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VNSNYSubscriberImportFileInf](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Recs] [int] NOT NULL,
	[ImportFile_ID] [varchar](32) NULL,
	[FileName] [varchar](150) NULL,
	[FileDateTimeStamp] [datetime] NULL,
	[DateOfImport] [datetime] NULL,
 CONSTRAINT [PK_VNSNYSubscriberImportFileInf] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VNSNYSubscriberStage]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VNSNYSubscriberStage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SubFirstname] [varchar](50) NULL,
	[SubMiddleInit] [varchar](3) NULL,
	[SubLastname] [varchar](50) NULL,
	[ProductID] [varchar](10) NULL,
	[DOB] [varchar](10) NULL,
	[Sex] [varchar](3) NULL,
	[Phonenumber] [varchar](10) NULL,
	[ResidenceAddress] [varchar](50) NULL,
	[ResidenceAddress2] [varchar](50) NULL,
	[ResidenceZip] [varchar](10) NULL,
	[ResidenceCity] [varchar](50) NULL,
	[ResidenceState] [varchar](3) NULL,
	[County] [varchar](25) NULL,
	[PrimaryLanguage] [varchar](120) NULL,
	[CaseNumber] [varchar](25) NULL,
	[MemberNumber] [varchar](12) NULL,
	[DiagnosisCode] [varchar](10) NULL,
	[EligibilityStatus] [varchar](25) NULL,
	[RecordUpdated] [varchar](12) NULL,
	[Respondername] [varchar](60) NULL,
	[Relation] [varchar](120) NULL,
	[Key] [varchar](3) NULL,
	[CallIfNVI] [varchar](3) NULL,
	[Comments] [varchar](120) NULL,
	[ResponderHomePhone] [varchar](10) NULL,
	[ResponderCellPhone] [varchar](10) NULL,
	[ResponderWorkPhone] [varchar](10) NULL,
	[LookupLang] [varchar](25) NULL,
	[LookupRel] [varchar](125) NULL,
	[ImportFileID] [varchar](32) NULL,
 CONSTRAINT [PK_VNSNYSubscriberStage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[DescriptionD]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionD]
as 
Select * from Description where CodeType = 'D'

GO
/****** Object:  View [dbo].[v_MedtimeODSignals]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_MedtimeODSignals]

as

SELECT
    Subscriber.Agency_ID, Subscriber.AgencyName, Subscriber.subscriber_ID,  
    Subscriber.Lastname + ', ' + Subscriber.Firstname as SubName, 
    Dailycalls.CallDateTime, Dailycalls.Alarmcode1 as AlarmCode, 
    ERCAlarmCodes.Description as AlarmDescription,
    callhistory.datetimestamp,descriptiond.descr as Disposition 
 
FROM
    AMAC.dbo.subscriber subscriber
       left outer join ERC.dbo.dailycalls dailycalls on subscriber.subscriber_id = dailycalls.subno  
       left outer join ERC.dbo.ercalarmcodes ercalarmcodes on dailycalls.callsource=ercalarmcodes.calltype 
             and dailycalls.alarmcode1=ercalarmcodes.code
       left outer join ERC.dbo.callhistory on dailycalls.dailycallsid=callhistory.dailycallsid
       left outer join ERC.dbo.descriptiond on callhistory.disposition=descriptiond.code 
        and dailycalls.callsource=descriptiond.calltype

where

    DailyCalls.alarmcode1 in ('0D')

and 
	callhistory.newstatus>='0100'
/*AND
   ERC.dbo.descriptiond.Calltype = 'p'*/

GO
/****** Object:  View [dbo].[CallActionView]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CallActionView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'A')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[CallDesignationView]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CallDesignationView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'C')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[DescriptionA]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionA] 
as 
Select * from Description where CodeType = 'A'

GO
/****** Object:  View [dbo].[DescriptionC]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionC] 
as 
Select * from Description where CodeType = 'C'

GO
/****** Object:  View [dbo].[DescriptionS]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[DescriptionS]
as 
Select * from Description where CodeType = 'S'

GO
/****** Object:  View [dbo].[DispositionView]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DispositionView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'D')
ORDER BY Calltype, Code

GO
/****** Object:  View [dbo].[Operators]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Operators]
AS
SELECT     '0' + LoginName AS Operator, FirstName + ' ' + LastName AS Name, 'A' AS Status, User_ID AS ID
FROM         AMAC.dbo.Users users
WHERE     (Active = 'Y') AND (Deleted = 'N') AND (NOT (FirstName IS NULL)) AND (FirstName NOT IN ('Dummy', 'System', 'Web', 'Approve', 'ERC', 'Guest', 'Temp', 
                      'AMAC')) AND (User_ID IN
                          (SELECT     user_ID
                            FROM          AMAC.dbo.users
                            WHERE      isnumeric(AMAC.dbo.users.LoginName) = 1)) AND (LoginName NOT IN ('000', '001')) OR
                      (Active = 'Y') AND (Deleted = 'N') AND (FirstName NOT IN ('Dummy', 'System', 'Web', 'Approve', 'ERC', 'Guest', 'Temp', 'AMAC')) AND 
                      (User_ID IN
                          (SELECT     user_ID
                            FROM          AMAC.dbo.users
                            WHERE      isnumeric(AMAC.dbo.users.LoginName) = 1)) AND (LoginName NOT IN ('000', '001')) AND (NOT (LastName IS NULL))

GO
/****** Object:  View [dbo].[PassiveStatus]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[PassiveStatus] as
select top 100 percent Date, Status, count(1) as Count from (
select convert(char(10),calldatetime,121) as Date, case when alarmcode1='NO_TONE' and  callmatch='No' then 'No Tones from unit'
when callmatch='No' and alarmcode1<>'NO_TONE' then 'No Match on unitid/alarmcode'
when callmatch='Yes' and alarmcode1<>'NO_TONE' and subno is not null then 'Matched Subscriber'
else 'Unit id no link to sub' end  status 
 from sql1.erc.dbo.dailycalls where calldatetime>=convert(char(10),getdate()-1,121) and calldatetime<convert(char(10),getdate(),121)
) StatusCheck
group by date, status order by date,status

GO
/****** Object:  View [dbo].[SafecomPassiveCalls]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SafecomPassiveCalls]
AS
SELECT     dbo.DailyCalls.DailyCallsID, dbo.DailyCalls.TRUEUnitID, dbo.DailyCalls.AlarmCode1, dbo.DailyCalls.CallDateTime, dbo.DailyCalls.CallMatch, 
                      dbo.DailyCalls.CallSource, dbo.ERCAlarmCodes.Description, dbo.ERCAlarmCodes.CallType
FROM         dbo.DailyCalls INNER JOIN
                      dbo.ERCAlarmCodes ON dbo.DailyCalls.AlarmCode1 = dbo.ERCAlarmCodes.Code
WHERE     (dbo.DailyCalls.CallSource = 'W') AND (dbo.DailyCalls.CallMatch = 'YES') AND (dbo.ERCAlarmCodes.CallType = 'W')

GO
/****** Object:  View [dbo].[SafeComSubscribers]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SafeComSubscribers]
AS
SELECT     *
FROM         AMAC.dbo.Subscriber
WHERE     (Type = 'Safecom') AND (Status <> 'REMOVED')

GO
/****** Object:  View [dbo].[StatusView]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StatusView]
AS
SELECT     TOP 100 PERCENT Calltype, Code, Descr
FROM         dbo.Description
WHERE     (CodeType = 'S')
ORDER BY Calltype, Code

GO
/****** Object:  StoredProcedure [dbo].[amac_UnitStatusUpdate]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Name    		: amac_UnitStatusUpdate
Input Parameter  	: 
Output Parameter 	: 
Author  		: Kirk A
Created 		: 13 OCT 03
Purpose 		: Update the unit table for unit_id's in holding
Modification       	:
****************************************************************************************
AUTHOR          DATE           Purpose
****************************************************************************************
*/

CREATE PROCEDURE [dbo].[amac_UnitStatusUpdate] as

Declare @PendingHoldDays int
Declare @TempHoldDays int

--PendingHoldDays for Removals
select @PendingHoldDays=95

--TempHoldDays for UnitID Swap
select @TempHoldDays=15



--Re-enable unit_id with proper date and status 'P', to available 'Y' and null the description for future removals
update unit set available='Y', description=Null  where available='P' and description>'00000000' and description< convert(varchar(10),getdate()-@PendingHoldDays,112) 


GO
/****** Object:  StoredProcedure [dbo].[ap_Safecom_WLGNSTestingVCR_s]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*Author  		:Solyman
Created 		: 04 / 08 / 2005
Purpose 		: This procedure will retreive Testing and VCR-signals for Walgreens Stores
Modification       :
****************************************************************************************
AUTHOR          DATE           Purpose
****************************************************************************************
*/
CREATE  PROCEDURE [dbo].[ap_Safecom_WLGNSTestingVCR_s]
             @vChrSubscriber_ID varChar(30),
             @vChrRegion varChar(6)
             
AS
     SET NOCOUNT ON
	
	 DECLARE @chrSQL varchar(8000)             
	  
	 
	      SET @chrSQL = "SELECT top 10 SafeComSubscribers.Subscriber_ID, SafecomPassiveCalls.CallDateTime,
				 SafeComSubscribers.State, SafeComSubscribers.Lastname,
                                                  SafecomPassiveCalls.Description, CallHistory.DateTimeStamp, DescriptionD.Descr, CallHistory.Comment           
                                                 FROM SafecomPassiveCalls SafecomPassiveCalls 
                                                 LEFT OUTER JOIN SafecomSubscribers SafeComSubscribers
                                                  ON  SafecomPassiveCalls.TRUEUnitID = SafeComSubscribers.Unit_ID 
                                                 LEFT OUTER JOIN  AMAC.dbo.SafecomRegionDistrict SafecomRegionDistrict 
                                                 ON  SafeComSubscribers.Subscriber_ID = SafecomRegionDistrict.Subscriber_ID
                                                 LEFT OUTER JOIN CallHistory CallHistory 
                                                 ON  SafecomPassiveCalls.DailyCallsID = CallHistory.DailyCallsID 
                                                 LEFT OUTER JOIN DescriptionD DescriptionD 
                                                 ON  CallHistory.Disposition = DescriptionD.Code
                                                 WHERE (DescriptionD.Calltype = 'W') AND (SafecomPassiveCalls.CallSource = 'W') 
                                                  AND SafecomPassiveCalls.AlarmCode1 in ('03','09','08','00','0A','0B','0C')   
                                                  AND SafecomRegionDistrict.subscriber_id = '"+@vChrSubscriber_ID+"'
                                                  AND SafecomRegionDistrict. region = '"+@vChrRegion+"'"       	    	                                    	              
       
	      EXECUTE(@chrSQL)
GO
/****** Object:  StoredProcedure [dbo].[ClearOutTests]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ClearOutTests] AS
Update erc.dbo.dailycal set status=110 where 
isnull(status,'NULL')='NULL' and calltype='W' and 
alarmcode1='22' and callmatch='Yes'
GO
/****** Object:  StoredProcedure [dbo].[sp_AutoClearPassive]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_AutoClearPassive] AS
--Per David Garza - signals to auto close - New status code 0111 for SQL Server Agent
--KA 7/20/2011
--KA 7/22/2011 added ('00','05','22')
select cast(count(*) as varchar(20)) +' Records to be cleared' from erc.dbo.dailycalls where status is null and alarmcode1 in ('00','05','22') and callmatch='Yes'
Update erc.dbo.dailycalls set status='0111' where status is null and alarmcode1 in ('00','05','22') and callmatch='Yes'

GO
/****** Object:  StoredProcedure [dbo].[SP_ERCPassiveCursor]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

/*
ERCPassiveCursor - 10/27/2008 
KRA - Cursor to append new 00 records in the PASSIVE database to the AMACSERVICELIST
7/4/2011 added all subscriber types per rich rallo
7/11/2011 add filter for max dailycallsid where subno is not null to prevent premature loading of data
*/



CREATE procedure [dbo].[SP_ERCPassiveCursor]
	@RowsToRead int = NULL
as
SET NOCOUNT ON

DECLARE @chrSQL varchar(8000)

if @RowsToRead is null
	set @chrSQL='SELECT '
else
	set @chrSQL='SELECT TOP '+cast(@RowsToRead as varchar(10))+' ' 
	
set @chrSQL=@chrSQL+' dailycallsid as PassiveDailyCallsId, null as KshemaCallID, subno as subscriber_id, CallDateTime,'
set @chrSQL=@chrSQL+' alarmcode1 as AlarmCode,dbo.fnCountPassivesSincePendant(subno,amac.dbo.fnEarliestPendantInstall(subno))'
set @chrSQL=@chrSQL+' as PassivesSinceLastPendant,'
set @chrSQL=@chrSQL+' subscriber.installdate, amac.dbo.fnFullPendant(subno) As OnlinePendants, '
set @chrSQL=@chrSQL+' amac.dbo.fnEarliestPendantInstall(subno) as EarliestPendantInstalls,'
set @chrSQL=@chrSQL+' amac.dbo.fnPERSwSerial(subno) as PERSSerial, amac.dbo.fnContactLastServiceFax(subno) '
set @chrSQL=@chrSQL+' as LastContactServiceFax, amac.dbo.fnWorkOrderStatus(subno) as LastWorkOrderStatus,'
set @chrSQL=@chrSQL+' subscriber.Agency_id, subscriber.Subcontractor_id into ##tmpservicelist '
set @chrSQL=@chrSQL+' from dailycalls with (nolock)'
set @chrSQL=@chrSQL+' left outer join amac.dbo.subscriber subscriber on dailycalls.subno=subscriber.subscriber_id'
set @chrSQL=@chrSQL+' where callmatch=''YES'' '
set @chrSQL=@chrSQL+' and dailycallsid > (select max( passivedailycallsid) from licsql08.amac00program.dbo.amacservicelist)'
set @chrSQL=@chrSQL+' and dailycallsid <= (select max(dailycallsid) from dailycalls where subno is not null) '
set @chrSQL=@chrSQL+' and alarmcode1=''00'' order by calldatetime'

EXEC (@chrSQL)

declare ServiceListCursor cursor for select PassiveDailyCallsId, subscriber_id, CallDateTime from ##tmpservicelist
declare @intCallId Int
declare @chrSubscriber_id char(6)
declare @dteCallDateTime DateTime
declare @dteCheck DateTime
declare @intCheck TinyInt

Open ServiceListCursor
Fetch Next From ServiceListCUrsor INTO @intCallId , @chrSubscriber_id, @dteCallDateTime 

while @@fetch_status=0
BEGIN
	
	select @intcheck=count(LastTriggerDateTime) from licsql08.amac00program.dbo.AMACServiceListLookup where subscriber_id=@chrSubscriber_id
	select @dteCheck=LastTriggerDateTime from licsql08.amac00program.dbo.AMACServiceListLookup where subscriber_id=@chrSubscriber_id
	if @intcheck=0
		BEGIN
		if @chrSubscriber_id is not null
		Insert into licsql08.amac00program.dbo.AMACServiceListLookup (subscriber_id,LastTriggerDateTime,TriggerCounter) values (@chrSubscriber_id, @dteCallDateTime, 1)
		insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('Unreviewed' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		END
	ELSE
		if @dteCheck+22>@dteCallDateTime
			insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('No Action Required' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		else
		BEGIN
		if @chrSubscriber_id is not null
			Update licsql08.amac00program.dbo.AMACServiceList set ReviewStatus='Closed Incomplete' where subscriber_id=@chrSubscriber_id and ReviewStatus='Unreviewed'
			Update licsql08.amac00program.dbo.AMACServiceListLookup set LastTriggerDateTime=@dteCallDateTime, TriggerCounter=TriggerCounter+1 where subscriber_id=@chrSubscriber_id
			insert into licsql08.amac00program.dbo.AMACServiceList (PassiveDailyCallsId,KshemaCallID,subscriber_id,CallDateTime,AlarmCode,PassivesSinceLastPendant,installdate,OnlinePendants,EarliestPendantInstalls,PERSSerial,LastContactServiceFax,LastWorkOrderStatus,Agency_id,Subcontractor_id,ReviewStatus) Select *,cast ('Unreviewed' As char(20)) as ReviewStatus from ##tmpservicelist where PassiveDailyCallsId=@intCallId
		END

	Fetch Next From ServiceListCUrsor INTO @intCallId , @chrSubscriber_id, @dteCallDateTime 

END
close ServiceListCursor
deallocate ServiceListCursor
drop table ##tmpservicelist

SET QUOTED_IDENTIFIER OFF

GO
/****** Object:  StoredProcedure [dbo].[sp_NoMatch_Clearout]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
-Passive NonMatch closeout-
This procedure will check the PASSIVE ERC 'DailyCalls' table to prevent a large number of null status calls.
These cause the db to generate a high-read query.  
Setting these queries to status of '9999' gets them out of any other queries that are run.


11/16/2003-setup and scheduled
*/

CREATE PROCEDURE [dbo].[sp_NoMatch_Clearout] AS
update dailycalls set status='9999'  where callmatch='no' and status is null
GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveFoxProC]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE        PROCEDURE [dbo].[sp_PassiveFoxProC]
   @Subno varchar(8)

AS 
--declare @Subno varchar(8)

--Set @Subno = '205904'

--the Fox Pro app returns name, address and DOB info for a subscriber when 'C' criteria is submitted



Select 
	(Lastname + ', ' + FirstName + MiddleInitial) as names, --0
	Subscriber_ID as subno, --1
	isnull(Address1, ' ') as address, --2
	isnull(Address2, ' ') as apt, --3
	
	isnull(phone, ' ') as phone,  --4
	isnull(city, ' ') as city, --5
	isnull(state, ' ') as state, --6
	isnull(zip, ' ') as zip, --7

	isnull(dob, ' ') as dob  --8

   FROM amac.dbo.subscriber as subscriber
    WHERE (subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
       and Subscriber.Subscriber_ID=@Subno














GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveFoxProR]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE    PROCEDURE [dbo].[sp_PassiveFoxProR]
  @Subno varchar(8)

AS 

SET NOCOUNT ON 


declare @cnt int,
--@Subno varchar(8),
@subscriberresponder_id varchar (20),
@respondername varchar (30),
@haskey     	varchar (3),
@relationcode   varchar (10),
@callorder      varchar (2),
@nviorder     	varchar (2),
@phonetype1	varchar(20),
@phone1		varchar (22),
@phonetype2	varchar(20),
@phone2		varchar (22),
@phonetype3	varchar(20),
@phone3		varchar (22),
@phonetype4	varchar(20),
@phone4		varchar (22),
@empty		int, --indicates a spot where a second work phone could go
@ph1double 	int --indicates if homephone (phone1) has two numbers in it

--Set @Subno = '267840'
set @empty=0 --initialize as no spots empty
Set @cnt=0
--the Fox Pro app returns the responder for a subscriber when 'R' criteria is submitted



CREATE TABLE #temp_responder
	(cnt int,
	subno		varchar (20)
	,subscriberresponder_id varchar (20)
	,ResponderName  varchar (30)
	,haskey     	varchar (3)
	,relationcode   varchar (10)
	,callorder      varchar (2)
	,nviorder     	varchar (2)
	,phonetype1	varchar(20)
	,phone1		varchar (30)
	,phonetype2	varchar(20)
	,phone2		varchar (30)
	,phonetype3	varchar(20)
	,phone3		varchar (30)
	,phonetype4	varchar(20)
	,phone4		varchar (30)
	,PRIMARY KEY (subscriberresponder_id))
	--,UNIQUE (state, city, emp_id) )

DECLARE responder_cursor CURSOR FOR 
	Select 
		isnull(subscriber.subscriber_id, ' ') as subscriber_id,
		isnull(subscriberresponder_id, ' ') as suscriberresponder_id,
		isnull(ResponderName, ' ') as responder, 
		isnull(haskey, ' ') as 'key', 
		isnull(Codes.description, ' ') as relationcode,
		isnull(callorder, ' ') as callorder,
		isnull(nviorder, ' ') as nviorder,
		isnull(homephone, '0') as homephone,
		isnull(workphone, '0') as workphone,
		isnull(cellphone, '0') as cellphone,
		isnull(pager, '0') as pager
	FROM 	amac.dbo.Subscriber as subscriber left JOIN
                amac.dbo.SubscriberResponder as SubscriberResponder ON Subscriber.Subscriber_ID = SubscriberResponder.Subscriber_ID left JOIN
                amac.dbo.Codes as Codes ON SubscriberResponder.RelationType = Codes.Type AND SubscriberResponder.RelationCode = Codes.Code
   	WHERE 	(subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
   	    	and Subscriber.Subscriber_ID=@Subno


OPEN responder_cursor

FETCH NEXT FROM responder_cursor 
INTO @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode, @callorder,
	@nviorder, @phone1, @phone2, @phone3, @phone4


WHILE @@FETCH_STATUS = 0
BEGIN
	
   --sometimes work phone has 2 numbers in it divided by a ';' character
   --if we have empty space we'll split the work phone and put it in two fields
/*
@ph1double
if len(@phone1)>10

	--home
	IF len(@phone1)>1  set @phonetype1='Home'
	else set @empty=1
	If len(@phone2)>1  set @phonetype1='Work'
	else 
		if @empty=0 set @empty=2
				
	set @empty=2
	--cell
	IF len(@phone3)>1  set @phonetype3='Cell'
	else 
		If @empty=0 set @empty=3
	IF len(@phone4)>1 set @phonetype4='Pager'
	else 
		If @empty=0 set @empty=4

	--if there is an empty spot
	If @empty > 0 and len(@phone2) > 11 
	begin
		If @empty = 1 
		begin
			set @phone1 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype1='Work'		
			set @phonetype2='Work'		
			--set @phonetype2='bob'		
		end
		If @empty = 3
		begin
			set @phone3 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype3='Work'
			set @phonetype2='Work'
		end
		If @empty = 4
		begin
			set @phone4 = left(@phone2, 10)
			set @phone2 = right(@phone2, 10)
			set @phonetype4='Work'
			set @phonetype2='Work'
		end	
	end
	else 
	begin
		--all slots are full so work has to stay in phone2, even if really two numbers
		set @phonetype2='Work'
	end
			--set @phone1 = left(@phone2, 10)
			--set @phone2 = right(@phone2, 10)
						
	--set @phonetype1='Work'		
		--	set @phonetype2='Work'		

		
*/	
	set @phonetype1='Home'
	set @phonetype2='Work'
	set @phonetype3='Cell'
	set @phonetype4='Pager'
		
	set @cnt = @cnt + 1

	INSERT INTO #temp_responder 
	(cnt, subno, subscriberresponder_id, ResponderName, haskey, relationcode, callorder,
	nviorder, phone1, phonetype1, phone2, phonetype2, phone3, phonetype3, phone4,
	phonetype4)
	Values (@cnt, @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode,
	 @callorder, @nviorder, @phone1, @phonetype1, @phone2, @phonetype2, @phone3, @phonetype3,
	@phone4, @phonetype4)
	
	
	
	
	
   FETCH NEXT FROM responder_cursor 
   INTO @subno, @subscriberresponder_id, @ResponderName, @haskey, @relationcode, @callorder,
	@nviorder, @phone1, @phone2, @phone3, @phone4
END

CLOSE responder_cursor
DEALLOCATE responder_cursor




Select @cnt as cnt, 
--subno, 
--subscriberresponder_id, 
ResponderName, haskey, relationcode, callorder,	nviorder, 
isnull(replace(phone1, ';', ' '), ' ') phone1,  
isnull(phonetype1, ' ') phonetype1, 
isnull(replace(phone2, ';', ' '), ' ') phone2,
isnull(phonetype2, ' ') phonetype2,
isnull(replace(phone3, ';', ' '), ' ') phone3,
isnull(phonetype3, ' ') phonetype3,
isnull(replace(phone4, ';', ' '), ' ') phone4,
isnull(phonetype4, ' ') phonetype4 
 From #temp_responder








GO
/****** Object:  StoredProcedure [dbo].[sp_PassiveUpdateDailyCallswithSubno]    Script Date: 2/11/2016 2:00:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE        PROCEDURE [dbo].[sp_PassiveUpdateDailyCallswithSubno]
   @TABLE varchar(30) = 'sys%'
AS
/*
Kirk - 16 Jan 2005 - changed where clause selection
Kirk - 12 Feb 2007 - Added new sub types A-Walgreens B-Mckesson
Kirk - 1 11 2010 - Added sub type D=DSPNET
Kirk - 7 20 2011 - changed where close only callmatch=yes, further refined >=
*/


update dailycals 
set 
subno=t1.subno
-- subno='der'
FROM 
(   
Select dailycal.DailyCallsID,
dailycal.trueunitid AS UnitID, dailycal.alarmcode1 AS AlarmCode, 
   dailycal.calldatetime AS 'Call Date/Time', dailycal.DailyCallsID as DCID, 
   dailycal.CallSource AS Type, ErcAlarmCodes.Description AS Description, 
   description1.descr AS Status, 
   dailycal.status as HiddenStatus, description2.descr as Header, 
   dailycal.calldesignation as hiddencalldesignation, 
   Subscriber_ID as subno, (Lastname + ', ' + FirstName) as FullName

   FROM ERC.dbo.dailycalls as dailycal 
   LEFT OUTER JOIN ERC.dbo.ErcAlarmCodes as ErcAlarmCodes ON  ErcAlarmCodes.CallType=dailycal.CallSource 
    AND ErcAlarmCodes.Code = dailycal.alarmcode1 
   left outer join ERC.dbo.description as description1 on description1.calltype = dailycal.CallSource and 
description1.codetype='S' and description1.code=dailycal.status 
   left outer join ERC.dbo.description as description2 on description2.calltype=dailycal.CallSource and 
description2.codetype='C' and description2.code=dailycal.calldesignation 
   left join amac.dbo.subscriber as subscriber on subscriber.Unit_ID = dailycal.TrueUnitID 
   and (subscriber.type is null or subscriber.type = case when dailycal.callSource = 'W' then 'Safecom'  
				when dailycal.CallSource = 'A' then 'Walgreens' 
				when dailycal.CallSource = 'B' then 'McKesson' 
				when dailycal.CallSource = 'C' then 'Backup' 
				when dailycal.CallSource = 'D' then 'DSPNET'
				else  'Primary' end)
    WHERE 
	(dailycal.callmatch='yes')
/* (ISNULL(dailycal.status, '0') < '0100') AND (dailycal.callmatch = 'Yes') */
	 and (subscriber.STATUS is null or subscriber.STATUS not in ('REMOVED', 'PENDING INSTALL')) 
       and dailycal.subno is null 
--addition 1/10 due to slow system
 and dailycal.dailycallsid >=7643015
--and dailycal.DailyCallsID in (789555)  
)
 as t1, ERC.dbo.dailycalls as dailycals
where dailycals.DailyCallsID=t1.DailyCallsID



--update ERC.dbo.dailycalls set subno = null where DailyCallsID in (789212, 789192)


GO
USE [master]
GO
ALTER DATABASE [VNSNY] SET  READ_WRITE 
GO
