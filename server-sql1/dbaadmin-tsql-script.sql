USE [master]
GO
/****** Object:  Database [DBAadmin]    Script Date: 2/11/2016 1:47:47 PM ******/
CREATE DATABASE [DBAadmin] ON  PRIMARY 
( NAME = N'DBAadmin', FILENAME = N'F:\DATA\MSSQL\Data\DBAadmin.mdf' , SIZE = 4511744KB , MAXSIZE = UNLIMITED, FILEGROWTH = 2048KB )
 LOG ON 
( NAME = N'DBAadmin_log', FILENAME = N'G:\DATA\MSSQL\Data\DBAadmin_log.ldf' , SIZE = 660480KB , MAXSIZE = 2048GB , FILEGROWTH = 2048KB )
GO
ALTER DATABASE [DBAadmin] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBAadmin].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [DBAadmin] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBAadmin] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBAadmin] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBAadmin] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBAadmin] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBAadmin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBAadmin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBAadmin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBAadmin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBAadmin] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBAadmin] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBAadmin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBAadmin] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBAadmin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBAadmin] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBAadmin] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBAadmin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBAadmin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBAadmin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBAadmin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBAadmin] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBAadmin] SET RECOVERY FULL 
GO
ALTER DATABASE [DBAadmin] SET  MULTI_USER 
GO
ALTER DATABASE [DBAadmin] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBAadmin] SET DB_CHAINING OFF 
GO
USE [DBAadmin]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_SysJobs_GetProcessid]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_SysJobs_GetProcessid](@job_id uniqueidentifier)
RETURNS VARCHAR(8)
AS
BEGIN
RETURN (substring(left(@job_id,8),7,2) +
		substring(left(@job_id,8),5,2) +
		substring(left(@job_id,8),3,2) +
		substring(left(@job_id,8),1,2))
END
GO
/****** Object:  Table [dbo].[amacbilling]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[amacbilling](
	[BillingPeriod] [char](6) NOT NULL,
	[Agency_ID] [char](6) NOT NULL,
	[Subscriber_ID] [char](6) NOT NULL,
	[Unit_ID] [char](4) NULL,
	[SharedID] [int] NULL,
	[SecondUser] [char](1) NULL,
	[Status] [varchar](20) NULL,
	[InstallDate] [datetime] NULL,
	[RequestedRemovalDate] [datetime] NULL,
	[RemovalDate] [datetime] NULL,
	[Item_ID] [varchar](15) NULL,
	[SerialNo] [varchar](20) NULL,
	[TransferFrom] [char](6) NULL,
	[TransferType] [char](6) NULL,
	[AddDate] [datetime] NULL,
	[Closed] [char](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AMACTrace_06082015]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_06082015](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08042015]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08042015](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[HostName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08062015]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08062015](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08202015]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08202015](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08202015_10am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08202015_10am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08202015_11am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08202015_11am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08202015_3pm]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08202015_3pm](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08202015_9am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08202015_9am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_08232015_11am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_08232015_11am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Nov16]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Nov16](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[HostName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[SourceDatabaseID] [int] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[ObjectID] [int] NULL,
	[IndexID] [int] NULL,
	[Mode] [int] NULL,
	[LoginSid] [image] NULL,
	[EventSequence] [int] NULL,
	[IsSystem] [int] NULL,
	[SessionLoginName] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Nov1611am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Nov1611am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Nov18]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Nov18](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct13]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct13](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct14]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct14](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct16]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct16](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct19]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct19](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct20]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct20](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct21]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct21](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AMACTrace_Oct28]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AMACTrace_Oct28](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ERCTrace_Oct12]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERCTrace_Oct12](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ERCTrace_Oct21]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERCTrace_Oct21](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ERCTrace_Oct22]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERCTrace_Oct22](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ERCTrace_Oct2210am]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ERCTrace_Oct2210am](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ETL_Update_Subscriber_D0304]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ETL_Update_Subscriber_D0304](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[TextData] [ntext] NULL,
	[SPID] [int] NULL,
	[StartTime] [datetime] NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[Duration] [bigint] NULL,
	[EndTime] [datetime] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[Reads] [bigint] NULL,
	[RowCounts] [bigint] NULL,
	[ServerName] [nvarchar](128) NULL,
	[Writes] [bigint] NULL,
	[ObjectID] [int] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[ObjectType] [int] NULL,
	[BinaryData] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Events_Class]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_Class](
	[ID] [int] NULL,
	[Description] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReplTest]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReplTest](
	[ReplId] [int] NOT NULL,
	[Desc] [nchar](10) NULL,
 CONSTRAINT [PK_ReplTest] PRIMARY KEY CLUSTERED 
(
	[ReplId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[subscriber]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscriber](
	[Subscriber_ID] [char](6) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
	[Agency_ID] [char](6) NULL,
	[MiddleInitial] [char](1) NULL,
	[LastName] [varchar](30) NULL,
	[KnownName] [varchar](20) NULL,
	[Address1] [varchar](30) NULL,
	[Address2] [varchar](30) NULL,
	[Zip] [varchar](6) NULL,
	[City] [varchar](20) NULL,
	[County] [varchar](20) NULL,
	[State] [char](2) NULL,
	[NearIntersection] [varchar](255) NULL,
	[AgencyName] [varchar](50) NULL,
	[Unit_ID] [varchar](4) NULL,
	[Subcontractor_ID] [char](6) NULL,
	[SSN] [char](9) NULL,
	[Medicaid] [varchar](8) NULL,
	[Sex] [char](1) NULL,
	[Status] [varchar](20) NOT NULL,
	[DOB] [datetime] NULL,
	[Type] [varchar](20) NULL,
	[ForcedEntry] [char](1) NULL,
	[Cancel] [char](1) NULL,
	[UnitRented_Sold] [char](1) NULL,
	[ZipPlus4] [char](4) NULL,
	[TimeZoneCode] [int] NULL,
	[Phone] [varchar](10) NULL,
	[IsDayLightSaving] [char](1) NULL,
	[OrderDate] [datetime] NULL,
	[EntryDate] [datetime] NULL,
	[InstallDate] [datetime] NULL,
	[RemovalDate] [datetime] NULL,
	[RequestedRemovalDate] [datetime] NULL,
	[EquipmentRecovered] [char](1) NULL,
	[FileMagic] [datetime] NULL,
	[ConfirmReceived] [char](1) NULL,
	[SpecialInst] [varchar](1000) NULL,
	[Comments] [varchar](500) NULL,
	[Email] [varchar](50) NULL,
	[FaxNo] [varchar](10) NULL,
	[Deleted] [char](1) NOT NULL,
	[SecondUser] [char](1) NOT NULL,
	[OnlineSince] [datetime] NULL,
	[ProgramType] [varchar](25) NULL,
	[MasterReference] [char](6) NULL,
	[SharedID] [int] NULL,
	[SystemName] [varchar](50) NULL,
	[IPAddress] [varchar](20) NULL,
	[IsAddressVerified] [char](1) NULL,
	[ManualOverride] [char](1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[subscribergeneraldata]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscribergeneraldata](
	[Subscriber_ID] [char](6) NOT NULL,
	[GeneralData_ID] [char](10) NOT NULL,
	[Data] [varchar](255) NOT NULL,
	[Type] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[subscribergeneraldata20150102]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subscribergeneraldata20150102](
	[Subscriber_ID] [char](6) NOT NULL,
	[GeneralData_ID] [char](10) NOT NULL,
	[Data] [varchar](255) NOT NULL,
	[Type] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TracecPeformance_Oct122015]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TracecPeformance_Oct122015](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TracecPeformance_Oct1220151pm]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TracecPeformance_Oct1220151pm](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[RowCounts] [bigint] NULL,
	[TextData] [ntext] NULL,
	[ObjectID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TraceResults]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraceResults](
	[TextData] [varchar](max) NULL,
	[Duration] [int] NULL,
	[SPID] [int] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [int] NULL,
	[RowCounts] [int] NULL,
	[Starttime] [datetime] NULL,
	[Writes] [int] NULL,
	[dbname] [nvarchar](150) NULL,
	[ObjectName] [nvarchar](150) NULL,
	[ProcedureName] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TraceResults_EventClass]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraceResults_EventClass](
	[TextData] [varchar](max) NULL,
	[Duration] [int] NULL,
	[SPID] [int] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [int] NULL,
	[RowCounts] [int] NULL,
	[Starttime] [datetime] NULL,
	[Writes] [int] NULL,
	[dbname] [nvarchar](150) NULL,
	[ObjectName] [nvarchar](150) NULL,
	[HostName] [nvarchar](150) NULL,
	[EventClass] [nvarchar](150) NULL,
	[ProcedureName] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TraceResults_Hostname]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraceResults_Hostname](
	[TextData] [varchar](max) NULL,
	[Duration] [int] NULL,
	[SPID] [int] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [int] NULL,
	[RowCounts] [int] NULL,
	[Starttime] [datetime] NULL,
	[Writes] [int] NULL,
	[dbname] [nvarchar](150) NULL,
	[ObjectName] [nvarchar](150) NULL,
	[HostName] [nvarchar](150) NULL,
	[ProcedureName] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TraceResults_Kshema]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TraceResults_Kshema](
	[TextData] [varchar](max) NULL,
	[Duration] [int] NULL,
	[SPID] [int] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [int] NULL,
	[RowCounts] [int] NULL,
	[Starttime] [datetime] NULL,
	[Writes] [int] NULL,
	[dbname] [nvarchar](150) NULL,
	[ObjectName] [nvarchar](150) NULL,
	[ProcedureName] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VMctmssql01_Etl_Update_Subscriber]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VMctmssql01_Etl_Update_Subscriber](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[TextData] [ntext] NULL,
	[SPID] [int] NULL,
	[StartTime] [datetime] NULL,
	[CPU] [int] NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[Duration] [bigint] NULL,
	[EndTime] [datetime] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[Reads] [bigint] NULL,
	[RowCounts] [bigint] NULL,
	[TransactionID] [bigint] NULL,
	[Writes] [bigint] NULL,
	[XactSequence] [bigint] NULL,
	[ObjectID] [int] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[Error] [int] NULL,
	[ObjectType] [int] NULL,
	[BinaryData] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VMctmssql01_Etl_Update_Subscriber_D0223]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VMctmssql01_Etl_Update_Subscriber_D0223](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[TextData] [ntext] NULL,
	[SPID] [int] NULL,
	[StartTime] [datetime] NULL,
	[CPU] [int] NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[Duration] [bigint] NULL,
	[EndTime] [datetime] NULL,
	[HostName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[Reads] [bigint] NULL,
	[RowCounts] [bigint] NULL,
	[TransactionID] [bigint] NULL,
	[Writes] [bigint] NULL,
	[XactSequence] [bigint] NULL,
	[ObjectID] [int] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[Error] [int] NULL,
	[ObjectType] [int] NULL,
	[DatabaseID] [int] NULL,
	[IsSystem] [int] NULL,
	[LoginSid] [image] NULL,
	[NTDomainName] [nvarchar](128) NULL,
	[NTUserName] [nvarchar](128) NULL,
	[RequestID] [int] NULL,
	[ServerName] [nvarchar](128) NULL,
	[SessionLoginName] [nvarchar](128) NULL,
	[BinaryData] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vmtcmssql_sync]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vmtcmssql_sync](
	[RowNumber] [int] IDENTITY(0,1) NOT NULL,
	[EventClass] [int] NULL,
	[TextData] [ntext] NULL,
	[SPID] [int] NULL,
	[StartTime] [datetime] NULL,
	[BinaryData] [image] NULL,
	[ApplicationName] [nvarchar](128) NULL,
	[CPU] [int] NULL,
	[ClientProcessID] [int] NULL,
	[DatabaseID] [int] NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[Duration] [bigint] NULL,
	[EndTime] [datetime] NULL,
	[EventSequence] [int] NULL,
	[HostName] [nvarchar](128) NULL,
	[IntegerData] [int] NULL,
	[IntegerData2] [int] NULL,
	[IsSystem] [int] NULL,
	[LineNumber] [int] NULL,
	[LoginName] [nvarchar](128) NULL,
	[LoginSid] [image] NULL,
	[NTDomainName] [nvarchar](128) NULL,
	[NTUserName] [nvarchar](128) NULL,
	[NestLevel] [int] NULL,
	[Offset] [int] NULL,
	[Reads] [bigint] NULL,
	[RequestID] [int] NULL,
	[RowCounts] [bigint] NULL,
	[ServerName] [nvarchar](128) NULL,
	[SessionLoginName] [nvarchar](128) NULL,
	[TransactionID] [bigint] NULL,
	[Writes] [bigint] NULL,
	[XactSequence] [bigint] NULL,
	[State] [int] NULL,
	[EventSubClass] [int] NULL,
	[ObjectID] [int] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[SqlHandle] [image] NULL,
	[Error] [int] NULL,
	[ObjectType] [int] NULL,
	[SourceDatabaseID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaitStats]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaitStats](
	[RowNum] [bigint] IDENTITY(1,1) NOT NULL,
	[CaptureDate] [datetime] NULL,
	[WaitType] [nvarchar](120) NULL,
	[Wait_S] [decimal](14, 2) NULL,
	[Resource_S] [decimal](14, 2) NULL,
	[Signal_S] [decimal](14, 2) NULL,
	[WaitCount] [bigint] NULL,
	[Percentage] [decimal](4, 2) NULL,
	[AvgWait_S] [decimal](14, 2) NULL,
	[AvgRes_S] [decimal](14, 2) NULL,
	[AvgSig_S] [decimal](14, 2) NULL
) ON [PRIMARY]

GO
/****** Object:  Index [CI_WaitStats]    Script Date: 2/11/2016 1:47:47 PM ******/
CREATE CLUSTERED INDEX [CI_WaitStats] ON [dbo].[WaitStats]
(
	[RowNum] ASC,
	[CaptureDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_RestoreScriptGenie]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*********************************************************************************************
Restore Script Generator v1.05 (2013-01-15)
(C) 2012, Paul Brewer

Feedback: mailto:paulbrewer@yahoo.co.uk
Updates: http://paul.dynalias.com/sql

License:
Restore Script Genie is free to download and use for personal, educational, and internal
corporate purposes, provided that this header is preserved. Redistribution or sale
of sp_RestoreScriptGenie, in whole or in part, is prohibited without the author's express
written consent.

Usage examples:

sp_RestoreScriptGenie
  No parameters = Generates RESTORE commands for all USER databases, from actual backup files to existing file locations to most current time, consistency checks, CHECKSUM where possible

sp_RestoreScriptGenie @Database = 'db_workspace', @StopAt = '2012-12-23 12:01:00.000', @StandbyMode = 1
  Generates RESTORE commands for a specific database from the most recent full backup + most recent differential + transaction log backups before to STOPAT.
  Databases left in STANDBY
  Ignores COPY_ONLY backups, restores to default file locations from default backup file.

sp_RestoreScriptGenie @Database = 'db_workspace', @StopAt = '2012-12-23 12:31:00.000', @ToFileFolder = 'c:\temp\', @ToLogFolder = 'c:\temp\' , @BackupDeviceFolder = 'c:\backup\'
  Overrides data file folder, log file folder and backup file folder.
  Generates RESTORE commands for a specific database from most recent full backup, most recent differential + transaction log backups before STOPAT.
  Ignores COPY_ONLY backups, includes WITH MOVE to simulate a restore to a test environment with different folder mapping.

CHANGE LOG:
December 23, 2012 - V1.01 - Release
January 4,2013  - V1.02 - LSN Checks + Bug fix to STOPAT date format
January 11,2013  - V1.03 - SQL Server 2005 compatibility (backup compression problem) & @StandbyMode for stepping through log restores with a readable database
January 14, 2013 - V1.04 - Cope with up to 10 striped backup files
January 15, 2013 - V1.05 - Format of constructed restore script, enclose database name in [ ]
*********************************************************************************************/

CREATE PROC [dbo].[sp_RestoreScriptGenie]
(
  @Database SYSNAME = NULL,
  @ToFileFolder VARCHAR(2000) = NULL,
  @ToLogFolder VARCHAR(2000) = NULL,
  @BackupDeviceFolder VARCHAR(2000) = NULL,
  @StopAt DATETIME = NULL,
  @StandbyMode BIT = 0,
  @IncludeSystemBackups BIT = 0
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET QUOTED_IDENTIFIER ON;
SET ANSI_PADDING ON;
SET CONCAT_NULL_YIELDS_NULL ON;
SET ANSI_WARNINGS ON;
SET NUMERIC_ROUNDABORT OFF;
SET ARITHABORT ON;

IF ISNULL(@StopAt,'') = ''
  SET @StopAt = GETDATE();

--------------------------------------------------------------------------------------------------------------
-- Full backup UNION Differential Backup UNION Log Backup
--------------------------------------------------------------------------------------------------------------
WITH CTE
(
   database_name
  ,current_compatibility_level
  ,Last_LSN
  ,current_is_read_only
  ,current_state_desc
  ,current_recovery_model_desc
  ,has_backup_checksums
  ,backup_size
  ,[type]
  ,backupmediasetid
  ,family_sequence_number
  ,backupfinishdate
  ,physical_device_name
  ,position
)
AS
(
--------------------------------------------------------------------------------------------------------------
-- Full backup (most current or immediately before @StopAt if supplied)
--------------------------------------------------------------------------------------------------------------

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'D' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,x.backup_finish_date AS backupfinishdate
  ,mf.physical_device_name
  ,bs.position
FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

INNER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] = 2
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,a.backup_finish_date)
  GROUP BY database_name
) x
  ON x.database_name = bs.database_name
  AND x.backup_finish_date = bs.backup_finish_date

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

WHERE bs.type = 'D'
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')

--------------------------------------------------------------------------------------------------------------
-- Differential backup (most current or immediately before @StopAt if supplied)
--------------------------------------------------------------------------------------------------------------
UNION

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'I' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,x.backup_finish_date AS backupfinishdate
  ,mf.physical_device_name
  ,bs.position
FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

INNER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'I'
  AND b.[Device_Type] = 2
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,GETDATE())
  GROUP BY database_name
) x
  ON x.database_name = bs.database_name
  AND x.backup_finish_date = bs.backup_finish_date

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

WHERE bs.type = 'I'
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')
AND bs.backup_finish_date <= ISNULL(@StopAt,GETDATE())

--------------------------------------------------------------------------------------------------------------
-- Log file backups after 1st full backup before @STOPAT, before next full backup after 1st full backup
--------------------------------------------------------------------------------------------------------------
UNION

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'L' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,bs.backup_finish_date as backupfinishdate
  ,mf.physical_device_name
  ,bs.position

FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

LEFT OUTER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] = 2
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,a.backup_finish_date)
  GROUP BY database_name
) y
  ON bs.database_name = y.Database_name

LEFT OUTER JOIN
(
  SELECT
    database_name
   ,MIN(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
   ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] = 2

  AND a.is_copy_only = 0
  AND a.backup_finish_date > ISNULL(@StopAt,'1 Jan, 1900')
  GROUP BY database_name
) z
  ON bs.database_name = z.database_name

WHERE bs.backup_finish_date > y.backup_finish_date
AND bs.backup_finish_date < ISNULL(z.backup_finish_date,GETDATE())
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')
AND bs.type = 'L'
AND mf.device_type = 2
)

---------------------------------------------------------------
-- Result set below is based on CTE above
---------------------------------------------------------------

SELECT
  a.Command AS TSQL_RestoreCommand_CopyPaste
FROM
(

--------------------------------------------------------------------
-- Most recent full backup
--------------------------------------------------------------------

SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
    WHEN 'Actual' THEN CTE.physical_device_name
    ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' + SPACE(1) +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  'WITH REPLACE, FILE = ' + CAST(CTE.Position AS VARCHAR(5)) + ',' +
  CASE CTE.has_backup_checksums WHEN 1 THEN 'CHECKSUM,' ELSE ' ' END +

  CASE @StandbyMode WHEN 0 THEN 'NORECOVERY,' ELSE 'STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + '''' + ',' END + SPACE(1) +

  'STATS=10,' + SPACE(1) +
  'MOVE ' + '''' + x.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN x.PhysicalName
    ELSE @ToFileFolder + SUBSTRING(x.PhysicalName,LEN(x.PhysicalName) - CHARINDEX('\',REVERSE(x.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(x.PhysicalName),1) + 1)
  END + '''' + ',' + SPACE(1) +

  'MOVE ' + '''' + y.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToLogFolder,'Actual')
    WHEN 'Actual' THEN y.PhysicalName
    ELSE @ToLogFolder + SUBSTRING(y.PhysicalName,LEN(y.PhysicalName) - CHARINDEX('\',REVERSE(y.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(y.PhysicalName),1) + 1)
  END + '''' AS Command,
  1 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d
JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name
   ,mf.Physical_Name AS PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'ROWS'
  AND mf.file_id = 1
) x
ON d.name = x.name

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name, type_desc
   ,mf.Physical_Name PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'LOG'
) y
ON d.name = y.name

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

WHERE CTE.[type] = 'D'
AND CTE.family_sequence_number = 1

--------------------------------------------------------------------
-- Most recent differential backup
--------------------------------------------------------------------
UNION

SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
    WHEN 'Actual' THEN CTE.physical_device_name
    ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' + SPACE(1) +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  'WITH REPLACE, FILE = ' + CAST(CTE.Position AS VARCHAR(5)) + ',' +
  CASE CTE.has_backup_checksums WHEN 1 THEN 'CHECKSUM,' ELSE ' ' END +

  CASE @StandbyMode WHEN 0 THEN 'NORECOVERY,' ELSE 'STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + ''''  + ',' END + SPACE(1) +

  'STATS=10,' + SPACE(1) +
  'MOVE ' + '''' + x.LogicalName + '''' + ' TO ' +
  '''' +
   CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN x.PhysicalName
    ELSE @ToFileFolder + SUBSTRING(x.PhysicalName,LEN(x.PhysicalName) - CHARINDEX('\',REVERSE(x.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(x.PhysicalName),1) + 1)
  END + '''' + ',' + SPACE(1) +

  'MOVE ' + '''' + y.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToLogFolder,'Actual')
    WHEN 'Actual' THEN y.PhysicalName
    ELSE @ToLogFolder + SUBSTRING(y.PhysicalName,LEN(y.PhysicalName) - CHARINDEX('\',REVERSE(y.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(y.PhysicalName),1) + 1)
  END + '''' AS Command,
  32769/2 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name
   ,mf.Physical_Name AS PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'ROWS'
  AND mf.file_id = 1
) x
ON d.name = x.name

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name, type_desc
   ,mf.Physical_Name PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'LOG'
) y
ON d.name = y.name

JOIN
(
  SELECT
   database_name,
   Last_LSN,
   backupfinishdate
  FROM CTE
  WHERE [Type] = 'D'
) z
  ON CTE.database_name = z.database_name

WHERE CTE.[type] = 'I'
AND CTE.backupfinishdate > z.backupfinishdate -- Differential backup was after selected full backup
AND CTE.Last_LSN > z.Last_LSN -- Differential Last LSN > Full Last LSN
AND CTE.backupfinishdate < @StopAt
AND CTE.family_sequence_number = 1

-----------------------------------------------------------------------------------------------------------------------------
UNION -- Restore Log backups taken since most recent full, these are filtered in the CTE to those after the full backup date
-----------------------------------------------------------------------------------------------------------------------------

SELECT
  ';RESTORE LOG [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' + --CTE.physical_device_name + '''' + SPACE(1) +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
   WHEN 'Actual' THEN CTE.physical_device_name
   ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  CASE @StandbyMode WHEN 0 THEN ' WITH NORECOVERY,' ELSE ' WITH STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + ''''  + ',' END + SPACE(1) +

  CASE CTE.has_backup_checksums WHEN 1 THEN ' CHECKSUM,' ELSE ' ' END +
 
  + 'FILE = ' + CAST(CTE.Position AS VARCHAR(5)) +
  CASE CTE.backupfinishdate
    WHEN z.backupfinishdate THEN ' ,STOPAT = ' + '''' + CONVERT(VARCHAR(21),@StopAt,120) + ''''
    ELSE ' '
  END
  AS Command,
  32769 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

LEFT OUTER JOIN  -- Next full backup after STOPAT
(
  SELECT
   database_name, MIN(BackupFinishDate) AS backup_finish_date
  FROM CTE
  WHERE type = 'D'
  AND backupfinishdate > @StopAt
  GROUP BY database_name

) x
  ON x.database_name = CTE.database_name

LEFT OUTER JOIN -- Highest differential backup date
(
  SELECT database_name, max(backupfinishdate) AS backupfinishdate
  FROM CTE
  WHERE CTE.type = 'I'
  AND CTE.backupfinishdate < @StandbyMode
  GROUP BY database_name
) y
  ON y.database_name = CTE.database_name

LEFT OUTER JOIN -- First log file after STOPAT
(
  SELECT database_name, min(backupfinishdate) AS backupfinishdate
  FROM CTE
  WHERE CTE.type = 'L'
  AND backupfinishdate > @StopAt
  GROUP BY database_name
) z
  ON z.database_name = CTE.database_name

JOIN
(
  SELECT
   database_name,
   MAX(Last_LSN) AS Last_LSN
  FROM CTE
  WHERE CTE.backupfinishdate < ISNULL(@StopAt,GETDATE())
  AND CTE.Type IN ('D','I')
  GROUP BY database_name
) x1
  ON CTE.database_name = x1.database_name

WHERE CTE.[type] = 'L'
AND CTE.backupfinishdate <= ISNULL(x.backup_finish_date,'31 Dec, 2199') -- Less than next full backup
AND CTE.backupfinishdate >= ISNULL(y.backupfinishdate, CTE.backupfinishdate) --Great than or equal to last differential backup
AND CTE.backupfinishdate <= ISNULL(z.backupfinishdate, CTE.backupfinishdate) -- Less than or equal to last file file in recovery chain (IE Log Backup datetime might be after STOPAT)
AND CTE.Last_LSN > x1.Last_LSN -- Differential or Full Last LSN < Log Last LSN
AND CTE.family_sequence_number = 1

--------------------------------------------------------------------
UNION -- Restore WITH RECOVERY
--------------------------------------------------------------------
SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) + 'WITH RECOVERY' AS Command,
  32771 AS Sequence,
  d.name AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

WHERE CTE.[type] = 'D'
AND @StandbyMode = 0

--------------------------------------------------------------------
UNION -- CHECKDB
--------------------------------------------------------------------
SELECT
  ';DBCC CHECKDB(' + '''' + d.[name] + '''' + ') WITH NO_INFOMSGS IF @@ERROR > 0 PRINT N''CONSISTENCY PROBLEMS IN DATABASE : ' + d.name + ''' ELSE PRINT N''CONSISTENCY GOOD IN DATABASE : ' + d.name + '''' AS Command,
  32772 AS Sequence,
  d.name AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

WHERE CTE.[type] = 'D'
AND @StandbyMode = 0

---------------------------------------------------------------------------------------------------------------------------------------------------
UNION -- MOVE full backup secondary data files, allows for up to 32769/2 file groups
---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT
  ', MOVE ' + '''' + b.name + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN b.physical_name
    ELSE @ToFileFolder + SUBSTRING(b.Physical_Name,LEN(b.Physical_Name) - CHARINDEX('\',REVERSE(b.Physical_Name),1) + 2,CHARINDEX('\',REVERSE(b.Physical_Name),1) + 1)
  END + '''',
  b.file_id AS Sequence,
  DB_NAME(b.database_id) AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.master_files b
INNER JOIN CTE
  ON CTE.database_name = DB_NAME(b.database_id)

WHERE CTE.[type] = 'D'
AND b.type_desc = 'ROWS'
AND b.file_id > 2

---------------------------------------------------------------------------------------------------------------------------------------------------
UNION -- MOVE differential backup secondary data files, allows for up to 32769/2 file groups
---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT
  ', MOVE ' + '''' + b.name + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN b.physical_name
    ELSE @ToFileFolder + SUBSTRING(b.Physical_Name,LEN(b.Physical_Name) - CHARINDEX('\',REVERSE(b.Physical_Name),1) + 2,CHARINDEX('\',REVERSE(b.Physical_Name),1) + 1)
  END + '''',
  ((b.file_id) + (32769/2)) AS Sequence,
  DB_NAME(b.database_id) AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.master_files b
INNER JOIN CTE
  ON CTE.database_name = DB_NAME(b.database_id)

WHERE CTE.[type] = 'I'
AND b.type_desc = 'ROWS'
AND b.file_id > 2
AND CTE.backupfinishdate < @StopAt
) a

WHERE a.database_name = ISNULL(@database,a.database_name)
AND (@IncludeSystemBackups = 1 OR a.database_name NOT IN('master','model','msdb'))

ORDER BY
  database_name,
  sequence,
  backupfinishdate

END
GO
/****** Object:  StoredProcedure [dbo].[sp_RestoreScriptGenie_Device]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*********************************************************************************************
Restore Script Generator v1.05 (2013-01-15)
(C) 2012, Paul Brewer

Feedback: mailto:paulbrewer@yahoo.co.uk
Updates: http://paul.dynalias.com/sql

License:
Restore Script Genie is free to download and use for personal, educational, and internal
corporate purposes, provided that this header is preserved. Redistribution or sale
of sp_RestoreScriptGenie, in whole or in part, is prohibited without the author's express
written consent.

Usage examples:

sp_RestoreScriptGenie
  No parameters = Generates RESTORE commands for all USER databases, from actual backup files to existing file locations to most current time, consistency checks, CHECKSUM where possible

sp_RestoreScriptGenie @Database = 'db_workspace', @StopAt = '2012-12-23 12:01:00.000', @StandbyMode = 1
  Generates RESTORE commands for a specific database from the most recent full backup + most recent differential + transaction log backups before to STOPAT.
  Databases left in STANDBY
  Ignores COPY_ONLY backups, restores to default file locations from default backup file.

sp_RestoreScriptGenie @Database = 'db_workspace', @StopAt = '2012-12-23 12:31:00.000', @ToFileFolder = 'c:\temp\', @ToLogFolder = 'c:\temp\' , @BackupDeviceFolder = 'c:\backup\'
  Overrides data file folder, log file folder and backup file folder.
  Generates RESTORE commands for a specific database from most recent full backup, most recent differential + transaction log backups before STOPAT.
  Ignores COPY_ONLY backups, includes WITH MOVE to simulate a restore to a test environment with different folder mapping.

CHANGE LOG:
December 23, 2012 - V1.01 - Release
January 4,2013  - V1.02 - LSN Checks + Bug fix to STOPAT date format
January 11,2013  - V1.03 - SQL Server 2005 compatibility (backup compression problem) & @StandbyMode for stepping through log restores with a readable database
January 14, 2013 - V1.04 - Cope with up to 10 striped backup files
January 15, 2013 - V1.05 - Format of constructed restore script, enclose database name in [ ]
*********************************************************************************************/

CREATE PROC [dbo].[sp_RestoreScriptGenie_Device]
(
  @Database SYSNAME = NULL,
  @ToFileFolder VARCHAR(2000) = NULL,
  @ToLogFolder VARCHAR(2000) = NULL,
  @BackupDeviceFolder VARCHAR(2000) = NULL,
  @StopAt DATETIME = NULL,
  @StandbyMode BIT = 0,
  @IncludeSystemBackups BIT = 0
)
AS
BEGIN

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET QUOTED_IDENTIFIER ON;
SET ANSI_PADDING ON;
SET CONCAT_NULL_YIELDS_NULL ON;
SET ANSI_WARNINGS ON;
SET NUMERIC_ROUNDABORT OFF;
SET ARITHABORT ON;

IF ISNULL(@StopAt,'') = ''
  SET @StopAt = GETDATE();

--------------------------------------------------------------------------------------------------------------
-- Full backup UNION Differential Backup UNION Log Backup
--------------------------------------------------------------------------------------------------------------
WITH CTE
(
   database_name
  ,current_compatibility_level
  ,Last_LSN
  ,current_is_read_only
  ,current_state_desc
  ,current_recovery_model_desc
  ,has_backup_checksums
  ,backup_size
  ,[type]
  ,backupmediasetid
  ,family_sequence_number
  ,backupfinishdate
  ,physical_device_name
  ,position
)
AS
(
--------------------------------------------------------------------------------------------------------------
-- Full backup (most current or immediately before @StopAt if supplied)
--------------------------------------------------------------------------------------------------------------

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'D' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,x.backup_finish_date AS backupfinishdate
  ,mf.physical_device_name
  ,bs.position
FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

INNER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] in (2,102)
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,a.backup_finish_date)
  GROUP BY database_name
) x
  ON x.database_name = bs.database_name
  AND x.backup_finish_date = bs.backup_finish_date

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

WHERE bs.type = 'D'
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')

--------------------------------------------------------------------------------------------------------------
-- Differential backup (most current or immediately before @StopAt if supplied)
--------------------------------------------------------------------------------------------------------------
UNION

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'I' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,x.backup_finish_date AS backupfinishdate
  ,mf.physical_device_name
  ,bs.position
FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

INNER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'I'
  AND b.[Device_Type] in (2,102)
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,GETDATE())
  GROUP BY database_name
) x
  ON x.database_name = bs.database_name
  AND x.backup_finish_date = bs.backup_finish_date

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

WHERE bs.type = 'I'
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')
AND bs.backup_finish_date <= ISNULL(@StopAt,GETDATE())

--------------------------------------------------------------------------------------------------------------
-- Log file backups after 1st full backup before @STOPAT, before next full backup after 1st full backup
--------------------------------------------------------------------------------------------------------------
UNION

SELECT
   bs.database_name
  ,d.[compatibility_level] AS current_compatibility_level
  ,bs.last_lsn
  ,d.[is_read_only] AS current_is_read_only
  ,d.[state_desc] AS current_state_desc
  ,d.[recovery_model_desc] current_recovery_model_desc
  ,bs.has_backup_checksums
  ,bs.backup_size AS backup_size
  ,'L' AS [type]
  ,bs.media_set_id AS backupmediasetid
  ,mf.family_sequence_number
  ,bs.backup_finish_date as backupfinishdate
  ,mf.physical_device_name
  ,bs.position

FROM msdb.dbo.backupset bs

INNER JOIN sys.databases d
  ON bs.database_name = d.name

JOIN msdb.dbo.backupmediafamily mf
  ON mf.media_set_id = bs.media_set_id
  AND mf.family_sequence_number Between bs.first_family_number And bs.last_family_number

LEFT OUTER JOIN
(
  SELECT
    database_name
   ,MAX(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
  ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] in (2,102)
  AND a.is_copy_only = 0
  AND a.backup_finish_date <= ISNULL(@StopAt,a.backup_finish_date)
  GROUP BY database_name
) y
  ON bs.database_name = y.Database_name

LEFT OUTER JOIN
(
  SELECT
    database_name
   ,MIN(backup_finish_date) backup_finish_date
  FROM msdb.dbo.backupset a
  JOIN msdb.dbo.backupmediafamily b
   ON a.media_set_id = b.media_set_id
  WHERE a.[type] = 'D'
  AND b.[Device_Type] in (2,102)

  AND a.is_copy_only = 0
  AND a.backup_finish_date > ISNULL(@StopAt,'1 Jan, 1900')
  GROUP BY database_name
) z
  ON bs.database_name = z.database_name

WHERE bs.backup_finish_date > y.backup_finish_date
AND bs.backup_finish_date < ISNULL(z.backup_finish_date,GETDATE())
AND mf.physical_device_name NOT IN ('Nul', 'Nul:')
AND bs.type = 'L'
AND mf.device_type in (2,102)
)

---------------------------------------------------------------
-- Result set below is based on CTE above
---------------------------------------------------------------

SELECT
  a.Command AS TSQL_RestoreCommand_CopyPaste
FROM
(

--------------------------------------------------------------------
-- Most recent full backup
--------------------------------------------------------------------

SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
    WHEN 'Actual' THEN CTE.physical_device_name
    ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' + SPACE(1) +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  'WITH REPLACE, FILE = ' + CAST(CTE.Position AS VARCHAR(5)) + ',' +
  CASE CTE.has_backup_checksums WHEN 1 THEN 'CHECKSUM,' ELSE ' ' END +

  CASE @StandbyMode WHEN 0 THEN 'NORECOVERY,' ELSE 'STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + '''' + ',' END + SPACE(1) +

  'STATS=10,' + SPACE(1) +
  'MOVE ' + '''' + x.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN x.PhysicalName
    ELSE @ToFileFolder + SUBSTRING(x.PhysicalName,LEN(x.PhysicalName) - CHARINDEX('\',REVERSE(x.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(x.PhysicalName),1) + 1)
  END + '''' + ',' + SPACE(1) +

  'MOVE ' + '''' + y.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToLogFolder,'Actual')
    WHEN 'Actual' THEN y.PhysicalName
    ELSE @ToLogFolder + SUBSTRING(y.PhysicalName,LEN(y.PhysicalName) - CHARINDEX('\',REVERSE(y.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(y.PhysicalName),1) + 1)
  END + '''' AS Command,
  1 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d
JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name
   ,mf.Physical_Name AS PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'ROWS'
  AND mf.file_id = 1
) x
ON d.name = x.name

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name, type_desc
   ,mf.Physical_Name PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'LOG'
) y
ON d.name = y.name

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

WHERE CTE.[type] = 'D'
AND CTE.family_sequence_number = 1

--------------------------------------------------------------------
-- Most recent differential backup
--------------------------------------------------------------------
UNION

SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
    WHEN 'Actual' THEN CTE.physical_device_name
    ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' + SPACE(1) +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  'WITH REPLACE, FILE = ' + CAST(CTE.Position AS VARCHAR(5)) + ',' +
  CASE CTE.has_backup_checksums WHEN 1 THEN 'CHECKSUM,' ELSE ' ' END +

  CASE @StandbyMode WHEN 0 THEN 'NORECOVERY,' ELSE 'STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + ''''  + ',' END + SPACE(1) +

  'STATS=10,' + SPACE(1) +
  'MOVE ' + '''' + x.LogicalName + '''' + ' TO ' +
  '''' +
   CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN x.PhysicalName
    ELSE @ToFileFolder + SUBSTRING(x.PhysicalName,LEN(x.PhysicalName) - CHARINDEX('\',REVERSE(x.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(x.PhysicalName),1) + 1)
  END + '''' + ',' + SPACE(1) +

  'MOVE ' + '''' + y.LogicalName + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToLogFolder,'Actual')
    WHEN 'Actual' THEN y.PhysicalName
    ELSE @ToLogFolder + SUBSTRING(y.PhysicalName,LEN(y.PhysicalName) - CHARINDEX('\',REVERSE(y.PhysicalName),1) + 2,CHARINDEX('\',REVERSE(y.PhysicalName),1) + 1)
  END + '''' AS Command,
  32769/2 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name
   ,mf.Physical_Name AS PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'ROWS'
  AND mf.file_id = 1
) x
ON d.name = x.name

JOIN
(
  SELECT
    DB_NAME(mf.database_id) AS name, type_desc
   ,mf.Physical_Name PhysicalName
   ,mf.Name AS LogicalName
  FROM sys.master_files mf
  WHERE type_desc = 'LOG'
) y
ON d.name = y.name

JOIN
(
  SELECT
   database_name,
   Last_LSN,
   backupfinishdate
  FROM CTE
  WHERE [Type] = 'D'
) z
  ON CTE.database_name = z.database_name

WHERE CTE.[type] = 'I'
AND CTE.backupfinishdate > z.backupfinishdate -- Differential backup was after selected full backup
AND CTE.Last_LSN > z.Last_LSN -- Differential Last LSN > Full Last LSN
AND CTE.backupfinishdate < @StopAt
AND CTE.family_sequence_number = 1

-----------------------------------------------------------------------------------------------------------------------------
UNION -- Restore Log backups taken since most recent full, these are filtered in the CTE to those after the full backup date
-----------------------------------------------------------------------------------------------------------------------------

SELECT
  ';RESTORE LOG [' + d.[name] + ']' + SPACE(1) +
  'FROM DISK = ' + '''' + --CTE.physical_device_name + '''' + SPACE(1) +
  CASE ISNULL(@BackupDeviceFolder,'Actual')
   WHEN 'Actual' THEN CTE.physical_device_name
   ELSE @BackupDeviceFolder + SUBSTRING(CTE.physical_device_name,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(CTE.physical_device_name),1) + 1)
  END + '''' +

  -- Striped backup files
  CASE ISNULL(Stripe2.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe2.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe2.physical_device_name,LEN(Stripe2.physical_device_name) - CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe2.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe3.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe3.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe3.physical_device_name,LEN(Stripe3.physical_device_name) - CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe3.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe4.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe4.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe4.physical_device_name,LEN(Stripe4.physical_device_name) - CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe4.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe5.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe5.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe5.physical_device_name,LEN(Stripe5.physical_device_name) - CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe5.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe6.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe6.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe6.physical_device_name,LEN(Stripe6.physical_device_name) - CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe6.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe7.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe7.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe7.physical_device_name,LEN(Stripe7.physical_device_name) - CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe7.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe8.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe8.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe8.physical_device_name,LEN(Stripe8.physical_device_name) - CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe8.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe9.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe9.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe9.physical_device_name,LEN(Stripe9.physical_device_name) - CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe9.physical_device_name),1) + 1) END + ''''
  END +

  CASE ISNULL(Stripe10.physical_device_name,'')
    WHEN '' THEN ''
    ELSE  ', DISK = ' + '''' + CASE ISNULL(@BackupDeviceFolder,'Actual') WHEN 'Actual' THEN Stripe10.physical_device_name ELSE @BackupDeviceFolder + SUBSTRING(Stripe10.physical_device_name,LEN(Stripe10.physical_device_name) - CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 2,CHARINDEX('\',REVERSE(Stripe10.physical_device_name),1) + 1) END + ''''
  END +

  CASE @StandbyMode WHEN 0 THEN ' WITH NORECOVERY,' ELSE ' WITH STANDBY =N' + '''' + ISNULL(@BackupDeviceFolder,SUBSTRING(CTE.physical_device_name,1,LEN(CTE.physical_device_name) - CHARINDEX('\',REVERSE(CTE.physical_device_name)))) + '\' + d.name + '_ROLLBACK_UNDO.bak ' + ''''  + ',' END + SPACE(1) +

  CASE CTE.has_backup_checksums WHEN 1 THEN ' CHECKSUM,' ELSE ' ' END +
 
  + 'FILE = ' + CAST(CTE.Position AS VARCHAR(5)) +
  CASE CTE.backupfinishdate
    WHEN z.backupfinishdate THEN ' ,STOPAT = ' + '''' + CONVERT(VARCHAR(21),@StopAt,120) + ''''
    ELSE ' '
  END
  AS Command,
  32769 AS Sequence,
  d.name AS database_name,
  CTE.physical_device_name AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

-- Striped backup files (caters for up to 10)
LEFT OUTER JOIN CTE AS Stripe2
  ON Stripe2.database_name = d.name
  AND Stripe2.backupmediasetid = CTE.backupmediasetid
  AND Stripe2.family_sequence_number = 2

LEFT OUTER JOIN CTE AS Stripe3
  ON Stripe3.database_name = d.name
  AND Stripe3.backupmediasetid = CTE.backupmediasetid
  AND Stripe3.family_sequence_number = 3

LEFT OUTER JOIN CTE AS Stripe4
  ON Stripe4.database_name = d.name
  AND Stripe4.backupmediasetid = CTE.backupmediasetid
  AND Stripe4.family_sequence_number = 4

LEFT OUTER JOIN CTE AS Stripe5
  ON Stripe5.database_name = d.name
  AND Stripe5.backupmediasetid = CTE.backupmediasetid
  AND Stripe5.family_sequence_number = 5

LEFT OUTER JOIN CTE AS Stripe6
  ON Stripe6.database_name = d.name
  AND Stripe6.backupmediasetid = CTE.backupmediasetid
  AND Stripe6.family_sequence_number = 6

LEFT OUTER JOIN CTE AS Stripe7
  ON Stripe7.database_name = d.name
  AND Stripe7.backupmediasetid = CTE.backupmediasetid
  AND Stripe7.family_sequence_number = 7

LEFT OUTER JOIN CTE AS Stripe8
  ON Stripe8.database_name = d.name
  AND Stripe8.backupmediasetid = CTE.backupmediasetid
  AND Stripe8.family_sequence_number = 8

LEFT OUTER JOIN CTE AS Stripe9
  ON Stripe9.database_name = d.name
  AND Stripe9.backupmediasetid = CTE.backupmediasetid
  AND Stripe9.family_sequence_number = 9

LEFT OUTER JOIN CTE AS Stripe10
  ON Stripe10.database_name = d.name
  AND Stripe10.backupmediasetid = CTE.backupmediasetid
  AND Stripe10.family_sequence_number = 10

LEFT OUTER JOIN  -- Next full backup after STOPAT
(
  SELECT
   database_name, MIN(BackupFinishDate) AS backup_finish_date
  FROM CTE
  WHERE type = 'D'
  AND backupfinishdate > @StopAt
  GROUP BY database_name

) x
  ON x.database_name = CTE.database_name

LEFT OUTER JOIN -- Highest differential backup date
(
  SELECT database_name, max(backupfinishdate) AS backupfinishdate
  FROM CTE
  WHERE CTE.type = 'I'
  AND CTE.backupfinishdate < @StandbyMode
  GROUP BY database_name
) y
  ON y.database_name = CTE.database_name

LEFT OUTER JOIN -- First log file after STOPAT
(
  SELECT database_name, min(backupfinishdate) AS backupfinishdate
  FROM CTE
  WHERE CTE.type = 'L'
  AND backupfinishdate > @StopAt
  GROUP BY database_name
) z
  ON z.database_name = CTE.database_name

JOIN
(
  SELECT
   database_name,
   MAX(Last_LSN) AS Last_LSN
  FROM CTE
  WHERE CTE.backupfinishdate < ISNULL(@StopAt,GETDATE())
  AND CTE.Type IN ('D','I')
  GROUP BY database_name
) x1
  ON CTE.database_name = x1.database_name

WHERE CTE.[type] = 'L'
AND CTE.backupfinishdate <= ISNULL(x.backup_finish_date,'31 Dec, 2199') -- Less than next full backup
AND CTE.backupfinishdate >= ISNULL(y.backupfinishdate, CTE.backupfinishdate) --Great than or equal to last differential backup
AND CTE.backupfinishdate <= ISNULL(z.backupfinishdate, CTE.backupfinishdate) -- Less than or equal to last file file in recovery chain (IE Log Backup datetime might be after STOPAT)
AND CTE.Last_LSN > x1.Last_LSN -- Differential or Full Last LSN < Log Last LSN
AND CTE.family_sequence_number = 1

--------------------------------------------------------------------
UNION -- Restore WITH RECOVERY
--------------------------------------------------------------------
SELECT
  ';RESTORE DATABASE [' + d.[name] + ']' + SPACE(1) + 'WITH RECOVERY' AS Command,
  32771 AS Sequence,
  d.name AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

WHERE CTE.[type] = 'D'
AND @StandbyMode = 0

--------------------------------------------------------------------
UNION -- CHECKDB
--------------------------------------------------------------------
SELECT
  ';DBCC CHECKDB(' + '''' + d.[name] + '''' + ') WITH NO_INFOMSGS IF @@ERROR > 0 PRINT N''CONSISTENCY PROBLEMS IN DATABASE : ' + d.name + ''' ELSE PRINT N''CONSISTENCY GOOD IN DATABASE : ' + d.name + '''' AS Command,
  32772 AS Sequence,
  d.name AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.databases d

JOIN CTE
  ON CTE.database_name = d.name

WHERE CTE.[type] = 'D'
AND @StandbyMode = 0

---------------------------------------------------------------------------------------------------------------------------------------------------
UNION -- MOVE full backup secondary data files, allows for up to 32769/2 file groups
---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT
  ', MOVE ' + '''' + b.name + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN b.physical_name
    ELSE @ToFileFolder + SUBSTRING(b.Physical_Name,LEN(b.Physical_Name) - CHARINDEX('\',REVERSE(b.Physical_Name),1) + 2,CHARINDEX('\',REVERSE(b.Physical_Name),1) + 1)
  END + '''',
  b.file_id AS Sequence,
  DB_NAME(b.database_id) AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.master_files b
INNER JOIN CTE
  ON CTE.database_name = DB_NAME(b.database_id)

WHERE CTE.[type] = 'D'
AND b.type_desc = 'ROWS'
AND b.file_id > 2

---------------------------------------------------------------------------------------------------------------------------------------------------
UNION -- MOVE differential backup secondary data files, allows for up to 32769/2 file groups
---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT
  ', MOVE ' + '''' + b.name + '''' + ' TO ' +
  '''' +
  CASE ISNULL(@ToFileFolder,'Actual')
    WHEN 'Actual' THEN b.physical_name
    ELSE @ToFileFolder + SUBSTRING(b.Physical_Name,LEN(b.Physical_Name) - CHARINDEX('\',REVERSE(b.Physical_Name),1) + 2,CHARINDEX('\',REVERSE(b.Physical_Name),1) + 1)
  END + '''',
  ((b.file_id) + (32769/2)) AS Sequence,
  DB_NAME(b.database_id) AS database_name,
  '' AS BackupDevice,
  CTE.backupfinishdate,
  CTE.backup_size

FROM sys.master_files b
INNER JOIN CTE
  ON CTE.database_name = DB_NAME(b.database_id)

WHERE CTE.[type] = 'I'
AND b.type_desc = 'ROWS'
AND b.file_id > 2
AND CTE.backupfinishdate < @StopAt
) a

WHERE a.database_name = ISNULL(@database,a.database_name)
AND (@IncludeSystemBackups = 1 OR a.database_name NOT IN('master','model','msdb'))

ORDER BY
  database_name,
  sequence,
  backupfinishdate

END

GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateRestoreScripts]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GenerateRestoreScripts] @DBname VARCHAR(100)
AS

SET NOCOUNT ON-- required because we're going to print T-SQL for the restores in the messages 'tab' of SSMS

/*  
Script creates the T-SQL to restore a database with info from MSDB
It helps by creating RESTORE command constructed from the last FULL backup, the last DIFFERENTIAL backup 
and all the required TRANSACTION LOG backups after this.
Neat when you have a high frequency of differential or log backups

The variable @DBName should be set to the name of the database you want to restore.

!!! BE AWARE: include MSDB in your backup plan for this T-SQL script to work in all circumstances !!!
I usually include MSDB in the log backup schedule (set the db to full recovery mode)

*/
DECLARE @lastFullBackup INT, @lastFullBackupPath VARCHAR(2000), @lastDifferentialBackup INT, @lastDifferentialBackupPath VARCHAR(2000)
DECLARE @i INT, @logBackupPath VARCHAR(1000)

-- remove temp object that might exist
IF OBJECT_ID('tempdb..#MSDBBackupHistory') IS NOT NULL
    DROP TABLE #MSDBBackupHistory

CREATE TABLE #MSDBBackupHistory (
    id INT IDENTITY(1,1),
    backup_start_date DATETIME,
    backup_type CHAR(1),
    physical_device_name VARCHAR(1000))

INSERT INTO #MSDBBackupHistory (backup_start_date,  backup_type, physical_device_name)
    SELECT BS.backup_start_date, BS.type, RTRIM(BMF.physical_device_name)
    FROM msdb..backupset BS JOIN msdb..backupmediafamily BMF ON BMF.media_set_id=BS.media_set_id
    WHERE BS.database_name = @DBName
    ORDER BY BS.backup_start_date -- dump the last backup first in table

-- get the last Full backup info.
SET @lastFullBackup = (SELECT MAX(id) FROM #MSDBBackupHistory WHERE backup_type='D')
SET @lastFullBackupPath = (SELECT physical_device_name FROM #MSDBBackupHistory WHERE id=@lastFullBackup)

-- Restore the Full backup
PRINT 'RESTORE DATABASE ' + @DBName
PRINT 'FROM DISK=''' + @lastFullBackupPath + ''''

-- IF it's there's no backup (differential or log) after it, we set to 'with recovery'
IF (@lastFullBackup = (SELECT MAX(id) FROM #MSDBBackupHistory))
    PRINT 'WITH RECOVERY'
ELSE PRINT 'WITH NORECOVERY'

PRINT 'GO'
PRINT ''

-- get the last Differential backup (it must be done after the last Full backup)
SET @lastDifferentialBackup = (SELECT MAX(id) FROM #MSDBBackupHistory WHERE backup_type='I' AND id > @lastFullBackup)
SET @lastDifferentialBackupPath = (SELECT physical_device_name FROM #MSDBBackupHistory WHERE id=@lastDifferentialBackup)

-- when there's a differential backup after the last full backup create the restore T-SQL commands
IF (@lastDifferentialBackup IS NOT NULL)
BEGIN
    -- Restore last diff. backup
    PRINT 'RESTORE DATABASE ' + @DBName
    PRINT 'FROM DISK=''' + @lastDifferentialBackupPath + ''''

    -- If no backup made (differential or log) after it, set to 'with recovery'
    IF (@lastDifferentialBackup = (SELECT MAX(id) FROM #MSDBBackupHistory))
        PRINT 'WITH RECOVERY'
    ELSE PRINT 'WITH NORECOVERY'

    PRINT 'GO'
    PRINT '' -- new line for readability
END

-- construct the required TRANSACTION LOGs restores
IF (@lastDifferentialBackup IS NULL) -- no diff backup made?
    SET @i = @lastFullBackup + 1    -- search for log dumps after the last full
ELSE SET @i = @lastDifferentialBackup + 1 -- search for log dumps after the last diff

-- script T-SQL restore commands from the log backup history
WHILE (@i <= (SELECT MAX(id) FROM #MSDBBackupHistory))
BEGIN 
    SET @logBackupPath = (SELECT physical_device_name FROM #MSDBBackupHistory WHERE id=@i)
    PRINT 'RESTORE LOG ' + @DBName
    PRINT 'FROM DISK=''' + @logBackupPath + ''''

    -- it's the last transaction log, set to 'with recovery'
    IF (@i = (SELECT MAX(id) FROM #MSDBBackupHistory))
        PRINT 'WITH RECOVERY'
    ELSE PRINT 'WITH NORECOVERY'    

    PRINT 'GO'
    PRINT '' -- new line for readability

    SET @i = @i + 1 -- try to find the next log entry
END

-- remove temp objects that exist
IF OBJECT_ID('tempdb..#MSDBBackupHistory') IS NOT NULL 
    DROP TABLE #MSDBBackupHistory


GO
/****** Object:  StoredProcedure [dbo].[usp_SQL_Server_System_Report]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-----------------------------------------------------------------------------------------------------------------------------
--	Stored Procedure Details: Listing Of Standard Details Related To The Stored Procedure
-----------------------------------------------------------------------------------------------------------------------------

--	Purpose: SQL Server System Report
--	Create Date (MM/DD/YYYY): 05/12/2010
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


-----------------------------------------------------------------------------------------------------------------------------
--	Modification History: Listing Of All Modifications Since Original Implementation
-----------------------------------------------------------------------------------------------------------------------------

--	Description: Added "@Output_Mode" And "@Unused_Index_Days" Input Parameters, Code Restructuring For Better Query Performance, Minor Changes To Code Style
--	Date (MM/DD/YYYY): 12/22/2011
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "connections", "first_day_of_week", "is_clustered", And "windows_version" To "Server Instance Property Information" Section
--	Date (MM/DD/YYYY): 12/23/2011
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "create_date" And "file_name" To "Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats" Section
--	Date (MM/DD/YYYY): 01/03/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "backup_finish_date", "database_version", "machine_name", And "server_name" To "Last Backup Set Details" Section
--	Date (MM/DD/YYYY): 01/04/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: "Last Backup Set Details" Section Now Only Shows Information For Databases Currently On The Instance
--	Date (MM/DD/YYYY): 01/17/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "schema_name" To "Unused Indexes" Section, Extended "CONVERT" Character Length To Deal With "Msg 234" Error, Rewrote Time Calculation Logic
--	Date (MM/DD/YYYY): 01/20/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added Additional Input Parameters, Reformatted Code, Bug Fixes, Added New Sections (Server Settings, Index Fragmentation, Missing Indexes)
--	Date (MM/DD/YYYY): 09/20/2013
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query: Create Procedure
-----------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[usp_SQL_Server_System_Report]

	 @Output_Mode AS CHAR (1) = NULL
	,@Unused_Index_Days AS INT = 7
	,@Recipients AS VARCHAR (MAX) = NULL
	,@Copy_Recipients AS VARCHAR (MAX) = NULL
	,@Server_Instance AS BIT = 1
	,@Server_Settings AS BIT = 1
	,@Drives_Space AS BIT = 1
	,@Database_Summary AS BIT = 1
	,@Database_Details AS BIT = 1
	,@Last_Backup AS BIT = 1
	,@Agent_Jobs AS BIT = 1
	,@Fragmentation AS BIT = 1
	,@Missing_Indexes AS BIT = 1
	,@Unused_Indexes AS BIT = 1

AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON
SET TEXTSIZE 2147483647


DECLARE @Body AS NVARCHAR (MAX)
DECLARE @Database_ID AS INT
DECLARE @Database_Name_Loop AS NVARCHAR (500)
DECLARE @Date_24_Hours_Ago AS DATETIME
DECLARE @Date_Now AS DATETIME
DECLARE @SQL_Server_Start_Time AS DATETIME
DECLARE @SQL_String AS NVARCHAR (MAX)
DECLARE @Subject AS NVARCHAR (255)
DECLARE @XML_String AS NVARCHAR (MAX)


DECLARE @Database_Names AS TABLE

	(
		database_name SYSNAME PRIMARY KEY CLUSTERED
	)


DECLARE @Fixed_Drives_Free_Space AS TABLE

	(
		 drive_letter VARCHAR (5) PRIMARY KEY CLUSTERED
		,free_space_mb BIGINT
	)


SET @Body = N''
SET @Date_24_Hours_Ago = GETDATE () - 1
SET @Date_Now = @Date_24_Hours_Ago + 1
SET @Subject = N'SQL Server System Report: ' + @@SERVERNAME
SET @XML_String = N''


IF (@Server_Instance = 1 OR @Unused_Indexes = 1)
BEGIN

	SET @SQL_Server_Start_Time =

		(
			SELECT
				DB.create_date
			FROM
				master.sys.databases DB
			WHERE
				DB.name = N'tempdb'
		)

END


INSERT INTO @Database_Names

	(
		database_name
	)

SELECT
	DB.name AS database_name
FROM
	master.sys.databases DB
WHERE
	DB.[state] = 0
	AND DB.is_read_only = 0
	AND DB.is_in_standby = 0
	AND DB.source_database_id IS NULL


-----------------------------------------------------------------------------------------------------------------------------
--	Error Trapping: Check If Temp Table(s) Already Exist(s) And Drop If Applicable
-----------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_instance_property', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_instance_property

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_server_settings', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_server_settings

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_database_size_distribution_stats', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_database_size_distribution_stats

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_model_compatibility_size_growth', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_model_compatibility_size_growth

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_last_backup_set', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_last_backup_set

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_agent_jobs', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_agent_jobs

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_index_fragmentation', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_index_fragmentation

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_missing_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_missing_indexes

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_unused_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_unused_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query I: Server Instance Property Information
-----------------------------------------------------------------------------------------------------------------------------

IF @Server_Instance = 0
BEGIN

	GOTO Skip_Instance_Property

END


SELECT
	 SERVERPROPERTY (N'ComputerNamePhysicalNetBIOS') AS netbios_name
	,@@SERVERNAME AS server_name
	,REPLACE (CONVERT (NVARCHAR (128), SERVERPROPERTY (N'Edition')), ' Edition', '') AS edition
	,SERVERPROPERTY (N'ProductVersion') AS [version]
	,SERVERPROPERTY (N'ProductLevel') AS [level]
	,(CASE SERVERPROPERTY (N'IsClustered')
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_clustered
	,CONVERT (NVARCHAR (19), @SQL_Server_Start_Time, 120) AS online_since
	,(CASE
		WHEN oaDSR.total_days = 0 THEN '_'
		ELSE oaDSR.total_days
		END) + ' Day(s) ' + (CASE
								WHEN oaDSR.seconds_remaining = 0 THEN '__:__:__'
								WHEN oaDSR.seconds_remaining < 60 THEN '__:__:' + RIGHT (oaDSR.total_seconds, 2)
								WHEN oaDSR.seconds_remaining < 3600 THEN '__:' + RIGHT (oaDSR.total_seconds, 5)
								ELSE oaDSR.total_seconds
								END) AS uptime
	,SERVERPROPERTY (N'ProcessID') AS process_id
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@CONNECTIONS), 1)), 4, 23)) AS connections
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@TOTAL_READ), 1)), 4, 23)) AS reads
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@TOTAL_WRITE), 1)), 4, 23)) AS writes
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DOSI.cpu_count), 1)), 4, 23)) AS logical_cpu_count
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DOSI.cpu_count / DOSI.hyperthread_ratio), 1)), 4, 23)) AS physical_cpu_count
	,REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (RIGHT (@@VERSION, CHARINDEX (REVERSE (' on Windows '), REVERSE (@@VERSION)) - 1), 'Service Pack ', 'SP'), '(', ''), ')', ''), '<', '('), '>', ')') AS windows_version
INTO
	dbo.#temp_sssr_instance_property
FROM
	master.sys.dm_os_sys_info DOSI
	CROSS JOIN

		(
			SELECT
				DATEDIFF (SECOND, @SQL_Server_Start_Time, GETDATE ()) AS uptime_seconds
		) sqUTS

	OUTER APPLY

		(
			SELECT
				 CONVERT (VARCHAR (5), sqUTS.uptime_seconds / 86400) AS total_days
				,CONVERT (CHAR (8), DATEADD (SECOND, sqUTS.uptime_seconds % 86400, 0), 108) AS total_seconds
				,sqUTS.uptime_seconds % 86400 AS seconds_remaining
		) oaDSR


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Instance_Property

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.netbios_name AS 'td'
					,'',X.server_name AS 'td'
					,'',X.edition AS 'td'
					,'',X.[version] AS 'td'
					,'',X.[level] AS 'td'
					,'',X.is_clustered AS 'td'
					,'',X.online_since AS 'td'
					,'',X.uptime AS 'td'
					,'',X.process_id AS 'td'
					,'','right_align' + X.connections AS 'td'
					,'','right_align' + X.reads AS 'td'
					,'','right_align' + X.writes AS 'td'
					,'','right_align' + X.logical_cpu_count AS 'td'
					,'','right_align' + X.physical_cpu_count AS 'td'
					,'',X.windows_version AS 'td'
				FROM
					dbo.#temp_sssr_instance_property X
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<h3><center>Server Instance Property Information</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>NetBIOS Name</th>
						<th>Server Name</th>
						<th>Edition</th>
						<th>Version</th>
						<th>Level</th>
						<th>Clustered</th>
						<th>Online Since</th>
						<th>Uptime</th>
						<th>Process ID</th>
						<th>Connections</th>
						<th>Reads</th>
						<th>Writes</th>
						<th>Logical CPU Count</th>
						<th>Physical CPU Count</th>
						<th>Windows Version</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.netbios_name
		,X.server_name
		,X.edition
		,X.[version]
		,X.[level]
		,X.is_clustered AS [clustered]
		,X.online_since
		,X.uptime
		,X.process_id
		,X.connections
		,X.reads
		,X.writes
		,X.logical_cpu_count
		,X.physical_cpu_count
		,X.windows_version
	FROM
		dbo.#temp_sssr_instance_property X

END


Skip_Instance_Property:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_instance_property', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_instance_property

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query II: Server Settings
-----------------------------------------------------------------------------------------------------------------------------

IF @Server_Settings = 0
BEGIN

	GOTO Skip_Server_Settings

END


SELECT
	 (CASE @@DATEFIRST
		WHEN 1 THEN 'Monday'
		WHEN 2 THEN 'Tuesday'
		WHEN 3 THEN 'Wednesday'
		WHEN 4 THEN 'Thursday'
		WHEN 5 THEN 'Friday'
		WHEN 6 THEN 'Saturday'
		WHEN 7 THEN 'Sunday'
		ELSE 'N/A'
		END) AS first_day_of_week
	,SERVERPROPERTY (N'Collation') AS collation
	,(CASE
		WHEN 'a' = 'A' THEN 'No'
		ELSE 'Yes'
		END) AS is_case_sensitive
	,(CASE SERVERPROPERTY (N'IsFullTextInstalled')
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_full_text_installed
	,(CASE sqCFG.is_show_advanced_options_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_show_advanced_options_enabled
	,(CASE sqCFG.is_clr_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_clr_enabled
	,(CASE sqCFG.is_xp_cmdshell_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_xp_cmdshell_enabled
	,(CASE sqCFG.is_database_mail_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_database_mail_enabled
	,(CASE sqCFG.is_default_trace_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_default_trace_enabled
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCFG.min_server_memory_mb), 1)), 4, 23)) AS min_server_memory_mb
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCFG.max_server_memory_mb), 1)), 4, 23)) AS max_server_memory_mb
INTO
	dbo.#temp_sssr_server_settings
FROM

	(
		SELECT
			 MAX (CASE
					WHEN CFG.configuration_id = 518 THEN CFG.value
					END) AS is_show_advanced_options_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1562 THEN CFG.value
					END) AS is_clr_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 16390 THEN CFG.value
					END) AS is_xp_cmdshell_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 16386 THEN CFG.value
					END) AS is_database_mail_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1568 THEN CFG.value
					END) AS is_default_trace_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1543 THEN CFG.value
					END) AS min_server_memory_mb
			,MAX (CASE
					WHEN CFG.configuration_id = 1544 THEN CFG.value
					END) AS max_server_memory_mb
		FROM
			sys.configurations CFG
		WHERE
			CFG.configuration_id IN (518, 1543, 1544, 1562, 1568, 16386, 16390)
	) sqCFG


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Server_Settings

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.first_day_of_week AS 'td'
					,'',X.collation AS 'td'
					,'',X.is_case_sensitive AS 'td'
					,'',X.is_full_text_installed AS 'td'
					,'',X.is_show_advanced_options_enabled AS 'td'
					,'',X.is_clr_enabled AS 'td'
					,'',X.is_xp_cmdshell_enabled AS 'td'
					,'',X.is_database_mail_enabled AS 'td'
					,'',X.is_default_trace_enabled AS 'td'
					,'','right_align' + X.min_server_memory_mb AS 'td'
					,'','right_align' + X.max_server_memory_mb AS 'td'
				FROM
					dbo.#temp_sssr_server_settings X
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<h3><center>Server Settings</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>First Day Of Week</th>
						<th>Collation</th>
						<th>Case Sensitive</th>
						<th>Full-Text Installed</th>
						<th>Advanced Options Enabled</th>
						<th>CLR Enabled</th>
						<th>Command Shell Enabled</th>
						<th>Database Mail Enabled</th>
						<th>Default Trace Enabled</th>
						<th>Minimum Memory (MB)</th>
						<th>Maximum Memory (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.first_day_of_week
		,X.collation
		,X.is_case_sensitive AS case_sensitive
		,X.is_full_text_installed AS full_text_installed
		,X.is_show_advanced_options_enabled AS advanced_options_enabled
		,X.is_clr_enabled AS clr_enabled
		,X.is_xp_cmdshell_enabled AS command_shell_enabled
		,X.is_database_mail_enabled AS database_mail_enabled
		,X.is_default_trace_enabled AS default_trace_enabled
		,X.min_server_memory_mb AS minimum_memory_mb
		,X.max_server_memory_mb AS maximum_memory_mb
	FROM
		dbo.#temp_sssr_server_settings X

END


Skip_Server_Settings:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_server_settings', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_server_settings

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query III: Fixed Drives Free Space
-----------------------------------------------------------------------------------------------------------------------------

IF @Drives_Space = 0
BEGIN

	GOTO Skip_Fixed_Drives_Free_Space

END


INSERT INTO @Fixed_Drives_Free_Space

	(
		 drive_letter
		,free_space_mb
	)

EXECUTE master.dbo.xp_fixeddrives


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Fixed_Drives_Free_Space

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.drive_letter + ':' AS 'td'
					,'','right_align' + REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, X.free_space_mb), 1)), 4, 23)) AS 'td'
				FROM
					@Fixed_Drives_Free_Space X
				ORDER BY
					X.drive_letter
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Fixed Drives Free Space</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Drive Letter</th>
						<th>Free Space (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.drive_letter + ':' AS drive_letter
		,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, X.free_space_mb), 1)), 4, 23)) AS free_space_mb
	FROM
		@Fixed_Drives_Free_Space X
	ORDER BY
		X.drive_letter

END


Skip_Fixed_Drives_Free_Space:


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query IV: Database Size (Summary) / Distribution Stats
-----------------------------------------------------------------------------------------------------------------------------

IF @Database_Summary = 0
BEGIN

	GOTO Skip_Database_Size_Distribution_Stats

END


CREATE TABLE dbo.#temp_sssr_database_size_distribution_stats

	(
		 database_name NVARCHAR (500)
		,total_size_mb VARCHAR (15)
		,unallocated_mb VARCHAR (15)
		,reserved_mb VARCHAR (15)
		,data_mb VARCHAR (15)
		,index_mb VARCHAR (15)
		,unused_mb VARCHAR (15)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_database_size_distribution_stats

				(
					 database_name
					,total_size_mb
					,unallocated_mb
					,reserved_mb
					,data_mb
					,index_mb
					,unused_mb
				)

			SELECT
				 DB_NAME () AS database_name
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((sqDBF.total_size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS total_size_mb
				,(CASE
					WHEN sqDBF.database_size >= cjPGS.total_pages THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((sqDBF.database_size - cjPGS.total_pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23))
					ELSE ''0''
					END) AS unallocated_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((cjPGS.total_pages * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS reserved_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((cjPGS.pages * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS data_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((cjPGS.used_pages - cjPGS.pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS index_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((cjPGS.total_pages - cjPGS.used_pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS unused_mb
			FROM

				(
					SELECT
						 SUM (CASE
								WHEN DBF.[type] = 0 THEN DBF.size
								ELSE 0
								END) AS database_size
						,SUM (DBF.size) AS total_size
					FROM
						sys.database_files AS DBF
					WHERE
						DBF.[type] IN (0, 1)
				) sqDBF

				CROSS JOIN

					(
						SELECT
							 SUM (AU.total_pages) AS total_pages
							,SUM (AU.used_pages) AS used_pages
							,SUM (CASE
									WHEN IT.internal_type IN (202, 204) THEN 0
									WHEN AU.[type] <> 1 THEN AU.used_pages
									WHEN P.index_id <= 1 THEN AU.data_pages
									ELSE 0
									END) AS pages
						FROM
							sys.partitions P
							INNER JOIN sys.allocation_units AU ON AU.container_id = P.partition_id
							LEFT JOIN sys.internal_tables IT ON IT.[object_id] = P.[object_id]
					) cjPGS
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_database_size_distribution_stats X)
BEGIN

	GOTO Skip_Database_Size_Distribution_Stats

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'','right_align' + X.total_size_mb AS 'td'
					,'','right_align' + X.unallocated_mb AS 'td'
					,'','right_align' + X.reserved_mb AS 'td'
					,'','right_align' + X.data_mb AS 'td'
					,'','right_align' + X.index_mb AS 'td'
					,'','right_align' + X.unused_mb AS 'td'
				FROM
					dbo.#temp_sssr_database_size_distribution_stats X
				ORDER BY
					X.database_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Database Size (Summary) / Distribution Stats</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Total Size (MB)</th>
						<th>Unallocated (MB)</th>
						<th>Reserved (MB)</th>
						<th>Data (MB)</th>
						<th>Index (MB)</th>
						<th>Unused (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.total_size_mb
		,X.unallocated_mb
		,X.reserved_mb
		,X.data_mb
		,X.index_mb
		,X.unused_mb
	FROM
		dbo.#temp_sssr_database_size_distribution_stats X
	ORDER BY
		X.database_name

END


Skip_Database_Size_Distribution_Stats:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_database_size_distribution_stats', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_database_size_distribution_stats

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query V: Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats
-----------------------------------------------------------------------------------------------------------------------------

IF @Database_Details = 0
BEGIN

	GOTO Skip_Model_Compatibility_Size_Growth

END


SELECT
	 DB_NAME (MF.database_id) AS database_name
	,DB.recovery_model_desc
	,DB.[compatibility_level]
	,CONVERT (NVARCHAR (10), LEFT (UPPER (MF.type_desc), 1) + LOWER (SUBSTRING (MF.type_desc, 2, 250))) AS file_type
	,MF.name AS [file_name]
	,CONVERT (NVARCHAR (19), DB.create_date, 120) AS create_date
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS file_size_mb
	,RIGHT ((CASE
				WHEN MF.growth = 0 THEN 'Fixed Size'
				WHEN MF.max_size = -1 THEN 'Unrestricted'
				WHEN MF.max_size = 0 THEN 'None'
				WHEN MF.max_size = 268435456 THEN '2 TB'
				ELSE REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.max_size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) + ' MB'
				END), 15) AS max_size
	,RIGHT ((CASE
				WHEN MF.growth = 0 THEN 'N/A'
				WHEN MF.is_percent_growth = 1 THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, MF.growth), 1)), 4, 23)) + ' %'
				ELSE REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.growth * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) + ' MB'
				END), 15) AS growth_increment
	,ROW_NUMBER () OVER
						(
							PARTITION BY
								MF.database_id
							ORDER BY
								 MF.[type]
								,(CASE
									WHEN MF.[file_id] = 1 THEN 10
									ELSE 99
									END)
								,MF.name
						) AS database_filter_id
INTO
	dbo.#temp_sssr_model_compatibility_size_growth
FROM
	master.sys.master_files MF
	INNER JOIN master.sys.databases DB ON DB.database_id = MF.database_id


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Model_Compatibility_Size_Growth

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',(CASE
							WHEN X.database_filter_id = 1 THEN X.database_name
							ELSE ''
							END) AS 'td'
					,'',(CASE
							WHEN X.database_filter_id = 1 THEN X.recovery_model_desc
							ELSE ''
							END) AS 'td'
					,'',(CASE
							WHEN X.database_filter_id = 1 THEN ISNULL (CONVERT (VARCHAR (5), X.[compatibility_level]), 'N/A')
							ELSE ''
							END) AS 'td'
					,'',X.file_type AS 'td'
					,'',X.[file_name] AS 'td'
					,'',X.create_date AS 'td'
					,'','right_align' + X.file_size_mb AS 'td'
					,'','right_align' + X.max_size AS 'td'
					,'','right_align' + X.growth_increment AS 'td'
				FROM
					dbo.#temp_sssr_model_compatibility_size_growth X
				ORDER BY
					 X.database_name
					,X.database_filter_id
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Recovery Model</th>
						<th>Compatibility</th>
						<th>File Type</th>
						<th>File Name</th>
						<th>Create Date</th>
						<th>File Size (MB)</th>
						<th>Max Size</th>
						<th>Growth Increment</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 (CASE
			WHEN X.database_filter_id = 1 THEN X.database_name
			ELSE ''
			END) AS database_name
		,(CASE
			WHEN X.database_filter_id = 1 THEN X.recovery_model_desc
			ELSE ''
			END) AS recovery_model
		,(CASE
			WHEN X.database_filter_id = 1 THEN ISNULL (CONVERT (VARCHAR (5), X.[compatibility_level]), 'N/A')
			ELSE ''
			END) AS compatibility
		,X.file_type
		,X.[file_name]
		,X.create_date
		,X.file_size_mb
		,X.max_size
		,X.growth_increment
	FROM
		dbo.#temp_sssr_model_compatibility_size_growth X
	ORDER BY
		 X.database_name
		,X.database_filter_id

END


Skip_Model_Compatibility_Size_Growth:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_model_compatibility_size_growth', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_model_compatibility_size_growth

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VI: Last Backup Set Details
-----------------------------------------------------------------------------------------------------------------------------

IF @Last_Backup = 0
BEGIN

	GOTO Skip_Last_Backup_Set

END


SELECT
	 DB.name AS database_name
	,ISNULL (CONVERT (VARCHAR (10), sqBS.backup_set_id), 'NONE') AS backup_set_id
	,(CASE sqBS.[type]
		WHEN 'D' THEN 'Database'
		WHEN 'F' THEN 'File Or Filegroup'
		WHEN 'G' THEN 'Differential File'
		WHEN 'I' THEN 'Differential Database'
		WHEN 'L' THEN 'Log'
		WHEN 'P' THEN 'Partial'
		WHEN 'Q' THEN 'Differential Partial'
		ELSE 'N/A'
		END) AS backup_type
	,ISNULL (CONVERT (VARCHAR (10), sqBS.database_version), 'N/A') AS database_version
	,ISNULL (sqBS.server_name, 'N/A') AS server_name
	,ISNULL (sqBS.machine_name, 'N/A') AS machine_name
	,ISNULL (CONVERT (VARCHAR (34), sqBS.backup_start_date, 120), 'N/A') AS backup_start_date
	,ISNULL (CONVERT (VARCHAR (34), sqBS.backup_finish_date, 120), 'N/A') AS backup_finish_date
	,ISNULL ((CASE
				WHEN sqBS.total_days = 0 THEN REPLICATE ('_', sqBS.day_length_max)
				ELSE REPLICATE ('0', sqBS.day_length_max - LEN (sqBS.total_days)) + sqBS.total_days
				END) + ' Day(s) ' + (CASE
										WHEN sqBS.seconds_remaining = 0 THEN '__:__:__'
										WHEN sqBS.seconds_remaining < 60 THEN '__:__:'+RIGHT (sqBS.total_seconds, 2)
										WHEN sqBS.seconds_remaining < 3600 THEN '__:'+RIGHT (sqBS.total_seconds, 5)
										ELSE sqBS.total_seconds
										END), 'N/A') AS duration
	,ISNULL (REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (sqBS.backup_size / 1048576.0, 0)), 1)), 4, 23)), 'N/A') AS backup_size_mb
	,ISNULL (REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DATEDIFF (DAY, sqBS.backup_start_date, GETDATE ())), 1)), 4, 23)), 'N/A') AS days_ago
	,ROW_NUMBER () OVER
						(
							PARTITION BY
								DB.name
							ORDER BY
								sqBS.[type]
						) AS database_filter_id
INTO
	dbo.#temp_sssr_last_backup_set
FROM
	master.sys.databases DB
	LEFT JOIN

		(
			SELECT
				 BS.database_name
				,BS.backup_set_id
				,BS.[type]
				,BS.database_version
				,BS.server_name
				,BS.machine_name
				,BS.backup_start_date
				,BS.backup_finish_date
				,BS.backup_size
				,cjDLM.day_length_max
				,oaDSR.seconds_remaining
				,oaDSR.total_days
				,oaDSR.total_seconds
			FROM
				msdb.dbo.backupset BS
				INNER JOIN

					(
						SELECT
							MAX (XBS.backup_set_id) AS backup_set_id_max
						FROM
							msdb.dbo.backupset XBS
						GROUP BY
							 XBS.database_name
							,XBS.[type]
					) sqMBS ON sqMBS.backup_set_id_max = BS.backup_set_id

				CROSS JOIN

					(
						SELECT
							MAX (LEN (DATEDIFF (DAY, YBS.backup_start_date, YBS.backup_finish_date))) AS day_length_max
						FROM
							msdb.dbo.backupset YBS
					) cjDLM

				OUTER APPLY

					(
						SELECT
							DATEDIFF (SECOND, BS.backup_start_date, BS.backup_finish_date) AS duration_seconds
					) oaDS

				OUTER APPLY

					(
						SELECT
							 CONVERT (VARCHAR (5), oaDS.duration_seconds / 86400) AS total_days
							,CONVERT (CHAR (8), DATEADD (SECOND, oaDS.duration_seconds % 86400, 0), 108) AS total_seconds
							,oaDS.duration_seconds % 86400 AS seconds_remaining
					) oaDSR

		) sqBS ON sqBS.database_name = DB.name

WHERE
	DB.name <> N'tempdb'


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Last_Backup_Set

END


IF EXISTS (SELECT * FROM dbo.#temp_sssr_last_backup_set X WHERE X.backup_set_id = 'NONE')
BEGIN

	UPDATE
		dbo.#temp_sssr_last_backup_set
	SET
		 backup_type = REPLICATE ('.', backup_type_length_max * 2)
		,database_version = REPLICATE ('.', database_version_length_max * 2)
		,server_name = REPLICATE ('.', server_name_length_max * 2)
		,machine_name = REPLICATE ('.', machine_name_length_max * 2)
		,backup_start_date = REPLICATE ('.', 34)
		,backup_finish_date = REPLICATE ('.', 34)
		,duration = REPLICATE ('.', (duration_length_max * 2) - 4)
		,backup_size_mb = REPLICATE ('.', backup_size_mb_length_max * 2)
	FROM

		(
			SELECT
				 MAX (LEN (X.backup_type)) AS backup_type_length_max
				,MAX (LEN (X.database_version)) AS database_version_length_max
				,MAX (LEN (X.server_name)) AS server_name_length_max
				,MAX (LEN (X.machine_name)) AS machine_name_length_max
				,MAX (LEN (X.duration)) AS duration_length_max
				,MAX (LEN (X.backup_size_mb)) AS backup_size_mb_length_max
			FROM
				dbo.#temp_sssr_last_backup_set X
		) sqX

	WHERE
		dbo.#temp_sssr_last_backup_set.backup_set_id = 'NONE'

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',(CASE
							WHEN X.database_filter_id = 1 THEN X.database_name
							ELSE ''
							END) AS 'td'
					,'',X.backup_set_id AS 'td'
					,'',X.backup_type AS 'td'
					,'',X.database_version AS 'td'
					,'',X.server_name AS 'td'
					,'',X.machine_name AS 'td'
					,'',X.backup_start_date AS 'td'
					,'',X.backup_finish_date AS 'td'
					,'',X.duration AS 'td'
					,'','right_align' + X.backup_size_mb AS 'td'
					,'','right_align' + X.days_ago AS 'td'
				FROM
					dbo.#temp_sssr_last_backup_set X
				ORDER BY
					 X.database_name
					,X.database_filter_id
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Last Backup Set Details</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Backup Set ID</th>
						<th>Backup Type</th>
						<th>Database Version</th>
						<th>Server Name</th>
						<th>Machine Name</th>
						<th>Backup Start Date</th>
						<th>Backup Finish Date</th>
						<th>Duration</th>
						<th>Backup Size (MB)</th>
						<th>Days Ago</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 (CASE
			WHEN X.database_filter_id = 1 THEN X.database_name
			ELSE ''
			END) AS database_name
		,X.backup_set_id
		,X.backup_type
		,X.database_version
		,X.server_name
		,X.machine_name
		,X.backup_start_date
		,X.backup_finish_date
		,X.duration
		,X.backup_size_mb
		,X.days_ago
	FROM
		dbo.#temp_sssr_last_backup_set X
	ORDER BY
		 X.database_name
		,X.database_filter_id

END


Skip_Last_Backup_Set:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_last_backup_set', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_last_backup_set

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VII: SQL Server Agent Jobs (Last 24 Hours)
-----------------------------------------------------------------------------------------------------------------------------

IF @Agent_Jobs = 0
BEGIN

	GOTO Skip_Agent_Jobs

END


SELECT
	 SJ.name AS job_name
	,CONVERT (VARCHAR (19), caLRDT.last_run_date_time, 120) AS last_run_date_time
	,(CASE SJH.run_status
		WHEN 0 THEN 'Failed'
		WHEN 1 THEN 'Succeeded'
		WHEN 2 THEN 'Retry'
		WHEN 3 THEN 'Canceled'
		WHEN 4 THEN 'In Progress'
		END) AS last_status
	,(CASE
		WHEN SJH.run_duration = 0 THEN '__:__:__'
		WHEN LEN (SJH.run_duration) <= 2 THEN '__:__:' + RIGHT ('0' + CONVERT (VARCHAR (2), SJH.run_duration), 2)
		WHEN LEN (SJH.run_duration) <= 4 THEN '__:' + STUFF (RIGHT ('0' + CONVERT (VARCHAR (4), SJH.run_duration), 4), 3, 0, ':')
		ELSE STUFF (STUFF (RIGHT ('0' + CONVERT (VARCHAR (6), SJH.run_duration), 6), 5, 0, ':'), 3, 0, ':')
		END) AS duration
	,ISNULL ((CASE
				WHEN SJ.[enabled] = 1 THEN CONVERT (VARCHAR (19), sqNRDT.next_run_date_time, 120)
				END), '___________________') AS next_run_date_time
	,ISNULL ((CASE
				WHEN SJ.[enabled] = 1 THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DATEDIFF (DAY, GETDATE (), sqNRDT.next_run_date_time)), 1)), 4, 23))
				END), 'N/A') AS days_away
INTO
	dbo.#temp_sssr_agent_jobs
FROM
	msdb.dbo.sysjobs SJ
	INNER JOIN msdb.dbo.sysjobhistory SJH ON SJH.job_id = SJ.job_id
	INNER JOIN

		(
			SELECT
				MAX (XSJ.instance_id) AS instance_id_max
			FROM
				msdb.dbo.sysjobhistory XSJ
			GROUP BY
				XSJ.job_id
		) sqIIM ON sqIIM.instance_id_max = SJH.instance_id

	LEFT JOIN

		(
			SELECT
				 SJS.job_id
				,MIN (CONVERT (DATETIME, CONVERT (VARCHAR (8), SJS.next_run_date) + ' ' + STUFF (STUFF (RIGHT ('000000' + CONVERT (VARCHAR (6), SJS.next_run_time), 6), 5, 0, ':'), 3, 0, ':'))) AS next_run_date_time
			FROM
				msdb.dbo.sysjobschedules SJS
				INNER JOIN msdb.dbo.sysschedules SS ON SS.schedule_id = SJS.schedule_id
					AND SS.[enabled] = 1
			WHERE
				SJS.next_run_date > 0
			GROUP BY
				SJS.job_id
		) sqNRDT ON sqNRDT.job_id = SJ.job_id

	CROSS APPLY

		(
			SELECT
				CONVERT (DATETIME, CONVERT (VARCHAR (8), SJH.run_date) + ' ' + STUFF (STUFF (RIGHT ('000000' + CONVERT (VARCHAR (6), SJH.run_time), 6), 5, 0, ':'), 3, 0, ':')) AS last_run_date_time
		) caLRDT

WHERE
	caLRDT.last_run_date_time >= @Date_24_Hours_Ago


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Agent_Jobs

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.job_name AS 'td'
					,'',X.last_run_date_time AS 'td'
					,'',X.last_status AS 'td'
					,'',X.duration AS 'td'
					,'',X.next_run_date_time AS 'td'
					,'','right_align' + X.days_away AS 'td'
				FROM
					dbo.#temp_sssr_agent_jobs X
				ORDER BY
					X.job_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>SQL Server Agent Jobs (Last 24 Hours)</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Job Name</th>
						<th>Last Run Date / Time</th>
						<th>Last Status</th>
						<th>Duration</th>
						<th>Next Run Date / Time</th>
						<th>Days Away</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.job_name
		,X.last_run_date_time
		,X.last_status
		,X.duration
		,X.next_run_date_time
		,X.days_away
	FROM
		dbo.#temp_sssr_agent_jobs X
	ORDER BY
		X.job_name

END


Skip_Agent_Jobs:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_agent_jobs', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_agent_jobs

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VIII: Index Fragmentation
-----------------------------------------------------------------------------------------------------------------------------

IF @Fragmentation = 0
BEGIN

	GOTO Skip_Index_Fragmentation

END


CREATE TABLE dbo.#temp_sssr_index_fragmentation

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,column_name SYSNAME
		,index_name SYSNAME
		,fragmentation VARCHAR (23)
		,index_type NVARCHAR (120)
		,is_pk VARCHAR (3)
		,is_unique VARCHAR (3)
		,recommendation VARCHAR (10)
		,alter_index_statement NVARCHAR (4000)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @Database_ID = DB_ID (@Database_Name_Loop)


	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_index_fragmentation

				(
					 database_name
					,[schema_name]
					,[object_name]
					,column_name
					,index_name
					,fragmentation
					,index_type
					,is_pk
					,is_unique
					,recommendation
					,alter_index_statement
				)


			SELECT
				 DB_NAME () AS database_name
				,SCHEMA_NAME (sqIF.[schema_id]) AS [schema_name]
				,sqIF.[object_name]
				,sqIF.column_name
				,sqIF.index_name
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqIF.avg_fragmentation_in_percent), 1)), 1, 23)) AS fragmentation
				,sqIF.type_desc AS index_type
				,(CASE sqIF.is_primary_key
					WHEN 0 THEN ''No''
					WHEN 1 THEN ''Yes''
					ELSE ''N/A''
					END) AS is_pk
				,(CASE sqIF.is_unique
					WHEN 0 THEN ''No''
					WHEN 1 THEN ''Yes''
					ELSE ''N/A''
					END) AS is_unique
				,caREC.recommendation
				,N''USE ['' + DB_NAME () + N'']; ALTER INDEX ['' + sqIF.index_name + ''] ON ['' + SCHEMA_NAME (sqIF.[schema_id]) + ''].['' + sqIF.[object_name] + ''] '' + (CASE caREC.recommendation
																																												WHEN ''REBUILD'' THEN caREC.recommendation + '' WITH (MAXDOP = 1)''
																																												ELSE caREC.recommendation
																																												END) + '';'' AS alter_index_statement
			FROM

				(
					SELECT
						 O.[schema_id]
						,O.name AS [object_name]
						,C.name AS column_name
						,I.name AS index_name
						,DDIPS.avg_fragmentation_in_percent
						,I.type_desc
						,I.is_primary_key
						,I.is_unique
						,ROW_NUMBER () OVER
										(
											PARTITION BY
												I.name
											ORDER BY
												DDIPS.avg_fragmentation_in_percent DESC
										) AS row_number_id
					FROM
						sys.dm_db_index_physical_stats (' + CONVERT (VARCHAR (11), @Database_ID) + ', NULL, NULL, NULL, N''LIMITED'') DDIPS
						INNER JOIN sys.objects O ON O.[object_id] = DDIPS.[object_id]
							AND O.is_ms_shipped = 0
						INNER JOIN sys.indexes I ON I.[object_id] = DDIPS.[object_id]
							AND I.index_id = DDIPS.index_id
							AND I.is_disabled <> 1
							AND I.is_hypothetical <> 1
						INNER JOIN sys.index_columns IC ON IC.[object_id] = DDIPS.[object_id]
							AND IC.index_id = DDIPS.index_id
						INNER JOIN sys.columns C ON C.[object_id] = DDIPS.[object_id]
							AND C.column_id = IC.column_id
					WHERE
						DDIPS.index_id <> 0
						AND DDIPS.avg_fragmentation_in_percent > 5
				) sqIF

				CROSS APPLY

					(
						SELECT
							(CASE
								WHEN sqIF.avg_fragmentation_in_percent <= 30.0 THEN ''REORGANIZE''
								ELSE ''REBUILD''
								END) AS recommendation
					) caREC

			WHERE
				sqIF.row_number_id = 1
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_index_fragmentation X)
BEGIN

	GOTO Skip_Index_Fragmentation

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'',X.column_name AS 'td'
					,'',X.index_name AS 'td'
					,'','right_align' + X.fragmentation AS 'td'
					,'',X.index_type AS 'td'
					,'',X.is_pk AS 'td'
					,'',X.is_unique AS 'td'
					,'',X.recommendation AS 'td'
					,'',X.alter_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_index_fragmentation X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.column_name
					,X.index_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Index Fragmentation</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Column Name</th>
						<th>Index Name</th>
						<th>Fragmentation</th>
						<th>Index Type</th>
						<th>PK</th>
						<th>Unique</th>
						<th>Recommendation</th>
						<th>Alter Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name
		,X.fragmentation
		,X.index_type
		,X.is_pk AS pk
		,X.is_unique AS [unique]
		,X.recommendation
		,X.alter_index_statement
	FROM
		dbo.#temp_sssr_index_fragmentation X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name

END


Skip_Index_Fragmentation:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_index_fragmentation', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_index_fragmentation

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query IX: Missing Indexes
-----------------------------------------------------------------------------------------------------------------------------

IF @Missing_Indexes = 0
BEGIN

	GOTO Skip_Missing_Indexes

END


CREATE TABLE dbo.#temp_sssr_missing_indexes

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,unique_compiles VARCHAR (23)
		,user_seeks VARCHAR (23)
		,user_scans VARCHAR (23)
		,avg_total_user_cost VARCHAR (23)
		,avg_user_impact VARCHAR (23)
		,overall_impact VARCHAR (23)
		,impact_rank VARCHAR (23)
		,[index_columns] NVARCHAR (MAX)
		,included_columns NVARCHAR (MAX)
		,table_column_count VARCHAR (23)
		,index_column_count VARCHAR (23)
		,include_column_count VARCHAR (23)
		,index_pct_of_columns VARCHAR (23)
		,include_pct_of_columns VARCHAR (23)
		,total_pct_of_columns VARCHAR (23)
		,create_index_statement NVARCHAR (MAX)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_missing_indexes

				(
					 database_name
					,[schema_name]
					,[object_name]
					,unique_compiles
					,user_seeks
					,user_scans
					,avg_total_user_cost
					,avg_user_impact
					,overall_impact
					,impact_rank
					,[index_columns]
					,included_columns
					,table_column_count
					,index_column_count
					,include_column_count
					,index_pct_of_columns
					,include_pct_of_columns
					,total_pct_of_columns
					,create_index_statement
				)


			SELECT
				 DB_NAME () AS database_name
				,SCHEMA_NAME (T.[schema_id]) AS [schema_name]
				,T.name AS [object_name]
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.unique_compiles), 1)), 4, 23)) AS unique_compiles
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.user_seeks), 1)), 4, 23)) AS user_seeks
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.user_scans), 1)), 4, 23)) AS user_scans
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.avg_total_user_cost), 1)), 1, 23)) AS avg_total_user_cost
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.avg_user_impact), 1)), 1, 23)) AS avg_user_impact
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.overall_impact), 1)), 1, 23)) AS overall_impact
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DENSE_RANK () OVER
																										(
																											ORDER BY
																												caIC.overall_impact DESC
																										)), 1)), 4, 23)) AS impact_rank
				,ISNULL (DDMID.equality_columns + caIC.comma_or_not, '''') + ISNULL (DDMID.inequality_columns, '''') AS [index_columns]
				,ISNULL (DDMID.included_columns, '''') AS included_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCC.table_column_count), 1)), 4, 23)) AS table_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.index_column_count), 1)), 4, 23)) AS index_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.include_column_count), 1)), 4, 23)) AS include_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, (caIC.index_column_count / sqCC.table_column_count) * 100), 1)), 1, 23)) AS index_pct_of_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, (caIC.include_column_count / sqCC.table_column_count) * 100), 1)), 1, 23)) AS include_pct_of_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ((caIC.index_column_count + caIC.include_column_count) / sqCC.table_column_count) * 100), 1)), 1, 23)) AS total_pct_of_columns
				,caCIS.create_index_statement
			FROM
				sys.tables T
				INNER JOIN sys.dm_db_missing_index_details DDMID ON DDMID.[object_id] = T.[object_id]
					AND DDMID.database_id = DB_ID ()
				INNER JOIN sys.dm_db_missing_index_groups DDMIG ON DDMIG.index_handle = DDMID.index_handle
				INNER JOIN sys.dm_db_missing_index_group_stats DDMIGS ON DDMIGS.group_handle = DDMIG.index_group_handle
				INNER JOIN

					(
						SELECT
							 C.[object_id]
							,COUNT (*) + .0 AS table_column_count
						FROM
							sys.columns C
						GROUP BY
							C.[object_id]
					) sqCC ON sqCC.[object_id] = T.[object_id]

				CROSS APPLY

					(
						SELECT
							 CONVERT (DECIMAL (18, 2), (DDMIGS.user_seeks + DDMIGS.user_scans) * (DDMIGS.avg_total_user_cost * DDMIGS.avg_user_impact)) AS overall_impact
							,ISNULL ((LEN (DDMID.equality_columns) - LEN (REPLACE (DDMID.equality_columns, '','', ''''))) + 1, 0) + ISNULL ((LEN (DDMID.inequality_columns) - LEN (REPLACE (DDMID.inequality_columns, '','', ''''))) + 1, 0) AS index_column_count
							,ISNULL ((LEN (DDMID.included_columns) - LEN (REPLACE (DDMID.included_columns, '','', ''''))) + 1, 0) AS include_column_count
							,(CASE
								WHEN DDMID.inequality_columns IS NOT NULL THEN '', ''
								ELSE ''''
								END) AS comma_or_not
							, ''IX_''
							+ T.name
							+ ''_''
							+ ISNULL (REPLACE (SUBSTRING (SUBSTRING (DDMID.equality_columns, 1, LEN (DDMID.equality_columns) - 1), 2, LEN (DDMID.equality_columns) - 1), ''], ['', ''_'')
							+ (CASE
									WHEN DDMID.inequality_columns IS NOT NULL THEN ''_''
									ELSE ''''
									END), '''')
							+ ISNULL (REPLACE (SUBSTRING (SUBSTRING (DDMID.inequality_columns, 1, LEN (DDMID.inequality_columns) - 1), 2, LEN (DDMID.inequality_columns) - 1), ''], ['', ''_''), '''')
							+ ISNULL (''_i_'' + REPLACE (SUBSTRING (SUBSTRING (DDMID.included_columns, 1, LEN (DDMID.included_columns) - 1), 2, LEN (DDMID.included_columns) - 1), ''], ['', ''_''), '''') AS index_base_string
					) caIC

				CROSS APPLY

					(
						SELECT
							  ''CREATE NONCLUSTERED INDEX ''
							+ (CASE
									WHEN LEN (caIC.index_base_string COLLATE DATABASE_DEFAULT) > 128 THEN ''<INDEX NAME>''
									ELSE ''['' + caIC.index_base_string + '']''
									END)
							+ '' ON ''
							+ ''[''
							+ DB_NAME ()
							+ '']''
							+ ''.''
							+ ''[''
							+ SCHEMA_NAME (T.[schema_id])
							+ '']''
							+ ''.''
							+ ''[''
							+ T.name
							+ '']''
							+ '' (''
							+ ISNULL (DDMID.equality_columns + caIC.comma_or_not, '''')
							+ ISNULL (DDMID.inequality_columns, '''')
							+ '')''
							+ ISNULL ('' INCLUDE ('' + DDMID.included_columns + '')'', '''')
							+ '';'' AS create_index_statement
					) caCIS
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_missing_indexes X)
BEGIN

	GOTO Skip_Missing_Indexes

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'','right_align' + X.unique_compiles AS 'td'
					,'','right_align' + X.user_seeks AS 'td'
					,'','right_align' + X.user_scans AS 'td'
					,'','right_align' + X.avg_total_user_cost AS 'td'
					,'','right_align' + X.avg_user_impact AS 'td'
					,'','right_align' + X.overall_impact AS 'td'
					,'','right_align' + X.impact_rank AS 'td'
					,'',X.[index_columns] AS 'td'
					,'',X.included_columns AS 'td'
					,'','right_align' + X.table_column_count AS 'td'
					,'','right_align' + X.index_column_count AS 'td'
					,'','right_align' + X.include_column_count AS 'td'
					,'','right_align' + X.index_pct_of_columns AS 'td'
					,'','right_align' + X.include_pct_of_columns AS 'td'
					,'','right_align' + X.total_pct_of_columns AS 'td'
					,'',X.create_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_missing_indexes X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.impact_rank
					,X.[index_columns]
					,X.included_columns
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Missing Indexes</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Unique Compiles</th>
						<th>User Seeks</th>
						<th>User Scans</th>
						<th>Avg User Cost</th>
						<th>Avg User Impact</th>
						<th>Overall Impact</th>
						<th>Impact Rank</th>
						<th>Index Column(s)</th>
						<th>Include Column(s)</th>
						<th>Table Column Count</th>
						<th>Index Column Count</th>
						<th>Include Column Count</th>
						<th>Index % Of Columns</th>
						<th>Include % Of Columns</th>
						<th>Total % Of Columns</th>
						<th>Create Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.unique_compiles
		,X.user_seeks
		,X.user_scans
		,X.avg_total_user_cost AS avg_user_cost
		,X.avg_user_impact
		,X.overall_impact
		,X.impact_rank
		,X.[index_columns]
		,X.included_columns AS include_columns
		,X.table_column_count
		,X.index_column_count
		,X.include_column_count
		,X.index_pct_of_columns
		,X.include_pct_of_columns
		,X.total_pct_of_columns
		,X.create_index_statement
	FROM
		dbo.#temp_sssr_missing_indexes X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.impact_rank
		,X.[index_columns]
		,X.included_columns

END


Skip_Missing_Indexes:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_missing_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_missing_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query X: Unused Indexes
-----------------------------------------------------------------------------------------------------------------------------

IF (@Unused_Indexes = 0 OR DATEDIFF (SECOND, @SQL_Server_Start_Time, GETDATE ()) < (@Unused_Index_Days * 86400))
BEGIN

	GOTO Skip_Unused_Indexes

END


CREATE TABLE dbo.#temp_sssr_unused_indexes

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,column_name SYSNAME
		,index_name SYSNAME
		,[disabled] VARCHAR (3)
		,hypothetical VARCHAR (3)
		,drop_index_statement NVARCHAR (4000)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_unused_indexes

				(
					 database_name
					,[schema_name]
					,[object_name]
					,column_name
					,index_name
					,[disabled]
					,hypothetical
					,drop_index_statement
				)

			SELECT
				 DB_NAME () AS database_name
				,S.name AS [schema_name]
				,O.name AS [object_name]
				,C.name AS column_name
				,I.name AS index_name
				,(CASE
					WHEN I.is_disabled = 1 THEN ''Yes''
					ELSE ''No''
					END) AS [disabled]
				,(CASE
					WHEN I.is_hypothetical = 1 THEN ''Yes''
					ELSE ''No''
					END) AS hypothetical
				,N''USE ['' + DB_NAME () + N'']; IF EXISTS (SELECT * FROM sys.indexes I WHERE I.[object_id] = '' + CONVERT (VARCHAR (11), I.[object_id]) + '' AND I.index_id = '' + CONVERT (VARCHAR (11), I.index_id) + '') BEGIN DROP INDEX ['' + I.name + ''] ON ['' + S.name + N''].['' + O.name + N''] END;'' AS drop_index_statement
			FROM
				sys.indexes I
				INNER JOIN sys.objects O ON O.[object_id] = I.[object_id]
					AND O.[type] = ''U''
					AND O.is_ms_shipped = 0
					AND O.name <> ''sysdiagrams''
				INNER JOIN sys.tables T ON T.[object_id] = I.[object_id]
				INNER JOIN sys.schemas S ON S.[schema_id] = T.[schema_id]
				INNER JOIN sys.index_columns IC ON IC.[object_id] = I.[object_id]
					AND IC.index_id = I.index_id
				INNER JOIN sys.columns C ON C.[object_id] = IC.[object_id]
					AND C.column_id = IC.column_id
			WHERE
				I.[type] > 0
				AND I.is_primary_key = 0
				AND I.is_unique_constraint = 0
				AND NOT EXISTS

					(
						SELECT
							*
						FROM
							sys.index_columns XIC
							INNER JOIN sys.foreign_key_columns FKC ON FKC.parent_object_id = XIC.[object_id]
								AND FKC.parent_column_id = XIC.column_id
						WHERE
							XIC.[object_id] = I.[object_id]
							AND XIC.index_id = I.index_id
					)

				AND NOT EXISTS

					(
						SELECT
							*
						FROM
							master.sys.dm_db_index_usage_stats DDIUS
						WHERE
							DDIUS.database_id = DB_ID (DB_NAME ())
							AND DDIUS.[object_id] = I.[object_id]
							AND DDIUS.index_id = I.index_id
					)
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_unused_indexes X)
BEGIN

	GOTO Skip_Unused_Indexes

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'',X.column_name AS 'td'
					,'',X.index_name AS 'td'
					,'',X.[disabled] AS 'td'
					,'',X.hypothetical AS 'td'
					,'',X.drop_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_unused_indexes X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.column_name
					,X.index_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Unused Indexes</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Column Name</th>
						<th>Index Name</th>
						<th>Disabled</th>
						<th>Hypothetical</th>
						<th>Drop Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name
		,X.[disabled]
		,X.hypothetical
		,X.drop_index_statement
	FROM
		dbo.#temp_sssr_unused_indexes X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name

END


Skip_Unused_Indexes:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_unused_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_unused_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Variable Update: Finalize @Body Variable Contents
-----------------------------------------------------------------------------------------------------------------------------

IF @Output_Mode = 'E'
BEGIN

	SET @Body =

		N'
			<html>
				<body>
				<style type="text/css">
					table {font-size:8.0pt;font-family:Arial;text-align:left;}
					tr {text-align:left;}
				</style>
		 '

		+ @Body +

		N'
				</body>
			</html>
		 '


	SET @Body = REPLACE (@Body, N'<td>right_align', N'<td align="right">')

END


-----------------------------------------------------------------------------------------------------------------------------
--	sp_send_dbmail: Deliver Results / Notification To End User(s)
-----------------------------------------------------------------------------------------------------------------------------

IF @Output_Mode = 'E'
BEGIN

	EXECUTE msdb.dbo.sp_send_dbmail

		 @recipients = @Recipients
		,@copy_recipients = @Copy_Recipients
		,@subject = @Subject
		,@body = @Body
		,@body_format = 'HTML'

END



GO
/****** Object:  StoredProcedure [dbo].[usp_SQL_Server_System_Report_Old]    Script Date: 2/11/2016 1:47:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-----------------------------------------------------------------------------------------------------------------------------
--	Stored Procedure Details: Listing Of Standard Details Related To The Stored Procedure
-----------------------------------------------------------------------------------------------------------------------------

--	Purpose: SQL Server System Report
--	Create Date (MM/DD/YYYY): 05/12/2010
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


-----------------------------------------------------------------------------------------------------------------------------
--	Modification History: Listing Of All Modifications Since Original Implementation
-----------------------------------------------------------------------------------------------------------------------------

--	Description: Added "@Output_Mode" And "@Unused_Index_Days" Input Parameters, Code Restructuring For Better Query Performance, Minor Changes To Code Style
--	Date (MM/DD/YYYY): 12/22/2011
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "connections", "first_day_of_week", "is_clustered", And "windows_version" To "Server Instance Property Information" Section
--	Date (MM/DD/YYYY): 12/23/2011
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "create_date" And "file_name" To "Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats" Section
--	Date (MM/DD/YYYY): 01/03/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "backup_finish_date", "database_version", "machine_name", And "server_name" To "Last Backup Set Details" Section
--	Date (MM/DD/YYYY): 01/04/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: "Last Backup Set Details" Section Now Only Shows Information For Databases Currently On The Instance
--	Date (MM/DD/YYYY): 01/17/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added "schema_name" To "Unused Indexes" Section, Extended "CONVERT" Character Length To Deal With "Msg 234" Error, Rewrote Time Calculation Logic
--	Date (MM/DD/YYYY): 01/20/2012
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


--	Description: Added Additional Input Parameters, Reformatted Code, Bug Fixes, Added New Sections (Server Settings, Index Fragmentation, Missing Indexes)
--	Date (MM/DD/YYYY): 09/20/2013
--	Developer: Sean Smith (s.smith.sql AT gmail DOT com)
--	Additional Notes: N/A


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query: Create Procedure
-----------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[usp_SQL_Server_System_Report_Old]

	 @Output_Mode AS CHAR (1) = NULL
	,@Unused_Index_Days AS INT = 7
	,@Recipients AS VARCHAR (MAX) = NULL
	,@Copy_Recipients AS VARCHAR (MAX) = NULL
	,@Server_Instance AS BIT = 1
	,@Server_Settings AS BIT = 1
	,@Drives_Space AS BIT = 1
	,@Database_Summary AS BIT = 1
	,@Database_Details AS BIT = 1
	,@Last_Backup AS BIT = 1
	,@Agent_Jobs AS BIT = 1
	,@Fragmentation AS BIT = 1
	,@Missing_Indexes AS BIT = 1
	,@Unused_Indexes AS BIT = 1

AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON
SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET ARITHIGNORE ON
SET TEXTSIZE 2147483647


DECLARE @Body AS NVARCHAR (MAX)
DECLARE @Database_ID AS INT
DECLARE @Database_Name_Loop AS NVARCHAR (500)
DECLARE @Date_24_Hours_Ago AS DATETIME
DECLARE @Date_Now AS DATETIME
DECLARE @SQL_Server_Start_Time AS DATETIME
DECLARE @SQL_String AS NVARCHAR (MAX)
DECLARE @Subject AS NVARCHAR (255)
DECLARE @XML_String AS NVARCHAR (MAX)


DECLARE @Database_Names AS TABLE

	(
		database_name SYSNAME PRIMARY KEY CLUSTERED
	)


DECLARE @Fixed_Drives_Free_Space AS TABLE

	(
		 drive_letter VARCHAR (5) PRIMARY KEY CLUSTERED
		,free_space_mb BIGINT
	)


SET @Body = N''
SET @Date_24_Hours_Ago = GETDATE () - 1
SET @Date_Now = @Date_24_Hours_Ago + 1
SET @Subject = N'SQL Server System Report: ' + @@SERVERNAME
SET @XML_String = N''


IF (@Server_Instance = 1 OR @Unused_Indexes = 1)
BEGIN

	SET @SQL_Server_Start_Time =

		(
			SELECT
				DB.create_date
			FROM
				master.sys.databases DB
			WHERE
				DB.name = N'tempdb'
		)

END


INSERT INTO @Database_Names

	(
		database_name
	)

SELECT
	DB.name AS database_name
FROM
	master.sys.databases DB
WHERE
	DB.[state] = 0
	AND DB.is_read_only = 0
	AND DB.is_in_standby = 0
	AND DB.source_database_id IS NULL


-----------------------------------------------------------------------------------------------------------------------------
--	Error Trapping: Check If Temp Table(s) Already Exist(s) And Drop If Applicable
-----------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_instance_property', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_instance_property

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_server_settings', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_server_settings

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_database_size_distribution_stats', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_database_size_distribution_stats

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_model_compatibility_size_growth', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_model_compatibility_size_growth

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_last_backup_set', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_last_backup_set

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_agent_jobs', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_agent_jobs

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_index_fragmentation', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_index_fragmentation

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_missing_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_missing_indexes

END


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_unused_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_unused_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query I: Server Instance Property Information
-----------------------------------------------------------------------------------------------------------------------------

IF @Server_Instance = 0
BEGIN

	GOTO Skip_Instance_Property

END


SELECT
	 SERVERPROPERTY (N'ComputerNamePhysicalNetBIOS') AS netbios_name
	,@@SERVERNAME AS server_name
	,REPLACE (CONVERT (NVARCHAR (128), SERVERPROPERTY (N'Edition')), ' Edition', '') AS edition
	,SERVERPROPERTY (N'ProductVersion') AS [version]
	,SERVERPROPERTY (N'ProductLevel') AS [level]
	,(CASE SERVERPROPERTY (N'IsClustered')
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_clustered
	,CONVERT (NVARCHAR (19), @SQL_Server_Start_Time, 120) AS online_since
	,(CASE
		WHEN oaDSR.total_days = 0 THEN '_'
		ELSE oaDSR.total_days
		END) + ' Day(s) ' + (CASE
								WHEN oaDSR.seconds_remaining = 0 THEN '__:__:__'
								WHEN oaDSR.seconds_remaining < 60 THEN '__:__:' + RIGHT (oaDSR.total_seconds, 2)
								WHEN oaDSR.seconds_remaining < 3600 THEN '__:' + RIGHT (oaDSR.total_seconds, 5)
								ELSE oaDSR.total_seconds
								END) AS uptime
	,SERVERPROPERTY (N'ProcessID') AS process_id
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@CONNECTIONS), 1)), 4, 23)) AS connections
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@TOTAL_READ), 1)), 4, 23)) AS reads
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, @@TOTAL_WRITE), 1)), 4, 23)) AS writes
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DOSI.cpu_count), 1)), 4, 23)) AS logical_cpu_count
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DOSI.cpu_count / DOSI.hyperthread_ratio), 1)), 4, 23)) AS physical_cpu_count
	,REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (RIGHT (@@VERSION, CHARINDEX (REVERSE (' on Windows '), REVERSE (@@VERSION)) - 1), 'Service Pack ', 'SP'), '(', ''), ')', ''), '<', '('), '>', ')') AS windows_version
INTO
	dbo.#temp_sssr_instance_property
FROM
	master.sys.dm_os_sys_info DOSI
	CROSS JOIN

		(
			SELECT
				DATEDIFF (SECOND, @SQL_Server_Start_Time, GETDATE ()) AS uptime_seconds
		) sqUTS

	OUTER APPLY

		(
			SELECT
				 CONVERT (VARCHAR (5), sqUTS.uptime_seconds / 86400) AS total_days
				,CONVERT (CHAR (8), DATEADD (SECOND, sqUTS.uptime_seconds % 86400, 0), 108) AS total_seconds
				,sqUTS.uptime_seconds % 86400 AS seconds_remaining
		) oaDSR


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Instance_Property

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.netbios_name AS 'td'
					,'',X.server_name AS 'td'
					,'',X.edition AS 'td'
					,'',X.[version] AS 'td'
					,'',X.[level] AS 'td'
					,'',X.is_clustered AS 'td'
					,'',X.online_since AS 'td'
					,'',X.uptime AS 'td'
					,'',X.process_id AS 'td'
					,'','right_align' + X.connections AS 'td'
					,'','right_align' + X.reads AS 'td'
					,'','right_align' + X.writes AS 'td'
					,'','right_align' + X.logical_cpu_count AS 'td'
					,'','right_align' + X.physical_cpu_count AS 'td'
					,'',X.windows_version AS 'td'
				FROM
					dbo.#temp_sssr_instance_property X
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<h3><center>Server Instance Property Information</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>NetBIOS Name</th>
						<th>Server Name</th>
						<th>Edition</th>
						<th>Version</th>
						<th>Level</th>
						<th>Clustered</th>
						<th>Online Since</th>
						<th>Uptime</th>
						<th>Process ID</th>
						<th>Connections</th>
						<th>Reads</th>
						<th>Writes</th>
						<th>Logical CPU Count</th>
						<th>Physical CPU Count</th>
						<th>Windows Version</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.netbios_name
		,X.server_name
		,X.edition
		,X.[version]
		,X.[level]
		,X.is_clustered AS [clustered]
		,X.online_since
		,X.uptime
		,X.process_id
		,X.connections
		,X.reads
		,X.writes
		,X.logical_cpu_count
		,X.physical_cpu_count
		,X.windows_version
	FROM
		dbo.#temp_sssr_instance_property X

END


Skip_Instance_Property:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_instance_property', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_instance_property

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query II: Server Settings
-----------------------------------------------------------------------------------------------------------------------------

IF @Server_Settings = 0
BEGIN

	GOTO Skip_Server_Settings

END


SELECT
	 (CASE @@DATEFIRST
		WHEN 1 THEN 'Monday'
		WHEN 2 THEN 'Tuesday'
		WHEN 3 THEN 'Wednesday'
		WHEN 4 THEN 'Thursday'
		WHEN 5 THEN 'Friday'
		WHEN 6 THEN 'Saturday'
		WHEN 7 THEN 'Sunday'
		ELSE 'N/A'
		END) AS first_day_of_week
	,SERVERPROPERTY (N'Collation') AS collation
	,(CASE
		WHEN 'a' = 'A' THEN 'No'
		ELSE 'Yes'
		END) AS is_case_sensitive
	,(CASE SERVERPROPERTY (N'IsFullTextInstalled')
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_full_text_installed
	,(CASE sqCFG.is_show_advanced_options_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_show_advanced_options_enabled
	,(CASE sqCFG.is_clr_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_clr_enabled
	,(CASE sqCFG.is_xp_cmdshell_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_xp_cmdshell_enabled
	,(CASE sqCFG.is_database_mail_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_database_mail_enabled
	,(CASE sqCFG.is_default_trace_enabled
		WHEN 0 THEN 'No'
		WHEN 1 THEN 'Yes'
		ELSE 'N/A'
		END) AS is_default_trace_enabled
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCFG.min_server_memory_mb), 1)), 4, 23)) AS min_server_memory_mb
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCFG.max_server_memory_mb), 1)), 4, 23)) AS max_server_memory_mb
INTO
	dbo.#temp_sssr_server_settings
FROM

	(
		SELECT
			 MAX (CASE
					WHEN CFG.configuration_id = 518 THEN CFG.value
					END) AS is_show_advanced_options_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1562 THEN CFG.value
					END) AS is_clr_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 16390 THEN CFG.value
					END) AS is_xp_cmdshell_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 16386 THEN CFG.value
					END) AS is_database_mail_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1568 THEN CFG.value
					END) AS is_default_trace_enabled
			,MAX (CASE
					WHEN CFG.configuration_id = 1543 THEN CFG.value
					END) AS min_server_memory_mb
			,MAX (CASE
					WHEN CFG.configuration_id = 1544 THEN CFG.value
					END) AS max_server_memory_mb
		FROM
			sys.configurations CFG
		WHERE
			CFG.configuration_id IN (518, 1543, 1544, 1562, 1568, 16386, 16390)
	) sqCFG


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Server_Settings

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.first_day_of_week AS 'td'
					,'',X.collation AS 'td'
					,'',X.is_case_sensitive AS 'td'
					,'',X.is_full_text_installed AS 'td'
					,'',X.is_show_advanced_options_enabled AS 'td'
					,'',X.is_clr_enabled AS 'td'
					,'',X.is_xp_cmdshell_enabled AS 'td'
					,'',X.is_database_mail_enabled AS 'td'
					,'',X.is_default_trace_enabled AS 'td'
					,'','right_align' + X.min_server_memory_mb AS 'td'
					,'','right_align' + X.max_server_memory_mb AS 'td'
				FROM
					dbo.#temp_sssr_server_settings X
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<h3><center>Server Settings</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>First Day Of Week</th>
						<th>Collation</th>
						<th>Case Sensitive</th>
						<th>Full-Text Installed</th>
						<th>Advanced Options Enabled</th>
						<th>CLR Enabled</th>
						<th>Command Shell Enabled</th>
						<th>Database Mail Enabled</th>
						<th>Default Trace Enabled</th>
						<th>Minimum Memory (MB)</th>
						<th>Maximum Memory (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.first_day_of_week
		,X.collation
		,X.is_case_sensitive AS case_sensitive
		,X.is_full_text_installed AS full_text_installed
		,X.is_show_advanced_options_enabled AS advanced_options_enabled
		,X.is_clr_enabled AS clr_enabled
		,X.is_xp_cmdshell_enabled AS command_shell_enabled
		,X.is_database_mail_enabled AS database_mail_enabled
		,X.is_default_trace_enabled AS default_trace_enabled
		,X.min_server_memory_mb AS minimum_memory_mb
		,X.max_server_memory_mb AS maximum_memory_mb
	FROM
		dbo.#temp_sssr_server_settings X

END


Skip_Server_Settings:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_server_settings', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_server_settings

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query III: Fixed Drives Free Space
-----------------------------------------------------------------------------------------------------------------------------

IF @Drives_Space = 0
BEGIN

	GOTO Skip_Fixed_Drives_Free_Space

END


INSERT INTO @Fixed_Drives_Free_Space

	(
		 drive_letter
		,free_space_mb
	)

EXECUTE master.dbo.xp_fixeddrives


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Fixed_Drives_Free_Space

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.drive_letter + ':' AS 'td'
					,'','right_align' + REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, X.free_space_mb), 1)), 4, 23)) AS 'td'
				FROM
					@Fixed_Drives_Free_Space X
				ORDER BY
					X.drive_letter
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Fixed Drives Free Space</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Drive Letter</th>
						<th>Free Space (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.drive_letter + ':' AS drive_letter
		,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, X.free_space_mb), 1)), 4, 23)) AS free_space_mb
	FROM
		@Fixed_Drives_Free_Space X
	ORDER BY
		X.drive_letter

END


Skip_Fixed_Drives_Free_Space:


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query IV: Database Size (Summary) / Distribution Stats
-----------------------------------------------------------------------------------------------------------------------------

IF @Database_Summary = 0
BEGIN

	GOTO Skip_Database_Size_Distribution_Stats

END


CREATE TABLE dbo.#temp_sssr_database_size_distribution_stats

	(
		 database_name NVARCHAR (500)
		,total_size_mb VARCHAR (15)
		,unallocated_mb VARCHAR (15)
		,reserved_mb VARCHAR (15)
		,data_mb VARCHAR (15)
		,index_mb VARCHAR (15)
		,unused_mb VARCHAR (15)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_database_size_distribution_stats

				(
					 database_name
					,total_size_mb
					,unallocated_mb
					,reserved_mb
					,data_mb
					,index_mb
					,unused_mb
				)

			SELECT
				 DB_NAME () AS database_name
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((sqDBF.total_size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS total_size_mb
				,(CASE
					WHEN sqDBF.database_size >= cjPGS.total_pages THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((sqDBF.database_size - cjPGS.total_pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23))
					ELSE ''0''
					END) AS unallocated_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((cjPGS.total_pages * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS reserved_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((cjPGS.pages * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS data_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((cjPGS.used_pages - cjPGS.pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS index_mb
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (((cjPGS.total_pages - cjPGS.used_pages) * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS unused_mb
			FROM

				(
					SELECT
						 SUM (CASE
								WHEN DBF.[type] = 0 THEN DBF.size
								ELSE 0
								END) AS database_size
						,SUM (DBF.size) AS total_size
					FROM
						sys.database_files AS DBF
					WHERE
						DBF.[type] IN (0, 1)
				) sqDBF

				CROSS JOIN

					(
						SELECT
							 SUM (AU.total_pages) AS total_pages
							,SUM (AU.used_pages) AS used_pages
							,SUM (CASE
									WHEN IT.internal_type IN (202, 204) THEN 0
									WHEN AU.[type] <> 1 THEN AU.used_pages
									WHEN P.index_id <= 1 THEN AU.data_pages
									ELSE 0
									END) AS pages
						FROM
							sys.partitions P
							INNER JOIN sys.allocation_units AU ON AU.container_id = P.partition_id
							LEFT JOIN sys.internal_tables IT ON IT.[object_id] = P.[object_id]
					) cjPGS
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_database_size_distribution_stats X)
BEGIN

	GOTO Skip_Database_Size_Distribution_Stats

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'','right_align' + X.total_size_mb AS 'td'
					,'','right_align' + X.unallocated_mb AS 'td'
					,'','right_align' + X.reserved_mb AS 'td'
					,'','right_align' + X.data_mb AS 'td'
					,'','right_align' + X.index_mb AS 'td'
					,'','right_align' + X.unused_mb AS 'td'
				FROM
					dbo.#temp_sssr_database_size_distribution_stats X
				ORDER BY
					X.database_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Database Size (Summary) / Distribution Stats</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Total Size (MB)</th>
						<th>Unallocated (MB)</th>
						<th>Reserved (MB)</th>
						<th>Data (MB)</th>
						<th>Index (MB)</th>
						<th>Unused (MB)</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.total_size_mb
		,X.unallocated_mb
		,X.reserved_mb
		,X.data_mb
		,X.index_mb
		,X.unused_mb
	FROM
		dbo.#temp_sssr_database_size_distribution_stats X
	ORDER BY
		X.database_name

END


Skip_Database_Size_Distribution_Stats:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_database_size_distribution_stats', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_database_size_distribution_stats

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query V: Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats
-----------------------------------------------------------------------------------------------------------------------------

IF @Database_Details = 0
BEGIN

	GOTO Skip_Model_Compatibility_Size_Growth

END


SELECT
	 DB_NAME (MF.database_id) AS database_name
	,DB.recovery_model_desc
	,DB.[compatibility_level]
	,CONVERT (NVARCHAR (10), LEFT (UPPER (MF.type_desc), 1) + LOWER (SUBSTRING (MF.type_desc, 2, 250))) AS file_type
	,MF.name AS [file_name]
	,CONVERT (NVARCHAR (19), DB.create_date, 120) AS create_date
	,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) AS file_size_mb
	,RIGHT ((CASE
				WHEN MF.growth = 0 THEN 'Fixed Size'
				WHEN MF.max_size = -1 THEN 'Unrestricted'
				WHEN MF.max_size = 0 THEN 'None'
				WHEN MF.max_size = 268435456 THEN '2 TB'
				ELSE REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.max_size * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) + ' MB'
				END), 15) AS max_size
	,RIGHT ((CASE
				WHEN MF.growth = 0 THEN 'N/A'
				WHEN MF.is_percent_growth = 1 THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, MF.growth), 1)), 4, 23)) + ' %'
				ELSE REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND ((MF.growth * CONVERT (BIGINT, 8192)) / 1048576.0, 0)), 1)), 4, 23)) + ' MB'
				END), 15) AS growth_increment
	,ROW_NUMBER () OVER
						(
							PARTITION BY
								MF.database_id
							ORDER BY
								 MF.[type]
								,(CASE
									WHEN MF.[file_id] = 1 THEN 10
									ELSE 99
									END)
								,MF.name
						) AS database_filter_id
INTO
	dbo.#temp_sssr_model_compatibility_size_growth
FROM
	master.sys.master_files MF
	INNER JOIN master.sys.databases DB ON DB.database_id = MF.database_id


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Model_Compatibility_Size_Growth

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',(CASE
							WHEN X.database_filter_id = 1 THEN X.database_name
							ELSE ''
							END) AS 'td'
					,'',(CASE
							WHEN X.database_filter_id = 1 THEN X.recovery_model_desc
							ELSE ''
							END) AS 'td'
					,'',(CASE
							WHEN X.database_filter_id = 1 THEN ISNULL (CONVERT (VARCHAR (5), X.[compatibility_level]), 'N/A')
							ELSE ''
							END) AS 'td'
					,'',X.file_type AS 'td'
					,'',X.[file_name] AS 'td'
					,'',X.create_date AS 'td'
					,'','right_align' + X.file_size_mb AS 'td'
					,'','right_align' + X.max_size AS 'td'
					,'','right_align' + X.growth_increment AS 'td'
				FROM
					dbo.#temp_sssr_model_compatibility_size_growth X
				ORDER BY
					 X.database_name
					,X.database_filter_id
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Database Recovery Model / Compatibility / Size (Detailed) / Growth Stats</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Recovery Model</th>
						<th>Compatibility</th>
						<th>File Type</th>
						<th>File Name</th>
						<th>Create Date</th>
						<th>File Size (MB)</th>
						<th>Max Size</th>
						<th>Growth Increment</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 (CASE
			WHEN X.database_filter_id = 1 THEN X.database_name
			ELSE ''
			END) AS database_name
		,(CASE
			WHEN X.database_filter_id = 1 THEN X.recovery_model_desc
			ELSE ''
			END) AS recovery_model
		,(CASE
			WHEN X.database_filter_id = 1 THEN ISNULL (CONVERT (VARCHAR (5), X.[compatibility_level]), 'N/A')
			ELSE ''
			END) AS compatibility
		,X.file_type
		,X.[file_name]
		,X.create_date
		,X.file_size_mb
		,X.max_size
		,X.growth_increment
	FROM
		dbo.#temp_sssr_model_compatibility_size_growth X
	ORDER BY
		 X.database_name
		,X.database_filter_id

END


Skip_Model_Compatibility_Size_Growth:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_model_compatibility_size_growth', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_model_compatibility_size_growth

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VI: Last Backup Set Details
-----------------------------------------------------------------------------------------------------------------------------

IF @Last_Backup = 0
BEGIN

	GOTO Skip_Last_Backup_Set

END


SELECT
	 DB.name AS database_name
	,ISNULL (CONVERT (VARCHAR (10), sqBS.backup_set_id), 'NONE') AS backup_set_id
	,(CASE sqBS.[type]
		WHEN 'D' THEN 'Database'
		WHEN 'F' THEN 'File Or Filegroup'
		WHEN 'G' THEN 'Differential File'
		WHEN 'I' THEN 'Differential Database'
		WHEN 'L' THEN 'Log'
		WHEN 'P' THEN 'Partial'
		WHEN 'Q' THEN 'Differential Partial'
		ELSE 'N/A'
		END) AS backup_type
	,ISNULL (CONVERT (VARCHAR (10), sqBS.database_version), 'N/A') AS database_version
	,ISNULL (sqBS.server_name, 'N/A') AS server_name
	,ISNULL (sqBS.machine_name, 'N/A') AS machine_name
	,ISNULL (CONVERT (VARCHAR (34), sqBS.backup_start_date, 120), 'N/A') AS backup_start_date
	,ISNULL (CONVERT (VARCHAR (34), sqBS.backup_finish_date, 120), 'N/A') AS backup_finish_date
	,ISNULL ((CASE
				WHEN sqBS.total_days = 0 THEN REPLICATE ('_', sqBS.day_length_max)
				ELSE REPLICATE ('0', sqBS.day_length_max - LEN (sqBS.total_days)) + sqBS.total_days
				END) + ' Day(s) ' + (CASE
										WHEN sqBS.seconds_remaining = 0 THEN '__:__:__'
										WHEN sqBS.seconds_remaining < 60 THEN '__:__:'+RIGHT (sqBS.total_seconds, 2)
										WHEN sqBS.seconds_remaining < 3600 THEN '__:'+RIGHT (sqBS.total_seconds, 5)
										ELSE sqBS.total_seconds
										END), 'N/A') AS duration
	,ISNULL (REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ROUND (sqBS.backup_size / 1048576.0, 0)), 1)), 4, 23)), 'N/A') AS backup_size_mb
	,ISNULL (REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DATEDIFF (DAY, sqBS.backup_start_date, GETDATE ())), 1)), 4, 23)), 'N/A') AS days_ago
	,ROW_NUMBER () OVER
						(
							PARTITION BY
								DB.name
							ORDER BY
								sqBS.[type]
						) AS database_filter_id
INTO
	dbo.#temp_sssr_last_backup_set
FROM
	master.sys.databases DB
	LEFT JOIN

		(
			SELECT
				 BS.database_name
				,BS.backup_set_id
				,BS.[type]
				,BS.database_version
				,BS.server_name
				,BS.machine_name
				,BS.backup_start_date
				,BS.backup_finish_date
				,BS.backup_size
				,cjDLM.day_length_max
				,oaDSR.seconds_remaining
				,oaDSR.total_days
				,oaDSR.total_seconds
			FROM
				msdb.dbo.backupset BS
				INNER JOIN

					(
						SELECT
							MAX (XBS.backup_set_id) AS backup_set_id_max
						FROM
							msdb.dbo.backupset XBS
						GROUP BY
							 XBS.database_name
							,XBS.[type]
					) sqMBS ON sqMBS.backup_set_id_max = BS.backup_set_id

				CROSS JOIN

					(
						SELECT
							MAX (LEN (DATEDIFF (DAY, YBS.backup_start_date, YBS.backup_finish_date))) AS day_length_max
						FROM
							msdb.dbo.backupset YBS
					) cjDLM

				OUTER APPLY

					(
						SELECT
							DATEDIFF (SECOND, BS.backup_start_date, BS.backup_finish_date) AS duration_seconds
					) oaDS

				OUTER APPLY

					(
						SELECT
							 CONVERT (VARCHAR (5), oaDS.duration_seconds / 86400) AS total_days
							,CONVERT (CHAR (8), DATEADD (SECOND, oaDS.duration_seconds % 86400, 0), 108) AS total_seconds
							,oaDS.duration_seconds % 86400 AS seconds_remaining
					) oaDSR

		) sqBS ON sqBS.database_name = DB.name

WHERE
	DB.name <> N'tempdb'


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Last_Backup_Set

END


IF EXISTS (SELECT * FROM dbo.#temp_sssr_last_backup_set X WHERE X.backup_set_id = 'NONE')
BEGIN

	UPDATE
		dbo.#temp_sssr_last_backup_set
	SET
		 backup_type = REPLICATE ('.', backup_type_length_max * 2)
		,database_version = REPLICATE ('.', database_version_length_max * 2)
		,server_name = REPLICATE ('.', server_name_length_max * 2)
		,machine_name = REPLICATE ('.', machine_name_length_max * 2)
		,backup_start_date = REPLICATE ('.', 34)
		,backup_finish_date = REPLICATE ('.', 34)
		,duration = REPLICATE ('.', (duration_length_max * 2) - 4)
		,backup_size_mb = REPLICATE ('.', backup_size_mb_length_max * 2)
	FROM

		(
			SELECT
				 MAX (LEN (X.backup_type)) AS backup_type_length_max
				,MAX (LEN (X.database_version)) AS database_version_length_max
				,MAX (LEN (X.server_name)) AS server_name_length_max
				,MAX (LEN (X.machine_name)) AS machine_name_length_max
				,MAX (LEN (X.duration)) AS duration_length_max
				,MAX (LEN (X.backup_size_mb)) AS backup_size_mb_length_max
			FROM
				dbo.#temp_sssr_last_backup_set X
		) sqX

	WHERE
		dbo.#temp_sssr_last_backup_set.backup_set_id = 'NONE'

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',(CASE
							WHEN X.database_filter_id = 1 THEN X.database_name
							ELSE ''
							END) AS 'td'
					,'',X.backup_set_id AS 'td'
					,'',X.backup_type AS 'td'
					,'',X.database_version AS 'td'
					,'',X.server_name AS 'td'
					,'',X.machine_name AS 'td'
					,'',X.backup_start_date AS 'td'
					,'',X.backup_finish_date AS 'td'
					,'',X.duration AS 'td'
					,'','right_align' + X.backup_size_mb AS 'td'
					,'','right_align' + X.days_ago AS 'td'
				FROM
					dbo.#temp_sssr_last_backup_set X
				ORDER BY
					 X.database_name
					,X.database_filter_id
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Last Backup Set Details</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Backup Set ID</th>
						<th>Backup Type</th>
						<th>Database Version</th>
						<th>Server Name</th>
						<th>Machine Name</th>
						<th>Backup Start Date</th>
						<th>Backup Finish Date</th>
						<th>Duration</th>
						<th>Backup Size (MB)</th>
						<th>Days Ago</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 (CASE
			WHEN X.database_filter_id = 1 THEN X.database_name
			ELSE ''
			END) AS database_name
		,X.backup_set_id
		,X.backup_type
		,X.database_version
		,X.server_name
		,X.machine_name
		,X.backup_start_date
		,X.backup_finish_date
		,X.duration
		,X.backup_size_mb
		,X.days_ago
	FROM
		dbo.#temp_sssr_last_backup_set X
	ORDER BY
		 X.database_name
		,X.database_filter_id

END


Skip_Last_Backup_Set:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_last_backup_set', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_last_backup_set

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VII: SQL Server Agent Jobs (Last 24 Hours)
-----------------------------------------------------------------------------------------------------------------------------

IF @Agent_Jobs = 0
BEGIN

	GOTO Skip_Agent_Jobs

END


SELECT
	 SJ.name AS job_name
	,CONVERT (VARCHAR (19), caLRDT.last_run_date_time, 120) AS last_run_date_time
	,(CASE SJH.run_status
		WHEN 0 THEN 'Failed'
		WHEN 1 THEN 'Succeeded'
		WHEN 2 THEN 'Retry'
		WHEN 3 THEN 'Canceled'
		WHEN 4 THEN 'In Progress'
		END) AS last_status
	,(CASE
		WHEN SJH.run_duration = 0 THEN '__:__:__'
		WHEN LEN (SJH.run_duration) <= 2 THEN '__:__:' + RIGHT ('0' + CONVERT (VARCHAR (2), SJH.run_duration), 2)
		WHEN LEN (SJH.run_duration) <= 4 THEN '__:' + STUFF (RIGHT ('0' + CONVERT (VARCHAR (4), SJH.run_duration), 4), 3, 0, ':')
		ELSE STUFF (STUFF (RIGHT ('0' + CONVERT (VARCHAR (6), SJH.run_duration), 6), 5, 0, ':'), 3, 0, ':')
		END) AS duration
	,ISNULL ((CASE
				WHEN SJ.[enabled] = 1 THEN CONVERT (VARCHAR (19), sqNRDT.next_run_date_time, 120)
				END), '___________________') AS next_run_date_time
	,ISNULL ((CASE
				WHEN SJ.[enabled] = 1 THEN REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DATEDIFF (DAY, GETDATE (), sqNRDT.next_run_date_time)), 1)), 4, 23))
				END), 'N/A') AS days_away
INTO
	dbo.#temp_sssr_agent_jobs
FROM
	msdb.dbo.sysjobs SJ
	INNER JOIN msdb.dbo.sysjobhistory SJH ON SJH.job_id = SJ.job_id
	INNER JOIN

		(
			SELECT
				MAX (XSJ.instance_id) AS instance_id_max
			FROM
				msdb.dbo.sysjobhistory XSJ
			GROUP BY
				XSJ.job_id
		) sqIIM ON sqIIM.instance_id_max = SJH.instance_id

	LEFT JOIN

		(
			SELECT
				 SJS.job_id
				,MIN (CONVERT (DATETIME, CONVERT (VARCHAR (8), SJS.next_run_date) + ' ' + STUFF (STUFF (RIGHT ('000000' + CONVERT (VARCHAR (6), SJS.next_run_time), 6), 5, 0, ':'), 3, 0, ':'))) AS next_run_date_time
			FROM
				msdb.dbo.sysjobschedules SJS
				INNER JOIN msdb.dbo.sysschedules SS ON SS.schedule_id = SJS.schedule_id
					AND SS.[enabled] = 1
			WHERE
				SJS.next_run_date > 0
			GROUP BY
				SJS.job_id
		) sqNRDT ON sqNRDT.job_id = SJ.job_id

	CROSS APPLY

		(
			SELECT
				CONVERT (DATETIME, CONVERT (VARCHAR (8), SJH.run_date) + ' ' + STUFF (STUFF (RIGHT ('000000' + CONVERT (VARCHAR (6), SJH.run_time), 6), 5, 0, ':'), 3, 0, ':')) AS last_run_date_time
		) caLRDT

WHERE
	caLRDT.last_run_date_time >= @Date_24_Hours_Ago


IF @@ROWCOUNT = 0
BEGIN

	GOTO Skip_Agent_Jobs

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.job_name AS 'td'
					,'',X.last_run_date_time AS 'td'
					,'',X.last_status AS 'td'
					,'',X.duration AS 'td'
					,'',X.next_run_date_time AS 'td'
					,'','right_align' + X.days_away AS 'td'
				FROM
					dbo.#temp_sssr_agent_jobs X
				ORDER BY
					X.job_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>SQL Server Agent Jobs (Last 24 Hours)</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Job Name</th>
						<th>Last Run Date / Time</th>
						<th>Last Status</th>
						<th>Duration</th>
						<th>Next Run Date / Time</th>
						<th>Days Away</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.job_name
		,X.last_run_date_time
		,X.last_status
		,X.duration
		,X.next_run_date_time
		,X.days_away
	FROM
		dbo.#temp_sssr_agent_jobs X
	ORDER BY
		X.job_name

END


Skip_Agent_Jobs:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_agent_jobs', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_agent_jobs

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query VIII: Index Fragmentation
-----------------------------------------------------------------------------------------------------------------------------

IF @Fragmentation = 0
BEGIN

	GOTO Skip_Index_Fragmentation

END


CREATE TABLE dbo.#temp_sssr_index_fragmentation

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,column_name SYSNAME
		,index_name SYSNAME
		,fragmentation VARCHAR (23)
		,index_type NVARCHAR (120)
		,is_pk VARCHAR (3)
		,is_unique VARCHAR (3)
		,recommendation VARCHAR (10)
		,alter_index_statement NVARCHAR (4000)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @Database_ID = DB_ID (@Database_Name_Loop)


	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_index_fragmentation

				(
					 database_name
					,[schema_name]
					,[object_name]
					,column_name
					,index_name
					,fragmentation
					,index_type
					,is_pk
					,is_unique
					,recommendation
					,alter_index_statement
				)


			SELECT
				 DB_NAME () AS database_name
				,SCHEMA_NAME (sqIF.[schema_id]) AS [schema_name]
				,sqIF.[object_name]
				,sqIF.column_name
				,sqIF.index_name
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqIF.avg_fragmentation_in_percent), 1)), 1, 23)) AS fragmentation
				,sqIF.type_desc AS index_type
				,(CASE sqIF.is_primary_key
					WHEN 0 THEN ''No''
					WHEN 1 THEN ''Yes''
					ELSE ''N/A''
					END) AS is_pk
				,(CASE sqIF.is_unique
					WHEN 0 THEN ''No''
					WHEN 1 THEN ''Yes''
					ELSE ''N/A''
					END) AS is_unique
				,caREC.recommendation
				,N''USE ['' + DB_NAME () + N'']; ALTER INDEX ['' + sqIF.index_name + ''] ON ['' + SCHEMA_NAME (sqIF.[schema_id]) + ''].['' + sqIF.[object_name] + ''] '' + (CASE caREC.recommendation
																																												WHEN ''REBUILD'' THEN caREC.recommendation + '' WITH (MAXDOP = 1)''
																																												ELSE caREC.recommendation
																																												END) + '';'' AS alter_index_statement
			FROM

				(
					SELECT
						 O.[schema_id]
						,O.name AS [object_name]
						,C.name AS column_name
						,I.name AS index_name
						,DDIPS.avg_fragmentation_in_percent
						,I.type_desc
						,I.is_primary_key
						,I.is_unique
						,ROW_NUMBER () OVER
										(
											PARTITION BY
												I.name
											ORDER BY
												DDIPS.avg_fragmentation_in_percent DESC
										) AS row_number_id
					FROM
						sys.dm_db_index_physical_stats (' + CONVERT (VARCHAR (11), @Database_ID) + ', NULL, NULL, NULL, N''LIMITED'') DDIPS
						INNER JOIN sys.objects O ON O.[object_id] = DDIPS.[object_id]
							AND O.is_ms_shipped = 0
						INNER JOIN sys.indexes I ON I.[object_id] = DDIPS.[object_id]
							AND I.index_id = DDIPS.index_id
							AND I.is_disabled <> 1
							AND I.is_hypothetical <> 1
						INNER JOIN sys.index_columns IC ON IC.[object_id] = DDIPS.[object_id]
							AND IC.index_id = DDIPS.index_id
						INNER JOIN sys.columns C ON C.[object_id] = DDIPS.[object_id]
							AND C.column_id = IC.column_id
					WHERE
						DDIPS.index_id <> 0
						AND DDIPS.avg_fragmentation_in_percent > 5
				) sqIF

				CROSS APPLY

					(
						SELECT
							(CASE
								WHEN sqIF.avg_fragmentation_in_percent <= 30.0 THEN ''REORGANIZE''
								ELSE ''REBUILD''
								END) AS recommendation
					) caREC

			WHERE
				sqIF.row_number_id = 1
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_index_fragmentation X)
BEGIN

	GOTO Skip_Index_Fragmentation

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'',X.column_name AS 'td'
					,'',X.index_name AS 'td'
					,'','right_align' + X.fragmentation AS 'td'
					,'',X.index_type AS 'td'
					,'',X.is_pk AS 'td'
					,'',X.is_unique AS 'td'
					,'',X.recommendation AS 'td'
					,'',X.alter_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_index_fragmentation X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.column_name
					,X.index_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Index Fragmentation</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Column Name</th>
						<th>Index Name</th>
						<th>Fragmentation</th>
						<th>Index Type</th>
						<th>PK</th>
						<th>Unique</th>
						<th>Recommendation</th>
						<th>Alter Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name
		,X.fragmentation
		,X.index_type
		,X.is_pk AS pk
		,X.is_unique AS [unique]
		,X.recommendation
		,X.alter_index_statement
	FROM
		dbo.#temp_sssr_index_fragmentation X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name

END


Skip_Index_Fragmentation:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_index_fragmentation', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_index_fragmentation

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query IX: Missing Indexes
-----------------------------------------------------------------------------------------------------------------------------

IF @Missing_Indexes = 0
BEGIN

	GOTO Skip_Missing_Indexes

END


CREATE TABLE dbo.#temp_sssr_missing_indexes

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,unique_compiles VARCHAR (23)
		,user_seeks VARCHAR (23)
		,user_scans VARCHAR (23)
		,avg_total_user_cost VARCHAR (23)
		,avg_user_impact VARCHAR (23)
		,overall_impact VARCHAR (23)
		,impact_rank VARCHAR (23)
		,[index_columns] NVARCHAR (MAX)
		,included_columns NVARCHAR (MAX)
		,table_column_count VARCHAR (23)
		,index_column_count VARCHAR (23)
		,include_column_count VARCHAR (23)
		,index_pct_of_columns VARCHAR (23)
		,include_pct_of_columns VARCHAR (23)
		,total_pct_of_columns VARCHAR (23)
		,create_index_statement NVARCHAR (MAX)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_missing_indexes

				(
					 database_name
					,[schema_name]
					,[object_name]
					,unique_compiles
					,user_seeks
					,user_scans
					,avg_total_user_cost
					,avg_user_impact
					,overall_impact
					,impact_rank
					,[index_columns]
					,included_columns
					,table_column_count
					,index_column_count
					,include_column_count
					,index_pct_of_columns
					,include_pct_of_columns
					,total_pct_of_columns
					,create_index_statement
				)


			SELECT
				 DB_NAME () AS database_name
				,SCHEMA_NAME (T.[schema_id]) AS [schema_name]
				,T.name AS [object_name]
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.unique_compiles), 1)), 4, 23)) AS unique_compiles
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.user_seeks), 1)), 4, 23)) AS user_seeks
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.user_scans), 1)), 4, 23)) AS user_scans
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.avg_total_user_cost), 1)), 1, 23)) AS avg_total_user_cost
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DDMIGS.avg_user_impact), 1)), 1, 23)) AS avg_user_impact
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.overall_impact), 1)), 1, 23)) AS overall_impact
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, DENSE_RANK () OVER
																										(
																											ORDER BY
																												caIC.overall_impact DESC
																										)), 1)), 4, 23)) AS impact_rank
				,ISNULL (DDMID.equality_columns + caIC.comma_or_not, '''') + ISNULL (DDMID.inequality_columns, '''') AS [index_columns]
				,ISNULL (DDMID.included_columns, '''') AS included_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, sqCC.table_column_count), 1)), 4, 23)) AS table_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.index_column_count), 1)), 4, 23)) AS index_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, caIC.include_column_count), 1)), 4, 23)) AS include_column_count
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, (caIC.index_column_count / sqCC.table_column_count) * 100), 1)), 1, 23)) AS index_pct_of_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, (caIC.include_column_count / sqCC.table_column_count) * 100), 1)), 1, 23)) AS include_pct_of_columns
				,REVERSE (SUBSTRING (REVERSE (CONVERT (VARCHAR (23), CONVERT (MONEY, ((caIC.index_column_count + caIC.include_column_count) / sqCC.table_column_count) * 100), 1)), 1, 23)) AS total_pct_of_columns
				,caCIS.create_index_statement
			FROM
				sys.tables T
				INNER JOIN sys.dm_db_missing_index_details DDMID ON DDMID.[object_id] = T.[object_id]
					AND DDMID.database_id = DB_ID ()
				INNER JOIN sys.dm_db_missing_index_groups DDMIG ON DDMIG.index_handle = DDMID.index_handle
				INNER JOIN sys.dm_db_missing_index_group_stats DDMIGS ON DDMIGS.group_handle = DDMIG.index_group_handle
				INNER JOIN

					(
						SELECT
							 C.[object_id]
							,COUNT (*) + .0 AS table_column_count
						FROM
							sys.columns C
						GROUP BY
							C.[object_id]
					) sqCC ON sqCC.[object_id] = T.[object_id]

				CROSS APPLY

					(
						SELECT
							 CONVERT (DECIMAL (18, 2), (DDMIGS.user_seeks + DDMIGS.user_scans) * (DDMIGS.avg_total_user_cost * DDMIGS.avg_user_impact)) AS overall_impact
							,ISNULL ((LEN (DDMID.equality_columns) - LEN (REPLACE (DDMID.equality_columns, '','', ''''))) + 1, 0) + ISNULL ((LEN (DDMID.inequality_columns) - LEN (REPLACE (DDMID.inequality_columns, '','', ''''))) + 1, 0) AS index_column_count
							,ISNULL ((LEN (DDMID.included_columns) - LEN (REPLACE (DDMID.included_columns, '','', ''''))) + 1, 0) AS include_column_count
							,(CASE
								WHEN DDMID.inequality_columns IS NOT NULL THEN '', ''
								ELSE ''''
								END) AS comma_or_not
							, ''IX_''
							+ T.name
							+ ''_''
							+ ISNULL (REPLACE (SUBSTRING (SUBSTRING (DDMID.equality_columns, 1, LEN (DDMID.equality_columns) - 1), 2, LEN (DDMID.equality_columns) - 1), ''], ['', ''_'')
							+ (CASE
									WHEN DDMID.inequality_columns IS NOT NULL THEN ''_''
									ELSE ''''
									END), '''')
							+ ISNULL (REPLACE (SUBSTRING (SUBSTRING (DDMID.inequality_columns, 1, LEN (DDMID.inequality_columns) - 1), 2, LEN (DDMID.inequality_columns) - 1), ''], ['', ''_''), '''')
							+ ISNULL (''_i_'' + REPLACE (SUBSTRING (SUBSTRING (DDMID.included_columns, 1, LEN (DDMID.included_columns) - 1), 2, LEN (DDMID.included_columns) - 1), ''], ['', ''_''), '''') AS index_base_string
					) caIC

				CROSS APPLY

					(
						SELECT
							  ''CREATE NONCLUSTERED INDEX ''
							+ (CASE
									WHEN LEN (caIC.index_base_string COLLATE DATABASE_DEFAULT) > 128 THEN ''<INDEX NAME>''
									ELSE ''['' + caIC.index_base_string + '']''
									END)
							+ '' ON ''
							+ ''[''
							+ DB_NAME ()
							+ '']''
							+ ''.''
							+ ''[''
							+ SCHEMA_NAME (T.[schema_id])
							+ '']''
							+ ''.''
							+ ''[''
							+ T.name
							+ '']''
							+ '' (''
							+ ISNULL (DDMID.equality_columns + caIC.comma_or_not, '''')
							+ ISNULL (DDMID.inequality_columns, '''')
							+ '')''
							+ ISNULL ('' INCLUDE ('' + DDMID.included_columns + '')'', '''')
							+ '';'' AS create_index_statement
					) caCIS
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_missing_indexes X)
BEGIN

	GOTO Skip_Missing_Indexes

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'','right_align' + X.unique_compiles AS 'td'
					,'','right_align' + X.user_seeks AS 'td'
					,'','right_align' + X.user_scans AS 'td'
					,'','right_align' + X.avg_total_user_cost AS 'td'
					,'','right_align' + X.avg_user_impact AS 'td'
					,'','right_align' + X.overall_impact AS 'td'
					,'','right_align' + X.impact_rank AS 'td'
					,'',X.[index_columns] AS 'td'
					,'',X.included_columns AS 'td'
					,'','right_align' + X.table_column_count AS 'td'
					,'','right_align' + X.index_column_count AS 'td'
					,'','right_align' + X.include_column_count AS 'td'
					,'','right_align' + X.index_pct_of_columns AS 'td'
					,'','right_align' + X.include_pct_of_columns AS 'td'
					,'','right_align' + X.total_pct_of_columns AS 'td'
					,'',X.create_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_missing_indexes X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.impact_rank
					,X.[index_columns]
					,X.included_columns
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Missing Indexes</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Unique Compiles</th>
						<th>User Seeks</th>
						<th>User Scans</th>
						<th>Avg User Cost</th>
						<th>Avg User Impact</th>
						<th>Overall Impact</th>
						<th>Impact Rank</th>
						<th>Index Column(s)</th>
						<th>Include Column(s)</th>
						<th>Table Column Count</th>
						<th>Index Column Count</th>
						<th>Include Column Count</th>
						<th>Index % Of Columns</th>
						<th>Include % Of Columns</th>
						<th>Total % Of Columns</th>
						<th>Create Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.unique_compiles
		,X.user_seeks
		,X.user_scans
		,X.avg_total_user_cost AS avg_user_cost
		,X.avg_user_impact
		,X.overall_impact
		,X.impact_rank
		,X.[index_columns]
		,X.included_columns AS include_columns
		,X.table_column_count
		,X.index_column_count
		,X.include_column_count
		,X.index_pct_of_columns
		,X.include_pct_of_columns
		,X.total_pct_of_columns
		,X.create_index_statement
	FROM
		dbo.#temp_sssr_missing_indexes X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.impact_rank
		,X.[index_columns]
		,X.included_columns

END


Skip_Missing_Indexes:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_missing_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_missing_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Main Query X: Unused Indexes
-----------------------------------------------------------------------------------------------------------------------------

IF (@Unused_Indexes = 0 OR DATEDIFF (SECOND, @SQL_Server_Start_Time, GETDATE ()) < (@Unused_Index_Days * 86400))
BEGIN

	GOTO Skip_Unused_Indexes

END


CREATE TABLE dbo.#temp_sssr_unused_indexes

	(
		 database_name NVARCHAR (512)
		,[schema_name] SYSNAME
		,[object_name] SYSNAME
		,column_name SYSNAME
		,index_name SYSNAME
		,[disabled] VARCHAR (3)
		,hypothetical VARCHAR (3)
		,drop_index_statement NVARCHAR (4000)
	)


SET @Database_Name_Loop =

	(
		SELECT TOP (1)
			tvDBN.database_name
		FROM
			@Database_Names tvDBN
		ORDER BY
			tvDBN.database_name
	)


WHILE @Database_Name_Loop IS NOT NULL
BEGIN

	SET @SQL_String =

		N'
			USE [' + @Database_Name_Loop + N'];


			INSERT INTO dbo.#temp_sssr_unused_indexes

				(
					 database_name
					,[schema_name]
					,[object_name]
					,column_name
					,index_name
					,[disabled]
					,hypothetical
					,drop_index_statement
				)

			SELECT
				 DB_NAME () AS database_name
				,S.name AS [schema_name]
				,O.name AS [object_name]
				,C.name AS column_name
				,I.name AS index_name
				,(CASE
					WHEN I.is_disabled = 1 THEN ''Yes''
					ELSE ''No''
					END) AS [disabled]
				,(CASE
					WHEN I.is_hypothetical = 1 THEN ''Yes''
					ELSE ''No''
					END) AS hypothetical
				,N''USE ['' + DB_NAME () + N'']; IF EXISTS (SELECT * FROM sys.indexes I WHERE I.[object_id] = '' + CONVERT (VARCHAR (11), I.[object_id]) + '' AND I.index_id = '' + CONVERT (VARCHAR (11), I.index_id) + '') BEGIN DROP INDEX ['' + I.name + ''] ON ['' + S.name + N''].['' + O.name + N''] END;'' AS drop_index_statement
			FROM
				sys.indexes I
				INNER JOIN sys.objects O ON O.[object_id] = I.[object_id]
					AND O.[type] = ''U''
					AND O.is_ms_shipped = 0
					AND O.name <> ''sysdiagrams''
				INNER JOIN sys.tables T ON T.[object_id] = I.[object_id]
				INNER JOIN sys.schemas S ON S.[schema_id] = T.[schema_id]
				INNER JOIN sys.index_columns IC ON IC.[object_id] = I.[object_id]
					AND IC.index_id = I.index_id
				INNER JOIN sys.columns C ON C.[object_id] = IC.[object_id]
					AND C.column_id = IC.column_id
			WHERE
				I.[type] > 0
				AND I.is_primary_key = 0
				AND I.is_unique_constraint = 0
				AND NOT EXISTS

					(
						SELECT
							*
						FROM
							sys.index_columns XIC
							INNER JOIN sys.foreign_key_columns FKC ON FKC.parent_object_id = XIC.[object_id]
								AND FKC.parent_column_id = XIC.column_id
						WHERE
							XIC.[object_id] = I.[object_id]
							AND XIC.index_id = I.index_id
					)

				AND NOT EXISTS

					(
						SELECT
							*
						FROM
							master.sys.dm_db_index_usage_stats DDIUS
						WHERE
							DDIUS.database_id = DB_ID (DB_NAME ())
							AND DDIUS.[object_id] = I.[object_id]
							AND DDIUS.index_id = I.index_id
					)
		 '


	EXECUTE (@SQL_String)


	SET @Database_Name_Loop =

		(
			SELECT TOP (1)
				tvDBN.database_name
			FROM
				@Database_Names tvDBN
			WHERE
				tvDBN.database_name > @Database_Name_Loop
			ORDER BY
				tvDBN.database_name
		)

END


IF NOT EXISTS (SELECT * FROM dbo.#temp_sssr_unused_indexes X)
BEGIN

	GOTO Skip_Unused_Indexes

END


IF @Output_Mode = 'E'
BEGIN

	SET @XML_String =

		CONVERT (NVARCHAR (MAX),
			(
				SELECT
					 '',X.database_name AS 'td'
					,'',X.[schema_name] AS 'td'
					,'',X.[object_name] AS 'td'
					,'',X.column_name AS 'td'
					,'',X.index_name AS 'td'
					,'',X.[disabled] AS 'td'
					,'',X.hypothetical AS 'td'
					,'',X.drop_index_statement AS 'td'
				FROM
					dbo.#temp_sssr_unused_indexes X
				ORDER BY
					 X.database_name
					,X.[schema_name]
					,X.[object_name]
					,X.column_name
					,X.index_name
				FOR
					XML PATH ('tr')
			)
		)


	SET @Body = @Body +

		N'
			<br><br>
			<h3><center>Unused Indexes</center></h3>
			<center>
				<table border=1 cellpadding=2>
					<tr>
						<th>Database Name</th>
						<th>Schema Name</th>
						<th>Object Name</th>
						<th>Column Name</th>
						<th>Index Name</th>
						<th>Disabled</th>
						<th>Hypothetical</th>
						<th>Drop Index Statement</th>
					</tr>
		 '


	SET @Body = @Body + @XML_String +

		N'
				</table>
			</center>
		 '

END
ELSE BEGIN

	SELECT
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name
		,X.[disabled]
		,X.hypothetical
		,X.drop_index_statement
	FROM
		dbo.#temp_sssr_unused_indexes X
	ORDER BY
		 X.database_name
		,X.[schema_name]
		,X.[object_name]
		,X.column_name
		,X.index_name

END


Skip_Unused_Indexes:


IF OBJECT_ID (N'tempdb.dbo.#temp_sssr_unused_indexes', N'U') IS NOT NULL
BEGIN

	DROP TABLE dbo.#temp_sssr_unused_indexes

END


-----------------------------------------------------------------------------------------------------------------------------
--	Variable Update: Finalize @Body Variable Contents
-----------------------------------------------------------------------------------------------------------------------------

IF @Output_Mode = 'E'
BEGIN

	SET @Body =

		N'
			<html>
				<body>
				<style type="text/css">
					table {font-size:8.0pt;font-family:Arial;text-align:left;}
					tr {text-align:left;}
				</style>
		 '

		+ @Body +

		N'
				</body>
			</html>
		 '


	SET @Body = REPLACE (@Body, N'<td>right_align', N'<td align="right">')

END


-----------------------------------------------------------------------------------------------------------------------------
--	sp_send_dbmail: Deliver Results / Notification To End User(s)
-----------------------------------------------------------------------------------------------------------------------------

IF @Output_Mode = 'E'
BEGIN

	EXECUTE msdb.dbo.sp_send_dbmail

		 @recipients = @Recipients
		,@copy_recipients = @Copy_Recipients
		,@subject = @Subject
		,@body = @Body
		,@body_format = 'HTML'

END

GO
USE [master]
GO
ALTER DATABASE [DBAadmin] SET  READ_WRITE 
GO
